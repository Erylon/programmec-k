#ifndef RegisterDefinition_h
#define RegisterDefinition_h

#define RS485_BAUDRATE 19200
#define SERIAL_BAUDRATE 115200



//JACK
enum { //CUR = lecture par le maitre, ecriture par l'esclave
	JACK_CUR_COILS,					//COILS= EV
	JACK_CDE_COILS,					//commande aussi le chrono et le mode autorecatch
	JACK_PT,						//Mesure Pression
	JACK_CUR_GRAFCET,				//grfcet en cours d'execution
	JACK_CUR_STATE,					//Etape du grafcet
	JACK_CDE_GRAFCET,				//Si grafcet=NO_GRAFCET alors on est en mode R&D : on peut envoyer des CDE_COILS
	JACK_CUR_BOARD_RELEASE,			//Version de la carte
	JACK_CUR_CHRONO,				//lecture du chrono final : 0 tant que c'est pas fini
	JACK_CUR_PATM,					//Pression atmosphérique
	JACK_CUR_PSL,					//PSL du verin
	JACK_CUR_PSH,					//PSH du verin 
	JACK_CDE_PSL,
	JACK_CDE_PSH,
	JACK_RS485_HOLDING_REGS_SIZE	//Ne pas modifier : calcul automatique la taille de l'enum
};


enum{
	C_K_CUR_COILS,					//COILS= EV / MOTOR
	C_K_CDE_COILS,			
	C_K_CUR_BTN,					
	C_K_CUR_PTP,					//Pression air Comprimé
	C_K_CUR_PTV,					//Pression vide
	C_K_CUR_XVRO_PERIOD,
	C_K_CUR_XVSX_PERIOD,
	C_K_CUR_XVRO_DURATION,
	C_K_CUR_XVSX_DURATION,
	C_K_CDE_XVRO_PERIOD,
	C_K_CDE_XVSX_PERIOD,
	C_K_CDE_XVRO_DURATION,
	C_K_CDE_XVSX_DURATION,
	C_K_CUR_ROLL_SPEED,
	C_K_CDE_ROLL_SPEED,
	C_K_CUR_US_ECHO,				//µs
	C_K_CDE_BUZZER,
	//C_K_CUR_PTVSH,				//Seuil de perte de vide)
	//C_K_CUR_PTPSL,				//Seuil de perte de pression air
	C_K_CUR_BOARD_RELEASE,		//Version de la carte
	C_K_RS485_HOLDING_REGS_SIZE	//Ne pas modifier : calcul automatique la taille de l'enum
};



enum { //CUR = lecture par le maitre, ecriture par l'esclave
	JACK_A_CUR_COILS,					//COILS= EV
	JACK_A_CDE_COILS,					//commande aussi le chrono
	JACK_A_PT,						//Mesure Pression
	JACK_A_CUR_GRAFCET,	
	JACK_A_CUR_STATE,					//Etape du grafcet
	JACK_A_CDE_GRAFCET,
	JACK_A_CUR_BOARD_RELEASE,			//Version de la carte
	JACK_A_CUR_CHRONO,				//lecture du chrono final : 0 tant que c'est pas fini
	JACK_A_CUR_PATM,					//Pression atmosphérique
	JACK_A_CUR_PSL,					//PSL du verin
	JACK_A_CUR_PSH,					//PSH du verin 
	JACK_A_CDE_PSL,
	JACK_A_CDE_PSH,

	JACK_B_CUR_COILS,					//COILS= EV
	JACK_B_CDE_COILS,					//commande aussi le chrono
	JACK_B_PT,						//Mesure Pression
	JACK_B_CUR_GRAFCET,	
	JACK_B_CUR_STATE,					//Etape du grafcet
	JACK_B_CDE_GRAFCET,
	JACK_B_CUR_BOARD_RELEASE,			//Version de la carte
	JACK_B_CUR_CHRONO,				//lecture du chrono final : 0 tant que c'est pas fini
	JACK_B_CUR_PATM,					//Pression atmosphérique
	JACK_B_CUR_PSL,					//PSL du verin
	JACK_B_CUR_PSH,					//PSH du verin 
	JACK_B_CDE_PSL,
	JACK_B_CDE_PSH,

	JACK_C_CUR_COILS,					//COILS= EV
	JACK_C_CDE_COILS,					//commande aussi le chrono
	JACK_C_PT,						//Mesure Pression
	JACK_C_CUR_GRAFCET,	
	JACK_C_CUR_STATE,					//Etape du grafcet
	JACK_C_CDE_GRAFCET,
	JACK_C_CUR_BOARD_RELEASE,			//Version de la carte
	JACK_C_CUR_CHRONO,				//lecture du chrono final : 0 tant que c'est pas fini
	JACK_C_CUR_PATM,					//Pression atmosphérique
	JACK_C_CUR_PSL,					//PSL du verin
	JACK_C_CUR_PSH,					//PSH du verin 
	JACK_C_CDE_PSL,
	JACK_C_CDE_PSH,

	JACK_D_CUR_COILS,					//COILS= EV
	JACK_D_CDE_COILS,					//commande aussi le chrono
	JACK_D_PT,						//Mesure Pression
	JACK_D_CUR_GRAFCET,	
	JACK_D_CUR_STATE,					//Etape du grafcet
	JACK_D_CDE_GRAFCET,
	JACK_D_CUR_BOARD_RELEASE,			//Version de la carte
	JACK_D_CUR_CHRONO,				//lecture du chrono final : 0 tant que c'est pas fini
	JACK_D_CUR_PATM,					//Pression atmosphérique
	JACK_D_CUR_PSL,					//PSL du verin
	JACK_D_CUR_PSH,					//PSH du verin 
	JACK_D_CDE_PSL,
	JACK_D_CDE_PSH,
//*/
	ROBOT_CUR_COILS,				//COILS= EV / MOTOR
	ROBOT_CDE_COILS,			
	ROBOT_CUR_BTN,					
	ROBOT_CUR_PTP,					//Pression air Comprimé
	ROBOT_CUR_PTV,					//Pression vide
	ROBOT_CUR_XVRO_PERIOD,
	ROBOT_CUR_XVSX_PERIOD,
	ROBOT_CUR_XVRO_DURATION,
	ROBOT_CUR_XVSX_DURATION,
	ROBOT_CDE_XVRO_PERIOD,
	ROBOT_CDE_XVSX_PERIOD,
	ROBOT_CDE_XVRO_DURATION,
	ROBOT_CDE_XVSX_DURATION,
	ROBOT_CUR_ROLL_SPEED,
	ROBOT_CDE_ROLL_SPEED,
	ROBOT_CUR_US_ECHO,				//µs
	ROBOT_CDE_BUZZER,
	//ROBOT_CUR_PTVSH,				//Seuil de perte de vide)
	//ROBOT_CUR_PTPSL,				//Seuil de perte de pression air
	ROBOT_CUR_BOARD_RELEASE,		//Version de la carte
//*/
	HOIST_RS485_HOLDING_REGS_SIZE	//Ne pas modifier : calcul automatique la taille de l'enum
};
//*/

//addresse des verins en modbus
#define JACK_A 					65
#define JACK_B 					66
#define JACK_C 					67
#define JACK_D 					68
#define ROBOT_ADRESS			82 //R


#define MODBUS_TIME_OUT 		1000
#define MODBUS_POLLING 			20 //200 // the scan rate
#define MODBUS_RETRY_COUNT		10



#ifdef DEBUG
	#define IFDEBUG(X)	(X)
#else
	#define IFDEBUG(X)
#endif

#ifdef SOFT_SERIAL
	#define IFSOFT_SERIAL(X)	(X)
#else
	#define IFSOFT_SERIAL(X)
#endif

/*
#ifdef MEGA_BOARD
  #define IFMEGA_BOARD(X) (X)
#else
  #define IFMEGA_BOARD(X)
#endif
*/

#ifdef TRACE_SERIAL
	#define IFTRACE_SERIAL(X) (X)
#else
	#define IFTRACE_SERIAL(X)
#endif

#define VERSION_RS485			0.11

#ifdef SOFT_SERIAL
	extern SoftwareSerial mySerial;
#endif


#endif