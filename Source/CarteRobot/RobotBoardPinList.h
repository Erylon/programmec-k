#define VERSION_TEXT			"ROBOT 2.0 MODBUS"
#define VERSION_NUM 			20

#define WATCHDOG_PULSE			5000		//ms
#define DO_OPEN					HIGH	
#define DO_CLOSE				LOW	


#define PASS					false //ne force pas la commande des EV

//BUZZER
#define BUZZ_ALERT_DURATION 	4000	//ms
#define BUZZ_INIT_DURATION 		500		//ms
#define HOLD_DURATION			3000 	//ms



//BUZZER SOUND FREQUENCY
#define NO_BUZZ_FREQ			0
#define BUZZ_INIT_FREQ			2000
#define BUZZ_END_FREQ			3000
#define BUZZ_ERROR_FREQ			100
#define BUZZ_TRAVERSE_FREQ		4000

//tableau des frequences buzzer : doit partir de 0, incrémental sans interruption
#define NO_BUZZ					0
#define BUZZ_INIT				1
#define BUZZ_END				2
#define BUZZ_ERROR				3
#define BUZZ_TRAVERSE 			4
#define T_BUZZ_FREQUENCY 		NO_BUZZ_FREQ,BUZZ_INIT_FREQ,BUZZ_END_FREQ,BUZZ_ERROR_FREQ,BUZZ_TRAVERSE_FREQ

//PT
#define PT_SAMPLE_DELAY		 	500 	//ms
#define PTVSH					760
#define PTPSL 					460
//US
#define US_SAMPLE_PERIOD		500 //ms
#define SOUND_SPEED 			340.0 / 1000; //mm/µs
#define MEASURE_TIMEOUT 		6000 //µs 0.9m (+A/R) à 340m/s
#define US_LIMIT				5000 //µs
	//pin
#define PIN_LED 				13 	//Watchdog
#define PIN_RESET_JACK			9  	//Reset les verins
#define PIN_US_TRIG				10	// Detection obstacle
#define PIN_US_ECHOO			11	// Detection obstacle //Terence a affecté à la pin 11 mais elle n'est pas IRQ
//on a ponté la pin 2 qui est IRQ sur la pin 11
#define PIN_US_ECHO				2	// Detection obstacle
#define PIN_BUZZER				12	//
#define PIN_ZSHSW				33	// Niveau Eau sale 	 haut
#define PIN_ZSLCW				39	// Niveau Eau propre bas

#define PIN_PTP					A1	// Pression compresseur
#define PIN_PTV					A0	// Pression vide
//#define PIN_ZSLR				41	// Fdc Bas rouleau
//#define PIN_ZSHR				45	// Fdc Haut rouleau
#define PIN_HSRC				22	// Btn Cleaning
#define PIN_HSRU				24	// Btn Up
#define PIN_HSRD				26	// Btn Down
#define PIN_HSRM				32	// Btn Manual ex HSCFR
#define PIN_HSRP				28	// Btn Position 0
#define PIN_VM					47	// Moteur Aspi
#define PIN_RM					31	// Moteur Rouleau
#define PIN_SWP					34	// Pompe pression eau (ex SWP) / CWP ?
#define PIN_XVRJ				36	// 
#define PIN_XVRO				48	//
#define PIN_XVSH				42	//
#define PIN_XVSM				44	//
#define PIN_XVSL 				46	//
#define PIN_BUMP1 				23	//
#define PIN_BUMP2 				25	//
#define PIN_HVCW 				40	// Electrovanne eau sous rouleau


#define T_NAME_EV				"XVRO","XVSH","XVSM","XVSL","XVRJ","HVCW"
#define T_EV 					PIN_XVRO,PIN_XVSH,PIN_XVSM,PIN_XVSL,PIN_XVRJ,PIN_HVCW
#define NB_EV					6


#define PIN_RS485_DE 			7
#define PIN_RS485_RE 			8
#define RS485_TX_ENABLE			PIN_RS485_DE
#define RS485_RX_ENABLE			PIN_RS485_RE

#define T_BTN 					PIN_HSRC,PIN_HSRM,PIN_HSRU,PIN_HSRD,PIN_HSRP
#define T_NAME_BTN				"HSRC","HSRM","HSRU","HSRD","HSRP"
#define NB_BTN					5



