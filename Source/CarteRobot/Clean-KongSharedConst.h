#ifndef Clean_KongSharedConst_h
#define Clean_KongSharedConst_h

//Ne pas modifier l'ordre des boutons
//tableau de commande des actionneurs : doit partir de 0, incrémental sans interruption
#define XVRO 					0	//0
#define XVSH 					1	//2
#define XVSM 					2	//4
#define XVSL 					3	//8
#define XVRJ 					4	//16
#define HVCW 					5	//32 Arrosage sous tete
#define VM 						6	//64
#define RM 						7	//128
#define SWP 					8	//pompe a vide
#define DISPLAY 				10
#define T_COIL_ROBOT			XVRO,XVSH,XVSM,XVSL,XVRJ,HVCW,VM,RM,SWP

//tableau de lecture des capteurs : doit partir de 0, incrémental sans interruption
#define HSRC 					0	//Btn Cleaning
#define HSRM 					1	//Btn Manuel
#define HSRU 					2	//Btn Up
#define HSRD 					3	//Btn Down
#define HSRP 					4	//reset alt
#define ZSHSW 					5	//niveau haut eau sale
#define ZSLCW 					6	//niveau bas eau propre

#define XVRO_DURATION 			3000	//ms
#define XVRO_PERIOD 			3001	//ms
#define XVSX_DURATION 			3002	//ms
#define XVSX_PERIOD 			3003	//ms

#define ROLL_SPEED_PERIOD		100
#define MAX_ROLL_SPEED			ROLL_SPEED_PERIOD

#endif