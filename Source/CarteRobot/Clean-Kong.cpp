#include ".\Clean-Kong.h"

extern CleanKong *oCleanKong;

void mesureUS_Start(){
  if(measuringUS){
      echoStart=micros();
  }
}
void mesureUS_End(){
  if(measuringUS){
      echoEnd=micros();
      US_Done=true;
  }
}


CleanKong::CleanKong() {
	rollSpeedStartTime=curTime=millis();
	#ifdef DEBUG
		Serial.print("\033[2J"); 		//Clear screen
	#endif

	//curAdress=ROBOT_ADRESS;
	Serial.println();					// Start writing from a new clean line
	Serial.print("Clean-Kong ");
	Serial.print((char)ROBOT_ADRESS);
	Serial.print(" Version ");
	Serial.println(VERSION_TEXT);
	Serial.println(" Initializing...");

	//initialisation des variables de registre (nécessaire car précède la com)
	holdingRegs[C_K_CUR_BOARD_RELEASE]=VERSION_NUM;

	pinMode(PIN_ZSHSW,	INPUT);			//Niveau Haut eau sale
	pinMode(PIN_ZSLCW,	INPUT);			//Niveau Bas eau propre   
	pinMode(PIN_PTP,	INPUT);			//Pression d'air comprimé
	pinMode(PIN_PTV,	INPUT);			//Pression vide
	pinMode(PIN_RESET_JACK,	OUTPUT);	//Reset Verin
	
	digitalWrite(PIN_RESET_JACK,LOW);	//Reset Cartes Verin
	delay(100);
	digitalWrite(PIN_RESET_JACK,HIGH);

	//pinMode(PIN_ZSLR,	INPUT);			//Fdc Bas Verin Rouleau
	//pinMode(PIN_ZSHR,	INPUT);			//Fdc Haut Verin Rouleau
	//pinMode(PIN_BUMP1, 	INPUT);
	//pinMode(PIN_BUMP2, 	INPUT);
	//isBumpDown=!digitalRead(PIN_BUMP1);
	//isBumpUp=!digitalRead(PIN_BUMP2);

	/*pinMode(PIN_HSRP,	INPUT);			//Bouton inutilise
	pinMode(PIN_HSRF,	INPUT);			//Bouton inutilise
	//*/

	pinMode(PIN_SWP,	OUTPUT);		//Pompe vide 
	pinMode(PIN_VM,		OUTPUT);		//Moteur Aspirateur
	pinMode(PIN_RM,		OUTPUT);		//Moteur Rouleau

	isVM=isRM=isSWP=false;

	digitalWrite(PIN_SWP,	isSWP);
	digitalWrite(PIN_VM,	isVM);
	digitalWrite(PIN_RM,	isRM);

/*
holdingRegs[C_K_CUR_PTV]
	C_K_CUR_COILS,				//COILS= EV / MOTOR
	C_K_CDE_COILS,			
	C_K_CUR_BOARD_RELEASE,		//Version de la carte
	*/
	
	
	//Mesure US
	pinMode(PIN_US_ECHO,INPUT);
	pinMode(PIN_US_ECHOO,INPUT); //cf header
	pinMode(PIN_US_TRIG,OUTPUT);
	#ifndef NO_US
	 	attachInterrupt(digitalPinToInterrupt(PIN_US_ECHO), mesureUS_Start, RISING);  
  		attachInterrupt(digitalPinToInterrupt(PIN_US_ECHO), mesureUS_End,   FALLING);   
  	#endif
  	measuringUS=false;
  	US_Done=false;
  	echoStart=micros();
	
	
	pinMode(PIN_BUZZER,	OUTPUT);
	pinMode(PIN_LED,	OUTPUT);

	digitalWrite(PIN_LED,LOW);
	digitalWrite(PIN_BUZZER,LOW);
	digitalWrite(PIN_US_TRIG,LOW);
	

	watchDogStatus=true; 
	//digitalWrite(PIN_LED,watchDogStatus);
	
	holdingRegs[C_K_CUR_BOARD_RELEASE] = 	VERSION_NUM;


	prevTime=millis();
	isHold=false;
	delay(2);
	holdManualStartTime=lastPTSampleTime=xvroStartTime=xvsxStartTime=curTime=lastUSSampleTime=startTime=millis();
	isBuzz=false;
	Serial.flush();
	

	holdingRegs[C_K_CDE_XVRO_PERIOD]=	holdingRegs[C_K_CUR_XVRO_PERIOD]= 	xvroPeriod=XVRO_PERIOD;
	holdingRegs[C_K_CDE_XVSX_PERIOD]=	holdingRegs[C_K_CUR_XVSX_PERIOD]= 	xvsxPeriod=XVSX_PERIOD;
	holdingRegs[C_K_CDE_XVRO_DURATION]=	holdingRegs[C_K_CUR_XVRO_DURATION]= xvroDuration=XVRO_DURATION;
	holdingRegs[C_K_CDE_XVSX_DURATION]=	holdingRegs[C_K_CUR_XVSX_DURATION]= xvsxDuration=XVSX_DURATION;
	holdingRegs[C_K_CUR_BOARD_RELEASE]=VERSION_NUM;

	int tEV[NB_EV]={T_EV};				//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_EV;i++){
		EV_Pin[i]=tEV[i];
		pinMode(EV_Pin[i],	OUTPUT);
		EV_State[i]=LOW;
	}
	EV_State[XVRJ]=HIGH;

	holdingRegs[C_K_CDE_BUZZER] = NO_BUZZ_FREQ;

	holdingRegs[C_K_CDE_ROLL_SPEED]=	holdingRegs[C_K_CUR_ROLL_SPEED]=	rollSpeed=MAX_ROLL_SPEED;

	int tBtn[NB_BTN]={T_BTN};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_BTN;i++){
		Btn_Pin[i]=tBtn[i];
		pinMode(Btn_Pin[i],	INPUT);	
	}

	holdingRegs[C_K_CUR_COILS] = 	EV_State[XVSH]<<XVSH |			
									EV_State[XVSM]<<XVSM |
									EV_State[XVSL]<<XVSL |
									EV_State[XVRJ]<<XVRJ |
									EV_State[XVRO]<<XVRO |
									EV_State[HVCW]<<HVCW |
									isVM<<VM |
									isRM<<RM |
									isSWP<<SWP;
	holdingRegs[C_K_CDE_COILS] = holdingRegs[C_K_CUR_COILS];	

	holdingRegs[C_K_CUR_BTN] = 		Btn_State[HSRC]<<HSRC |			
									Btn_State[HSRM]<<HSRM |
									Btn_State[HSRU]<<HSRU |
									Btn_State[HSRD]<<HSRD |
									Btn_State[HSRP]<<HSRP |
									digitalRead(PIN_ZSHSW)<<ZSHSW |
									digitalRead(PIN_ZSLCW)<<ZSLCW;

	modbus_configure(&Serial1, RS485_BAUDRATE, SERIAL_8N2, ROBOT_ADRESS, RS485_RX_ENABLE, C_K_RS485_HOLDING_REGS_SIZE, holdingRegs, RS485_TX_ENABLE);
  	modbus_update_comms(RS485_BAUDRATE, SERIAL_8N2, ROBOT_ADRESS);

}

void CleanKong::checkUS(){
	if((curTime-lastUSSampleTime)>US_SAMPLE_PERIOD){ //prise de mesure pas plus rapide que 3ms
		lastUSSampleTime=curTime;
		echoStart=micros();
		digitalWrite(PIN_US_TRIG,HIGH);
		delayMicroseconds(10);
		digitalWrite(PIN_US_TRIG,LOW);
		US_Done=false;
		measuringUS=true;
	}

	//If Timeout ignore
    if(((micros()-echoStart)>MEASURE_TIMEOUT) && measuringUS){
		measuringUS=false;
		US_Done=false;
		holdingRegs[C_K_CUR_US_ECHO] = 	MEASURE_TIMEOUT;
    }

    if(US_Done){ //prepare for next sample and eventually send alert
		measuringUS=false;
		US_Done=false;
		if((echoEnd-echoStart)<US_LIMIT){
			IFTRACE_SERIAL(Serial.print("US alert"));
			IFTRACE_SERIAL(Serial.println(echoEnd-echoStart));
			holdingRegs[C_K_CUR_US_ECHO] = 	echoEnd-echoStart;
		}
    }

}


void CleanKong::buzz(int buzzCode, int duration){
	if(buzzCode==NO_BUZZ){
		noTone(PIN_BUZZER);
		isBuzz=false;
		//Serial.println("no Buzz");
	}
	else {
		isBuzz=true;
		startBuzz=curTime;
		buzzDuration=duration;
		#ifdef NO_SOUND
			Serial.println("Buzz");
		#else
			tone(PIN_BUZZER,A_BUZZ_FREQ[buzzCode]);
		#endif
	}
}

void CleanKong::watchDog(){
	//curTime=millis();

	if(isBuzz && (curTime-startBuzz)>buzzDuration){
		buzz(NO_BUZZ);
	}

	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		prevTime=curTime;
		#ifdef DEBUG
			Serial.print("\033[2J"); 	//Clear screen
		#endif
	}

	if((curTime-xvroStartTime)>xvroDuration && EV_State[XVRO] || !EV_State[XVRO]){
		digitalWrite(EV_Pin[XVRO],LOW);
	}
	if(((curTime-xvroStartTime)>xvroPeriod && EV_State[XVRO]) ){
		xvroStartTime=curTime;
		digitalWrite(EV_Pin[XVRO],HIGH);	
	}

	if((curTime-xvsxStartTime)>xvsxDuration){
		for(int i=XVSH; i<XVRJ; i++){
			digitalWrite(EV_Pin[i],LOW);
		}
	} 
	
	if((curTime-xvsxStartTime)>xvsxPeriod) {
		for(int i=XVSH; i<XVRJ; i++){
			digitalWrite(EV_Pin[i],EV_State[i]);	
		}
		xvsxStartTime=curTime;
	}

	if(isRM){
		if((curTime-rollSpeedStartTime)>rollSpeed){
			digitalWrite(PIN_RM,LOW);
		}

		if((curTime-rollSpeedStartTime)>ROLL_SPEED_PERIOD){
			digitalWrite(PIN_RM,HIGH);
			rollSpeedStartTime=curTime;
		}

	}
//*/
	#ifdef MESURE_TEMPS_CYLE
		if(++logN%100==0){
			//IFTRACE_SERIAL(Serial.print("100 temps cycle (ms) =");Serial.println(curTime-cycleTime);)
			cycleTime=curTime;
			logN=100;
		}
	#endif
}

/*void CleanKong::cdeEV(int _EV, int _cde, bool _force){
	//digitalWrite(EV_Pin[_EV],_cde);
	EV_State[_EV]=_cde;
	#ifdef TRACE_SERIAL
		if(EV_State[_EV]!=_cde || _force){
			if(_cde==DO_OPEN){
				Serial.print(" Opening ");
			}
			else  {
				Serial.print(" Closing ");	
			}
			Serial.println(A_EV_NAME[_EV]);
		}
	#endif
}
//*/


void CleanKong::cdeEV(int _EV, int _cde, bool _force){
	if(EV_State[_EV]!=_cde || _force){
		digitalWrite(EV_Pin[_EV],_cde);
		/*
		if(_cde){
			IFTRACE_SERIAL(Serial.print("\033[31m"));
		}
		else {
			IFTRACE_SERIAL(Serial.print("\033[32m"));

		}		
		IFTRACE_SERIAL(Serial.print(A_EV_NAME[_EV]));
		IFTRACE_SERIAL(Serial.println("\033[37m"));
		*/
	}
	EV_State[_EV]=_cde;
}


void CleanKong::loop(){
	//temps de cycle 130µs
	#ifdef DEBUG
		//Serial.print("\033[2J"); 	//Clear screen
		Serial.print("\033[0;0H");	//Cursor to 0 0
	#endif

	curTime=millis();
	//GESTION LOCALE DES BOUTONS
	bool btnState;
	#ifndef DEACTIVATE_C_K_BUTTONS
		for(int i=0;i<NB_BTN;i++){
			Btn_State[i]=digitalRead(Btn_Pin[i]);
			if( Btn_State[i]){
				if(i==HSRM){						//Manual Btn
					holdManualStartTime=curTime; 	//start holdManualStartTime of Manual mode (for regeneration) only once : when btn pressed
				}
				if(isHold){
					IFTRACE_SERIAL(Serial.println("Stop Regen"));
					isHold=isVM=isRM=false;
					digitalWrite(PIN_VM,isVM);
					digitalWrite(PIN_RM,isRM);
				}
			}	
			else {
				if(i==HSRM){							//Manual Btn
					holdManualStartTime=curTime; 	
				}
			}
		}

		if(curTime-holdManualStartTime>HOLD_DURATION && !isHold){
			/*isVM=*/isRM=isHold=true;
			IFTRACE_SERIAL(Serial.println("Regen"));
			isHold=isVM=isRM=true;
			//digitalWrite(PIN_VM,isVM);
			digitalWrite(PIN_RM,isRM);
		}
	#endif	


	/*
	if((isBumpDown && digitalRead(PIN_BUMP1))||(!isBumpDown && !digitalRead(PIN_BUMP1))){
		isBumpDown=!digitalRead(PIN_BUMP1);
		oRS485->addMsg(HOIST,'A',ALERT_BUMP_DOWN,(long)isBumpDown);
	}
	if((isBumpUp && digitalRead(PIN_BUMP2))||(!isBumpUp && !digitalRead(PIN_BUMP2))){
		isBumpUp=!digitalRead(PIN_BUMP2);
		oRS485->addMsg(HOIST,'A',ALERT_BUMP_UP,(long)isBumpUp);
	}
	*/

	//LECTURE DES PRESSIONS
	if(curTime-lastPTSampleTime>PT_SAMPLE_DELAY){
		lastPTSampleTime=curTime;
		holdingRegs[C_K_CUR_PTV] =analogRead(PIN_PTV);
		holdingRegs[C_K_CUR_PTP] =analogRead(PIN_PTP);
	}

	#ifndef NO_US	
		checkUS();
	#endif

	//ECOUTE MODBUS
	modbus_update();

	//AFFECTATION DES REGISTRES PWM
	if(holdingRegs[C_K_CDE_XVRO_PERIOD] != xvroPeriod){
		xvroPeriod=holdingRegs[C_K_CDE_XVRO_PERIOD]; 
	}
	if(holdingRegs[C_K_CDE_XVRO_DURATION] != xvroDuration){
		xvroDuration=holdingRegs[C_K_CDE_XVRO_DURATION]; 
	}
	if(holdingRegs[C_K_CDE_XVSX_PERIOD] != xvsxPeriod){
		xvsxPeriod=holdingRegs[C_K_CDE_XVSX_PERIOD]; 
	}
	if(holdingRegs[C_K_CDE_XVSX_DURATION] != xvsxDuration){
		xvsxDuration=holdingRegs[C_K_CDE_XVSX_DURATION]; 
	}
	if(xvroDuration>xvroPeriod){
		xvroPeriod=xvroDuration;
	}
	if(xvsxDuration>xvsxPeriod){
		xvsxPeriod=xvsxDuration;
	}
	holdingRegs[C_K_CUR_XVRO_PERIOD] 	= xvroPeriod;
	holdingRegs[C_K_CUR_XVSX_PERIOD] 	= xvsxPeriod;
	holdingRegs[C_K_CUR_XVRO_DURATION]= xvroDuration;
	holdingRegs[C_K_CUR_XVSX_DURATION]= xvsxDuration;

	if(holdingRegs[C_K_CDE_ROLL_SPEED] != rollSpeed){
		rollSpeed=min(holdingRegs[C_K_CDE_ROLL_SPEED],MAX_ROLL_SPEED);
	}
	holdingRegs[C_K_CUR_ROLL_SPEED]= rollSpeed;


	// GESTION DES ENTREES
	holdingRegs[C_K_CUR_COILS] = 	EV_State[XVSH]<<XVSH |			
									EV_State[XVSM]<<XVSM |
									EV_State[XVSL]<<XVSL |
									EV_State[XVRJ]<<XVRJ |
									EV_State[XVRO]<<XVRO |
									EV_State[HVCW]<<HVCW |
									isVM<<VM |
									isRM<<RM |
									isSWP<<SWP;

	holdingRegs[C_K_CUR_BTN] = 		Btn_State[HSRC]<<HSRC |			
									Btn_State[HSRM]<<HSRM |
									Btn_State[HSRU]<<HSRU |
									Btn_State[HSRD]<<HSRD |
									Btn_State[HSRP]<<HSRP |
									digitalRead(PIN_ZSHSW)<<ZSHSW |
									digitalRead(PIN_ZSLCW)<<ZSLCW;
	/*Serial.print(" Btn_State[HSRC]<<HSRC=");Serial.println(Btn_State[HSRC]<<HSRC);
	Serial.print(" Btn_State[HSRM]<<HSRM=");Serial.println(Btn_State[HSRM]<<HSRM);
	Serial.print(" Btn_State[HSRU]<<HSRU=");Serial.println(Btn_State[HSRU]<<HSRU);
	Serial.print(" Btn_State[HSRD]<<HSRD=");Serial.println(Btn_State[HSRD]<<HSRD);
	Serial.print(" Btn_State[HSRP]<<HSRP=");Serial.println(Btn_State[HSRP]<<HSRP);
	Serial.print(" Btn_State[PIN_ZSHSW]<<ZSHSW=");Serial.println(digitalRead(PIN_ZSHSW)<<ZSHSW);
	Serial.print(" Btn_State[PIN_ZSLCW]<<ZSLCW=");Serial.println(digitalRead(PIN_ZSLCW)<<ZSLCW);
	Serial.print(" ZSHSW=");Serial.println(digitalRead(PIN_ZSHSW));
	Serial.print(" [C_K_CUR_BTN]=");Serial.println(holdingRegs[C_K_CUR_BTN]);
	*/
	
	//TRAITEMENT DES COMMANDES
	for(int i=0;i<NB_EV;i++){//COILS
		if(bitRead(holdingRegs[C_K_CDE_COILS],i)!=EV_State[i]){
			cdeEV(i, bitRead(holdingRegs[C_K_CDE_COILS],i));
		}
	}
	if(bitRead(holdingRegs[C_K_CDE_COILS],VM) != isVM){
		isVM=bitRead(holdingRegs[C_K_CDE_COILS],VM);
		digitalWrite(PIN_VM,isVM);
	}
	if(bitRead(holdingRegs[C_K_CDE_COILS],RM) != isRM){
		isRM=bitRead(holdingRegs[C_K_CDE_COILS],RM);
		digitalWrite(PIN_RM,isRM);
	}
	if(bitRead(holdingRegs[C_K_CDE_COILS],SWP) != isSWP){
		isSWP=bitRead(holdingRegs[C_K_CDE_COILS],SWP);
		digitalWrite(PIN_SWP,isSWP);
	}
	if(bitRead(holdingRegs[C_K_CDE_COILS],DISPLAY)){
		bitWrite(holdingRegs[C_K_CUR_COILS],DISPLAY,0);
		displayParams();
	}

	if(holdingRegs[C_K_CDE_BUZZER] != NO_BUZZ_FREQ){
		buzz(holdingRegs[C_K_CDE_BUZZER]);
	}
	#ifdef DEBUG
		displayParams();
	#endif

    watchDog();

}

void CleanKong::displayParams(){
	Serial.print("\n-----------------------------------------------\r\nETAT DU ROBOT ");
	Serial.println((char)ROBOT_ADRESS);
	Serial.print("version=\t\t");Serial.println(VERSION_TEXT);
	Serial.print("curTime=\t\t");Serial.println(curTime);
	Serial.print("prevTime=\t\t");Serial.println(prevTime);
	Serial.print("startTime=\t\t");Serial.println(startTime);
	Serial.print("cycleTime=\t\t");Serial.println(cycleTime);
	Serial.print("rollSpeedStartT.=\t");Serial.println(rollSpeedStartTime);
	Serial.print("rollSpeed=\t\t");Serial.println(rollSpeed);
	Serial.print("rollSpeedStartT.=\t");Serial.println(rollSpeedStartTime);
	Serial.print("xvroPeriod=\t\t");Serial.println(xvroPeriod);
	Serial.print("xvsxPeriod=\t\t");Serial.println(xvsxPeriod);
	Serial.print("xvroDuration=\t\t");Serial.println(xvroDuration);
	Serial.print("xvsxDuration=\t\t");Serial.println(xvsxDuration);
	for(int i=0;i<NB_BTN;i++){
		Serial.print(A_BTN_NAME[i]);
		Serial.print("=\t\t\t");
		Serial.println(Btn_State[i]);
	}
	Serial.print("isVM=\t\t\t");Serial.println(isVM);
	Serial.print("isRM=\t\t\t");Serial.println(isRM);
	Serial.print("isSWP=\t\t\t");Serial.println(isSWP);
	Serial.print("PTP=\t\t\t");Serial.println(analogRead(PIN_PTP));
	Serial.print("PTV=\t\t\t");Serial.println(analogRead(PIN_PTV));
	for(int i=0;i<NB_EV;i++){
		Serial.print(A_EV_NAME[i]);Serial.print("=\t\t\t");Serial.println(EV_State[i]);
	}

	Serial.print("[C_K_CUR_COILS]=\t");Serial.println(holdingRegs[C_K_CUR_COILS]);				//Serial.print("\t[0]=\t");Serial.println(holdingRegs[0]);
	Serial.print("[C_K_CDE_COILS]=\t");Serial.println(holdingRegs[C_K_CDE_COILS]);				//Serial.print("\t[1]=\t");Serial.println(holdingRegs[1]);
	Serial.print("[C_K_CUR_BTN]=\t\t");Serial.println(holdingRegs[C_K_CUR_BTN]);					//Serial.print("\t[2]=\t");Serial.println(holdingRegs[2]);
	Serial.print("[C_K_CUR_PTP]=\t\t");Serial.println(holdingRegs[C_K_CUR_PTP]);					//Serial.print("\t[3]=\t");Serial.println(holdingRegs[3]);
	Serial.print("[C_K_CUR_PTV]=\t\t");Serial.println(holdingRegs[C_K_CUR_PTV]);					//Serial.print("\t[4]=\t");Serial.println(holdingRegs[4]);
	Serial.print("[C_K_CUR_XVRO_PERIOD]=\t");Serial.println(holdingRegs[C_K_CUR_XVRO_PERIOD]);			//Serial.print("\t[5]=\t");Serial.println(holdingRegs[5]);
	Serial.print("[C_K_CUR_XVSX_PERIOD]=\t");Serial.println(holdingRegs[C_K_CUR_XVSX_PERIOD]);			//Serial.print("\t[6]=\t");Serial.println(holdingRegs[6]);
	Serial.print("[C_K_CUR_XVRO_DURATION]=");Serial.println(holdingRegs[C_K_CUR_XVRO_DURATION]);		//Serial.print("\t[7]=\t");Serial.println(holdingRegs[7]);
	Serial.print("[C_K_CUR_XVSX_DURATION]=");Serial.println(holdingRegs[C_K_CUR_XVSX_DURATION]);		//Serial.print("\t[8]=\t");Serial.println(holdingRegs[8]);
	Serial.print("[C_K_CDE_XVRO_PERIOD]=\t");Serial.println(holdingRegs[C_K_CDE_XVRO_PERIOD]);			//Serial.print("\t[9]=\t");Serial.println(holdingRegs[9]);
	Serial.print("[C_K_CDE_XVSX_PERIOD]=\t");Serial.println(holdingRegs[C_K_CDE_XVSX_PERIOD]);			//Serial.print("\t[10]=\t");Serial.println(holdingRegs[10]);
	Serial.print("[C_K_CDE_XVRO_DURATION]=");Serial.println(holdingRegs[C_K_CDE_XVRO_DURATION]);		//Serial.print("\t[11]=\t");Serial.println(holdingRegs[11]);
	Serial.print("[C_K_CDE_XVSX_DURATION]=");Serial.println(holdingRegs[C_K_CDE_XVSX_DURATION]);		//Serial.print("\t[12]=\t");Serial.println(holdingRegs[12]);
	Serial.print("[C_K_CUR_ROLL_SPEED]=\t");Serial.println(holdingRegs[C_K_CUR_ROLL_SPEED]);		//Serial.print("\t[13]=\t");Serial.println(holdingRegs[13]);
	Serial.print("[C_K_CDE_ROLL_SPEED]=\t");Serial.println(holdingRegs[C_K_CDE_ROLL_SPEED]);		//Serial.print("\t[14]=\t");Serial.println(holdingRegs[14]);
	Serial.print("[C_K_CUR_US_ECHO]=\t");Serial.println(holdingRegs[C_K_CUR_US_ECHO]);			//Serial.print("\t[15]=\t");Serial.println(holdingRegs[15]);
	Serial.print("[C_K_CDE_BUZZER]=\t");Serial.println(holdingRegs[C_K_CDE_BUZZER]);				//Serial.print("\t[16]=\t");Serial.println(holdingRegs[16]);
	Serial.print("[C_K_CUR_BOARD_R.]=\t");Serial.println(holdingRegs[C_K_CUR_BOARD_RELEASE]);		//Serial.print("\t[17]=\t");Serial.println(holdingRegs[17]);


}



