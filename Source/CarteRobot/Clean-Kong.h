#ifndef CleanKongMegei_h
#define CleanKongMegei_h

#include "..\Librairies\SimpleModBusSlave\SimpleModbusSlave.cpp"              	// Revision V10 https://drive.google.com/drive/folders/0B0B286tJkafVYnBhNGo4N3poQ2c?tid=0B0B286tJkafVSENVcU1RQVBfSzg
#include "..\CodeRelease.h"
//#include "..\C-KErrorList.h"
#include "..\RegisterDefinition.h"
#include "RobotBoardPinList.h"
#include "Clean-KongSharedConst.h"

//void	requestEvent();

unsigned long		curTime;
volatile long 		echoStart,echoEnd; 	//mesure du temps US en µs
bool    			US_Done;			//mesure US terminée
bool    			measuringUS;		//mesure US en cours


const char* 		A_EV_NAME[]=		{T_NAME_EV};
const char* 		A_BTN_NAME[]=		{T_NAME_BTN};
const char* 		A_VALVE_STATUS[]=	{"CLOSED","OPENED"};
const int  			A_BUZZ_FREQ[]=		{T_BUZZ_FREQUENCY};	

void mesureUS_Start();
void mesureUS_End();

class CleanKong {
public:
					CleanKong();
	void			loop();
//	void			cdeEV(int _pinEV, int _cde, bool _force=true);
	void 			displayParams();
	void			cdeEV(int _pinEV, int _cde, bool _force=false);



private:
	void			watchDog();
	void			buzz(int buzzCode=0, int duration=BUZZ_ALERT_DURATION);
	void 			checkUS();							//declenche si traverse detectee
	bool			watchDogStatus;						//clignotement de la led
	unsigned long	curTime,prevTime,holdManualStartTime,startTime,cycleTime,lastUSSampleTime,lastPTSampleTime;
	unsigned long 	xvroPeriod,xvsxPeriod,xvroDuration,xvsxDuration,xvroStartTime,xvsxStartTime,rollSpeed,rollSpeedStartTime;
	//bool 			isBumpDown;
	//bool 			isBumpUp;
	bool			isHold;
	//char 			curAdress;
	long 			distUS;
	int				EV_Pin[NB_EV];
	int				EV_State[NB_EV];
	int 			Btn_Pin[NB_BTN];
	int 			Btn_State[NB_BTN];
	unsigned long 	logN=0;								//mesure du temps de cycle
//	bool 			isBtnSent[NB_BTN];
	unsigned long	startBuzz,buzzDuration;
	bool			isBuzz;
	unsigned int 	holdingRegs[C_K_RS485_HOLDING_REGS_SIZE]; // function 3 and 16 register array
	bool 			isVM, isRM, isSWP;
};



#endif
