#ifndef JackBoard_h
#define JackBoard_h

#include "..\Librairies\SimpleModBusSlave\SimpleModbusSlave.cpp"              	// Revision V10 https://drive.google.com/drive/folders/0B0B286tJkafVYnBhNGo4N3poQ2c?tid=0B0B286tJkafVSENVcU1RQVBfSzg
#include "..\CodeRelease.h"
//#include "..\C-KErrorList.h"
#include "..\RegisterDefinition.h"
#include "JackBoardPinList.h"
#include "JackSharedConst.h"




const char* 		A_EV_NAME[]=		 {T_NAME_EV};
const char* 		A_VALVE_STATUS[]=	 {"CLOSED","OPENED"};
int 				A_SEUIL_PRESS_BAS[]	={T_SEUIL_PRESS_BAS};
int 				A_VACCUM_DURATION[]	={T_VACCUM_DURATION};
int 				A_INIT_PRESS_ATMO[]	={T_INIT_PRESS_ATMO};
const char*			A_NAME_ETAPE[]		={T_NAME_JACK_ETAPE};
const char*			A_NAME_GRAFCET[]	={T_NAME_JACK_GRAFCET};
//const char* 		version_text=VERSION_TEXT;
//const unsigned int	version_num=VERSION_NUM;


class Jack {
public:
					Jack(HardwareSerial *_refSerial);
	void			loop();
	void			cdeEV(int _pinEV, int _cde, bool _force=false);
	void 			displayParams();

	byte			curGrafcet;
	byte			curState;
	int				curPress;
	char 			curAdress;
	int 			pilePress[N_MOYENNE_GLISSANTE]; //tableau pour calcul de la moyenne glissante de pression
	int 			curPilePressIdx;				//index courant dans ce tableau pour la mesure
	int				PSH; 							//seuil pression haut
	int				PSL;							//seuil pression bas
	int				pAtm;							//pression Atmospherique

	int				EV_Pin[NB_EV];
	int				EV_State[NB_EV];
	int				errorCode;



private:
	void			watchDog();
	unsigned long	curTime;						
	unsigned long	prevWatchDogTime;				//pour watchdog
	unsigned long	startTime;						//temps demarrage etape grafcet
	unsigned long 	cycleTime;						//temps pour mesure temps de cycle
	unsigned long 	mesureTime;						//temps pour mesure pression (la mesure de la pression ne peut intervenir que toutes les 3 ms)
	unsigned long   logN=0;							//nb de cycle pour la mesure du temps de cycle
	unsigned long   moveDownDeepTime, moveDownSoftTime, vaccumDuration,fillUpTime,
					timeOutJackZSH,movingUpDuration,autocatchDelay;
	unsigned long	startChrono;  					//temps de démarrage du chrono pour la mesure du temps d'une action	

//	bool 			isSoftCatch;
//	bool 			isResuckUpMode;
//	bool			isFrameDetected;
	bool 			isAutoRecatchMode;
	unsigned int 	holdingRegs[JACK_RS485_HOLDING_REGS_SIZE]; // function 3 and 16 register array
	bool			watchDogStatus;					//watchdog led status

};



#endif
