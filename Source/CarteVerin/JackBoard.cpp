#include ".\JackBoard.h"
//#include <Wire.h> //for I2C purposes
/*color
\033[3nm
n:
0 noir
1 rouge
2 vert
3 jaune
4 bleu
5 violet
6 cyan
7 blanc
//*/

extern Jack *oJack;

Jack::Jack(HardwareSerial *refSerial) {
	pinMode(PIN_DIP0, INPUT);
	pinMode(PIN_DIP1, INPUT);
	pinMode(PIN_DIP2, INPUT);

	//Determination de l'adresse de la carte en fonction du DIP
	curAdress=digitalRead(PIN_DIP0)+2*digitalRead(PIN_DIP1)+4*digitalRead(PIN_DIP2);
	curAdress+=RS485_ADRR_START;

	IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
	IFSOFT_SERIAL(mySerial.print("Jack \033[36m"));
	IFSOFT_SERIAL(mySerial.print(curAdress));
	IFSOFT_SERIAL(mySerial.print("\033[37m version "));
	IFSOFT_SERIAL(mySerial.print(VERSION_TEXT));
	IFSOFT_SERIAL(mySerial.println(" Initializing..."));
//*/
	pinMode(PIN_ZSH,	INPUT);
	pinMode(PIN_ZSL,	INPUT);
	pinMode(PIN_PT,		INPUT);
	pinMode(PIN_XVJ,	OUTPUT);
	pinMode(PIN_XVV,	OUTPUT);
	pinMode(PIN_XVP,	OUTPUT);
	pinMode(PIN_XVS,	OUTPUT);
	pinMode(PIN_LED,	OUTPUT);
	digitalWrite(PIN_LED,LOW);

	modbus_configure(&Serial, RS485_BAUDRATE, SERIAL_8N2, curAdress, RS485_TX_ENABLE, JACK_RS485_HOLDING_REGS_SIZE, holdingRegs, RS485_RX_ENABLE);
  	//modbus_update_comms(RS485_BAUDRATE, SERIAL_8N2, curAdress);
	
	//oRS485=  new RS485(curAdress, HOIST, PIN_RS485_DE, PIN_RS485_RE , refSerial);
	
	//isSoftCatch=		false;
	//isResuckUpMode=		false;
	isAutoRecatchMode=	false;
	vaccumDuration=		A_VACCUM_DURATION[curAdress-RS485_ADRR_START];
	timeOutJackZSH=		TIMEOUT_JACK_ZSH;
	movingUpDuration=	MOVING_UP_DURATION;
	autocatchDelay=		AUTOCATCH_DELAY;
	fillUpTime=			FILL_UP_TIME;
	prevWatchDogTime=	millis();
	delay(2);
	startTime=			millis();
	mesureTime=			startTime;
	moveDownDeepTime=	MOVE_DOWN_DEEP_TIME;
	moveDownSoftTime=	MOVE_DOWN_SOFT_TIME;
	watchDogStatus=		false;
	//initialisation des variables de registre (nécessaire car précède la com)
	
//	holdingRegs[JACK_CDE_GRAFCET]=	holdingRegs[JACK_CUR_GRAFCET]=		curGrafcet=		DO_FREE;
//	holdingRegs[JACK_CUR_STATE]=										curState=		INITIALIAZING;
	holdingRegs[JACK_CDE_GRAFCET]=	DO_FREE;			
	curGrafcet 					 =	DO_FREE;
	holdingRegs[JACK_CUR_GRAFCET]=	DO_FREE;
	
	holdingRegs[JACK_CUR_STATE]  =	INITIALIAZING;
	curState 					 =	INITIALIAZING;	

	holdingRegs[JACK_CUR_BOARD_RELEASE]=												VERSION_NUM;
	holdingRegs[JACK_CUR_CHRONO]=														startTime;
	
	curPress=			analogRead(PIN_PT);
	
	for(int i=0;i<N_MOYENNE_GLISSANTE;i++){
		pilePress[i]=curPress;
	}	
	curPilePressIdx=1;

	holdingRegs[JACK_CDE_PSH]=			holdingRegs[JACK_CUR_PSH]=			PSH=		SEUIL_PRESS_HAUT; 			  					//seuil pression haut
	holdingRegs[JACK_CDE_PSL]=			holdingRegs[JACK_CUR_PSL]=			PSL=		A_SEUIL_PRESS_BAS[curAdress-RS485_ADRR_START];	//seuil pression bas
	holdingRegs[JACK_CUR_PATM]=												pAtm=		A_INIT_PRESS_ATMO[curAdress-RS485_ADRR_START];

	holdingRegs[JACK_CDE_COILS]=		0u;
	holdingRegs[JACK_CUR_COILS]=		0u;

	int tEV[NB_EV]={T_EV};				//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_EV;i++){
		EV_Pin[i]=tEV[i];
		cdeEV(i,DO_CLOSE);

	}


	#ifdef DEBUG
		IFSOFT_SERIAL(mySerial.print("\033[2J")); 	//Clear screen
	#endif
	displayParams();

}


void Jack::cdeEV(int _EV, int _cde, bool _force){
	if(EV_State[_EV]!=_cde || _force){
		digitalWrite(EV_Pin[_EV],_cde);
		
		if(_cde==DO_CLOSE){
			IFSOFT_SERIAL(mySerial.print("\033[31m"));
		}
		else {
			IFSOFT_SERIAL(mySerial.print("\033[32m"));

		}		
		IFSOFT_SERIAL(mySerial.print(A_EV_NAME[_EV]));
		IFSOFT_SERIAL(mySerial.println("\033[37m"));
		
	}
	EV_State[_EV]=_cde;
}


void Jack::watchDog(){
	curTime=millis();
	if((curTime-prevWatchDogTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		prevWatchDogTime=curTime;
		#ifdef DEBUG
		IFSOFT_SERIAL(mySerial.print("\033[2J")); 	//Clear screen
		#else
		if(bitRead(holdingRegs[JACK_CDE_COILS],DISPLAY)){
			IFSOFT_SERIAL(mySerial.print("\033[2J"));
		}
		#endif
	}
	#ifdef MESURE_TEMPS_CYLE
		if(++logN%100==0){
			IFSOFT_SERIAL(mySerial.print("100 temps cycle (ms) ="));IFSOFT_SERIAL(mySerial.println(curTime-cycleTime));
			cycleTime=curTime;
		}
	#endif

}


void Jack::loop(){
	//temps de cycle 130µs
	#ifdef DEBUG
		//IFSOFT_SERIAL(mySerial.print("\033[2J")); 	//Clear screen
		IFSOFT_SERIAL(mySerial.print("\033[0;0H"));	//Cursor to 0 0
		displayParams();
	#endif

	//LIRE LA PRESSION
	curTime=millis();
	if(curTime>mesureTime+2){ //prise de mesure pas plus rapide que 3ms
		mesureTime=curTime;
		//curPress=analogRead(PIN_PT);
		pilePress[curPilePressIdx]=analogRead(PIN_PT);
		curPress=0;
		for (int i=0;i<N_MOYENNE_GLISSANTE;i++){
			curPress+=pilePress[i];
		}
		curPress=(int)(curPress/N_MOYENNE_GLISSANTE);
		curPilePressIdx=(++curPilePressIdx)%N_MOYENNE_GLISSANTE;
		
		/*if(logN>0){
			IFSOFT_SERIAL(mySerial.println(curPress));
			logN--;
		}
		
		/*
		if(curPress<835 && curGrafcet==DO_CATCH && curState==MONITORING && !isFrameDetected){
			IFSOFT_SERIAL(mySerial.println("Trav start"));
			oRS485->addMsg(HOIST,'G',CDE_FRAME_DETECT,(long)1);
			isFrameDetected=true;
		}
		if(curPress>840 && curGrafcet==DO_CATCH && curState==MONITORING && isFrameDetected){
			IFSOFT_SERIAL(mySerial.println("Trav end"));
			oRS485->addMsg(HOIST,'G',CDE_FRAME_DETECT,(long)0);
			isFrameDetected=false;
		}//*/
	}

	//ECOUTE MODBUS
	modbus_update();


	//MAJ REGISTRE MODBUS
	// Gestion des commandes
	
	for(int i=XVS;i<=XVJ;i++){//COILS
		if(bitRead(holdingRegs[JACK_CDE_COILS],i)!=EV_State[i] && holdingRegs[JACK_CDE_GRAFCET]==NO_GRAFCET){
			cdeEV(i, bitRead(holdingRegs[JACK_CDE_COILS],i),true);
			IFSOFT_SERIAL(mySerial.print("Cde EV :"));
			IFSOFT_SERIAL(mySerial.println(bitRead(holdingRegs[JACK_CDE_COILS],i)));
		}
	}

	if(bitRead(holdingRegs[JACK_CDE_COILS],START_CHRONO)!=0){
		logN=50; 
	}
	isAutoRecatchMode=bitRead(holdingRegs[JACK_CDE_COILS],AUTORECATCH_MODE);
	if(holdingRegs[JACK_CDE_GRAFCET] != curGrafcet){
		IFSOFT_SERIAL(mySerial.print("----GRAFCET Set--------------"));
		IFSOFT_SERIAL(mySerial.print(curGrafcet));
		IFSOFT_SERIAL(mySerial.print("="));
		IFSOFT_SERIAL(mySerial.print(A_NAME_GRAFCET[curGrafcet]));
		curGrafcet=holdingRegs[JACK_CDE_GRAFCET]; 
		IFSOFT_SERIAL(mySerial.print("   ->     "));
		IFSOFT_SERIAL(mySerial.print(curGrafcet));
		IFSOFT_SERIAL(mySerial.print("="));
		IFSOFT_SERIAL(mySerial.println(A_NAME_GRAFCET[curGrafcet]));
		curState=INITIALIAZING;
	}
	if(bitRead(holdingRegs[JACK_CDE_COILS],DISPLAY)){
		bitWrite(holdingRegs[JACK_CUR_COILS],DISPLAY,0);
		displayParams();
	}
	if(holdingRegs[JACK_CDE_PSL] != PSL){
		PSL=holdingRegs[JACK_CDE_PSL]; 
		IFSOFT_SERIAL(mySerial.println("----PSL Set--------------"));
	}
	if(holdingRegs[JACK_CDE_PSH] != PSH){
		PSH=holdingRegs[JACK_CDE_PSH]; 
		IFSOFT_SERIAL(mySerial.println("----PSH Set--------------"));
	}


	// Gestion des entrées
	holdingRegs[JACK_CUR_COILS] = 	EV_State[XVS]<<XVS |			
									EV_State[XVP]<<XVP |
									EV_State[XVV]<<XVV |
									EV_State[XVJ]<<XVJ |
									digitalRead(PIN_ZSL)<<ZSL |
									digitalRead(PIN_ZSH)<<ZSH;//set register according to status on slave
									//nbSteps<<START_CHRONO;   // 
	holdingRegs[JACK_PT] = 			curPress;
	holdingRegs[JACK_CUR_GRAFCET] = curGrafcet;
	holdingRegs[JACK_CUR_STATE] = 	curState;
	holdingRegs[JACK_CUR_PATM] = 	pAtm;
	holdingRegs[JACK_CUR_PSL] = 	PSL;
	holdingRegs[JACK_CUR_PSH] = 	PSH;



	//holdingRegs[JACK_CUR_COILS]=22;// 100+curAdress;
#ifdef SOFT_SERIAL
/*    mySerial.print("S");mySerial.print(curAdress);;mySerial.print(RS485_BAUDRATE);
    mySerial.print(" W");mySerial.print(holdingRegs[JACK_CUR_COILS]);
    mySerial.print(" R");mySerial.println(holdingRegs[JACK_CDE_COILS]);
*/
#endif
  


/******************************************************************************************************* */
/**                                                      GRAFCETS                                         */
/******************************************************************************************************* */

	//SUIVI DES ACTION

	switch(curGrafcet){
		case DO_FREE:
			switch(curState){
				case MOVING_UP:
					if(digitalRead(PIN_ZSH)){
						#ifdef MESURE_TEMPS
						holdingRegs[JACK_CUR_CHRONO]=curTime-startChrono;
						#endif
						////cdeEV(XVJ,DO_CLOSE);
						IFSOFT_SERIAL(mySerial.println("FREE->M. UP->ZSH->FREED"));
						cdeEV(XVP,DO_CLOSE);	//Au cas fdc atteint avant fin libération du vide
						startTime=curTime;
						if(isAutoRecatchMode){
							IFSOFT_SERIAL(mySerial.println("FREE->M. UP->ZSH->W. CATCH"));
							curState=WAITING_TO_CATCH;
						}
						else {
							curState=MONITORING;
						}
					}
					else if(curTime-startTime>timeOutJackZSH){//Time out etant plus long que FILL_UP_TIME on le verifie ne premier
						//cdeEV(XVJ,DO_CLOSE);
						startTime=curTime;
						if(isAutoRecatchMode){
							IFSOFT_SERIAL(mySerial.println("FREE->M. UP->TIMEOUT ZSH->W. CATCH"));
							curState=WAITING_TO_CATCH;
						}
						else {
							IFSOFT_SERIAL(mySerial.println("FREE->M. UP->TIME_OUT_ZSH"));
							curState=TIME_OUT_ZSH;
						}
						cdeEV(XVP,DO_CLOSE);
					} 
					else if (curTime-startTime>fillUpTime){
						IFSOFT_SERIAL(mySerial.println("FREE->M. UP->TIME OUT FILL UP"));
						cdeEV(XVP,DO_CLOSE);
					}
					break;
				case WAITING_TO_CATCH:
					if(curTime-startTime>autocatchDelay){
//							isSoftCatch=true;
							IFSOFT_SERIAL(mySerial.println("FREE->W. CATCH->CATCH"));
							curGrafcet=DO_CATCH;
							curState=INITIALIAZING;
					}
					break;
				case MONITORING:
					if(curTime-startTime>PATM_WAITING_TIME){
						IFSOFT_SERIAL(mySerial.println("FREE->MONIT.->END"));
						curState=GRAFCET_END;
						pAtm=curPress;
					}
					break;
				case TIME_OUT_ZSH:
					// TODO: Traiter l'erreur
				case GRAFCET_END:
					break;
				default:
					//IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
					//IFSOFT_SERIAL(mySerial.println("\033[35m____\033[37m"));
					#ifdef MESURE_TEMPS
						startChrono=curTime;
					#endif
					IFSOFT_SERIAL(mySerial.println("START MOVING UP"));
					cdeEV(XVJ,DO_OPEN);//cdeEV(XVJ,DO_CLOSE);
					cdeEV(XVP,DO_OPEN);
					cdeEV(XVV,DO_CLOSE);
					cdeEV(XVS,DO_CLOSE);
//					isFrameDetected=false;
					curState=MOVING_UP;
					startTime=curTime;
					break;
			}
			break;
		//END DO_FREE
		case DO_DEEP_CATCH:
		case DO_CATCH:
			switch(curState){
				case MOVING_DOWN:
					#ifdef MESURE_TEMPS
					if(digitalRead(PIN_ZSL)){
						holdingRegs[JACK_CUR_CHRONO]=curTime-startChrono;
					}
					#endif

					//isEndOfCatch : arret sur fdc dans le cas d'une soft catch
//					if((!isSoftCatch &&  (curTime-startTime>moveDownDeepTime))
//					 ||( isSoftCatch && ((curTime-startTime>moveDownSoftTime) || digitalRead(PIN_ZSL)))){
					if(( (curTime-startTime>moveDownDeepTime))
					 ||( ((curTime-startTime>moveDownSoftTime) || digitalRead(PIN_ZSL)))){
						IFSOFT_SERIAL(mySerial.println("CATCH->M. DOWN->PUMP"));
						cdeEV(XVV,DO_OPEN); 				// ouverture EV pompage
						startTime=curTime;
						curState=PUMPING;
					}
					break;
				case PUMPING:
					if((curTime-startTime>vaccumDuration) ){//}  ||( (curPress<(PSH+pAtm))) ){
						IFSOFT_SERIAL(mySerial.println("CATCH->PUMP->M. UP"));
						cdeEV(XVV,DO_CLOSE);				// On ferme le pompage
						cdeEV(XVJ,DO_OPEN);					// On envoi de l'air dans le verin pour le faire monter
						startTime=curTime;
						curState=MOVING_UP;
					}
					break;
				case MOVING_UP:
					if((curTime-startTime>movingUpDuration) ) {
						if(digitalRead(PIN_ZSH) || curPress>(PSH+pAtm)) {
							IFSOFT_SERIAL(mySerial.println("CATCH->M. UP->FAIL."));
							curState=FAILING;	
						}
						else{
							IFSOFT_SERIAL(mySerial.println("CATCH->M. UP->MONIT."));
							curState=MONITORING;
							IFSOFT_SERIAL(mySerial.print("Linked P="));
							IFSOFT_SERIAL(mySerial.print(curPress));
							IFSOFT_SERIAL(mySerial.print("<Patm="));
							IFSOFT_SERIAL(mySerial.println(pAtm));
						}
						//else if(digitalRead(PIN_ZSH)){
						//	IFSOFT_SERIAL(mySerial.print("ZSH !"));
						//curState=FAILING;
						//}
					}
					break;
				case MONITORING:
					if(curPress>pAtm+PSH+PSL){
						IFSOFT_SERIAL(mySerial.println("CATCH->MONITO->FAIL."));
						IFSOFT_SERIAL(mySerial.print("P="));
						IFSOFT_SERIAL(mySerial.print(curPress));
						IFSOFT_SERIAL(mySerial.print(">Patm="));
						IFSOFT_SERIAL(mySerial.print(pAtm));
						IFSOFT_SERIAL(mySerial.print("+PSL="));
						IFSOFT_SERIAL(mySerial.print(PSL));
						IFSOFT_SERIAL(mySerial.print("+PSH="));
						IFSOFT_SERIAL(mySerial.println(PSH));
						curState=FAILING;
					}
					break;
				case FAILING:
					IFSOFT_SERIAL(mySerial.println("CATCH->FAIL->FREE"));
					cdeEV(XVV,DO_CLOSE);
					curGrafcet=DO_FREE;
					curState=INITIALIAZING;
					break;
				default:
					//IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
					//IFSOFT_SERIAL(mySerial.println("\033[35m____\033[37m"));
					#ifdef MESURE_TEMPS
						startChrono=curTime;
					#endif
					IFSOFT_SERIAL(mySerial.println("START M. DOWN"));
					cdeEV(XVS,DO_CLOSE);
					cdeEV(XVP,DO_CLOSE);
					cdeEV(XVV,DO_CLOSE);
					cdeEV(XVJ,DO_CLOSE);
					curState=MOVING_DOWN;
					startTime=curTime;
//					isFrameDetected=false;
					break;
			}
			break;
		case NO_GRAFCET:
			break;
		//END DO_CATCH
		default:
		break;
	}
//*/
	
    watchDog();
}

void Jack::displayParams(){
	
	#ifdef SOFT_SERIAL
		mySerial.print("\n--------------------------------------------\r\nETAT DU VERIN \033[36m");
		mySerial.println(curAdress);
		mySerial.print("\033[37mversion=\t\t");mySerial.println(VERSION_TEXT);
		mySerial.print("ZSL:\t\t\t"); mySerial.println(digitalRead(PIN_ZSL));
		mySerial.print("PSL:\t\t\t");mySerial.print(PSL);mySerial.print("\t bar:");mySerial.println(PSL/120.);
		mySerial.print("PSH:\t\t\t");mySerial.print(PSH);mySerial.print("\t bar:");mySerial.println(PSH/120.);
		mySerial.print("pAtm=\t\t\t");mySerial.print(pAtm);mySerial.print("\t bar:");mySerial.println(pAtm/120.);
		mySerial.print("curPress=\t\t");mySerial.print(curPress);mySerial.print("\t bar:");mySerial.println(curPress/120.);
		mySerial.print("curGrafcet=\t\t");mySerial.print(curGrafcet);mySerial.print("\t ");mySerial.println(A_NAME_GRAFCET[curGrafcet]);
		mySerial.print("curEtape=\t\t");mySerial.print(curState);mySerial.print("\t ");mySerial.println(A_NAME_ETAPE[curState]);
		mySerial.print("vaccumDuration(ms)=\t");mySerial.println(vaccumDuration);
		mySerial.print("timeOutJackZSH(ms)=\t");mySerial.println(timeOutJackZSH);
		mySerial.print("movingUpDuration(ms)=\t");mySerial.println(movingUpDuration);
//		mySerial.print("isSoftCatch=\t\t");mySerial.println(isSoftCatch);
//		mySerial.print("isResuckUpMode=\t\t");mySerial.println(isResuckUpMode);
		mySerial.print("isAutoRecatchMod=\t");mySerial.println(isAutoRecatchMode);
		mySerial.print("curTime=\t\t");mySerial.println(curTime);
		mySerial.print("prevWatchDogTime=\t");mySerial.println(prevWatchDogTime);
		mySerial.print("mesureTime=\t\t");mySerial.println(mesureTime);
		mySerial.print("startTime=\t\t");mySerial.println(startTime);
		mySerial.print("moveDownSoftTime=\t");mySerial.println(moveDownSoftTime);
		mySerial.print("moveDownDeepTime=\t");mySerial.println(moveDownDeepTime);
		mySerial.print("cycleTime=\t\t");mySerial.println(cycleTime);
		
		for(int i=0;i<NB_EV;i++){
			mySerial.print(A_EV_NAME[i]);mySerial.print("=\t\t\tCur:");mySerial.print(EV_State[i]);mySerial.print("=Reg:");mySerial.println(bitRead(holdingRegs[JACK_CDE_COILS],i));
		}
		mySerial.print("JACK_CUR_COILS=\t\t");mySerial.println(holdingRegs[JACK_CUR_COILS]);
		mySerial.print("JACK_CDE_COILS=\t\t");mySerial.println(holdingRegs[JACK_CDE_COILS]);
		mySerial.print("JACK_CUR_GRAFCET=\t");mySerial.println(holdingRegs[JACK_CUR_GRAFCET]);
		mySerial.print("JACK_CDE_GRAFCET=\t");mySerial.println(holdingRegs[JACK_CDE_GRAFCET]);
		mySerial.println(logN++);
		
		#ifdef DEBUG
			//delay(40);
		#endif
	#endif
}
