#ifndef JackSharedConst_h
#define JackSharedConst_h

//LISTE DES GRAFCETS VERINS
#define NO_GRAFCET				0
#define DO_CATCH 				1	
#define DO_DEEP_CATCH 			2
#define DO_FREE					3	

//Nom des grafcets
#define T_NAME_JACK_GRAFCET 	"Pas de grafcet","Catch","Deep Catch","Free"



//LISTE DES ETAPES DES GRAFCETS VERINS
#define INITIALIAZING			0
#define MOVING_DOWN				1	
#define MOVING_UP				2	
#define PUMPING					3	
#define MONITORING				4 //monitoring vaccum
#define FAILING					5	
#define FILLING_UP				6
#define FREED					7
#define WAITING_TO_CATCH 		8
#define TIME_OUT_ZSH 			9
#define GRAFCET_END				10

//nom des etapes
#define T_NAME_JACK_ETAPE 		"Init.","Moving Down","Moving Up","Pumping","Monitoring","Failing","Filling Up","Freed","WaitingToCatch","TimeOutZSH","Terminated"


//tableau de commande des EV : doit partir de 0, incrémental sans interruption
#define XVS 					0
#define XVP 					1
#define XVV 					2
#define XVJ 					3
#define ZSL 					4
#define ZSH 					5
#define START_CHRONO			6
#define AUTORECATCH_MODE		7
#define DISPLAY 				10

#define SEUIL_PRESS_BAS_A		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_B		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_C		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_D		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_E		-7		//Seuil a partir duquel on arrete le pompage

#define SEUIL_PRESS_HAUT		4	    //Seuil au dessus de PRESS_BAS a partir duquel on decide qu'on a perdu le catch
#define T_SEUIL_PRESS_BAS		SEUIL_PRESS_BAS_A,SEUIL_PRESS_BAS_B,SEUIL_PRESS_BAS_C,SEUIL_PRESS_BAS_D,SEUIL_PRESS_BAS_E


#endif