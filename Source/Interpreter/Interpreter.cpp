#include ".\Interpreter.h"

#define MAX_BUF (64)                        // What is the longest message Arduino can store?
char acBuffer[MAX_BUF];                     // where we store the message until we get a ';'
int sofar=0;                                // how much is in the buffer

#define VERSION_INTERPRETER     0.2

Interpreter::Interpreter(){
  help();
}


void Interpreter::setup(){
}

void Interpreter::NYI(){
  Serial.println("Not yet implemanted !");
}




void Interpreter::helpGetter(){
  NYI();
  Serial.println("\nValue:");
  Serial.print("V");Serial.print(GET_GCODE_RELEASE);Serial.println("\t\tGCODE_RELEASE");
  Serial.println("\nVerin:");
  Serial.print("V");Serial.print(GET_VACCUM_DURATION);Serial.println("\t\tVACCUM_DURATION\t\t");
  Serial.print("V");Serial.print(GET_PSL);Serial.println("\t\tPSL");
  Serial.print("V");Serial.print(GET_PSH);Serial.println("\t\tPSH");
  Serial.print("V");Serial.print(GET_ZSL);Serial.println("\t\tZSL");
  Serial.print("V");Serial.print(GET_ZSH);Serial.println("\t\tZSH");
  Serial.print("V");Serial.print(GET_XVS);Serial.println("\t\tXVS");
  Serial.print("V");Serial.print(GET_XVP);Serial.println("\t\tXVP");
  Serial.print("V");Serial.print(GET_XVV);Serial.println("\t\tXVV");
  Serial.print("V");Serial.print(GET_XVJ);Serial.println("\t\tXVJ");
  Serial.print("V");Serial.print(GET_PRESSURE);Serial.println("\t\tPRESSURE");
  Serial.print("V");Serial.print(GET_RAW_PRESSURE);Serial.println("\t\tRAW_PRESSURE");
  Serial.print("V");Serial.print(GET_ATM_PRESSURE);Serial.println("\t\tATM_PRESSURE");
  Serial.print("V");Serial.print(GET_MOVE_DOWN_DEEP_TIME);Serial.println("\t\tMOVE_DOWN_DEEP_TIME");
  Serial.print("V");Serial.print(GET_MOVE_DOWN_SOFT_TIME);Serial.println("\t\tMOVE_DOWN_SOFT_TIME");
  Serial.print("V");Serial.print(GET_RESUCK_UP_MODE);Serial.println("\t\tRESUCK_UP_MODE");
  Serial.print("V");Serial.print(GET_AUTO_RECATCH_MODE);Serial.println("\t\tAUTO_RECATCH_MODE");
  Serial.print("V");Serial.print(GET_TIMEOUT_JACK_ZSH);Serial.println("\t\tTIMEOUT_JACK_ZSH\t\t");
  Serial.print("V");Serial.print(GET_MOVING_UP_DURATION);Serial.println("\t\tMOVING_UP_DURATION");
  Serial.print("V");Serial.print(GET_AUTOCATCH_DELAY);Serial.println("\t\tAUTOCATCH_DELAY");

  Serial.println("\nRobot:");
  Serial.print("V");Serial.print(GET_HSRC);Serial.println("\t\tHSRC");
  Serial.print("V");Serial.print(GET_HSRM);Serial.println("\t\tHSRM");
  Serial.print("V");Serial.print(GET_HSRU);Serial.println("\t\tHSRU");
  Serial.print("V");Serial.print(GET_HSRD);Serial.println("\t\tHSRD");
  Serial.print("V");Serial.print(GET_ZSHSW);Serial.println("\t\tZSHSW");
  Serial.print("V");Serial.print(GET_ZSLR);Serial.println("\t\tZSLR");
  Serial.print("V");Serial.print(GET_ZSHR);Serial.println("\t\tZSHR");
  Serial.print("V");Serial.print(GET_PTP);Serial.println("\t\tPTP");
  Serial.print("V");Serial.print(GET_PTV);Serial.println("\t\tPTV");
  Serial.print("V");Serial.print(GET_XVRO);Serial.println("\t\tXVRO");
  Serial.print("V");Serial.print(GET_XVSH);Serial.println("\t\tXVSH");
  Serial.print("V");Serial.print(GET_XVSM);Serial.println("\t\tXVSM");
  Serial.print("V");Serial.print(GET_XVSL);Serial.println("\t\tXVSL");
  Serial.print("V");Serial.print(GET_XVRJ);Serial.println("\t\tXVRJ");
  Serial.print("V");Serial.print(GET_HVCW);Serial.println("\t\tHVCW");
  Serial.print("V");Serial.print(GET_VM);Serial.println("\t\tVM");
  Serial.print("V");Serial.print(GET_RM);Serial.println("\t\tRM");
  Serial.print("V");Serial.print(GET_SWP);Serial.println("\t\tSWP");
  Serial.print("V");Serial.print(GET_CWP);Serial.println("\t\tCWP");
  Serial.print("V");Serial.print(GET_XVRO_DURATION);Serial.println("\t\tXVRO_DURATION");
  Serial.print("V");Serial.print(GET_XVSX_DURATION);Serial.println("\t\tXVSx_DURATION");
  Serial.print("V");Serial.print(GET_XVRO_PERIOD);Serial.println("\t\tXVRO_PERIOD");
  Serial.print("V");Serial.print(GET_XVSX_PERIOD);Serial.println("\t\tXVSx_PERIOD");
  Serial.print("V");Serial.print(GET_ROLL_SPEED);Serial.println("\t\tROLL_SPEED");
 
  Serial.println();
  Serial.print("ex : V");Serial.print(GET_MOVE_DOWN_SOFT_TIME);Serial.println(" A");
//*/
}




void Interpreter::helpCde(){
  Serial.println("Commands :");

  Serial.print("G");Serial.print(CDE_CATCH);Serial.println("\t\tCATCH");
  Serial.print("G");Serial.print(CDE_DEEP_CATCH);Serial.println("\t\tDEEP_CATCH");
  Serial.print("G");Serial.print(CDE_FREE);Serial.println("\t\tFREE");
  Serial.print("G");Serial.print(CDE_XVS_OPEN);Serial.println("\t\tXVS_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVP_OPEN);Serial.println("\t\tXVP_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVV_OPEN);Serial.println("\t\tXVV_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVJ_OPEN);Serial.println("\t\tXVJ_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_PING);Serial.println("\t\tPING");
  Serial.print("G");Serial.print(CDE_PONG);Serial.println("\t\tPONG");
  Serial.print("G");Serial.print(CDE_ACK_ERROR);Serial.println("\t\tACK_ERROR");
  Serial.print("G");Serial.print(CDE_DISPLAY_PARAMS);Serial.println("\t\tCDE_DISPLAY_PARAMS");
  Serial.print("G");Serial.print(CDE_RAZ);Serial.println("\t\tRAZ");
  Serial.print("G");Serial.print(CDE_FORCE_ZSH_STATE);Serial.println("\t\tCDE_FORCE_ZSH_STATE P[[0];1]");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_UP);Serial.println("\t\tCDE_FORCE_MOVE_UP");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_DOWN);Serial.println("\t\tCDE_FORCE_MOVE_DOWN");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_STOP);Serial.println("\t\tCDE_FORCE_MOVE_STOP");
  Serial.print("G");Serial.print(CDE_FORCE_CLEANING);Serial.println("\t\tCDE_FORCE_CLEANING");
  Serial.print("G");Serial.print(CDE_FORCE_MANUAL);Serial.println("\t\tCDE_FORCE_MANUAL");

  Serial.print("G");Serial.print(CDE_HSRC);Serial.println("\t\tHSRC");
  Serial.print("G");Serial.print(CDE_HSRM);Serial.println("\t\tHSRM");
  Serial.print("G");Serial.print(CDE_HSRU);Serial.println("\t\tHSRU");
  Serial.print("G");Serial.print(CDE_HSRD);Serial.println("\t\tHSRD");
  Serial.print("G");Serial.print(CDE_HSRP);Serial.println("\t\tSet index O ");
  Serial.print("G");Serial.print(CDE_XVRO_OPEN);Serial.println("\t\tXVRO_OPEN");
  Serial.print("G");Serial.print(CDE_XVSH_OPEN);Serial.println("\t\tXVSH_OPEN");
  Serial.print("G");Serial.print(CDE_XVSM_OPEN);Serial.println("\t\tXVSM_OPEN");
  Serial.print("G");Serial.print(CDE_XVSL_OPEN);Serial.println("\t\tXVSL_OPEN");
  Serial.print("G");Serial.print(CDE_XVRJ_OPEN);Serial.println("\t\tXVRJ_OPEN");
  Serial.print("G");Serial.print(CDE_HVCW_OPEN);Serial.println("\t\tHVCW_OPEN");
  Serial.print("G");Serial.print(CDE_VM);Serial.println("\t\tVM [0;1]");
  Serial.print("G");Serial.print(CDE_RM);Serial.println("\t\tRM [0;1]");
  Serial.print("G");Serial.print(CDE_SWP);Serial.println("\t\tSWP [0;1]");
  Serial.print("G");Serial.print(CDE_CLEAN);Serial.print("\t\tCLEAN P[");
      Serial.print(CLEAN_STOP);Serial.print(" Stop; ");Serial.print(CLEAN_UP);Serial.print(" Up; ");
      Serial.print(CLEAN_DOWN);Serial.print(" Down]");
      Serial.println();
  Serial.print("ex : G");Serial.print(CDE_XVP_OPEN);Serial.println(" P1 A");
  Serial.print("ex : G");Serial.print(CDE_CATCH);Serial.println(" A");

//*/
}

void Interpreter::help() { 
  Serial.print("Console V");
  Serial.print(VERSION_INTERPRETER);
  Serial.print(" GCode:V");
  Serial.println(VERSION_GCODE);
  Serial.println();
  Serial.println("Commands:");
  Serial.println("? HELP [G commande ; M memory read for partial related help]");
  Serial.println("$[dest] Display values ");
  Serial.print(  "Available dest : ");Serial.println(AVAILABLE_DEST);
  //Serial.println("% Load test");

  Serial.println();
}

int Interpreter::parseNumber(char code, int val) {
  char *ptr=acBuffer;
  char *firstChar;
  int result;
  while(ptr && *ptr && ptr){
    if(*ptr==code) {
      result=atoi(ptr+1);
      firstChar=ptr+1;
      if (firstChar[0]!='0' && result==0){  //Error: it is not 0 but conversion gives 0 : return old value
        //interpreterError=true;
        return val;
      }
      else
        return result;                      // if something with no error, return value
    }
    ptr=strchr(ptr,' ')+1;
  }
  return val;                               //if nothing return original value
}

char Interpreter::parseLetter(const char *codeArray) {  //returns char founded otherwise 0
  unsigned int chr=0,i;
  while(acBuffer[chr]){                       //scan the acBuffer
   i=0;
   while(codeArray[i]){                     //scan for a code letter
      if(acBuffer[chr]==codeArray[i]) {
        return codeArray[i];
      }
      i++;
    }
    chr++;    
  }
  return 0;
}


void Interpreter::prepareGcode(){
  sInterpretedGCode.cCde=0;
  sInterpretedGCode.iGcode=-1;
  sInterpretedGCode.iParam=-1;
  sInterpretedGCode.cDest=0;
}


bool Interpreter::getCommand() {
   if( Serial.available() ) { // if something is available
    char c = Serial.read();   // get it
    Serial.print(c);          // optional: repeat back what I got for debugging

    // store the byte as long as there's room in the buffer.
    // if the buffer is full some data might get lost
    if(sofar < MAX_BUF) acBuffer[sofar++]=c;

    if(c=='\n') {             // if we got a return character (\n) the message is done.
      acBuffer[sofar]=0;      // strings must end with a \0.
      sofar=0;
      char toDest;
      char cCdeChar;
      long  param;

      prepareGcode();         //init Gcode    

      if(parseLetter("?")){
        cCdeChar=parseLetter(helpMode); 
        switch(cCdeChar) {
          case 'G':
            helpCde();
            break;
          case 'M':
            helpGetter();
            break;
          default:
            help();  
        }
      }
      else if (parseLetter("$")) {
        toDest=parseLetter(AVAILABLE_DEST);
        sInterpretedGCode.cCde='$';
        if (toDest){
          sInterpretedGCode.cDest=toDest;
        }
      }
      else if (parseLetter("%")){
        Serial.println("\nLoad Test starting");
      }
      else {
          // look for commands that start with 'G'
        sInterpretedGCode.cCde=parseLetter("GM");
        sInterpretedGCode.iGcode=parseNumber('G',-1);
        toDest=parseLetter(AVAILABLE_DEST);
        if(toDest){
          sInterpretedGCode.cDest=toDest;  
        }
        sInterpretedGCode.iParam=parseNumber('P',-1);
        if(sInterpretedGCode.iGcode<0){
          Serial.println("Erreur GCode");
          return false;   
        }
        else {
          Serial.print("Interpreted : ");
          Serial.print(sInterpretedGCode.cCde);
          Serial.print(sInterpretedGCode.iGcode);
          Serial.print(" P");
          Serial.print(sInterpretedGCode.iParam);
          Serial.print(" to ");
          Serial.print(sInterpretedGCode.cDest);
          Serial.println(".");
          
        }
      } 
      return true;
    }
  }

  return false;
}
