#ifndef Interpreter_h
#define Interpreter_h

#include "..\C-KErrorList.h"
#include "..\GCodeC-K.h"

struct gcode {
	char 		cCde;	 	//G ou M ou $ ou %		 0 : absence 				
							//G : commande avec parametre P et destinataire  	ex : G52 P1 A 	(G52 on A with param=1)
							//M : lecture memoire et destinataire				ex : M53  		(Display self value on memory 53 )
							//$ : display values et destinataire 				ex : $A  		(display A values)
							//% : test ex load test								ex : % 			(load test)
	int 		iGcode;		//Numero de a commande  -1 : absence
	int 		iParam;		//Numero du parametre 	-1 : absence
	char 		cDest;		//Lettre du destinataire 0 : absence ou self si $
};

char    				destName[]		=AVAILABLE_DEST;
char    				helpMode[]		="GM$%";

class Interpreter{
//FUNCTIONS
public:
	Interpreter();
	void				setup();
	bool				getCommand(); 								// false : error or nothing, true : something in sInterpretedGCode; 
	gcode 				sInterpretedGCode;

private:
	void 				NYI(); 										//not yet implemented
	void				help();
	void				helpGetter();
	void				helpCde();
	void				prepareGcode();
	int 				parseNumber(char code,int val);
	char				parseLetter(const char *codeArray);
	void 				resetGcode();


//VARIABLES

};



#endif
