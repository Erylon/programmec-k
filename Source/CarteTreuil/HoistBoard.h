#ifndef HoistBoard_h
#define HoistBoard_h

#include <arduino.h>
#include "..\Librairies\SimpleModBusMaster\SimpleModbusMaster.cpp"              	// Revision V10 https://drive.google.com/drive/folders/0B0B286tJkafVYnBhNGo4N3poQ2c?tid=0B0B286tJkafVSENVcU1RQVBfSzg
#include "..\CodeRelease.h"
#include "..\C-KErrorList.h"
#include "..\RegisterDefinition.h"
#include "HoistBoardPinList.h"
#include "..\GCodeC-K.h"
#include "..\Interpreter\Interpreter.cpp"
#include "..\CarteVerin\JackSharedConst.h"
#include "..\CarteRobot\Clean-KongSharedConst.h"
#include <HX711.h>					//Jauge
//#include <TimerOne.h> 

//PACKET FOR GRAFCET MODE
enum
{
  /*PACKET1,
  PACKET2,
 /* PACKET3,
  PACKET4,
*/


  PACKET_CDE_COILS_JACK_A,
  PACKET_CUR_COILS_JACK_A,
  PACKET_CDE_GRAFCET_JACK_A,
  PACKET_CUR_GRAFCET_JACK_A,
  PACKET_CUR_STATE_JACK_A,
#if NB_VENTOUSES >1
  PACKET_CDE_COILS_JACK_B,
  PACKET_CUR_COILS_JACK_B,
  PACKET_CDE_GRAFCET_JACK_B,
  PACKET_CUR_GRAFCET_JACK_B,
  PACKET_CUR_STATE_JACK_B,
#endif
#if NB_VENTOUSES >2
  PACKET_CDE_COILS_JACK_C,
  PACKET_CUR_COILS_JACK_C,
  PACKET_CDE_GRAFCET_JACK_C,
  PACKET_CUR_GRAFCET_JACK_C,
  PACKET_CUR_STATE_JACK_C,
#endif
#if NB_VENTOUSES >3
  PACKET_CDE_COILS_JACK_D,
  PACKET_CUR_COILS_JACK_D,
  PACKET_CDE_GRAFCET_JACK_D,
  PACKET_CUR_GRAFCET_JACK_D,
  PACKET_CUR_STATE_JACK_D,
#endif

  PACKET_CDE_ROBOT_COILS,
  PACKET_CUR_ROBOT_COILS,
  PACKET_CUR_ROBOT_BTN,

  TOTAL_NO_OF_PACKETS_GRAFCET_MODE // leave this last entry
};

#define NB_GRAFCET_PACKETS_BY_JACK 			5


const int  				A_BUZZ_FREQ[]=		{T_BUZZ_FREQUENCY};	
const char* 			A_NAME_IHM[]=		{T_IHM_NAME};							//nom des boutons
const char*				A_NAME_MODE[]=		{T_MODE_NAME};
const char*				A_NAME_MOVE[]=		{T_MOVE_NAME};
const char* 			A_GRAFCET_NAME[]=	{T_GRAFCET_NAME};
const char* 			A_STATE_NAME[]=		{T_STATE_NAME};
int 					A_SEUIL_PRESS_BAS[]={T_SEUIL_PRESS_BAS};
const int 				A_COIL_ROBOT[]=		{T_COIL_ROBOT};

#define RS485_ADRR_START	65

void 					codeurGetPos();
void 					codeurGetRpm();
	

//void	requestEvent();

unsigned long		curTime,mesureStartTime;

unsigned long 		zz=0;


void(* resetFunc) (void) = 0; //declare reset function @ address 0

class Hoist {
public:
					Hoist();
	void			loop();
	void 			displayParams();
	void 			printIndex(bool _force=false,int _a=2, int _b=2);
	long 			curPosIndex;



private:
	void			watchDog();
	void			buzz(int buzzCode=0, int duration=BUZZ_ERROR_DURATION);
	void 			checkGauge();
	void 			clean(bool doStartCleaning);
	void			updateMode(byte _mode);
	void 			move(byte _dir, bool _isFast=true);
	void 			freeAll();
	void 			catchAll();
	
	Packet 			packetsGrafcet[TOTAL_NO_OF_PACKETS_GRAFCET_MODE];

	Interpreter		*oInterpreter;
	
	HX711* 			gauge;
	long 			curLoad;
	float 			curSpeed;
	byte 			curGrafcet;
	byte			curState;

	long 			prevSpeedIndex;
	long 			minLoad,maxLoad;

	byte 			mode;
	bool			isHold;
	bool			isSleeping;
	int 			dir;
	bool 			isMoveBlocked;		//le mouvement n'est pas possible tant que la commande précédente n'est pas termiéne
	
	unsigned long 	nLog=0;

	bool			watchDogStatus;
	unsigned long	curTime,prevTime,startTime,cycleTime,holdStartTime,newModeStartTime,pingStartTime,loadStartTime, speedStartTime;
	unsigned long   sleepTime;
	bool 			isOverLoad,isUnderLoad;
	bool			isRollDown;
	bool 			isSWP;
	bool			isRegenerating;
	int 			pongId;				//pour la boucle pong du cleaning start
	int 			Btn_Pin[NB_HOIST_BTN];
	int 			Btn_State[NB_HOIST_BTN];
	bool 			isRobotBtnPressed[NB_ROBOT_BTN];
	int 			Led_Pin[NB_LED];
	bool 			Led_State[NB_LED];
	int				errorCode;
	unsigned long	startBuzz,buzzDuration;
	unsigned 		US_DownAlertTime,US_UpAlertTime;
	bool			US_Alert_Down,US_Alert_Up;
	bool			isBuzz;
	bool			doDisplayLoad;
	bool 			isBumpDown,isBumpUp,oldIsBumpDown,oldIsBumpUp;
	bool			forceIsZSH;
	int 			tempDir;
	int 			tempMode;

	unsigned int 	holdingRegs[HOIST_RS485_HOLDING_REGS_SIZE];

#ifdef SIMU_CODEUR
	byte 			tempoCodeur;
#endif
	int 			cnt;
};

#endif