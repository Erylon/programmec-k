#include ".\HoistBoard.h"

/*TODO
//*/



extern Hoist *oHoist; //necessaire pour codeurGetPos

long getInt(){
	Serial.println("Please set value");
	while (Serial.available()==0) {}
	return Serial.parseInt();
}



void codeurGetPos(){
	int a =digitalRead(PIN_VOIE_A);
	int b =digitalRead(PIN_VOIE_B);
	
	if(a==b){
		oHoist->curPosIndex++;
	}
	else {
		oHoist->curPosIndex--;
	}
	//oHoist->printIndex(FORCE_DISPLAY);
    //oHoist->curPosIndex+=digitalRead(PIN_VOIE_A)*digitalRead(PIN_VOIE_B);
    
    //Serial.print("new Pos=");
    //Serial.print(oHoist->curPosIndex);
    /*
    if(((oHoist->moveDir==MOVE_UP)||(oHoist->moveDir==MOVE_DOWN))&&(oHoist->prevPos==oHoist->curPosIndex)){
        oHoist->errorCode=ERR_NO_ENCODEUR;        //on déclare erreur si le treuil monte ou descend et que le codeur ne change pas d'état
    }
    if((oHoist->moveDir==STOP)&&(oHoist->prevPos!=oHoist->curPosIndex)){
        oHoist->errorCode=ERR_NO_STOP;            //on déclare erreur si le treuil est en stop et que le codeur change d'etat
    }
    //*/
}

Hoist::Hoist() {//Constructeur
	curTime=millis();
	curSpeed=0;
	curGrafcet=NO_GRAFCET;
	Serial.println();							// Start writing from a new clean line
	Serial.print("Hoist ");
	Serial.print(" version ");
	Serial.print(VERSION_TEXT);
	Serial.print(" GCode:V");
  	Serial.print(VERSION_GCODE);
	Serial.print(" Code Release:");
  	Serial.print(CODE_RELEASE);
  	Serial.print(" RS485 Baud:");
  	Serial.print(RS485_BAUDRATE);
	Serial.println(" Initializing...");
	Serial.flush();
	delay(2000);
  	//Parametrage des echanges Modbus en mode GRAFCET
	//  					--------------------->registre JACK_CDE_COILS de JACK_A, taille 1, offset JACK_A_CDE_COILS du maitre
 	//modbus_construct(&packets, Adresse, READ_HOLDING_REGISTERS, adresse chez l'esclave, 1, adresse chez le maitre);
 
  	for(int i=0;i<NB_VENTOUSES;i++){

	    modbus_construct(&packetsGrafcet[PACKET_CDE_COILS_JACK_A	+NB_GRAFCET_PACKETS_BY_JACK*i]	, JACK_A +i, PRESET_MULTIPLE_REGISTERS, 	JACK_CDE_COILS, 	1, JACK_A_CDE_COILS		+i*JACK_RS485_HOLDING_REGS_SIZE);    
	    modbus_construct(&packetsGrafcet[PACKET_CUR_COILS_JACK_A	+NB_GRAFCET_PACKETS_BY_JACK*i]	, JACK_A +i, READ_HOLDING_REGISTERS, 		JACK_CUR_COILS, 	1, JACK_A_CUR_COILS		+i*JACK_RS485_HOLDING_REGS_SIZE);    
	    modbus_construct(&packetsGrafcet[PACKET_CDE_GRAFCET_JACK_A	+NB_GRAFCET_PACKETS_BY_JACK*i]	, JACK_A +i, PRESET_MULTIPLE_REGISTERS, 	JACK_CDE_GRAFCET, 	1, JACK_A_CDE_GRAFCET	+i*JACK_RS485_HOLDING_REGS_SIZE);    
	    modbus_construct(&packetsGrafcet[PACKET_CUR_GRAFCET_JACK_A	+NB_GRAFCET_PACKETS_BY_JACK*i]	, JACK_A +i, READ_HOLDING_REGISTERS, 		JACK_CUR_GRAFCET, 	1, JACK_A_CUR_GRAFCET	+i*JACK_RS485_HOLDING_REGS_SIZE);    
	    modbus_construct(&packetsGrafcet[PACKET_CUR_STATE_JACK_A	+NB_GRAFCET_PACKETS_BY_JACK*i]	, JACK_A +i, READ_HOLDING_REGISTERS, 		JACK_CUR_STATE, 	1, JACK_A_CUR_STATE		+i*JACK_RS485_HOLDING_REGS_SIZE);    
 

  	}


    modbus_construct(&packetsGrafcet[PACKET_CDE_ROBOT_COILS]	, ROBOT_ADRESS, PRESET_SINGLE_REGISTER, 	C_K_CDE_COILS, 	1, ROBOT_CDE_COILS);    
    modbus_construct(&packetsGrafcet[PACKET_CUR_ROBOT_COILS]	, ROBOT_ADRESS, READ_HOLDING_REGISTERS, 	C_K_CUR_COILS, 	1, ROBOT_CUR_COILS);    
    modbus_construct(&packetsGrafcet[PACKET_CUR_ROBOT_BTN]		, ROBOT_ADRESS, READ_HOLDING_REGISTERS, 	C_K_CUR_BTN, 	1, ROBOT_CUR_BTN);    

    modbus_configure(&Serial2, RS485_BAUDRATE, SERIAL_8N2, MODBUS_TIME_OUT, MODBUS_POLLING, MODBUS_RETRY_COUNT, PIN_RS485_DE, packetsGrafcet, TOTAL_NO_OF_PACKETS_GRAFCET_MODE, holdingRegs);


	/// Configuration des broches en entrée
	pinMode(PIN_VOK,INPUT);				//2
	pinMode(PIN_HSER_BTN,INPUT);		//8
	pinMode(PIN_ZSH,INPUT);				//22
	pinMode(PIN_INDEX,INPUT);			//23
	pinMode(PIN_WAL,INPUT);				//24
	pinMode(PIN_VOIE_A,INPUT);			//25
	pinMode(PIN_VOIE_AA,INPUT);			//25
	pinMode(PIN_VOIE_B,INPUT);			//27
	pinMode(PIN_HSRST,INPUT);			//26

	int tBtn[NB_HOIST_BTN]={T_BTN};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_HOIST_BTN;i++){
		Btn_Pin[i]=tBtn[i];
		pinMode(Btn_Pin[i],	INPUT);
	}


	int tLed[NB_LED]={T_LED};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_LED;i++){
		Led_Pin[i]=tLed[i];
		pinMode(Led_Pin[i],	OUTPUT);	
		digitalWrite(Led_Pin[i],LOW);
		Led_State[i]=LOW;
	}

	isRollDown=false;
	isSWP=false;
	isRegenerating=false;
	isUnderLoad=false;
	isOverLoad=false;
	isBumpDown=false;
	isBumpUp=false;
	forceIsZSH=false;
	tempMode=MODE_MANUAL;
	tempDir=MOVE_STOP;

	//Configurationdesbrochesdesortie
	pinMode(PIN_BUZZER,OUTPUT);			//
	pinMode(PIN_BUZZER2,OUTPUT);		//
	pinMode(PIN_LED,OUTPUT);			//
	pinMode(PIN_HSVC,OUTPUT);			//
	pinMode(PIN_SCL,OUTPUT);			//LowSpeed
	pinMode(PIN_SCH,OUTPUT);			//MoveAck
	pinMode(PIN_ZCDN,OUTPUT);			//Counterclockwise
	pinMode(PIN_ZCUP,OUTPUT);			//ClockWise
	
	//US alert
	US_DownAlertTime=US_UpAlertTime=0;
	US_Alert_Down=US_Alert_Up=false;


  	// Forcage au démarage
  	digitalWrite(PIN_LED, LOW);          // Extinction de la LED
  	digitalWrite(PIN_BUZZER, LOW);       // Extinction du BUZZER
  	digitalWrite(PIN_BUZZER2, LOW);      // Extinction du BUZZER

	Serial.println("If help not displayed below : Gauge not connected, or VIRTUAL_GAUGE not set");
	Serial.flush();

	#ifdef VIRTUAL_GAUGE
		delay(200);
		Serial.println("Virtual Gauge set");
	#else
		gauge=new HX711(PIN_DATA_GAUGE, PIN_CLOCK_GAUGE);
		delay(200); 					//otherwise bootpb
		gauge->set_scale(SCALE_MULTIPLIER); // permet de convertir les données en donées lisibles 
		gauge->set_gain(128);			//32 //voie A
	  	//scale->tare();
	#endif

	oInterpreter= new  Interpreter();
    oInterpreter->setup();

	digitalWrite(PIN_SCH,HIGH);			//ON=!STOP
	digitalWrite(PIN_ZCUP,HIGH);		//UP
	digitalWrite(PIN_ZCDN,HIGH);		//DOWN
	
	prevSpeedIndex=curPosIndex=Z_INIT_PT;
	#ifndef NO_CODEUR
	attachInterrupt(digitalPinToInterrupt(PIN_VOIE_A), codeurGetPos, CHANGE);  // detection des fronts descendant
	//attachInterrupt(digitalPinToInterrupt(PIN_INDEX),  codeurGetRpm, CHANGE);
	#endif
	watchDogStatus=true;

	doDisplayLoad=false;
	isSleeping=false;
	sleepTime=SLEEP_TIME;
	isMoveBlocked=false;
	isOverLoad=isUnderLoad=false;

	dir=MOVE_STOP;
	mode=MODE_INIT;//MODE_BEFORE_INIT; //Mettre n'importe quoi pour different de mode_init pour qu'il n'y ait pas de mode à l'init
	//mode=MODE_NO_LOAD;
		//updateMode(MODE_INIT);

	prevTime=millis();
	startBuzz=speedStartTime=loadStartTime=newModeStartTime=curTime;//newDirStartTime
	
	//initialisation du registre
	for(int i=0;i<NB_VENTOUSES;i++){
		holdingRegs[JACK_A_CUR_GRAFCET 	+i*JACK_RS485_HOLDING_REGS_SIZE]=holdingRegs[JACK_A_CDE_GRAFCET 	+i*JACK_RS485_HOLDING_REGS_SIZE]=DO_FREE;
		holdingRegs[JACK_A_CDE_COILS 	+i*JACK_RS485_HOLDING_REGS_SIZE]=holdingRegs[JACK_A_CDE_COILS 		+i*JACK_RS485_HOLDING_REGS_SIZE]=0;	//2^XVJ;
		holdingRegs[JACK_A_CDE_PSL 		+i*JACK_RS485_HOLDING_REGS_SIZE]=holdingRegs[JACK_A_CDE_PSL 		+i*JACK_RS485_HOLDING_REGS_SIZE]=A_SEUIL_PRESS_BAS[i];
		holdingRegs[JACK_A_CDE_PSH 		+i*JACK_RS485_HOLDING_REGS_SIZE]=SEUIL_PRESS_HAUT;
	}

	holdingRegs[ROBOT_CDE_XVRO_PERIOD]=		XVRO_PERIOD;
	holdingRegs[ROBOT_CDE_XVSX_PERIOD]=		XVSX_PERIOD;
	holdingRegs[ROBOT_CDE_XVRO_DURATION]=	XVRO_DURATION;
	holdingRegs[ROBOT_CDE_XVSX_DURATION]=	XVSX_DURATION;
	holdingRegs[ROBOT_CDE_COILS]=			2^SWP;
	holdingRegs[ROBOT_CUR_BTN]=				108;
	holdingRegs[ROBOT_CDE_ROLL_SPEED]=		MAX_ROLL_SPEED;
	holdingRegs[ROBOT_CDE_BUZZER]=			BUZZ_INIT_FREQ;



	isBuzz=false;
	buzz(BUZZ_INIT, BUZZ_INIT_DURATION);
	displayParams();
}

void Hoist::move(byte _dir, bool _isFast){
	bool isNotZSH=digitalRead(PIN_ZSH);
	#ifdef NO_LIMIT_SWITCH
		isNotZSH=!forceIsZSH;
	#endif
	byte nbCatched=0;
	for(int i=0;i<NB_VENTOUSES;i++){
		if(	holdingRegs[JACK_A_CUR_GRAFCET+i*JACK_RS485_HOLDING_REGS_SIZE]==DO_CATCH &&
			holdingRegs[JACK_A_CUR_STATE  +i*JACK_RS485_HOLDING_REGS_SIZE]==MONITORING){
			nbCatched++;	
		}
	}
	//*/

	if(_dir!=dir){			//Le changement de dir arrête les fonctions en cours
		Serial.println("New Dir");
		isHold=false;
	/*			IFTRACE_SERIAL(Serial.print("NoHoldIci D="));
				IFTRACE_SERIAL(Serial.print(dir));
				IFTRACE_SERIAL(Serial.print(" _D= "));
				IFTRACE_SERIAL(Serial.print(_dir));
	//*/
		//newDirStartTime=curTime;
		isSleeping=false;
		holdStartTime=curTime;
		
		if(mode==MODE_CLEANING /*&& nbPong==5 /*&& nbCatched>1 && !isMoveBlocked*/){
			//digitalWrite(PIN_NIR_LED,HIGH);
			curGrafcet=(_dir==MOVE_DOWN)?CLEANING_DOWN:CLEANING_STOP;
			curGrafcet=(_dir==MOVE_UP)?CLEANING_UP:curGrafcet;
			Serial.print("new cleaning grafcet ");Serial.println(A_GRAFCET_NAME[curGrafcet]);
			/**********************************************/
			curGrafcet=(isUnderLoad || isOverLoad || !isNotZSH )?NO_GRAFCET:curGrafcet;
			curState=(curGrafcet!=NO_GRAFCET)?INITIALIAZING:curState;
			startTime=curTime; //Il n'y a pas tjs de curState et donc d'init du temps dans les grafcets
		}
		else {
			isMoveBlocked=false;
			//digitalWrite(PIN_NIR_LED,LOW);
		}

		dir=(_dir==MOVE_UP) && !isOverLoad && isNotZSH ? MOVE_UP:MOVE_STOP;
		dir=(_dir==MOVE_DOWN) && !isUnderLoad ? MOVE_DOWN:dir;
	}

	//Arret en cas de problème
	dir=(   mode==MODE_INIT 
		 || (isOverLoad && dir ==MOVE_UP)
		 || (isUnderLoad && dir ==MOVE_DOWN)
		 || (!isNotZSH && dir == MOVE_UP)
		 || isMoveBlocked 
//		 || (mode==MODE_CLEANING && nbCatched<2))
		)?MOVE_STOP:dir;

	if(dir==MOVE_STOP || (US_Alert_Down && dir==MOVE_UP)) {//Supprime alert US si stop ou mvt inverse
		US_Alert_Down=false;
	}
	if(dir==MOVE_STOP || (US_Alert_Up && dir==MOVE_DOWN)) {//Supprime alert US si stop ou mvt inverse
		US_Alert_Up=false;
	}

	if ((dir==MOVE_UP && curGrafcet==CLEANING_DOWN)){
		Serial.println("INVERSION UP CDOWN");
	}
	if ((dir==MOVE_DOWN && curGrafcet==CLEANING_UP)){
		Serial.println("INVERSION DOWN CUP");
	}

	//Led d'erreurs
	/**********************************************/

	digitalWrite(PIN_WIH_LED,isOverLoad);
	digitalWrite(PIN_WIL_LED,isUnderLoad);
	digitalWrite(PIN_ZIH_LED,!isNotZSH);

#ifdef SIMU_CODEUR
	if(dir!=MOVE_STOP){
		tempoCodeur++;
		if(tempoCodeur>MODULO_CODEUR){
			tempoCodeur=0;
			if(dir==MOVE_UP){
				curPosIndex++;
			}
			else if (dir==MOVE_DOWN){
				curPosIndex--;
			}
			#ifdef TRACE_CODEUR
				printIndex();
			#endif
		}
	}
#endif

	//led boutons
	digitalWrite(PIN_HSMU_LED,dir == MOVE_UP); 
	digitalWrite(PIN_HSMD_LED,dir == MOVE_DOWN);
	digitalWrite(PIN_HSS_LED,dir == MOVE_STOP);

	//commande treuil
	//Par sécurité on remet les conditions :
	digitalWrite(PIN_SCH,dir==MOVE_STOP);		//ON=!STOP
	#ifdef NO_LIMIT_SWITCH
		digitalWrite(PIN_ZCUP,dir != MOVE_UP || isOverLoad); //UP cablé en pull up
	#else
		digitalWrite(PIN_ZCUP,dir != MOVE_UP || !digitalRead(PIN_ZSH) || isOverLoad); //UP cablé en pull up
	#endif
	digitalWrite(PIN_ZCDN,dir != MOVE_DOWN || isUnderLoad);	//DOWN cablé en pull up
	digitalWrite(PIN_SCL,_isFast);				//Fast

	//Fonction HOLD
	if(curTime-holdStartTime>HOLD_DURATION && dir!=MOVE_STOP && isHold==false && mode!=MODE_MANUAL){
		isHold=true;
		buzz(BUZZ_HOLD,BUZZ_ALERT_HOLD);
		IFTRACE_SERIAL(Serial.println("Hold"));
	}
}

void Hoist::freeAll(){
	IFTRACE_SERIAL(Serial.println("\033[41mFree All\033[40m"));
	for(int i=0;i<NB_VENTOUSES;i++){
		holdingRegs[JACK_A_CDE_GRAFCET  +i*JACK_RS485_HOLDING_REGS_SIZE]=DO_FREE;
	}
}

void Hoist::catchAll(){
	IFTRACE_SERIAL(Serial.println("\033[41mCatch All\033[40m"));
	for(int i=0;i<NB_VENTOUSES;i++){
		holdingRegs[JACK_A_CDE_GRAFCET  +i*JACK_RS485_HOLDING_REGS_SIZE]=DO_CATCH;
	}
}

void Hoist::updateMode(byte _mode){
	if(mode!=_mode){		//Le changement de mode arrête les fonctions ne cours
		isRegenerating=isHold=false;
		IFTRACE_SERIAL(Serial.println("NoHoldLa"));
		newModeStartTime=curTime;
		move(MOVE_STOP);
		for(int i=0;i<6;i++){//Eteindre toutes les leds de mode et de deplacement
			digitalWrite(Led_Pin[i],LOW);
		}
		//digitalWrite(PIN_NIR_LED,LOW);
		IFTRACE_SERIAL(Serial.print("New mode : "));
		IFTRACE_SERIAL(Serial.print("\033[32m"));
		IFTRACE_SERIAL(Serial.print(A_NAME_MODE[_mode]));
		IFTRACE_SERIAL(Serial.println("\033[37m"));
		if(_mode!=MODE_CLEANING && _mode !=MODE_CLEANING_SLEEP){
			freeAll();
			curGrafcet=CLEANING_HALT;//Start le grafcet
			curState=INITIALIAZING;
		}
		else if(_mode==MODE_CLEANING){
			#ifndef VIRTUAL_IHM_BUTTON
			//Si pas de com avec jack
			//	resetFunc();
			#endif
			curGrafcet=CLEANING_START;//Start le grafcet
			if(isSleeping){			  //On sort du sleeping
				curState=ROLL_DOWN;
			}
			else {
				curState=INITIALIAZING;
				//isMoveBlocked=true;
			}
			//digitalWrite(PIN_NIR_LED,HIGH);
		}
		isSleeping=false;
	}

	isRobotBtnPressed[HSMA]=false;
	isRobotBtnPressed[HSCL]=false;
	

	//Reallumer les leds du mode
	switch (_mode){
		case MODE_NO_LOAD:
			digitalWrite(PIN_HSNL_LED,HIGH);
			maxLoad=MAX_LOAD_FREE_MOVE;
			minLoad=NO_MIN_LOAD;
			if(isSWP) {
				isSWP=false;
				bitWrite(holdingRegs[ROBOT_CDE_COILS], SWP, 0);
			}
			break;
		case MODE_MANUAL:
			digitalWrite(PIN_HSMA_LED,HIGH);
			maxLoad=MAX_LOAD;
			minLoad=NO_MIN_LOAD;
			break;
		case MODE_CLEANING:
			maxLoad=MAX_LOAD;
			minLoad=MIN_LOAD;
			if((curTime-newModeStartTime>sleepTime) && (dir==MOVE_STOP)){
				if(!isSleeping) {
					IFTRACE_SERIAL(Serial.println(" \033[33mSleep\033[37m"));
					curGrafcet=CLEANING_SLEEP;
					_mode=MODE_CLEANING_SLEEP;
				}
				isSleeping=true;
			}
			else {
				digitalWrite(PIN_HSCL_LED,HIGH);	
			}
			break;
		case MODE_CLEANING_SLEEP:
			break;
		default :
			_mode=MODE_INIT;
			maxLoad=MAX_LOAD_FREE_MOVE;
			minLoad=NO_MIN_LOAD;

			break;
	}

	mode=_mode;
}

void Hoist::buzz(int buzzCode, int duration){
	if(buzzCode==NO_BUZZ){
		noTone(PIN_BUZZER);
		digitalWrite(PIN_BUZZER2,LOW);
		isBuzz=false;
		startBuzz=curTime;
		//Serial.println("no Buzz");
	}
	else {
		isBuzz=true;
		startBuzz=curTime;
		buzzDuration=duration;
		#ifdef NO_SOUND
			Serial.println("Buzz");
		#else
			tone(PIN_BUZZER,A_BUZZ_FREQ[buzzCode]);
		digitalWrite(PIN_BUZZER2,HIGH);
		#endif
	}
} 


void Hoist::watchDog(){
	if(isBuzz && (curTime-startBuzz)>buzzDuration){
		buzz(NO_BUZZ);
	}

	if((US_Alert_Down || US_Alert_Up) && !isBuzz){
		if(dir==MOVE_DOWN && ((curTime-startBuzz)>(US_DownAlertTime/7))){
			buzz(BUZZ_US_ALERT,BUZZ_US_ALERT_DURATION);
		} //	unsigned 		US_DownAlertTime,US_UpAlertTime;

	}


	if((curTime-speedStartTime)>SPEED_PERIOD){
		speedStartTime=curTime;
		curSpeed=(float)((prevSpeedIndex-curPosIndex))/SPEED_PERIOD;
		prevSpeedIndex=curPosIndex;

		#ifdef DISPLAY_SPEED
			Serial.print(curSpeed);Serial.println("pt/ms");
		#endif

	}
	
	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		if(isSleeping){
			digitalWrite(PIN_HSCL_LED,watchDogStatus);
		}
		prevTime=curTime;
	}
	#ifdef MESURE_TEMPS_CYLE
		if(++nLog%1000==0){
			Serial.print("1000 temps cycle (ms) =");Serial.println(curTime-cycleTime);
			cycleTime=curTime;
		}
	#endif
}


void Hoist::checkGauge(){
		long tempRead;
	#ifndef VIRTUAL_GAUGE
		if(gauge->is_ready()){
	#endif

			#ifdef MESURE_GAUGE_CYCLE
				cumulatedGaugeTime+=curTime-gaugeTime;
				gaugeCycle++;
				if(gaugeCycle>=100){
					gaugeCycle=0;
					Serial.print("Temps de cycle Jauge=");Serial.println((float)(cumulatedGaugeTime/100.));
					cumulatedGaugeTime=0;
					gaugeTime=curTime;
				}
			#endif
			#ifdef VIRTUAL_GAUGE
				curLoad=(mode==MODE_NO_LOAD)?0:VIRTUAL_LOAD;
			#else
				gauge->power_up();
				tempRead=gauge->read();//read_average(3);
				curLoad=SCALE_A*tempRead+SCALE_B;//read_average(3);
			#endif			
			#ifdef SIMU_OVERLOAD
				tempRead=digitalRead(PIN_HSER_BTN)?MAX_LOAD+1:VIRTUAL_LOAD;
				if(tempRead!=curLoad){
					Serial.print("New Load=");Serial.println(tempRead);	
				} 
				curLoad=tempRead;


			#endif
			#ifdef SIMU_UNDERLOAD
				tempRead=digitalRead(PIN_HSER_BTN)?10:VIRTUAL_LOAD;
				if(tempRead!=curLoad){
					Serial.print("New Load=");Serial.println(tempRead);	
				} 
				curLoad=tempRead;
			#endif

			//filtrage temporel
			if(curLoad>minLoad && curLoad<maxLoad){
				loadStartTime=curTime;
				isOverLoad=isUnderLoad=false;
			} else if(curTime-loadStartTime>LOAD_FILTER_TIME){
				isOverLoad=curLoad>maxLoad?true:false;
				isUnderLoad=curLoad<minLoad?true:false;
			}

			#ifdef DISPLAY_GAUGE_READ
			//doDisplayLoad=true;
			//if(doDisplayLoad){
				Serial.print(nLog++);
				Serial.print(" / charge :");
				Serial.print(curLoad);
				Serial.print(" N \t");
				Serial.print(tempRead);
				Serial.print("pt");
				Serial.println();
			//}
			#endif
	#ifndef VIRTUAL_GAUGE
		}
	#endif	
	
}


void Hoist::loop(){
	curTime=millis();
/******************************************************************************************************* */
/**                                                TRAITEMENT DES INPUTS                                 */
/******************************************************************************************************* */


	
	//Gauge	
	checkGauge();
	//Button	
	for(int i=0;i<NB_HOIST_BTN;i++){
		Btn_State[i]=digitalRead(Btn_Pin[i]);
	}

/******************************************************************************************************* */
/**                                          TRAITEMENT  INTERPRETER                                     */
/******************************************************************************************************* */
	if(oInterpreter->getCommand()){
		switch (oInterpreter->sInterpretedGCode.cCde){
			case 'M':
				break;
			case 'G':
				switch(oInterpreter->sInterpretedGCode.iGcode){
					case CDE_CATCH:
						if(oInterpreter->sInterpretedGCode.cDest>64 && oInterpreter->sInterpretedGCode.cDest<69){
							bitWrite(holdingRegs[ROBOT_CDE_COILS], SWP,	1);
							holdingRegs[JACK_A_CDE_GRAFCET+JACK_RS485_HOLDING_REGS_SIZE*(oInterpreter->sInterpretedGCode.cDest-65)]=DO_CATCH;
						}
						break;
					case CDE_FREE:
						if(oInterpreter->sInterpretedGCode.cDest>64 && oInterpreter->sInterpretedGCode.cDest<69){
							holdingRegs[JACK_A_CDE_GRAFCET+JACK_RS485_HOLDING_REGS_SIZE*(oInterpreter->sInterpretedGCode.cDest-65)]=DO_FREE;
						}
						break;
					case CDE_XVRO_OPEN:
					case CDE_XVSH_OPEN:
					case CDE_XVSM_OPEN:
					case CDE_XVSL_OPEN:
					case CDE_XVRJ_OPEN:
					case CDE_HVCW_OPEN:
					case CDE_VM:
					case CDE_RM:
					case CDE_SWP:	
						bitWrite(holdingRegs[ROBOT_CDE_COILS], A_COIL_ROBOT[oInterpreter->sInterpretedGCode.iGcode-CDE_XVRO_OPEN],  oInterpreter->sInterpretedGCode.iParam);
						break;	
					case CDE_XVS_OPEN:
					case CDE_XVP_OPEN:
					case CDE_XVV_OPEN:
					case CDE_XVJ_OPEN:
						if(oInterpreter->sInterpretedGCode.cDest>64 && oInterpreter->sInterpretedGCode.cDest<69){
							Serial.println(oInterpreter->sInterpretedGCode.iGcode-CDE_XVS_OPEN);
							bitWrite(holdingRegs[JACK_A_CDE_COILS+	(oInterpreter->sInterpretedGCode.cDest-65)*JACK_RS485_HOLDING_REGS_SIZE], oInterpreter->sInterpretedGCode.iGcode-CDE_XVS_OPEN,  oInterpreter->sInterpretedGCode.iParam);
							holdingRegs[		 JACK_A_CDE_GRAFCET+(oInterpreter->sInterpretedGCode.cDest-65)*JACK_RS485_HOLDING_REGS_SIZE]=NO_GRAFCET;

						}
						break;
					default:
						break;
				}
				break;
			case '$':
				switch(oInterpreter->sInterpretedGCode.cDest){
					case 0:
						displayParams();
						break;
					case 'R':
						bitWrite(holdingRegs[ROBOT_CDE_COILS],DISPLAY,1);
						break;
					default:
						bitWrite(holdingRegs[JACK_A_CDE_COILS+(oInterpreter->sInterpretedGCode.cDest-65)*JACK_RS485_HOLDING_REGS_SIZE],DISPLAY,!bitRead(holdingRegs[JACK_A_CDE_COILS+(oInterpreter->sInterpretedGCode.cDest-65)*JACK_RS485_HOLDING_REGS_SIZE],DISPLAY));
					break;
				}
			break;

		}
	}

//*/

/******************************************************************************************************* */
/**                                                TRAITEMENT DES BOUTONS                                */
/******************************************************************************************************* */
#ifndef VIRTUAL_IHM_BUTTON
	if(Btn_State[HSRST]){ //Reset Altitude
		Serial.println("Pos 0 Set by Hoist");
		curPosIndex=Z_INIT_PT;
	}

	//MOVING
	tempDir=isHold?dir:MOVE_STOP;
	/*if(!isComOK && (mode==MODE_CLEANING_SLEEP || mode==MODE_CLEANING)) {
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;
	}//*/

	//LECTURE DES BOUTONS ROBOT
	for(int i=0;i<NB_ROBOT_BTN;i++){ //remarque les N° d'index des boutons du robot sont les mm que ceux du treuil
		isRobotBtnPressed[i]=bitRead(holdingRegs[ROBOT_CUR_BTN],i);	
	}

	if(!Btn_State[HSMU] && !Btn_State[HSMD] && Btn_State[HSS]){ //Stop
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;
	}
	if((Btn_State[HSMU]^isRobotBtnPressed[HSMU]) && !Btn_State[HSMD] && !Btn_State[HSS] && !isRobotBtnPressed[HSMD] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMU])){ //Up
		tempDir=MOVE_UP;
		newModeStartTime=curTime;
	}
	if(!Btn_State[HSMU] && (Btn_State[HSMD]^isRobotBtnPressed[HSMD]) && !Btn_State[HSS] && !isRobotBtnPressed[HSMU] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMD])){ //Down
		tempDir=MOVE_DOWN;
		newModeStartTime=curTime;
	}

	//Serial.print(holdingRegs[ROBOT_CUR_BTN]);
	if(isRobotBtnPressed[HSMU]||isRobotBtnPressed[HSMD]){//Appui sur Up ou Down du robot
		//Serial.println("\033[31mSTOP ASKED\033[37m");
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;	
	}
	
/*
	if(bitRead(holdingRegs[JACK_A_CUR_COILS],DISPLAY)==true){
		bitWrite(holdingRegs[JACK_A_CDE_COILS],DISPLAY,0);
	}	
	if(bitRead(holdingRegs[ROBOT_CUR_COILS],DISPLAY)==true){
		//bitWrite(holdingRegs[ROBOT_CDE_COILS],DISPLAY,0);
	}	
*/
/*
	//mode defini par les boutons
	////update but doesn't change mode on invalid conf :
	tempMode=mode;
	if(!Btn_State[HSCL] && !Btn_State[HSMA] && Btn_State[HSNL]){ //No Load
		tempMode=MODE_NO_LOAD;
	}
	if((Btn_State[HSCL]^isRobotBtnPressed[HSCL]) && !Btn_State[HSMA] && !Btn_State[HSNL] && !isRobotBtnPressed[HSMA]  && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSCL])){ //Cleaning
		tempMode=MODE_CLEANING;
	}
	if(!Btn_State[HSCL] && (Btn_State[HSMA] ^ isRobotBtnPressed[HSMA] ) && !Btn_State[HSNL] && !isRobotBtnPressed[HSCL] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMA])){ //Manual
		tempMode=MODE_MANUAL;
	}
	if(Btn_State[HSRST] && Btn_State[HSNL] && tempMode==MODE_NO_LOAD && !isRegenerating){//regeneration
		//bitWrite(holdingRegs[ROBOT_CDE_COILS], VM, 1);
		bitWrite(holdingRegs[ROBOT_CDE_COILS], RM, 1);
		isRegenerating=true;
	}
//*/
#endif
	
	if(tempDir!=MOVE_STOP && mode==MODE_CLEANING_SLEEP){
		//Serial.print(A_NAME_MOVE[tempDir]);
		Serial.println("\033[31mRestart Cleaning\033[37m");
		tempMode=MODE_CLEANING;
		//bitWrite(holdingRegs[ROBOT_CDE_COILS], VM, 1);
		bitWrite(holdingRegs[ROBOT_CDE_COILS], RM, 1);
		if(tempDir==MOVE_UP){
			Serial.println("XVRO On");
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 1);
		}
		
	}

	if(Btn_State[HSER] ){//reset error
		digitalWrite(PIN_HSER_LED,LOW);
		digitalWrite(PIN_ZIH_LED,LOW);
	}

#ifndef VIRTUAL_IHM_BUTTON
	if(Btn_State[HSER] && Btn_State[HSRST]){//reset Arduino
		resetFunc();
	}
#endif
	updateMode(tempMode);	
	move(tempDir);	

	modbus_update();				

/******************************************************************************************************* */
/**                                                      GRAFETS                                         */
/******************************************************************************************************* */

	//SUIVI DES ACTION
	switch(curGrafcet){
		case CLEANING_HALT: //Sortie du mode cleaning
			switch(curState){
				case FREE_NEXT:
					if(curTime-startTime>1000){
						holdingRegs[JACK_B_CDE_GRAFCET]= DO_FREE;
						holdingRegs[JACK_D_CDE_GRAFCET]= DO_FREE;
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRJ, 1);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 0);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 0);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 0);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 0);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], VM,   0);
						bitWrite(holdingRegs[ROBOT_CDE_COILS], RM,   0);
						Serial.println("\033[31mCLEANING STOP\033[37m");
						curGrafcet=NO_GRAFCET;	
					}
					break;					
				default :
					holdingRegs[JACK_A_CDE_GRAFCET]= DO_FREE;
					holdingRegs[JACK_C_CDE_GRAFCET]= DO_FREE;
					Serial.println("\033[31mCLEANING HALT\033[37m");
					curState=FREE_NEXT;
					startTime=curTime;		
					break;
			}
			break;
		case CLEANING_STOP:
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 0);
			if(curTime-startTime>REDUCE_WATER_DELAY){
				IFTRACE_SERIAL(Serial.println("\033[31mSTOP & REDUCE WATER\033[37m"));
				bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 0);
				bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 0);
				bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 0);
				curGrafcet=NO_GRAFCET;
			}
			break;
		case CLEANING_UP:
			switch(curState){
				case REDUCE_WATER:
					if(curTime-startTime>REDUCE_WATER_DELAY){
						Serial.println("\033[31mUP & REDUCE WATER\033[37m");
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 0);
						curGrafcet=NO_GRAFCET;
					}
					break;
				default:
					//holdingRegs[ROBOT_CUR_XVRO_DURATION]=4000;
					//holdingRegs[ROBOT_CUR_XVRO_PERIOD]=4000;
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 0);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], VM,   1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], RM,   1);
					Serial.println("CLEANING UP XVRO on");
					curState=REDUCE_WATER;
					startTime=curTime;
					break;
			}
			break;
		case CLEANING_DOWN:
			switch(curState){
				case REDUCE_WATER:
					#ifndef WATER_ON_DESCENT
					#endif

					curGrafcet=NO_GRAFCET;
					break;
				default:
					#ifdef WATER_ON_DESCENT
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 1);
					#else
						bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 0);
					#endif
					//oInterpreter->oRS485->addMsg('R','S',SET_XVRO_DURATION,2000);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 0);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 0);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], VM,   1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], RM,   1);
					Serial.println("XVRO Off");
					Serial.println("\033[31mCLEANING DOWN\033[37m");
					curState=REDUCE_WATER;
					startTime=curTime;
					break;
			}
			break;
		case CLEANING_SLEEP:
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 0);
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 0);
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 0);
			bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 0);
			bitWrite(holdingRegs[ROBOT_CDE_COILS], VM,   0);
			bitWrite(holdingRegs[ROBOT_CDE_COILS], RM,   0);
			isSleeping=true;
			Serial.println("\033[32mCLEANING SLEEP\033[37m");
			curGrafcet=NO_GRAFCET;
			break;
		case CLEANING_START:
			switch(curState){
				case WETTING:
					if(curTime-startTime>WETTING_TIME){
						Serial.println("\033[31mnew STEP : CATCH ALL\033[37m");
						catchAll();
						startTime=curTime;
						curState=CATCH_ALL;
					}
					break;
				case CATCH_ALL:
					byte nbCatched=0;
					#ifndef VIRTUAL_IHM_BUTTON
						for(int i=NB_VENTOUSES-1;i>-1;i--){
							if(	holdingRegs[JACK_A_CUR_GRAFCET+i*JACK_RS485_HOLDING_REGS_SIZE]==DO_CATCH && 
								holdingRegs[JACK_A_CUR_STATE  +i*JACK_RS485_HOLDING_REGS_SIZE]==MONITORING){
								nbCatched++;
							}
						}
					#else
						startTime=curTime+3000;
						nbCatched=NB_VENTOUSES;
					#endif
					if(nbCatched>=1 && curTime-startTime>2000){ //Au moins 2 catch en moins de 2s
						Serial.println("\033[31mNew step : ROLL DOWN\033[37m");
						curState=ROLL_DOWN;
						isRollDown=false;
						startTime=curTime;
					}
					if(curTime-startTime>GRAFCET_TIMEOUT){
						Serial.println("***gracet timout L972 Hoist : le catch all n'a pas marché dans les temps"); 
						errorCode=ERR_GRAFCET_TIMEOUT;
						updateMode(MODE_MANUAL);
					}
					break;
				case ROLL_DOWN:
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRJ, 0);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVRO, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], VM,   1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], RM,   1);
					isSleeping=false;
					isMoveBlocked=false;
					Serial.println("\033[31mCLEANING READY\033[37m");
					//digitalWrite(PIN_NIR_LED,HIGH);
					curGrafcet=NO_GRAFCET;

					break;
				default:
					IFTRACE_SERIAL(Serial.println("\033[31mSTART CATCH ALL\033[37m"));
					isSWP=true;
					bitWrite(holdingRegs[ROBOT_CDE_COILS], SWP,   1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSH, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSM, 1);
					bitWrite(holdingRegs[ROBOT_CDE_COILS], XVSL, 1);

					for(int i=NB_VENTOUSES-1;i>-1;i--){
						bitWrite(holdingRegs[JACK_A_CDE_COILS+i*JACK_RS485_HOLDING_REGS_SIZE],AUTORECATCH_MODE,1);
					}

					startTime=curTime;
					curState=WETTING;
					Serial.println("\033[31mnew STEP : WETTING\033[37m");
					startTime=curTime;
					break;
			}
			break;
		default:
			curGrafcet=NO_GRAFCET;
			break;
	}
//*/
	watchDog();


/******************************************************************************************************* */
/**                                                TRAITEMENT DE LA PILE                                 */
/******************************************************************************************************* */
/*
	if (oInterpreter->oRS485->isNewRcvMsg()){
		newOrder=false;
		bool isMsgComputed=true;
		isPongTime[(int)(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?
									ROBOT_ID:
									oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START)]=curTime;
		switch (oInterpreter->oRS485->curRcvMsg->cdeC){
			case 'G':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case CDE_REPING:
						break;					
					case CDE_PING:
						Serial.print("Received a PING from ");
						Serial.println(oInterpreter->oRS485->curRcvMsg->fromAdr);
						oInterpreter->oRS485->addMsg(oInterpreter->oRS485->curRcvMsg->fromAdr,'G',CDE_PONG,VERSION_GCODE);
						pingStartTime=curTime;
					case CDE_PONG:
						if(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'){
//							isPongTime[ROBOT_ID]=curTime;
							isSWP=true;
							oInterpreter->oRS485->addMsg('R','G',CDE_SWP,1);
						}
						else {
//							isPongTime[oInterpreter->oRS485->curRcvMsg->fromAdr-65]=curTime;
							//Serial.print("Pong[");Serial.print((int)(oInterpreter->oRS485->curRcvMsg->fromAdr-65));
							//Serial.print("]=");Serial.println(isPongTime[oInterpreter->oRS485->curRcvMsg->fromAdr-65]);
						}
						isPongTime[(int)(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?
									ROBOT_ID:
									oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START)]=curTime;

						if(oInterpreter->oRS485->curRcvMsg->param!=VERSION_GCODE){
							Serial.print("BAD GCODE RELEASE :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);
							Serial.print(" from \033[31m");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);
							Serial.print("\033[37m expected :");Serial.println(VERSION_GCODE);
						}
						break;
					case CDE_FREE:
						//Serial.print("FREE (");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						if(oInterpreter->oRS485->curRcvMsg->param==1){
							//Serial.println(") SUCCESS !");
							isFree[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START ]=true;
						}
						else {
							//Serial.println(") FAILURE !");	
						}
						#ifdef MESURE_TEMPS
							Serial.print("Temps Free ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.print(curTime-mesureStartTime);Serial.println("ms");
						#endif
						isCatched[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=false;
						break;
					case CDE_CATCH:
						//Serial.print("CATCH (");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						if(oInterpreter->oRS485->curRcvMsg->param==1){
							//Serial.println(") SUCCESS !");	
							isCatched[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=true;
						}
						else {
							//Serial.println(") FAILURE !");	
						}
						isFree[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=false;
						#ifdef MESURE_TEMPS
							Serial.print("Temps Catch ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.print(curTime-mesureStartTime);Serial.println("ms");
						#endif
						break;
					case CDE_INFO:
						#ifdef MESURE_TEMPS
							Serial.print("Info ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.println(oInterpreter->oRS485->curRcvMsg->param);
						#endif
						break;
					case CDE_HSRC:
					case CDE_HSRM:
					case CDE_HSRU:
					case CDE_HSRD:
						isRobotBtnPressed[oInterpreter->oRS485->curRcvMsg->cdeN-CDE_HSRC]=(bool)oInterpreter->oRS485->curRcvMsg->param;
						//Si j'ai recu arret HSRU ou arret HSRD => Stop from Robot (il n'y a pas de hold sur le robot)
						isRobotAskedStop=   ((oInterpreter->oRS485->curRcvMsg->cdeN==CDE_HSRU) && !isRobotBtnPressed[HSMU])
										 || ((oInterpreter->oRS485->curRcvMsg->cdeN==CDE_HSRD) && !isRobotBtnPressed[HSMD]);
						break;
					case CDE_HSRP:
						if(!oInterpreter->oRS485->curRcvMsg->param){
							Serial.println("Pos 0 Set by Robot");
							curPosIndex=Z_INIT_PT;
							resetTraverses();
						}
						break;
					case CDE_FRAME_DETECT:
						Serial.print("Frame(");
						Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						Serial.print(")=");
						Serial.println(oInterpreter->oRS485->curRcvMsg->param);
						break;
			        case CDE_FORCE_ZSH_STATE:
						forceIsZSH=oInterpreter->oRS485->curRcvMsg->param;
			            Serial.print("forceIsZSH=");Serial.println(forceIsZSH);
			        	break;
			        case CDE_FORCE_MOVE_UP:
			        case CDE_FORCE_MOVE_DOWN:
			        case CDE_FORCE_MOVE_STOP:
			        	tempDir=oInterpreter->oRS485->curRcvMsg->cdeN-40;
			        	Serial.print("force Dir=");Serial.println(A_NAME_MOVE[tempDir]);
			        	break;
			        case CDE_FORCE_CLEANING:
			        case CDE_FORCE_MANUAL:
			        	tempMode=oInterpreter->oRS485->curRcvMsg->cdeN-50;
			        	Serial.print("force Mode=");Serial.println(A_NAME_MODE[tempMode]);
			            break;

					default:
						IFTRACE_SERIAL(Serial.println("Received G Msg not computed."));
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					break;
				}
				break;
			case 'V':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case GET_PTP:
						Serial.print("PTP=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_PTV:
						Serial.print("PTV=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_ZSLR:
						isRollDown=(bool)oInterpreter->oRS485->curRcvMsg->param;
						break;
					case GET_ZSL:
						Serial.print("ZSL(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_ZSH:
						Serial.print("ZSH(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_FILL_UP_TIME:
						Serial.print("FillUpTime(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_MOVE_DOWN_DEEP_TIME:
						Serial.print("Move Down Deep Time(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_MOVE_DOWN_SOFT_TIME:
						Serial.print("Move Soft Deep Time(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_VACCUM_DURATION:
						Serial.print("Vaccum duration(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_TIMEOUT_JACK_ZSH:
						Serial.print("Timeout Jack ZSH(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_PRESSURE:
						Serial.print("P(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					default:
						Serial.print("Received Msg not computed : V");
						Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
						Serial.print(" P");
						Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					break;
				}
				break;
			case 'A':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case ALERT_BUMP_DOWN:
						Serial.print("Bump Down :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);Serial.print("\t");
						printIndex(FORCE_DISPLAY);
						isBumpDown=oInterpreter->oRS485->curRcvMsg->param;
						break;
					case ALERT_BUMP_UP:
						Serial.print("Bump Up :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);Serial.print("\t");
						printIndex(FORCE_DISPLAY);
						isBumpUp=oInterpreter->oRS485->curRcvMsg->param;
						break;
					case ALERT_US_DOWN:
						US_DownAlertTime=oInterpreter->oRS485->curRcvMsg->param;
						US_Alert_Down=true;
						break;
					case ALERT_US_UP:
						US_UpAlertTime=oInterpreter->oRS485->curRcvMsg->param;
						US_Alert_Up=true;
						break;
					default:
						Serial.print("Received Msg not computed : A");
						Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
						Serial.print(" P");
						Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					}
				break;
			case 'E':
				//if(oInterpreter->oRS485->curRcvMsg->cdeN!=ERR_PTV_SH_ERROR){
					Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(" : Error ");Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
					Serial.print(" ");
					Serial.print(A_MSG_ERROR[oInterpreter->oRS485->curRcvMsg->cdeN]);
					Serial.println(oInterpreter->oRS485->curRcvMsg->param);
				//}
				if (//oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTP_SL_ERROR || 
					oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTV_SH_ERROR){
					digitalWrite(PIN_HSER_LED,HIGH);	
				}
				
				if (oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTP_SL_ERROR){
					digitalWrite(PIN_ZIH_LED,HIGH);	
				}

				break;
			default:
				IFTRACE_SERIAL(Serial.println("Received Msg not computed."));
				oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
				isMsgComputed=false;
				break;
		}
		oInterpreter->oRS485->moveNextRcvMsg();	
	}
	//*/
	


}

void Hoist::printIndex(bool _force, int _a, int _b){
    if((oHoist->curPosIndex%RESOL_Z)==0 || _force){//Affiche la position tous les RESOL_Z points
	   	/*if(_a<2 && _b<2){
		   	Serial.print(_a);Serial.print(_b);
	   	}*/
		Serial.print(curPosIndex);	
#ifdef INDEX_IN_MM
		Serial.print("\t");Serial.print(MM_PAR_POINT*curPosIndex);Serial.print("mm");
#endif
		Serial.println();
    }
}

void Hoist::displayParams(){
	Serial.print("\n-----------------------------------------------\nETAT DU TREUIL ");
	Serial.print("version=\t\t");Serial.println(VERSION_TEXT);
	Serial.print("version GCode=\t\t");Serial.println(VERSION_GCODE);
	Serial.print("curLoad=\t\t");Serial.println(curLoad);
	Serial.print("maxLoad=\t\t");Serial.println(maxLoad);
	Serial.print("minLoad=\t\t");Serial.println(minLoad);
	Serial.print("isUnderLoad=\t\t");Serial.println(isUnderLoad);
	Serial.print("isOverLoad=\t\t");Serial.println(isOverLoad);
	Serial.print("isSWP=\t\t\t");Serial.println(isSWP);
	Serial.print("isHold=\t\t\t");Serial.println(isHold);
	Serial.print("isSleeping=\t\t");Serial.println(isSleeping);
	Serial.print("isRollDown=\t\t");Serial.println(isRollDown);
	Serial.print("isRegenerating=\t\t");Serial.println(isRegenerating);
	Serial.print("curPosIndex=\t\t");printIndex(FORCE_DISPLAY);
	#ifdef VIRTUAL_ZSH
		Serial.print("vZSH=\t\t\t");Serial.println(forceIsZSH);
	 #else
		Serial.print("ZSH=\t\t\t");Serial.println(!digitalRead(PIN_ZSH));
	#endif		
	Serial.print("curTime=\t\t");Serial.println(curTime);
	Serial.print("prevTime=\t\t");Serial.println(prevTime);
	Serial.print("startTime=\t\t");Serial.println(startTime);
	Serial.print("cycleTime=\t\t");Serial.println(cycleTime);
	Serial.print("mode=\t\t\t");Serial.println(A_NAME_MODE[mode]);
	Serial.print("dir=\t\t\t");Serial.println(A_NAME_MOVE[dir]);
	Serial.print("curGrafcet=\t\t");Serial.println(A_GRAFCET_NAME[curGrafcet]);
	Serial.print("curState=\t\t");Serial.println(curState);

	Serial.println("Name\tBtn\tLed\tRobot btn");
	for(int i=0;i<NB_ROBOT_BTN;i++){
		Serial.print(A_NAME_IHM[i]);Serial.print("\t");Serial.print(digitalRead(Btn_Pin[i]));Serial.print("\t");Serial.print(Led_State[i]);
		if(i<NB_ROBOT_BTN){
			Serial.print("\t");Serial.println(bitRead(holdingRegs[ROBOT_CUR_BTN],i));
		}
		else {
			Serial.println();
		}
	}
	
	Serial.println("Jack\tCatched\tFreed\tisPAtm");
	for(int i=0;i<NB_VENTOUSES;i++){
		Serial.print(char(65+i));
		Serial.print("\t");Serial.print(  holdingRegs[JACK_A_CUR_GRAFCET+i*JACK_RS485_HOLDING_REGS_SIZE]==DO_CATCH && 
										  holdingRegs[JACK_A_CUR_STATE  +i*JACK_RS485_HOLDING_REGS_SIZE]==MONITORING); //Catched
		Serial.print("\t");Serial.print(  holdingRegs[JACK_A_CUR_GRAFCET+i*JACK_RS485_HOLDING_REGS_SIZE]==DO_FREE &&  //Freed
										  (holdingRegs[JACK_A_CUR_STATE  +i*JACK_RS485_HOLDING_REGS_SIZE]==GRAFCET_END ||
										   holdingRegs[JACK_A_CUR_STATE  +i*JACK_RS485_HOLDING_REGS_SIZE]==TIME_OUT_ZSH));
		Serial.print("\t");Serial.println(holdingRegs[JACK_A_CUR_PATM   +i*JACK_RS485_HOLDING_REGS_SIZE]);
	}

	Serial.print("Error Led \t");Serial.println(Led_State[NB_LED-1]);

	Serial.println("* = MAJ constamment\t\tA \tB \tC \tD");
	
	Serial.print("*JACK_CUR_COILS=\t\t");		Serial.print(holdingRegs[JACK_A_CUR_COILS]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CUR_COILS]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CUR_COILS]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CUR_COILS]);
	Serial.print("*JACK_CDE_COILS=\t\t");		Serial.print(holdingRegs[JACK_A_CDE_COILS]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CDE_COILS]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CDE_COILS]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CDE_COILS]);
	Serial.print("*JACK_CUR_GRAFCET=\t\t");		Serial.print(holdingRegs[JACK_A_CUR_GRAFCET]);Serial.print("\t");		Serial.print(holdingRegs[JACK_B_CUR_GRAFCET]);Serial.print("\t");		Serial.print(holdingRegs[JACK_C_CUR_GRAFCET]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CUR_GRAFCET]);
	Serial.print("*JACK_CUR_STATE=\t\t");		Serial.print(holdingRegs[JACK_A_CUR_STATE]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CUR_STATE]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CUR_STATE]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CUR_STATE]);
	Serial.print("*JACK_CDE_GRAFCET=\t\t");		Serial.print(holdingRegs[JACK_A_CDE_GRAFCET]);Serial.print("\t");		Serial.print(holdingRegs[JACK_B_CDE_GRAFCET]);Serial.print("\t");		Serial.print(holdingRegs[JACK_C_CDE_GRAFCET]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CDE_GRAFCET]);
	Serial.print("JACK_CUR_BOARD_REL.=\t\t");	Serial.print(holdingRegs[JACK_A_CUR_BOARD_RELEASE]);Serial.print("\t");	Serial.print(holdingRegs[JACK_B_CUR_BOARD_RELEASE]);Serial.print("\t");	Serial.print(holdingRegs[JACK_C_CUR_BOARD_RELEASE]);Serial.print("\t");	Serial.println(holdingRegs[JACK_D_CUR_BOARD_RELEASE]);
	Serial.print("JACK_CUR_CHRONO=\t\t");		Serial.print(holdingRegs[JACK_A_CUR_CHRONO]);Serial.print("\t");		Serial.print(holdingRegs[JACK_B_CUR_CHRONO]);Serial.print("\t");		Serial.print(holdingRegs[JACK_C_CUR_CHRONO]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CUR_CHRONO]);
	Serial.print("JACK_CUR_PATM=\t\t\t");		Serial.print(holdingRegs[JACK_A_CUR_PATM]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CUR_PATM]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CUR_PATM]);Serial.print("\t");			Serial.println(holdingRegs[JACK_D_CUR_PATM]);
	Serial.print("JACK_CUR_PSL=\t\t\t");		Serial.print(holdingRegs[JACK_A_CUR_PSL]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CUR_PSL]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CUR_PSL]);Serial.print("\t");				Serial.println(holdingRegs[JACK_D_CUR_PSL]);
	Serial.print("JACK_CUR_PSH=\t\t\t");		Serial.print(holdingRegs[JACK_A_CUR_PSH]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CUR_PSH]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CUR_PSH]);Serial.print("\t");				Serial.println(holdingRegs[JACK_D_CUR_PSH]);
	Serial.print("JACK_CDE_PSL=\t\t\t");		Serial.print(holdingRegs[JACK_A_CDE_PSL]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CDE_PSL]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CDE_PSL]);Serial.print("\t");				Serial.println(holdingRegs[JACK_D_CDE_PSL]);
	Serial.print("JACK_CDE_PSH=\t\t\t");		Serial.print(holdingRegs[JACK_A_CDE_PSH]);Serial.print("\t");			Serial.print(holdingRegs[JACK_B_CDE_PSH]);Serial.print("\t");			Serial.print(holdingRegs[JACK_C_CDE_PSH]);Serial.print("\t");				Serial.println(holdingRegs[JACK_D_CDE_PSH]);
	
	Serial.println();
	Serial.print("*ROBOT_CUR_COILS=\t\t");Serial.println(holdingRegs[ROBOT_CUR_COILS]);
	Serial.print("*ROBOT_CDE_COILS*=\t\t");Serial.println(holdingRegs[ROBOT_CDE_COILS]);
	Serial.print("*ROBOT_CUR_BTN=\t\t\t");Serial.println(holdingRegs[ROBOT_CUR_BTN]);
	Serial.print("ROBOT_CUR_PTP=\t\t\t");Serial.println(holdingRegs[ROBOT_CUR_PTP]);
	Serial.print("ROBOT_CUR_PTV=\t\t\t");Serial.println(holdingRegs[ROBOT_CUR_PTV]);
	Serial.print("ROBOT_CUR_XVRO_PERIOD=\t\t");Serial.print(holdingRegs[ROBOT_CUR_XVRO_PERIOD]);
	Serial.print("\tROBOT_CUR_XVRO_DURATION=\t");Serial.println(holdingRegs[ROBOT_CUR_XVRO_DURATION]);
	Serial.print("ROBOT_CDE_XVRO_PERIOD*=\t\t");Serial.print(holdingRegs[ROBOT_CDE_XVRO_PERIOD]);
	Serial.print("\tROBOT_CDE_XVRO_DURATION*=\t");Serial.println(holdingRegs[ROBOT_CDE_XVRO_DURATION]);
	Serial.print("\nROBOT_CUR_XVSX_PERIOD=\t\t");Serial.print(holdingRegs[ROBOT_CUR_XVSX_PERIOD]);
	Serial.print("\tROBOT_CUR_XVSX_DURATION=\t");Serial.println(holdingRegs[ROBOT_CUR_XVSX_DURATION]);
	Serial.print("ROBOT_CDE_XVSX_PERIOD*=\t\t");Serial.print(holdingRegs[ROBOT_CDE_XVSX_PERIOD]);
	Serial.print("\tROBOT_CDE_XVSX_DURATION*=\t");Serial.println(holdingRegs[ROBOT_CDE_XVSX_DURATION]);
	Serial.print("ROBOT_CUR_ROLL_SPEED=\t\t");Serial.println(holdingRegs[ROBOT_CUR_ROLL_SPEED]);
	Serial.print("ROBOT_CDE_ROLL_SPEED*=\t\t");Serial.println(holdingRegs[ROBOT_CDE_ROLL_SPEED]);
	Serial.print("ROBOT_CUR_US_ECHO=\t\t");Serial.println(holdingRegs[ROBOT_CUR_US_ECHO]);
	Serial.print("ROBOT_CDE_BUZZER*=\t\t");Serial.println(holdingRegs[ROBOT_CDE_BUZZER]);
	Serial.print("ROBOT_CUR_BOARD_RELEASE=\t");Serial.println(holdingRegs[ROBOT_CUR_BOARD_RELEASE]);
//*/
}
