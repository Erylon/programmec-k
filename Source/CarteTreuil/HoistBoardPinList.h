#define VERSION_TEXT			"TREUIL 1.8 MODBUS"
#define VERSION_NUM 			18



//Constantes
#define	D_POULIE				70 			// En mm
#define D_CABLE					4			// En mm
#define NB_PAS  				360			//Pour le codeur
#define MM_PAR_POINT			0.3223//89 	// ((D_POULIE+D_CABLE)*3.141592653589794/2*NB_PAS)
#define RESOL_Z					25			//affiche le Z tous les RESOL_Z points
#define NO_MIN_LOAD				-200.		//valeur de la charge min 
#ifdef TEST_BUT_CANT_LOAD_ON_HOOK
	#define MIN_LOAD			NO_MIN_LOAD
	#define MAX_LOAD_FREE_MOVE	20000. //150.			//valeur de la charge max sur treuil sans robot /!\ valeur
	#define MAX_LOAD			20000.		//valeur de la charge max /!\ valeur
#else
	#define MIN_LOAD			500.		//valeur de la charge min 
	#define MAX_LOAD_FREE_MOVE	200. //150.			//valeur de la charge max sur treuil sans robot /!\ valeur
	#define MAX_LOAD			1000.		//valeur de la charge max /!\ valeur
#endif
#define VIRTUAL_LOAD			600.		//valeur pour tests
#define WATCHDOG_PULSE			1000			//ms
//#define TIMEOUT_MOVEMENT		10000		//temps entre deux tours de roues codeuses dépassé donc arret

#define MODE_INIT				0
#define MODE_NO_LOAD			1
#define MODE_MANUAL				2
#define MODE_CLEANING			3
#define MODE_CLEANING_SLEEP		4
#define MODE_BEFORE_INIT		5
#define T_MODE_NAME 			"Init","No load","Manual","Cleaning","Sleep"

#define MOVE_STOP				0
#define MOVE_UP					1
#define MOVE_DOWN				2
#define T_MOVE_NAME 			"STOP","Up","Down"
#define T_INC_TRAV 				0,1,-1 //[MOVE_UP]=>1 [MOVE_DOWN]=> -1

#define HOLD_DURATION			3000 //ms
#define SLEEP_TIME				10000 //ms //15s
#define LOAD_FILTER_TIME		200 //time until over or underload is confirmed
//VITESSE
#define FORCE_DISPLAY			true
#define SPEED_PERIOD 			250
#define SPEED_DISPLAY_PERIOD 	1000
//BUZZ
#define BUZZ_US_ALERT_DURATION 	200		//ms
#define BUZZ_INIT_DURATION 		500			//ms
#define BUZZ_ALERT_HOLD		 	500		//ms
#define BUZZ_ERROR_DURATION 	4000		//ms

//BUZZER SOUND FREQUENCY
#define NO_BUZZ_FREQ			0
#define BUZZ_INIT_FREQ			2000 //2000
#define BUZZ_END_FREQ			3000
#define BUZZ_ERROR_FREQ			1000
#define BUZZ_UNDERLOAD_FREQ		1000
#define BUZZ_OVERLOAD_FREQ		700
#define BUZZ_HOLD_FREQ			3000
#define BUZZ_US_ALERT_FREQ 		5000

//tableau des frequences buzzer : doit partir de 0, incrémental sans interruption
#define NO_BUZZ					0
#define BUZZ_INIT				1
#define BUZZ_END				2
#define BUZZ_ERROR				3
#define BUZZ_UNDERLOAD 			4
#define BUZZ_OVERLOAD			5
#define BUZZ_HOLD 				6
#define BUZZ_US_ALERT 			7

#define T_BUZZ_FREQUENCY 		NO_BUZZ_FREQ,BUZZ_INIT_FREQ,BUZZ_END_FREQ,BUZZ_ERROR_FREQ,BUZZ_UNDERLOAD_FREQ,BUZZ_OVERLOAD_FREQ,BUZZ_HOLD_FREQ,BUZZ_US_ALERT_FREQ

//Adresse des pins 
#define PIN_VOK     			2     // 
#ifdef VIRTUAL_ZSH
	#define PIN_ZSH     		PIN_HSER_BTN    // 
#else
	#define PIN_ZSH     		22    // 
#endif
#define PIN_INDEX   			23    // 
#define PIN_WAL     			24    // 
#define PIN_VOIE_A  			18    // Terence a mis la voie A sur la pin 25 qui ne peut etre attachée a une interruption
//La pin 18 etant non utilisée et compatible interuption, il faut faire un pontage 18-25
#define PIN_VOIE_AA  			25    // doit etre declaré car la carte electronique a un pontage vers pin voie A
#define PIN_HSRST   			26    // 
#define PIN_VOIE_B  			27    // 
#define PIN_HSMU_BTN			30    // Move UP
#define PIN_HSMD_BTN			32    // Move Down
#define PIN_HSS_BTN 			34    // Stop
#define PIN_HSCL_BTN			36    // Cleaning
#define PIN_HSMA_BTN			38    // Manual
#define PIN_HSHB_BTN			40    // 
#define PIN_HSNL_BTN			44    // No Load
#define PIN_HSHA_BTN			46    // 
#define PIN_HSER_BTN			8     // Error
#define T_BTN 					PIN_HSCL_BTN,PIN_HSMA_BTN,PIN_HSMU_BTN,PIN_HSMD_BTN,PIN_HSS_BTN,PIN_HSNL_BTN,PIN_HSHA_BTN,PIN_HSHB_BTN,PIN_HSER_BTN,PIN_HSRST
#define NB_HOIST_BTN			10
#define NB_ROBOT_BTN			5
//Garder les premiers boutons communs entre robot et treuil :
#define HSCL					0		// Cleaning
#define HSMA					1		// Manual
#define HSMU					2		// Move Up
#define HSMD					3		// move Down
#define HSS						4		// stop
#define HSNL					5		// no load
#define HSHA					6
#define HSHB					7
#define HSER					8
#define HSRST 					9

//Pour le tableau des noms voir les leds

// Configuration des broches de sortie
#define PIN_HSHB_LED			3     // 
#define PIN_HSHA_LED			4     // 
#define PIN_HSNL_LED			5     // No load
#define PIN_HSMA_LED			6     // Manual
#define PIN_HSMU_LED			7     // Up
#define PIN_HSMD_LED			42    // Down
#define PIN_HSS_LED 			48    // Stop
#define PIN_HSCL_LED			50    // Clean
#define PIN_HSER_LED			52    // Err
#define PIN_ZIH_LED				29    // Err ZSH
#define PIN_WIH_LED				31    // Err overload
#define PIN_WIL_LED				33    // Err underload
#define PIN_NIR_LED				35    // Ready

//n epas changer l'ordre des led dans le tableau : cela doit etre le mm que celui des boutons
#define T_LED 					PIN_HSCL_LED,PIN_HSMA_LED,PIN_HSMU_LED,PIN_HSMD_LED,PIN_HSS_LED,PIN_HSNL_LED,PIN_HSHA_LED,PIN_HSHB_LED,PIN_HSER_LED,PIN_ZIH_LED,PIN_WIH_LED,PIN_WIL_LED,PIN_NIR_LED
#define NB_LED					13
#define T_IHM_NAME				"HSCL","HSMA","HSMU","HSMD","HSS","HSNL","HSHA","HSHB","HSER","ZIH","WIH","WIL","NIR"

#define PIN_BUZZER  			12    // 
#define PIN_BUZZER2  			4    // 
#define PIN_HSVC    			28    // 
#define PIN_SCL     			37    //Low Speed
#define PIN_SCH     			39    //Move Ack 
#define PIN_ZCDN    			43    //Move Down 
#define PIN_ZCUP    			41    //Move Up
#define PIN_LED     			45    // 


#define PIN_RS485_DE 			53
#define PIN_RS485_RE 			53

//GRAFCET
#define NO_GRAFCET				0
#define CLEANING_START			1
#define CLEANING_STOP			2
#define CLEANING_HALT			3 //Passage en mode manuel : remonté de la tete
#define CLEANING_UP				4
#define CLEANING_DOWN			5
#define CLEANING_SLEEP			6

#define T_GRAFCET_NAME			"NO G.","CL START","CL STOP","CL HALT","CL UP","CL DOWN","CL SLEEP","MANUAL"

//ETAPE
#define INITIALIAZING			0
#define PING_ALL				1
#define CATCH_ALL				2
#define ROLL_DOWN				3
#define REDUCE_WATER			4
#define WETTING 				5
#define FREE_NEXT				6
#define T_STATE_NAME			"INIT","PING ALL","CATCH ALL","ROLL DOWN","REDUCE W","AUTOCATCH ALL","WETTING","FREE NEXT"



#define ROBOT_ID				5 //index du tableau isPing

#define PONG_WAITING_TIME		150 //ms expecting Pong form ping
//#define GET_WAITING_TIME		200 //ms 
#define GRAFCET_TIMEOUT			3000
#define REDUCE_WATER_DELAY		100 //ms expecting Pong form ping
#define FREE_WAITING_TIME		2000 //ms expecting free 3000
#define WETTING_TIME			2000 // Attention depend du temps de pompage

//Jauge
/*
#define GAUGE_READ_A	
#define GAUGE_WEIGHT_A
#define GAUGE_READ_B	
#define GAUGE_WEIGHT_B
#define SCALE_A 				((GAUGE_WEIGHT_B-GAUGE_WEIGHT_A)/(GAUGE_READ_B-GAUGE_READ_A))
#define SCALE_B					(GAUGE_WEIGHT_B-GAUGE_READ_B*SCALE_A)
*/
#define SCALE_MULTIPLIER 		17391.//4560.//2280.//57740.	//Facteur de conversion de la Jauge
#define SCALE_TARE 				0. //52
#define SCALE_A 				0.00010765 //0.0001184 //curLoad=SCALE_A*tempRead+SCALE_B
#define SCALE_B					-109.03//-5.15//-74.611
#define PIN_DATA_GAUGE			20			//I²C
#define	PIN_CLOCK_GAUGE			21			//I²C

/*
output		20mv/V
gain		128	
adc range	1024	
excitation	5V
cell range	45,4kg
Vcc 		5V
		
scale	57740,9691629956	

scale = (output * gain * adc_range * excitation)/(cell_range * vcc)
where:
output = 	the output rating of the load cell (mV/V). Should be in volts, if your final result looks wrong see if it is off by some factor of 10 to see if you forgot to account for the unit changes.
gain = 		the gain setting of the amplifier
adc_range = the range of values that the adc has (precision). For this 24 bit adc this is (2^24)-1= 16777215
excitation =the input voltage for the load cell. For Sparkfun's breakout board this is equal to vcc and the two values cancel out.
cell_range =the max rating for the load cell in whichever units you are using. I was using a load cell rated for 50KG, so to get an output in kilograms I would use the value 50 here.
vcc = 		the system voltage for the amplifier


//*/
/*
#define REPERE_VISU				+100 // par rapport à repere_absolu
#define REPERE_ABSOLU			0
#define REPERE_SUCK_A			290 //MM 
//prendre la valeur en mm et diviser par MM_par_point
#define BUMP_DOWN_INDEX			REPERE_ABSOLU
#define BUMP_UP_INDEX			(long)((REPERE_ABSOLU+REPERE_SUCK_A+600+110+260)/MM_PAR_POINT)
#define SUCK_A_BOTTOM_INDEX		(long)(((REPERE_SUCK_A))/MM_PAR_POINT)//361//position en point du bas de A par rapport a l'US bas//98 POUR VENTOUSE Ø75MM
#define SUCK_B_BOTTOM_INDEX 	(long)(((REPERE_SUCK_A)+150)/MM_PAR_POINT)//1512//
#define SUCK_C_BOTTOM_INDEX		(long)(((REPERE_SUCK_A)+300)/MM_PAR_POINT)//2105//
#define SUCK_D_BOTTOM_INDEX		(long)(((REPERE_SUCK_A)+450)/MM_PAR_POINT)//2105//
#define SUCK_E_BOTTOM_INDEX		(long)(((REPERE_SUCK_A)+600)/MM_PAR_POINT)//2105//

#define T_SUCK_BOTTOM_POS		SUCK_A_BOTTOM_INDEX,SUCK_B_BOTTOM_INDEX,SUCK_C_BOTTOM_INDEX,SUCK_D_BOTTOM_INDEX,SUCK_E_BOTTOM_INDEX
#define SUCK_SIZE				(long)(110/MM_PAR_POINT)//Grosse:110 Petite:75
#define TRAV_MARGIN 			5//5 //	en point      5=2mm
/*
#define TRAV_MARGIN_FREE_UP		250 //150 //	en point  	100=30mm
#define TRAV_MARGIN_FREE_DOWN	300 //150 //	en point  	100=30mm
#define TRAV_MARGIN_CATCH		1 //10 // 	en point 	 10=3mm
#define TRAV_MARGIN_STOP  		1
//*/
/*
#define TRAV_MARGIN_FREE_UP		200 //434 //((long)(140/MM_PAR_POINT)) //en mm 0.7s*12000mm/min/(60s/min)
#define TRAV_MARGIN_FREE_DOWN	200 //465 //((long)(150/MM_PAR_POINT)) //13m/min  	
#define TRAV_MARGIN_CATCH		1 //800 //10 // 		en ms 	
#define TRAV_MARGIN_STOP  		10 //pt

//Index du tableau des traverses
#define TRAV_SET				0
#define TRAV_BAS				1
#define TRAV_HAUT				2
#define TRAV_KNOWN				3

//valeur de la colonne 0 [TRAV_SET]
#define NOT_SET					0 //Ne pas changer cette valeur à cause de la fonction motif
#define TRAV_SET_BAS			1
#define TRAV_SET_HAUT			2
#define TRAV_SET_FULL			3

#define NB_TRAV_COL				3 	//for display purposes see downpage for other defines
#define NB_MEM_TRAVERSE			4
#define MIN_TRAVERSE_THICKNESS 	5 // en pt=2mm
#define MIN_TRAVERSE_SPACE		500 // en pt=200mm
#define MAX_ERROR_POS_TRAVERSE	10

#define T_TRAV					"NOT SET","BAS","HAUT"
#define T_BUMP_DOWN_TRAV		TRAV_SET_BAS,TRAV_SET_HAUT
#define T_BUMP_UP_TRAV			TRAV_SET_HAUT,TRAV_SET_BAS


//Pas de traverses pré définies
#define T_INIT_TRAVERSE         {NOT_SET,0,0},\
#define T_INIT_TRAVERSE_NB		0
//*/
/*
//MEGEI
//									//set, Z, H en mm  , known=0. Les Zbas, Zhaut sont ensuite recalculés et convertis en points
//tableau des traverses mémorisées doit commencer et finir par NOT_SET (rajouté automatiquement à la fin)
#define OFFSET_TRAVERSE			0
#define T_INIT_TRAVERSE         {TRAV_SET_FULL,-1000000,-1000000,0},\
								{TRAV_SET_FULL,1445,           		110,0},\
								{TRAV_SET_FULL,(1445+110+1290),		110,0},\
								{TRAV_SET_FULL,(1445+110+1290+110+341),	110,0},\
								{TRAV_SET_FULL,1000000,1000000,0}
								//tableau des traverses mémorisées doit se finir par NOT_SET rajouté automatiquement a la fin
#define T_INIT_TRAVERSE_NB		3 //ne pas compter les NOT_SET du debut ni de la fin
#define NB_MOTIF 				9 //Nb de fois en tout ou T_INIT_TRAVERSE est présent sur la facade


#define T_TRAVERSE_MAX 			50 //nb maximum de traverses dans le tableau 

#define NB_TRAV_RAW				((int)((T_INIT_TRAVERSE_NB*NB_MOTIF)/NB_TRAV_COL)+1)  //for display purposes
#define MAX_TRAVERSE			(NB_TRAV_COL*NB_TRAV_RAW) //25 //taille du tableau

*/

/*
TEST
								{TRAV_SET_FULL,900,     100,0},\
								{TRAV_SET_FULL,(1900),	100,0},\
								{TRAV_SET_FULL,(2900),	100,0},\

/*
CG13
#define OFFSET_TRAVERSE			0
#define T_INIT_TRAVERSE         {TRAV_SET_FULL,-1000000,-1000000,0},\
								{TRAV_SET_FULL,1445,           		110,0},\
								{TRAV_SET_FULL,(1445+110+1290),		110,0},\
								{TRAV_SET_FULL,(1445+110+1290+110+341),	110,0},\
								{TRAV_SET_FULL,1000000,1000000,0}
								//tableau des traverses mémorisées doit se finir par NOT_SET rajouté automatiquement a la fin
#define T_INIT_TRAVERSE_NB		3 //ne pas compter les NOT_SET du debut ni de la fin
#define NB_MOTIF 				9 //Nb de fois en tout ou T_INIT_TRAVERSE est présent sur la facade



MEGEI
#define OFFSET_TRAVERSE			0
#define T_INIT_TRAVERSE         {TRAV_SET_FULL,-1000000,-1000000,0},\
								{TRAV_SET_FULL,690,				26,0},\
								{TRAV_SET_FULL,1406,			84,0},\
								{TRAV_SET_FULL,2300,			56,0},\
								{TRAV_SET_FULL,3176,           	84,0},\
								{TRAV_SET_FULL,1000000,1000000,0}
#define T_INIT_TRAVERSE_NB		4 //ne pas compter les NOT_SET du debut ni de la fin
#define NB_MOTIF 				5 //Nb de fois en tout ou T_INIT_TRAVERSE est présent sur la facade

//*/




