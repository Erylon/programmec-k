#include "D:\ERYLON\ErylonDataCenter - Documents\150.ProductDev\7.Dossier technique\ProgrammeC-K\Demo\Modbus RTU libraries for Arduino\SimpleModbusMaster.cpp"

/*
   The example will use packet1 to read a register from address 0 (the adc ch0 value)
   from the arduino slave (id=1). It will then use this value to adjust the brightness
   of an led on pin 9 using PWM.
   It will then use packet2 to write a register (its own adc ch0 value) to address 1 
   on the arduino slave (id=1) adjusting the brightness of an led on pin 9 using PWM.
*/

//////////////////// Port information ///////////////////
#define baud 19200
#define timeout 1000
#define polling 200 // the scan rate
#define retry_count 10

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 53

// The total amount of available memory on the master to store data
//#define TOTAL_NO_OF_REGISTERS 2

#define ADRESS_SLAVE_1 65
#define ADRESS_SLAVE_2 66

enum //MAster Registers 
{     
  ADC_VAL1,     
  PWM_VAL1,        
  ADC_VAL2,     
  PWM_VAL2,        
  TOTAL_NO_OF_REGISTERS // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

enum //Slave Registers 
{     
  ADC_VAL,     
  PWM_VAL        
};

//#define TOTAL_NO_OF_REGISTERS 2
// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS
// is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

void setup()
{
  // Initialize each packet
  //modbus_construct packet packetid adress fonction adress data local
  modbus_construct(&packets[PACKET1], ADRESS_SLAVE_1, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL1);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
  modbus_construct(&packets[PACKET2], ADRESS_SLAVE_1, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
  modbus_construct(&packets[PACKET3], ADRESS_SLAVE_2, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL2);    //registre 0 de l'esclave, taille 1, offset 2 du maitre
  modbus_construct(&packets[PACKET4], ADRESS_SLAVE_2, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1,PWM_VAL2); //registre 1 de l'esclave, taille 1, offset 3 du maitre

  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial2, baud, SERIAL_8N2, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);
  Serial.begin(115200); 
}

void loop()
{
  modbus_update();
  
  regs[PWM_VAL1] = 111; // update data to be written to arduino slave
  regs[PWM_VAL2] = 112; // update data to be written to arduino slave
  Serial.print("M R");Serial.print(ADRESS_SLAVE_1);Serial.print(" ");Serial.print(regs[ADC_VAL1]);
  Serial.print(" R");Serial.print(ADRESS_SLAVE_2);Serial.print(" ");Serial.print(regs[ADC_VAL2]);
  Serial.print(" W");Serial.print(ADRESS_SLAVE_1);Serial.print(" ");Serial.print(regs[PWM_VAL1]);
  Serial.print(" W");Serial.print(ADRESS_SLAVE_2);Serial.print(" ");Serial.print(regs[PWM_VAL2]);
  Serial.println();
  //analogWrite(LED, regs[0]>>2); // constrain adc value from the arduino slave to 255
}
