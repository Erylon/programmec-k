#include "D:\ERYLON\ErylonDataCenter - Documents\150.ProductDev\7.Dossier technique\ProgrammeC-K\Demo\Modbus RTU libraries for Arduino\SimpleModbusMaster.cpp"

/*
   The example will use packet1 to read a register from address 0 (the adc ch0 value)
   from the arduino slave (id=1). It will then use this value to adjust the brightness
   of an led on pin 9 using PWM.
   It will then use packet2 to write a register (its own adc ch0 value) to address 1 
   on the arduino slave (id=1) adjusting the brightness of an led on pin 9 using PWM.
*/

//////////////////// Port information ///////////////////
#define baud 9600
#define timeout 1000
#define polling 200 // the scan rate
#define retry_count 10

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 53 //7

// The total amount of available memory on the master to store data
//#define TOTAL_NO_OF_REGISTERS 2


enum //Slave registers 
{     
  ADC_VAL,     
  PWM_VAL, 
 // INFO_VAL,        
       
  HOLDING_REGS_SIZE // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS
// is automatically updated.
enum
{
  PACKET1,
  PACKET2,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[HOLDING_REGS_SIZE];

void setup()
{
  // Initialize each packet
  modbus_construct(&packets[PACKET1], 82, READ_HOLDING_REGISTERS, 0, 1, 0);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
  modbus_construct(&packets[PACKET2], 82, PRESET_MULTIPLE_REGISTERS, 1, 1, 1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
  
  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial2, baud, SERIAL_8N2, timeout, polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS, regs);
  Serial.begin(115200); 
  regs[PWM_VAL] = 109;
}

void loop()
{
  modbus_update();
  
  regs[PWM_VAL]++; // update data to be written to arduino slave
  Serial.print(packets[PACKET1].retries);Serial.print("M R");Serial.print(regs[ADC_VAL]);Serial.print(" W");Serial.println(regs[PWM_VAL]);
  //analogWrite(LED, regs[0]>>2); // constrain adc value from the arduino slave to 255
}
