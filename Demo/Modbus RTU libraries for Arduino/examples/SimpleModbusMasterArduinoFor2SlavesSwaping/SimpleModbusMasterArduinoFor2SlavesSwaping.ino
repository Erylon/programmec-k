#include "D:\ERYLON\ErylonDataCenter - Documents\150.ProductDev\7.Dossier technique\ProgrammeC-K\Demo\Modbus RTU libraries for Arduino\SimpleModbusMaster.cpp"

/*
   The example will use packet1 to read a register from address 0 (the adc ch0 value)
   from the arduino slave (id=1). It will then use this value to adjust the brightness
   of an led on pin 9 using PWM.
   It will then use packet2 to write a register (its own adc ch0 value) to address 1
   on the arduino slave (id=1) adjusting the brightness of an led on pin 9 using PWM.
*/

//////////////////// Port information ///////////////////
#define baud 9600
#define timeout 1000
#define polling 200 // the scan rate
#define retry_count 10

// used to toggle the receive/transmit pin on the driver
#define TxEnablePin 53

// The total amount of available memory on the master to store data
//#define TOTAL_NO_OF_REGISTERS 2

unsigned int mode = 1;
unsigned int y;

enum //MAster Registers
{
  ADC_VAL1,
  PWM_VAL1,
  INFO_VAL1,
  ADC_VAL2,
  PWM_VAL2,
  INFO_VAL2,
  TOTAL_NO_OF_REGISTERS // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

enum //Slave Registers
{
  ADC_VAL,
  PWM_VAL,
  INFO_VAL
};

//#define TOTAL_NO_OF_REGISTERS 2
// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS
// is automatically updated.
enum
{
  PACKET1_M1,
  PACKET2_M1,
  PACKET3_M1,
  PACKET4_M1,
  TOTAL_NO_OF_PACKETS_MODE_1 // leave this last entry
};

enum
{
  PACKET1_M2,
  PACKET2_M2,
  TOTAL_NO_OF_PACKETS_MODE_2 // leave this last entry
};

#define MAX_BUF (64)      // What is the longest message Arduino can store?
char kbuffer[MAX_BUF];     // where we store the message until we get a ';'
int sofar;                // how much is in the buffer

// Create an array of Packets to be configured
Packet packetsMode1[TOTAL_NO_OF_PACKETS_MODE_1];
Packet packetsMode2[TOTAL_NO_OF_PACKETS_MODE_2];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
unsigned int param=200;
unsigned long ulTime;

void setupMode(unsigned int _mode) {
  mode=_mode;
  Serial.println("----------------------------------------------------");
  switch (_mode) { //Read ADC register on slave 1
    case 2:
    //  modbus_construct(&packetsMode2[PACKET1_M2], 1, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
    //  modbus_construct(&packetsMode2[PACKET2_M2], 1, READ_HOLDING_REGISTERS, INFO_VAL, 1, INFO_VAL1);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
      modbus_configure(&Serial2, baud, SERIAL_8N2, timeout, polling, retry_count, TxEnablePin, packetsMode2, TOTAL_NO_OF_PACKETS_MODE_2, regs);

      break;
    case 1:
    default :
      // Initialize each packet
      //modbus_construct packet packetid adress fonction adress data local
  //    modbus_construct(&packetsMode1[PACKET1_M1], 1, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL1);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
  //    modbus_construct(&packetsMode1[PACKET2_M1], 1, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
  //    modbus_construct(&packetsMode1[PACKET3_M1], 2, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL2);    //registre 0 de l'esclave, taille 1, offset 2 du maitre
  //    modbus_construct(&packetsMode1[PACKET4_M1], 2, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL2); //registre 1 de l'esclave, taille 1, offset 3 du maitre

      // Initialize the Modbus Finite State Machine
      modbus_configure(&Serial2, baud, SERIAL_8N2, timeout, polling, retry_count, TxEnablePin, packetsMode1, TOTAL_NO_OF_PACKETS_MODE_1, regs);
  }

}

void setup()
{
      modbus_construct(&packetsMode2[PACKET1_M2], 2, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
      modbus_construct(&packetsMode2[PACKET2_M2], 2, READ_HOLDING_REGISTERS, INFO_VAL, 1, INFO_VAL1);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
      modbus_construct(&packetsMode1[PACKET1_M1], 2, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL1);    //registre 0 de l'esclave, taille 1, offset 0 du maitre
      modbus_construct(&packetsMode1[PACKET2_M1], 2, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL1); //registre 1 de l'esclave, taille 1, offset 1 du maitre
      modbus_construct(&packetsMode1[PACKET3_M1], 1, READ_HOLDING_REGISTERS, ADC_VAL, 1, ADC_VAL2);    //registre 0 de l'esclave, taille 1, offset 2 du maitre
      modbus_construct(&packetsMode1[PACKET4_M1], 1, PRESET_MULTIPLE_REGISTERS, PWM_VAL, 1, PWM_VAL2); //registre 1 de l'esclave, taille 1, offset 3 du maitre
//*/
  setupMode(2);
  Serial.begin(115200);
}

char parseLetter(const char *codeArray) {  //returns char founded otherwise 0
  unsigned int chr=0,i;
  while(kbuffer[chr]){                       //scan the buffer
   i=0;
   while(codeArray[i]){                     //scan for a code letter
      if(kbuffer[chr]==codeArray[i]) {
        return codeArray[i];
      }
      i++;
    }
    chr++;    
  }
  return 0;
}

int parseNumber(char code, int val) {
  char *ptr=kbuffer;
  char *firstChar;
  int result;
  while(ptr && *ptr && ptr){
    if(*ptr==code) {
      result=atoi(ptr+1);
      firstChar=ptr+1;
      if (firstChar[0]!='0' && result==0){  //Error: it is not 0 but conversion gives 0 : return old value
        Serial.println("***************************************************************");//Erreur
        return val;
      }
      else
        return result;                      // if something with no error, return value
    }
    ptr=strchr(ptr,' ')+1;
  }
  return val;                               //if nothing return original value
}

void loop()
{
    if( Serial.available() ) { // if something is available
      char c = Serial.read();   // get it
      if(sofar < MAX_BUF) kbuffer[sofar++]=c;
      if(c=='\n') {             // if we got a return character (\n) the message is done.
        kbuffer[sofar]=0;        // strings must end with a \0.
  
        
        if(parseLetter("A")){
          setupMode(1);
        }
        int uu=parseNumber('B',-1);
        if(uu>0){
          setupMode(2);
          param=uu;
        }
        sofar=0;
      }
    }
    
    modbus_update();

    switch(mode){
    case 2:
      regs[PWM_VAL1] = param+1; // update data to be written to arduino slave
      regs[PWM_VAL2] = param+2; // update data to be written to arduino slave
      Serial.print("M2 R1 "); Serial.print(regs[INFO_VAL1]);
      Serial.println();
      break;
    case 1:
    default :
      regs[PWM_VAL1] = 111; // update data to be written to arduino slave
      regs[PWM_VAL2] = 112; // update data to be written to arduino slave
      Serial.print("M1 R1 "); Serial.print(regs[ADC_VAL1]);
      Serial.print(" R2 "); Serial.print(regs[ADC_VAL2]);
      Serial.print(" W1 "); Serial.print(regs[PWM_VAL1]);
      Serial.print(" W2 "); Serial.print(regs[PWM_VAL2]);
      Serial.println();
  }
}
