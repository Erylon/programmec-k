#include "D:\ERYLON\ErylonDataCenter - Documents\150.ProductDev\7.Dossier technique\ProgrammeC-K\Demo\Modbus RTU libraries for Arduino\SimpleModbusSlave.cpp"

#define SOFT_SERIAL
#define PIN_DIP0        2 
#define PIN_DIP1        3 
#define PIN_DIP2        4 

#ifdef SOFT_SERIAL //VERIN
  #include <SoftwareSerial.h>
  SoftwareSerial mySerial(A3,A4,true);//RX TX
  #define PIN_RS485_DE        10// verin / 7 robot
  #define PIN_RS485_RE        5// verin / 8 robot
  unsigned int curAdress=66;
#else     //ROBOT
  #define PIN_RS485_DE        7 // robot
  #define PIN_RS485_RE        8 // robot
  unsigned int curAdress=82;
#endif

#define RS485_TX_ENABLE     PIN_RS485_DE
#define RS485_RX_ENABLE     PIN_RS485_RE

//////////////// registers of your slave ///////////////////
enum 
{     
  // just add or remove registers and your good to go...
  // The first register starts at address 0
  ADC_VAL,     
  PWM_VAL,        
  //INFO_VAL,        
  HOLDING_REGS_SIZE // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};

unsigned int holdingRegs[HOLDING_REGS_SIZE]; // function 3 and 16 register array
////////////////////////////////////////////////////////////
  
void setup()
{
  /* parameters(HardwareSerial* SerialPort,
                long baudrate, 
		unsigned char byteFormat,
                unsigned char ID, 
                unsigned char transmit enable pin, 
                unsigned int holding registers size,
                unsigned int* holding register array)
  */
  
  //pinMode(PIN_DIP0, INPUT);
  //pinMode(PIN_DIP1, INPUT);
  //pinMode(PIN_DIP2, INPUT);
  //Determination de l'adresse de la carte en fonction du DIP
  //curAdress=digitalRead(PIN_DIP0)+2*digitalRead(PIN_DIP1)+4*digitalRead(PIN_DIP2)+65;

  #ifdef SOFT_SERIAL
    modbus_configure(&Serial, 19200, SERIAL_8N2, curAdress, PIN_RS485_RE, HOLDING_REGS_SIZE, holdingRegs,PIN_RS485_DE);
    mySerial.begin(115200); 
  #else
    modbus_configure(&Serial1, 19200, SERIAL_8N2, curAdress, PIN_RS485_RE, HOLDING_REGS_SIZE, holdingRegs,PIN_RS485_DE);
    Serial.begin(115200); 
  #endif
  // modbus_update_comms(baud, byteFormat, id) is not needed but allows for easy update of the
  // port variables and slave id dynamically in any function.
  modbus_update_comms(19200, SERIAL_8N2, curAdress);

}

void loop()
{
  // modbus_update() is the only method used in loop(). It returns the total error
  // count since the slave started. You don't have to use it but it's useful
  // for fault finding by the modbus master.
  
  modbus_update();

  holdingRegs[ADC_VAL] = 100+holdingRegs[PWM_VAL];//curAdress; // update data to be read by the master to adjust the PWM
  //holdingRegs[INFO_VAL] = holdingRegs[PWM_VAL]+10; // update data to be read by the master
  #ifdef SOFT_SERIAL
    mySerial.print("S");mySerial.print(curAdress);mySerial.print(" R");mySerial.print(holdingRegs[PWM_VAL]);mySerial.print(" +100=W");mySerial.println(holdingRegs[ADC_VAL]);
  #else
    Serial.print("S");Serial.print(curAdress);Serial.print(" R");Serial.print(holdingRegs[PWM_VAL]);Serial.print(" +100=W");Serial.println(holdingRegs[ADC_VAL]);
  #endif
  
  /* Note:
     The use of the enum instruction is not needed. You could set a maximum allowable
     size for holdinRegs[] by defining HOLDING_REGS_SIZE using a constant and then access 
     holdingRegs[] by "Index" addressing. 
     I.e.
     holdingRegs[0] = analogRead(A0);
     analogWrite(LED, holdingRegs[1]/4);
  */
  
}
