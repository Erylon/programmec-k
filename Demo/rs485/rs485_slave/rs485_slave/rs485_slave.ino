/*
  Mise en réseau de 2 cartes Arduino Nano (Maitre esclave en RS485)

  Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
  press), the output pin is toggled from LOW to HIGH or HIGH to LOW. There's a
  minimum delay between toggles to debounce the circuit (i.e. to ignore noise).

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached from pin 2 to +5V
  - 10 kilohm resistor attached from pin 2 to ground

  - Note: On most Arduino boards, there is already an LED on the board connected
    to pin 13, so you don't need any extra components for this example.

   this example receives 4 registers from the master [0-3]
   and sets 4 registers [4-7]

   SimpleModbusSlaveV10 supports function 3, 6 & 16.

   function 3: READ_HOLDING_REGISTERS    **** this example uses this function [4-7]
   function 6: PRESET_SINGLE_REGISTER
   function 16: PRESET_MULTIPLE_REGISTERS **** this example uses this function [0-3]

   The modbus_update() method updates the holdingRegs register array and checks
   communication.

   Note:
   The Arduino serial ring buffer is 64 bytes or 32 registers.
   Most of the time you will connect the arduino to a master via serial
   using a MAX485 or similar.

   In a function 3 request the master will attempt to read from your
   slave and since 5 bytes is already used for ID, FUNCTION, NO OF BYTES
   and two BYTES CRC the master can only request 58 bytes or 29 registers.

   In a function 16 request the master will attempt to write to your
   slave and since a 9 bytes is already used for ID, FUNCTION, ADDRESS,
   NO OF REGISTERS, NO OF BYTES and two BYTES CRC the master can only write
   54 bytes or 27 registers.

   Using a USB to Serial converter the maximum bytes you can send is
   limited to its internal buffer which differs between manufactures.

  created 10 May 2018
  by SmartEmbed

*/

// Fichier inclus
#include <SimpleModbusSlave.h>              // Revision V10 https://drive.google.com/drive/folders/0B0B286tJkafVYnBhNGo4N3poQ2c?tid=0B0B286tJkafVSENVcU1RQVBfSzg

// Configuration Modbus
#define RS485_TX_ENABLE             2         // Broche utilisée pour commuter en réception et en émission                                  
#define RS485_BAUDRATE              115200    // Vitesse de communication
#define RS485_SLAVE_ID              2         // Valeur par défaut de l'adresse de l'esclave
#define RS485_HOLDING_REGS_SIZE     8         // Taille des registres

#define NOP()                       __asm__("nop");

#define PB_DELAY_TIME               10   // Durée de commutation de l'électrovanne  (ms)
#define DEBOUNCE_TIME               50    // Durée anti-rebond du bouton poussoir

// Déclaration des broches 
#define LED_OUTPUT_PIN              13    // Led d'état
#define GREEN_LED_OUTPUT_PIN        14    // Led d'état

#define PB_INPUT_PIN                4     // Bouton poussoir de pilotage de l'électrovanne

// Definition des états de la machine
#define EV_IDLE_STATE               0
#define EV_POWER_ON_STATE           1
#define EV_WAITING_POWER_ON_STATE   2
#define EV_TOGGLE_STATE             3
#define EV_WAITING_TOGGLE_STATE     4

// Déclaration des variables locales
volatile int  g_iEvState    = EV_IDLE_STATE;

int g_iLedState             = HIGH;  // Etat de la broche de la led
int g_iCurrentButtonState   = LOW;   // Etat courant de l'entrée du bouton poussoir
int g_iLastButtonState      = LOW;   // Etat précédent du bouton poussoir

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long g_ulLastDebounceTime  = 0;                // the last time the output pin was toggled
unsigned long g_ulDebounceDelay     = DEBOUNCE_TIME;    // the debounce time; increase if the output flickers

//////////////// registers of your slave ///////////////////
/*
enum
{
  // just add or remove registers and your good to go...
  // The first register starts at address 0

  HOLDING_REGS_SIZE // leave this one
  // total number of registers for function 3 and 16 share the same register array
  // i.e. the same address space
};
*/

unsigned int holdingRegs[RS485_HOLDING_REGS_SIZE]; // function 3 and 16 register array
////////////////////////////////////////////////////////////

void setup()
{  
  //Mise en place des entrees/sorties
  Serial.begin(9600);

  // Initialisation des variables
  g_iEvState              = EV_IDLE_STATE;

  g_iLedState             = LOW;    // Etat de la broche de la led
  g_iCurrentButtonState   = LOW;   // Etat courant de l'entrée du bouton poussoir
  g_iLastButtonState      = LOW;   // Etat précédent du bouton poussoir

  // Affichage dans la console
  Serial.print("Démarrage du programme de test: RS485");
  
  // Configuration des broches en entrée
  pinMode(PB_INPUT_PIN, INPUT);             // Broche du bouton poussoir de pilotage de l'électrovanne

  // Configuration des broches de sortie
  pinMode(LED_OUTPUT_PIN, OUTPUT);          // Broche de la led
  pinMode(GREEN_LED_OUTPUT_PIN, OUTPUT);    // Broche de la led de visualisation

  // Forcage au démarage
  digitalWrite(LED_OUTPUT_PIN, g_iLedState);
  digitalWrite(GREEN_LED_OUTPUT_PIN, HIGH);

   /* parameters(HardwareSerial* SerialPort,
                long baudrate,
      unsigned char byteFormat,
                unsigned char ID,
                unsigned char transmit enable pin,
                unsigned int holding registers size,
                unsigned int* holding register array)
  */

  /* Valid modbus byte formats are:
     SERIAL_8N2: 1 start bit, 8 data bits, 2 stop bits
     SERIAL_8E1: 1 start bit, 8 data bits, 1 Even parity bit, 1 stop bit
     SERIAL_8O1: 1 start bit, 8 data bits, 1 Odd parity bit, 1 stop bit

     You can obviously use SERIAL_8N1 but this does not adhere to the
     Modbus specifications. That said, I have tested the SERIAL_8N1 option
     on various commercial masters and slaves that were suppose to adhere
     to this specification and was always able to communicate... Go figure.

     These byte formats are already defined in the Arduino global name space.
  */

  modbus_configure(&Serial, RS485_BAUDRATE, SERIAL_8N2, RS485_SLAVE_ID, RS485_TX_ENABLE, RS485_HOLDING_REGS_SIZE, holdingRegs);

  // modbus_update_comms(baud, byteFormat, id) is not needed but allows for easy update of the
  // port variables and slave id dynamically in any function.
  modbus_update_comms(RS485_BAUDRATE, SERIAL_8N2, RS485_SLAVE_ID);
}

void loop() {

  // Déclaration des variables locales
  int l_iEvIsPowerOk = LOW;
 
  // Lecture de l'état du bouton poussoir
  getPushButtonState();

  // Forcage au démarage
  digitalWrite(LED_OUTPUT_PIN, !digitalRead(LED_OUTPUT_PIN));

  /*Serial.print("Etat du bouton poussoir: ");
  Serial.println(holdingRegs[6], DEC);*/

  // modbus_update() is the only method used in loop(). It returns the total error
  // count since the slave started. You don't have to use it but it's useful
  // for fault finding by the modbus master.
  // the library manual suggests not using long delays but an interval instead
  modbus_update();
  
  // Gestion des entrées
  holdingRegs[4] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  holdingRegs[5] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  holdingRegs[6] = digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  holdingRegs[7] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave

  // this section prints the status of the 2 switch registers on the Slave
  if (holdingRegs[0] == 1)
  {
    // On allume la led
    digitalWrite(GREEN_LED_OUTPUT_PIN, LOW);
  }
  else
  {
    // On éteind la led
    digitalWrite(GREEN_LED_OUTPUT_PIN, HIGH);
  }

  /* Note:
     The use of the enum instruction is not needed. You could set a maximum allowable
     size for holdinRegs[] by defining HOLDING_REGS_SIZE using a constant and then access
     holdingRegs[] by "Index" addressing.
     I.e.
     holdingRegs[0] = analogRead(A0);
     analogWrite(LED, holdingRegs[1]/4);
  */
}

void getPushButtonState()
{
  // read the state of the switch into a local variable:
  int l_iReading = digitalRead(PB_INPUT_PIN);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (l_iReading != g_iLastButtonState) {
    // reset the debouncing timer
    g_ulLastDebounceTime = millis();
  }

  if ((millis() - g_ulLastDebounceTime) > g_ulDebounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (l_iReading != g_iCurrentButtonState) {
      g_iCurrentButtonState = l_iReading;

      // only toggle the LED if the new button state is HIGH
      if (g_iCurrentButtonState == HIGH)
      {
        // Toggle led
        g_iLedState = !g_iLedState;
        
        // Set led state
        digitalWrite(LED_OUTPUT_PIN, g_iLedState);

        // Changement d'état de la machine d'état
        g_iEvState = EV_POWER_ON_STATE;
      }
    }
  }

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  g_iLastButtonState = l_iReading;
}
