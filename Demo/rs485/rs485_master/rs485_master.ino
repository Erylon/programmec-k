/*
  Mise en réseau de 2 cartes Arduino Nano (Maitre esclave en RS485)

  Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
  press), the output pin is toggled from LOW to HIGH or HIGH to LOW. There's a
  minimum delay between toggles to debounce the circuit (i.e. to ignore noise).

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached from pin 2 to +5V
  - 10 kilohm resistor attached from pin 2 to ground

  - Note: On most Arduino boards, there is already an LED on the board connected
    to pin 13, so you don't need any extra components for this example.

  this example uses holding registers:
  [0-3] sends 4 registers (int) to the slave: status of 4 LEDs = digital outputs of the slave
  [6-7] reads 2 registers (int) from the slave: status of 2 PUSH BUTTONS = digital inputs of the slave
  actually we read 4 registers but only 2 are connected to buttons

  created 10 May 2018
  by SmartEmbed

*/

// Fichier inclus
#include <SimpleModbusMaster.h>              // Revision V22 https://drive.google.com/drive/folders/0B0B286tJkafVYnBhNGo4N3poQ2c?tid=0B0B286tJkafVSENVcU1RQVBfSzg

// Configuration Modbus
#define RS485_TX_ENABLE             2         // Broche utilisée pour commuter en réception et en émission                                
#define RS485_BAUDRATE              115200    // Vitesse de communication
#define RS485_SLAVE_ID              2         // Valeur par défaut de l'adresse de l'esclave

#define RS485_TIMEOUT               1000      // Timeout
#define RS485_POLLING               20        // the scan rate, standard was 200
#define RS485_RETRY_COUNT           30

// The total amount of available memory on the master to store data
#define TOTAL_NO_OF_REGISTERS       8

#define NOP()                       __asm__("nop");

#define PB_DELAY_TIME               10   // Durée de commutation de l'électrovanne  (ms)
#define DEBOUNCE_TIME               50    // Durée anti-rebond du bouton poussoir

// Déclaration des broches 
#define LED_OUTPUT_PIN              13    // Led d'état
#define REG_OUTPUT_PIN              14    // Led d'état

#define PB_INPUT_PIN                4     // Bouton poussoir de pilotage de l'électrovanne

// Definition des états de la machine
#define EV_IDLE_STATE               0
#define EV_POWER_ON_STATE           1
#define EV_WAITING_POWER_ON_STATE   2
#define EV_TOGGLE_STATE             3
#define EV_WAITING_TOGGLE_STATE     4

// Déclaration des variables locales
volatile int  g_iEvState    = EV_IDLE_STATE;

int g_iLedState             = HIGH;  // Etat de la broche de la led
int g_iCurrentButtonState   = LOW;   // Etat courant de l'entrée du bouton poussoir
int g_iLastButtonState      = LOW;   // Etat précédent du bouton poussoir

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long g_ulLastDebounceTime  = 0;                // the last time the output pin was toggled
unsigned long g_ulDebounceDelay     = DEBOUNCE_TIME;    // the debounce time; increase if the output flickers

// This is the easiest way to create new packets
// Add as many as you want. TOTAL_NO_OF_PACKETS
// is automatically updated.
enum
{
  PACKET1,                                          // set 4 registers
  PACKET2,                                          // read 4 registers
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];
unsigned long previousMillis = 0;
int counter = 0;
unsigned int test[TOTAL_NO_OF_REGISTERS]; 

void setup()
{  
  //Mise en place des entrees/sorties
  Serial.begin(9600);

  // Initialisation des variables
  g_iEvState              = EV_IDLE_STATE;

  g_iLedState             = LOW;    // Etat de la broche de la led
  g_iCurrentButtonState   = LOW;   // Etat courant de l'entrée du bouton poussoir
  g_iLastButtonState      = LOW;   // Etat précédent du bouton poussoir

  // Affichage dans la console
  Serial.print("Démarrage du programme de test: RS485");
  
  // Configuration des broches en entrée
  pinMode(PB_INPUT_PIN, INPUT);           // Broche du bouton poussoir de pilotage de l'électrovanne

  // Configuration des broches de sortie
  pinMode(LED_OUTPUT_PIN, OUTPUT);          // Broche de la led
  pinMode(REG_OUTPUT_PIN, OUTPUT);

  // Forcage au démarage
  digitalWrite(LED_OUTPUT_PIN, g_iLedState);
  digitalWrite(REG_OUTPUT_PIN, HIGH);         // Logique inverse

   // Initialize each packet: packet, slave-id, function, start of slave index, number of regs, start of master index
  // set 4 registers
  // read 4 registers

  modbus_construct(&packets[PACKET1], RS485_SLAVE_ID, PRESET_MULTIPLE_REGISTERS, 0, 4, 0);
  modbus_construct(&packets[PACKET2], RS485_SLAVE_ID, READ_HOLDING_REGISTERS, 4, 4, 4);

  // Initialize the Modbus Finite State Machine
  modbus_configure(&Serial, RS485_BAUDRATE, SERIAL_8N2, RS485_TIMEOUT, RS485_POLLING, RS485_RETRY_COUNT, RS485_TX_ENABLE, packets, TOTAL_NO_OF_PACKETS, regs);

  // Initialisation du compteur
  previousMillis = millis();
}

int index = 0;

void loop() {

  // Déclaration des variables locales
  int l_iEvIsPowerOk = LOW;
 
  // Lecture de l'état du bouton poussoir
  getPushButtonState();

  // Forcage au démarage
  digitalWrite(LED_OUTPUT_PIN, !digitalRead(LED_OUTPUT_PIN));

  // modbus_update() is the only method used in loop(). It returns the total error
  // count since the slave started. You don't have to use it but it's useful
  // for fault finding by the modbus master.
  // the library manual suggests not using long delays but an interval instead
  modbus_update();

  // the library manual suggests not using long delays but an interval instead
  // the below section updates a counter after an interval, 
  // and switches ON one LED of 8: 4 on the Master and 4 on the Slave
  // first switch all OFF
  // then if counter points to first half, switch ON one on Master
  // if counter points to second half, switch ON one on Slave 
  
  /*if ((millis() - previousMillis) > 2000)
  {
    for (index = 0; index < 4; index++) {
      regs[index] = digitalRead;  // switch on LED according to Master request
    }
  }
  else
  {
    for (index = 0; index < 4; index++) {
      regs[index] = 0;  // switch on LED according to Master request
    }
  }*/

  regs[0] = digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  regs[1] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  regs[2] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  regs[3] = 0;//digitalRead(PB_INPUT_PIN);    // set register according to button on Slave
  
  // this section prints the status of the 2 switch registers on the Slave
  if (regs[6] == 1)
  {
    // On allume la led
    digitalWrite(REG_OUTPUT_PIN, LOW);
  }
  else
  {
    // On éteind la led
    digitalWrite(REG_OUTPUT_PIN, HIGH);
  }
}

void getPushButtonState()
{
  // read the state of the switch into a local variable:
  int l_iReading = digitalRead(PB_INPUT_PIN);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (l_iReading != g_iLastButtonState) {
    // reset the debouncing timer
    g_ulLastDebounceTime = millis();
  }

  if ((millis() - g_ulLastDebounceTime) > g_ulDebounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (l_iReading != g_iCurrentButtonState) {
      g_iCurrentButtonState = l_iReading;

      // only toggle the LED if the new button state is HIGH
      if (g_iCurrentButtonState == HIGH)
      {
        // Toggle led
        g_iLedState = !g_iLedState;
        
        // Set led state
        digitalWrite(LED_OUTPUT_PIN, g_iLedState);

        // Changement d'état de la machine d'état
        g_iEvState = EV_POWER_ON_STATE;
      }
    }
  }

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  g_iLastButtonState = l_iReading;
}
