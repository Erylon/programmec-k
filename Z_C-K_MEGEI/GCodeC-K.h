#ifndef GCodeC_K_h
#define GCodeC_K_h

#define VERSION_GCODE			7	

//Adresses
#define ROBOT 					'R'
#define HOIST 					'H'

//COMMANDE CGODE : E
#define ALERT					666 //suivi code erreur

//COMMANDE CGODE : A
#define ALERT_US_DOWN			10
#define ALERT_US_UP				20
#define ALERT_BUMP_UP			30		
#define ALERT_BUMP_DOWN			40		

//COMMANDE CGODE : G
//all
#define CDE_QUERY_MSG			1		//Sent from master to get new message form client. expect same commande as a reply if no message
#define CDE_ACK_ERROR			4
#define CDE_RAZ					6 		//Reset all jack boards
#define CDE_DISPLAY_PARAMS		8 
#define CDE_REPING				9 		//Commande envoyée par le treuil
#define CDE_PING				10 		//Commande envoyée par les esclaves au démarrage
#define CDE_PONG				11		//Commande retournée apres un ping ou un reping
#define CDE_INFO				20		//Gives an info (time) to Hoist
#define CDE_FORCE_ZSH_STATE		30		//Force ZSH
#define CDE_FORCE_MOVE_STOP		40		//Force dir MoveDown
#define CDE_FORCE_MOVE_UP		41		//Force dir MoveUp
#define CDE_FORCE_MOVE_DOWN		42		//Force dir MoveDown
#define CDE_FORCE_MANUAL		52		//Force mode manual
#define CDE_FORCE_CLEANING		53		//Force mode cleaning

//verin 100
#define CDE_CATCH 				100
#define CDE_DEEP_CATCH			101
#define CDE_FREE				102
#define CDE_XVS_OPEN			131
#define CDE_XVP_OPEN			132
#define CDE_XVV_OPEN			133
#define CDE_XVJ_OPEN			134
#define CDE_FRAME_DETECT		180		//jack has detected begining or end of frame

//robot 200
//Ne pas modifier l'ordre des boutons
#define CDE_HSRC				200	// Btn Cleaning asked from Robot
#define CDE_HSRM				201	// Btn Manual asked from Robot (ex HSCFR)
#define CDE_HSRU				202	// Btn Up asked from Robot
#define CDE_HSRD				203	// Btn Down asked from Robot
#define CDE_HSRP				204	// Position 0 set from Robot
#define CDE_VM					210	// Moteur Aspi
#define CDE_RM					211	// Moteur Rouleau
#define CDE_SWP					212	// Pompe pression eau (ex SWP)
#define CDE_XVRO_OPEN			220	//
#define CDE_XVSH_OPEN			221	//
#define CDE_XVSM_OPEN			222	//
#define CDE_XVSL_OPEN			223	//
#define CDE_XVRJ_OPEN			224	// 
#define CDE_CLEAN				230 //
#define CLEAN_STOP				0
#define CLEAN_UP				1
#define CLEAN_DOWN				2
#define CLEANING_SLEEP			6



//COMMANDE CGODE : V
//all
#define GET_GCODE_RELEASE		10
//verin 100
#define GET_VACCUM_DURATION		101
#define GET_PSL 				110
#define GET_PSH 				111
#define GET_ZSL					120 // ne pas changer l'ordre avec ZSH
#define GET_ZSH					121
#define GET_XVS					131
#define GET_XVP					132
#define GET_XVV					133
#define GET_XVJ					134
#define GET_PRESSURE			140
#define GET_RAW_PRESSURE		141
#define GET_ATM_PRESSURE 		142 
#define GET_MOVE_DOWN_DEEP_TIME	151
#define GET_MOVE_DOWN_SOFT_TIME	152
#define GET_FILL_UP_TIME		153
#define GET_RESUCK_UP_MODE		170 //1 for rescuck up; 0 to free jack in case of lost vaccum
#define GET_AUTO_RECATCH_MODE	171 
#define GET_TIMEOUT_JACK_ZSH	172
#define GET_MOVING_UP_DURATION	173
#define GET_AUTOCATCH_DELAY		174

//robot 200
//Ne pas modifier l'ordre des boutons
#define GET_HSRC				200	// Btn Cleaning
#define GET_HSRM				201	// Btn Manual ex HSCFR
#define GET_HSRU				202	// Btn Up
#define GET_HSRD				203	// Btn Down
#define GET_HSRP				204	// Position 0 set from Robot
#define GET_PTP					207	// Pression compresseur
#define GET_PTV					208	// Pression vide
#define GET_VM					210	// Moteur Aspi
#define GET_RM					211	// Moteur Rouleau
#define GET_CWP					212	// Pompe pression eau (ex SWP)
#define GET_XVRO				220	//
#define GET_XVSH				221	//
#define GET_XVSM				222	//
#define GET_XVSL 				223	//
#define GET_XVRJ				224	// 
#define GET_SLEEP_TIME			231	//
#define GET_XVRO_PERIOD			232	//
#define GET_XVSX_PERIOD			233	//
#define GET_XVRO_DURATION		234	//
#define GET_XVSX_DURATION		235	//
#define GET_ROLL_SPEED			236	//
#define GET_ZSHSW				237	// Niveau d'eau sale haut
#define GET_ZSLR				240	// Fdc Bas rouleau
#define GET_ZSHR				241	// Fdc Haut rouleau



#define MSG_GCODE_JACK_V \
	"Vaccum Duration",\
	"PSL",\
	"PSH",\
	"ZSL",\
	"ZSH",\
	"XVS",\
	"XVP",\
	"XVV",\
	"XVJ",\
	"(Mean) Pressure",\
	"Raw Pressure",\
	"ATM pressure",\
	"Move Down deep time",\
	"Move Down soft time",\
	"Fill up Time",\
	"Resuck Up Mode",\
	"AutoRecatch Mode",\
	"Time Out Jack ZSH",\
	"Moving Up Duration",\
	"Autocatch delay"

//COMMANDE CGODE : S
//verin 100
#define SET_VACCUM_DURATION		101 
#define SET_PSL 				110
#define SET_PSH 				111
#define SET_ATM_PRESSURE 		142 //init atm pressure to cur pressure when jack is not catched
#define SET_MOVE_DOWN_DEEP_TIME	151
#define SET_MOVE_DOWN_SOFT_TIME	152
#define SET_FILL_UP_TIME		153
#define SET_RESUCK_UP_MODE		170 //1 for rescuck up; 0 to free jack in case of lost vaccum
#define SET_AUTO_RECATCH_MODE	171 
#define SET_TIMEOUT_JACK_ZSH	172
#define SET_MOVING_UP_DURATION	173
#define SET_AUTOCATCH_DELAY 	174

//robot 200
#define SET_ROBOT_BUZZER		230	//
#define SET_SLEEP_TIME			231	//
#define SET_XVRO_PERIOD			232	//
#define SET_XVSX_PERIOD			233	//
#define SET_XVRO_DURATION		234	//
#define SET_XVSX_DURATION		235	//
#define SET_ROLL_SPEED			236	//


/* ------------- SECTION DEVELOPPEUR -----------------*/
//Code retour
#define NO_PARAM				0
#define SUCCESS					1
#define CATCH_FAILURE			666

#define ROLL_SPEED_PERIOD		100
#define MAX_ROLL_SPEED			ROLL_SPEED_PERIOD

#endif