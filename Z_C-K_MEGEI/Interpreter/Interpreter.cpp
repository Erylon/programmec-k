#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\Interpreter\Interpreter.h"

#define MAX_BUF (64)      // What is the longest message Arduino can store?
char buffer[MAX_BUF];     // where we store the message until we get a ';'
int sofar;                // how much is in the buffer

#define VERSION_INTERPRETER     0.1

Interpreter::Interpreter(char _myAdress, int _pinRS485_DE, int _pinRS485_RE, HardwareSerial *_refSerial){
  //Serial.begin(115200);
  myAdress=_myAdress;
  loopAdress[0]=myAdress;
  versionGCode=VERSION_GCODE;
  help();
  oRS485=new RS485(_myAdress,'H', _pinRS485_DE, _pinRS485_RE, _refSerial);
  curTime=millis();
  doDisplayTraverse=false;
  doDisplayParams=false;
  doResetIndex=false;
}


void Interpreter::setup(){
  ready();
}


void Interpreter::helpGetter(){
  Serial.println("\nValue:");
  Serial.print("V");Serial.print(GET_GCODE_RELEASE);Serial.println("\t\tGCODE_RELEASE");
  Serial.println("\nVerin:");
  Serial.print("V");Serial.print(GET_VACCUM_DURATION);Serial.println("\t\tVACCUM_DURATION\t\t");
  Serial.print("V");Serial.print(GET_PSL);Serial.println("\t\tPSL");
  Serial.print("V");Serial.print(GET_PSH);Serial.println("\t\tPSH");
  Serial.print("V");Serial.print(GET_ZSL);Serial.println("\t\tZSL");
  Serial.print("V");Serial.print(GET_ZSH);Serial.println("\t\tZSH");
  Serial.print("V");Serial.print(GET_XVS);Serial.println("\t\tXVS");
  Serial.print("V");Serial.print(GET_XVP);Serial.println("\t\tXVP");
  Serial.print("V");Serial.print(GET_XVV);Serial.println("\t\tXVV");
  Serial.print("V");Serial.print(GET_XVJ);Serial.println("\t\tXVJ");
  Serial.print("V");Serial.print(GET_PRESSURE);Serial.println("\t\tPRESSURE");
  Serial.print("V");Serial.print(GET_RAW_PRESSURE);Serial.println("\t\tRAW_PRESSURE");
  Serial.print("V");Serial.print(GET_ATM_PRESSURE);Serial.println("\t\tATM_PRESSURE");
  Serial.print("V");Serial.print(GET_MOVE_DOWN_DEEP_TIME);Serial.println("\t\tMOVE_DOWN_DEEP_TIME");
  Serial.print("V");Serial.print(GET_MOVE_DOWN_SOFT_TIME);Serial.println("\t\tMOVE_DOWN_SOFT_TIME");
  Serial.print("V");Serial.print(GET_RESUCK_UP_MODE);Serial.println("\t\tRESUCK_UP_MODE");
  Serial.print("V");Serial.print(GET_AUTO_RECATCH_MODE);Serial.println("\t\tAUTO_RECATCH_MODE");
  Serial.print("V");Serial.print(GET_TIMEOUT_JACK_ZSH);Serial.println("\t\tTIMEOUT_JACK_ZSH\t\t");
  Serial.print("V");Serial.print(GET_MOVING_UP_DURATION);Serial.println("\t\tMOVING_UP_DURATION");
  Serial.print("V");Serial.print(GET_AUTOCATCH_DELAY);Serial.println("\t\tAUTOCATCH_DELAY");

  Serial.println("\nRobot:");
  Serial.print("V");Serial.print(GET_HSRC);Serial.println("\t\tHSRC");
  Serial.print("V");Serial.print(GET_HSRM);Serial.println("\t\tHSRM");
  Serial.print("V");Serial.print(GET_HSRU);Serial.println("\t\tHSRU");
  Serial.print("V");Serial.print(GET_HSRD);Serial.println("\t\tHSRD");
  Serial.print("V");Serial.print(GET_ZSHSW);Serial.println("\t\tZSHSW");
  Serial.print("V");Serial.print(GET_ZSLR);Serial.println("\t\tZSLR");
  Serial.print("V");Serial.print(GET_ZSHR);Serial.println("\t\tZSHR");
  Serial.print("V");Serial.print(GET_PTP);Serial.println("\t\tPTP");
  Serial.print("V");Serial.print(GET_PTV);Serial.println("\t\tPTV");
  Serial.print("V");Serial.print(GET_VM);Serial.println("\t\tVM");
  Serial.print("V");Serial.print(GET_RM);Serial.println("\t\tRM");
  Serial.print("V");Serial.print(GET_CWP);Serial.println("\t\tCWP");
  Serial.print("V");Serial.print(GET_XVRO);Serial.println("\t\tXVRO");
  Serial.print("V");Serial.print(GET_XVSH);Serial.println("\t\tXVSH");
  Serial.print("V");Serial.print(GET_XVSM);Serial.println("\t\tXVSM");
  Serial.print("V");Serial.print(GET_XVSL);Serial.println("\t\tXVSL");
  Serial.print("V");Serial.print(GET_XVRJ);Serial.println("\t\tXVRJ");
  Serial.print("V");Serial.print(GET_XVRO_DURATION);Serial.println("\t\tXVRO_DURATION");
  Serial.print("V");Serial.print(GET_XVSX_DURATION);Serial.println("\t\tXVSx_DURATION");
  Serial.print("V");Serial.print(GET_XVRO_PERIOD);Serial.println("\t\tXVRO_PERIOD");
  Serial.print("V");Serial.print(GET_XVSX_PERIOD);Serial.println("\t\tXVSx_PERIOD");
  Serial.print("V");Serial.print(GET_ROLL_SPEED);Serial.println("\t\tROLL_SPEED");
 
  Serial.println();
  Serial.print("ex : V");Serial.print(GET_MOVE_DOWN_SOFT_TIME);Serial.println(" A");
}



void Interpreter::helpSetter(){
  Serial.println("\nSetter:");
  Serial.print("S");Serial.print(SET_VACCUM_DURATION);Serial.println("\t\tVACCUM_DURATION");
  Serial.print("S");Serial.print(SET_RESUCK_UP_MODE);Serial.println("\t\tRESUCK_UP_MODE");
  Serial.print("S");Serial.print(SET_AUTO_RECATCH_MODE);Serial.println("\t\tAUTO_RECATCH_MODE");
  Serial.print("S");Serial.print(SET_ATM_PRESSURE);Serial.println("\t\tATM_PRESSURE");
  Serial.print("S");Serial.print(SET_MOVE_DOWN_DEEP_TIME);Serial.println("\t\tMOVE_DOWN_DEEP_TIME");
  Serial.print("S");Serial.print(SET_MOVE_DOWN_SOFT_TIME);Serial.println("\t\tMOVE_DOWN_SOFT_TIME");
  Serial.print("S");Serial.print(SET_PSL);Serial.println("\t\tSET_PSL");
  Serial.print("S");Serial.print(SET_PSH);Serial.println("\t\tSET_PSH");
  Serial.print("S");Serial.print(SET_TIMEOUT_JACK_ZSH);Serial.println("\t\tTIMEOUT_JACK_ZSH");
  Serial.print("S");Serial.print(SET_MOVING_UP_DURATION);Serial.println("\t\tMOVING_UP_DURATION");
  Serial.print("S");Serial.print(SET_AUTOCATCH_DELAY);Serial.println("\t\tAUTOCATCH_DELAY");
  
  Serial.print("S");Serial.print(SET_ROBOT_BUZZER);Serial.println("\t\tROBOT_BUZZER");
  Serial.print("S");Serial.print(SET_XVRO_DURATION);Serial.println("\t\tSET_XVRO_DURATION");
  Serial.print("S");Serial.print(SET_XVSX_DURATION);Serial.println("\t\tSET_XVSx_DURATION");
  Serial.print("S");Serial.print(SET_XVRO_PERIOD);Serial.println("\t\tSET_XVRO_PERIOD");
  Serial.print("S");Serial.print(SET_XVSX_PERIOD);Serial.println("\t\tSET_XVSx_PERIOD");
  Serial.print("S");Serial.print(SET_ROLL_SPEED);Serial.print("\t\tSET_ROLL_SPEED [0-");Serial.print(MAX_ROLL_SPEED);Serial.println("]");


  Serial.println();
  Serial.print("ex : S");Serial.print(SET_MOVE_DOWN_DEEP_TIME);Serial.println(" P100 A");
  }

void Interpreter::helpCde(){
  Serial.println("Commands :");
  Serial.print("G");Serial.print(CDE_CATCH);Serial.println("\t\tCATCH");
  Serial.print("G");Serial.print(CDE_DEEP_CATCH);Serial.println("\t\tDEEP_CATCH");
  Serial.print("G");Serial.print(CDE_FREE);Serial.println("\t\tFREE");
  Serial.print("G");Serial.print(CDE_XVS_OPEN);Serial.println("\t\tXVS_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVP_OPEN);Serial.println("\t\tXVP_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVV_OPEN);Serial.println("\t\tXVV_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_XVJ_OPEN);Serial.println("\t\tXVJ_OPEN P[[0];1]");
  Serial.print("G");Serial.print(CDE_PING);Serial.println("\t\tPING");
  Serial.print("G");Serial.print(CDE_PONG);Serial.println("\t\tPONG");
  Serial.print("G");Serial.print(CDE_ACK_ERROR);Serial.println("\t\tACK_ERROR");
  Serial.print("G");Serial.print(CDE_DISPLAY_PARAMS);Serial.println("\t\tCDE_DISPLAY_PARAMS");
  Serial.print("G");Serial.print(CDE_RAZ);Serial.println("\t\tRAZ");
  Serial.print("G");Serial.print(CDE_FORCE_ZSH_STATE);Serial.println("\t\tCDE_FORCE_ZSH_STATE P[[0];1]");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_UP);Serial.println("\t\tCDE_FORCE_MOVE_UP");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_DOWN);Serial.println("\t\tCDE_FORCE_MOVE_DOWN");
  Serial.print("G");Serial.print(CDE_FORCE_MOVE_STOP);Serial.println("\t\tCDE_FORCE_MOVE_STOP");
  Serial.print("G");Serial.print(CDE_FORCE_CLEANING);Serial.println("\t\tCDE_FORCE_CLEANING");
  Serial.print("G");Serial.print(CDE_FORCE_MANUAL);Serial.println("\t\tCDE_FORCE_MANUAL");

  Serial.print("G");Serial.print(CDE_HSRC);Serial.println("\t\tHSRC");
  Serial.print("G");Serial.print(CDE_HSRM);Serial.println("\t\tHSRM");
  Serial.print("G");Serial.print(CDE_HSRU);Serial.println("\t\tHSRU");
  Serial.print("G");Serial.print(CDE_HSRD);Serial.println("\t\tHSRD");
  Serial.print("G");Serial.print(CDE_HSRP);Serial.println("\t\tSet index O ");
  Serial.print("G");Serial.print(CDE_VM);Serial.println("\t\tVM [0;1]");
  Serial.print("G");Serial.print(CDE_RM);Serial.println("\t\tRM [0;1]");
  Serial.print("G");Serial.print(CDE_SWP);Serial.println("\t\tSWP");
  Serial.print("G");Serial.print(CDE_XVRO_OPEN);Serial.println("\t\tXVRO_OPEN");
  Serial.print("G");Serial.print(CDE_XVSH_OPEN);Serial.println("\t\tXVSH_OPEN");
  Serial.print("G");Serial.print(CDE_XVSL_OPEN);Serial.println("\t\tXVSL_OPEN");
  Serial.print("G");Serial.print(CDE_XVSM_OPEN);Serial.println("\t\tXVSM_OPEN");
  Serial.print("G");Serial.print(CDE_XVRJ_OPEN);Serial.println("\t\tXVRJ_OPEN");
  Serial.print("G");Serial.print(CDE_CLEAN);Serial.print("\t\tCLEAN P[");
      Serial.print(CLEAN_STOP);Serial.print(" Stop; ");Serial.print(CLEAN_UP);Serial.print(" Up; ");
      Serial.print(CLEAN_DOWN);Serial.print(" Down]");
      Serial.println();
  Serial.print("ex : G");Serial.print(CDE_XVP_OPEN);Serial.println(" P1 A");
  Serial.print("ex : G");Serial.print(CDE_CATCH);Serial.println(" A");

}

void Interpreter::help() { 
  Serial.print("Console V");
  Serial.print(VERSION_INTERPRETER);
  Serial.print(" Protocole RS485:V");
  Serial.print(VERSION_RS485);
  Serial.print(" GCode:V");
  Serial.println(versionGCode);
  /*helpCde();
  helpGetter();
  helpSetter();
  //*/
  Serial.println();
  Serial.println("Commands:");
  Serial.println("? HELP [G commande; V getter; S setter for partial related help]");
  Serial.println("H I now behave like the Hoist");
  Serial.println("R I now behave like the Robot");
  Serial.println("$ Display pile [R;A;B;C;D;E]");
  Serial.println("& Display traverse");
  Serial.println("% Load test");
    Serial.println("! Display params [;R;A;B;C;D;E]");

  Serial.println();
}



void Interpreter::ready() {
  sofar=0;                // clear input buffer
  interpreterError=false;
  //Serial.print(F("> "));  // signal ready to receive input
}

int Interpreter::parseNumber(char code, int val) {
  char *ptr=buffer;
  char *firstChar;
  int result;
  while(ptr && *ptr && ptr){
    if(*ptr==code) {
      result=atoi(ptr+1);
      firstChar=ptr+1;
      if (firstChar[0]!='0' && result==0){  //Error: it is not 0 but conversion gives 0 : return old value
        interpreterError=true;
        return val;
      }
      else
        return result;                      // if something with no error, return value
    }
    ptr=strchr(ptr,' ')+1;
  }
  return val;                               //if nothing return original value
}

char Interpreter::parseLetter(const char *codeArray) {  //returns char founded otherwise 0
  unsigned int chr=0,i;
  while(buffer[chr]){                       //scan the buffer
   i=0;
   while(codeArray[i]){                     //scan for a code letter
      if(buffer[chr]==codeArray[i]) {
        return codeArray[i];
      }
      i++;
    }
    chr++;    
  }
  return 0;
}


void Interpreter::processCommand() {
  int u;
  bool noCode=false;
  char dest;
  long  param;
  
  if(parseLetter(loopAdress)){
    Serial.print("Loop !");
  }
  else {
    //Serial.print("Debug:");Serial.println(parseLetter('?',1));
    if(parseLetter("?")){
      dest=parseLetter(helpMode); 
      switch(dest) {
        case 'G':
          helpCde();
          break;
        case 'V':
          helpGetter();
          break;
        case 'S':
          helpSetter();
          break;
        default:
          help();  
      }
    }
    /*else if (parseLetter("H")) {
      oRS485->changeAdress('H');
    }
    else if (parseLetter("R")) {
      oRS485->changeAdress('R');
    }
    //*/
    else if (parseLetter("&")) {
      doDisplayTraverse=true;
    }
    else if (parseLetter("$")) {
      char toJack=parseLetter("ABCDER");
      if (toJack){
        oRS485->addMsg(toJack,'$',0);
      }
      else {
        oRS485->displayPile();
      }
    }
    else if (parseLetter("!")){
      char toJack=parseLetter("ABCDER");
      if (toJack){
        oRS485->addMsg(toJack,'!',0);
      }
      else {
        doDisplayParams=true;
      }
      //Avoid GCode Error
    }
    else if (parseLetter("%")){
      for(int i=0;i<10;i++){
       oRS485->addMsg('R','G',CDE_XVRO_OPEN,1);    
      }
      Serial.println("\nRS485 Load Test starting");
    }
    else {
        // look for commands that start with 'G'
      int cmd=parseNumber('G',-1);
      dest=parseLetter(destName); 
      
      switch(cmd) {
        case CDE_XVS_OPEN:
        case CDE_XVP_OPEN:
        case CDE_XVV_OPEN:
        case CDE_XVJ_OPEN:
        
          param=(long)((bool)parseNumber('P',0)>0); 
          if(dest){
            Serial.print('G');Serial.print(cmd); Serial.print(" P");Serial.print(param);Serial.print(" to ");Serial.println(dest);
            oRS485->addMsg(dest,'G',cmd,param);
          }
          else {
            interpreterError=true;
          }
          break;
        case CDE_VM:
        case CDE_RM:
        case CDE_SWP:
        case CDE_XVRO_OPEN:
        case CDE_XVSH_OPEN:
        case CDE_XVSM_OPEN:
        case CDE_XVSL_OPEN:
        case CDE_XVRJ_OPEN:
          param=(long)((bool)parseNumber('P',0)>0);
          oRS485->addMsg('R','G',cmd,param>0);
          break;
        case CDE_HSRP:
              doResetIndex=true;
          break;
        case CDE_FORCE_ZSH_STATE:
            oRS485->addMsg('H','G',CDE_FORCE_ZSH_STATE,(long)(bool)(parseNumber('P',0)>0),0,MSG_IN);
            break;
        case CDE_FORCE_MOVE_UP:
        case CDE_FORCE_MOVE_DOWN:
        case CDE_FORCE_MOVE_STOP:
        case CDE_FORCE_CLEANING:
        case CDE_FORCE_MANUAL:
            oRS485->addMsg('H','G',cmd,0,0,MSG_IN);
            break;
        default:
            if(cmd!=-1){
              param=(long)((bool)parseNumber('P',0)>0);
              oRS485->addMsg(dest,'G',cmd,param);
              #ifdef MESURE_TEMPS
                mesureStartTime=curTime;
              #endif

            }
            else {
              noCode=true;
            }
        break;
      }  

      // look for commands that start with 'V'
      cmd=parseNumber('V',-1);
      if(cmd!=-1){
         oRS485->addMsg(dest,'V',cmd);
      }
      else {
        noCode=true^noCode;
      }


      // look for commands that start with 'S'
      cmd=parseNumber('S',-1);
      if(cmd!=-1){
          param=parseNumber('P',-1); 
          if(param>-1 || cmd==SET_ATM_PRESSURE || cmd==SET_ROLL_SPEED){
            oRS485->addMsg(dest,'S',cmd,param);
          }
          else {
            interpreterError=true;
          }
      }
      else  {
        noCode=true^noCode;
      }
    }
    if (noCode){                       //something to read but passed everything and still no command to interpret
      interpreterError=true;        
    } 
  }
  

  // if the string has no G or M commands it will get here and the Arduino will silently ignore it
}


void Interpreter::loop(){
   if( Serial.available() ) { // if something is available
    char c = Serial.read();   // get it
    Serial.print(c);          // optional: repeat back what I got for debugging

    // store the byte as long as there's room in the buffer.
    // if the buffer is full some data might get lost
    if(sofar < MAX_BUF) buffer[sofar++]=c;

    if(c=='\n') {             // if we got a return character (\n) the message is done.
      buffer[sofar]=0;        // strings must end with a \0.

      //if(strcmp(buffer,'?')==0){//Help
      /*if(buffer[0]=='?'){//Help
          help();
      }
      else {
//*/

          processCommand();   // do something with the command
          if (interpreterError){
            Serial.println("Erreur GCode");
          }
//      }
      ready();                // prompt
    }
//*/
  }


 oRS485->processMsg();       //Check input msg and process output
 
}

