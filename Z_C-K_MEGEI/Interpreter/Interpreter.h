#ifndef Interpreter_h
#define Interpreter_h
//#define PIN_RS485_DE 		2
//#define PIN_RS485_RE 		3


#ifdef MAC
#else
	#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorList.h"
	#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\GCodeC-K.h"
	#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRS485\ProtocoleRS485.cpp"
#endif


bool 					linked;
char    				destName[]		="ABCDERH";
char    				helpMode[]		="GVS";
char    				loopAdress[]	="Z";	//=myAdress in string format to avoid loop sending messages

extern unsigned long		curTime,mesureStartTime;

class Interpreter{
//FUNCTIONS
public:
	Interpreter(char _myAdress, int _pinRS485_DE, int _pinRS485_RE, HardwareSerial *_refSerial=&Serial2);
	void	setup();
	void	loop();

	void	help();
	void	helpGetter();
	void	helpSetter();
	void	helpCde();
	void	ready();
	void	processCommand();
	int 	parseNumber(char code,int val);
	char	parseLetter(const char *codeArray);

	RS485 	*oRS485;

	bool 	doDisplayTraverse;
	bool    doDisplayParams;
	bool 	doResetIndex;

private:
	int  	interpreterError;
	char 	myAdress;
	unsigned int versionGCode;


//VARIABLES

};



#endif
