#ifndef CleanKongMegei_h
#define CleanKongMegei_h

#include <arduino.h>
//#include <Timer.h>
#include <math.h>
#include <SPI.h> 		// gestion du bus SPI
/*#include <Mirf.h> // gestion de la communication
#include <nRF24L01.h> // définition des registres du nRF24L01
#include <MirfHardwareSpiDriver.h> // communication SPI nRF24L01
//*/
#include "D:\Owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteRobot\RobotBoardPinList.h" //doit etre placé avant ProtocoleRF24
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleSerial\ProtocolSerial.cpp"
//#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRF24\ProtocoleRF24.cpp"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleVerinRobot.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ErrorList.h"
#include <TimerOne.h> 

//RC
void		 			readPPM();							//calcul les temps
int 					PPMConvertAnalog(unsigned int value);
int 					PPMConvert3StateSwitch(unsigned int value, int retLOW,int retMID,int retHIGH);
int 					PPMConvert2StateSwitch(unsigned int value, int retLOW,int retHIGH);
volatile int          	RC_HoistSpeed;
volatile int          	RC_BrushSpeed;
volatile int            RC_OrderDir;
volatile int            RC_OrderHook;
volatile int            RC_OrderClean;
volatile int            RC_OrderCross;
volatile int          	RC_OldHoistSpeed		=0;
volatile int          	RC_OldBrushSpeed		=0;
volatile int            RC_OldOrderDir			=CK_INIT_ORDER;
volatile int            RC_OldOrderHook			=CK_INIT_ORDER;
volatile int            RC_OldOrderClean		=CK_INIT_ORDER;
volatile int            RC_OldOrderCross		=CK_INIT_ORDER;
volatile unsigned int 	maxAnaPPM				=1612;
volatile unsigned int 	minAnaPPM				=583;


volatile unsigned int   PPMvalue;
volatile unsigned long 	PPMcounter;
volatile int 			RC_channel;

//nRF24L01+
int 					msgRole 				=SLAVE_ROLE;
const int 				A_MSG_ORDER_DELAY[]		={T_MSG_ORDER_DELAY};
const char*				A_MSG_ROLE[] 			={T_MSG_ROLE};
const char*				A_MSG_ALERT[]			={T_MSG_ALERT};
const char*				A_MSG_REPORT[]			={T_MSG_REPORT};
const char				A_CDE_REPORT[]			={T_CDE_REPORT};
byte					curReport;
//Radio
const char				A_RC_DIR[] 				={T_RC_DIR};
const char				A_RC_HOOK[]				={T_RC_HOOK};



//Valves
const char* 			A_EV_NAME[]				={T_NAME_EV};
const byte 				A_VALVE_LIST[]			={T_EV};	//Used for sequential test
const byte 				A_WET_LIST[]			={T_WET};
const char* 			A_VALVE_STATUS[]		={"CLOSED","OPENED"};	
const int  				A_BUZZ_FREQ[]			={T_BUZZ_FREQUENCY};	


//JACK
//type_msgJack  			incomJackMessage;
type_msgJack			pileJackMsg[JACK_MSG_PILE];
int 					curPileJack,lastPileJack;

const long 				A_POS_INDEX[]			={T_POS_INDEX};
bool					doForwardJack;


#define MAX_BUF         12      // What is the longest message Arduino can store?
char buffer[MAX_BUF];     // where we store the message until we get a '\n'
int sofar;                // how much is in the buffer
int cnt;
int errorCode;

unsigned long logN=0;



int getInt();

unsigned long			curTime;

class CleanKong{
public:
		 				CleanKong();
	void				hook();								//hook whole robot
	void				free();								//free whole robot
	void				startCleanKong();					//start robot : to be defined
    void                setBrushSpeed(int action=DO_START,
    								  int percent=USE_ACTUAL_SPEED
    								  						);	//set brush speed
	void				spray(int action=DO_START);			//start/stop spraying water
	void				wet(int jack,int action=DO_START);	//start/stop weting jack sucker
	void				dryer(int action=DO_START);			//start/stop drying
	void				regenerate(int action=DO_START);	//start/stop sucking and weting brush
	void				waterChange(int action=DO_START);	//start/stop water change
	void				startClean();						//full sequence to clean
	
	void				fullStop();							//halt	(water cannot flow out of jack: hydraulic circuit locked)
	void				stand();			  				//stand (water can flow out) 
	void				loopValves();						//loop valves in correct order to check if they work
	void				restart();							//restart all
	void				buzz(int buzzCode=0, int duration=BUZZ_ALERT_DURATION);
	void				debug1();							//DEBUG : TESTS TO BE PLACED HERE
	void				debug2();							//DEBUG : TESTS TO BE PLACED HERE
	void				loop();		
	void				processReading();			
	void 				checkUS();							//declenche si traverse detectee
//Valves:
   	void				cdeEV(int _pinEV, int _cde, bool _force=true);
	int					EV_Pin[NB_EV];
	int					EV_State[NB_EV];

	bool				operationPending;

	int 	 			curState,linkedStatus;
	bool				isReadyToOperate=false;
	bool				isAllJackFreed=false;
	bool 				jackReady[NB_JACK];
	int                 isJackLinked[NB_JACK];
	bool 				isForcedAlone;
	int 				seuilUSHaut;					//pour declenchement franchissement
	int 				seuilUSBas;						//pour declenchement franchissement
	unsigned long 		lastUSSampleTime; 				//used to sample no faster then sample rate

	
//public car utilisé dans receive event pour forward des jacks vers le treuil
	byte 				I2C_JackAdr[NB_JACK];
	void				sendI2C(byte _cde, byte _toAdr, int _param);
	void				sendI2C(byte _cde, byte _toAdr, byte _param);


private:
//GETTER
	void				watchDog();
	void				reportState();
	void				displayHelp();
	void				doWetJack(bool _doit);			//Mouillage des verins   (AMO)
	void				doWetRoll(bool _doit);			//Mouillage des rouleaux (BMO)

	
	
private:
	int     			btnBlueState,btnRedState,btnGreenState;
	bool				CK_On;
	bool				isAllHooked;
	bool				isSecuPowerDown;
	bool 				isWettingRoll;
	unsigned long 		startTimeForwardJack;
	unsigned long 		readyTime;
	int 				pwmWet;
	int					curDist;
	int 				pileDist[N_MOYENNE_GLISSANTE];
	int 				curPileDist;

	
	const char* 		version;
	byte	 			I2C_RobotAdr;
	int 		 		brushSpeed;
	long 				curPosIndex;
	bool				watchDogStatus;					//watchdog led status
	bool				isErrorSent;
	char 				c;	

	//RC
	bool				doSendStopMsg;
	bool				isFailurePosition;

	int 				IHMmode; 				
	int 				countUnvalidnRFMsg;

	bool 				isTraverse;
	
	//unsigned long 		buzzStartTime;				//used to autostop buzz after a delay
	unsigned long   	redBtnPressStartTime,blueBtnPressStartTime,greenBtnPressStartTime;
	unsigned long   	frameDetectedTime;
	unsigned long		prevTime,startTime,cycleTime;
	//unsigned long 		startTimeDrench[3];
	unsigned long 		startJackWetTime;
	unsigned long 		startRollWetTime;
	unsigned long		cycleStartTime,cycletime;
	unsigned int		cycleCount;
	int 				Patm[NB_JACK];

	bool 				isUSOn;	
	bool 				isSDUSOn;
	File				myFile;							//data read from US
	int 				nSDFile=0;
	String				USFileName;
	bool 				doOnceRollWetCde;

	
	bool 				isWettingJack;
	unsigned long		startBuzz,buzzDuration;
	bool				isBuzz;

	bool				isBrushOn;
	bool				isAspiOn;
	bool 				isRAZasked;
	bool 				doRestart;

	int                 PPM_BrushSpeed;                 // Brush speed    calculated from the corresponding PPM pulse time
	int                 PPM_HoistSpeed;                 // Brush speed    calculated from the corresponding PPM pulse time
	byte                PPM_Dir;                    	// Clean-Kong Dir calculated form the corresponding PPM pulse time
    byte                PPM_Hook;                  		// Hook/free      calculated form the corresponding PPM pulse time
    byte                PPM_Clean;                 		// Spray On/Off   calculated form the corresponding PPM pulse time
    byte                PPM_Cross;                 		// Spray On/Off   calculated form the corresponding PPM pulse time
	int                 PPM_OldBrushSpeed;              // Brush speed    calculated from the corresponding PPM pulse time
	int                 PPM_OldHoistSpeed;              // Brush speed    calculated from the corresponding PPM pulse time
	byte                PPM_OldDir;                    	// Clean-Kong Dir calculated form the corresponding PPM pulse time
    byte                PPM_OldHook;                   	// Hook/free      calculated form the corresponding PPM pulse time
    byte                PPM_OldClean;                  	// Spray On/Off   calculated form the corresponding PPM pulse time
    byte                PPM_OldCross;                  	// Spray On/Off   calculated form the corresponding PPM pulse time
    //*/
	bool				RC_On;
	
};

#endif

