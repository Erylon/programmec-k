/*********************************************************************** PIN LIST
//*/
#define VERSION "ROBOT 0.1 MEGEI"

//#define JUMPER				27

#define NB_JACK					4
#define PIN_LED 				9 //Watchdog


#define PIN_VIRTUAL_GND			53
#define PIN_VIRTUAL_5V			22

//MOTEURS
#define PIN_MOT_ROULEAU			10
#define PIN_MOT_ASPI			49
#define PIN_MOT_SOUF			47
#define MIN_PWM					100
#define MAX_PWM					255
#define USE_ACTUAL_SPEED		-1

//VERINS
#define PIN_RAZ_VERIN			11
#define RESET_DURATION 			500	//ms


//ULTRASOUND SENSORS
#define PIN_US_TEACH_TOP		5
#define PIN_US_TOP 				A2
#define PIN_US_TEACH_DOWN		18
//#define PIN_US_DOWN 			A1
#define PIN_IR 	 				A5
#define PIN_US_DOWN 			PIN_IR

//WATER TANK
#define PIN_LVL_HIGH_H2O		41
#define PIN_LVL_LOW_H2O			39
#define PIN_LVL_HIGH_WASTE1		37
#define PIN_LVL_LOW_WASTE1		35
#define PIN_LVL_HIGH_WASTE2		33
#define PIN_LVL_LOW_WASTE2		31

//FDC
#define PIN_TOUCH_DOWN			45
#define PIN_TOUCH_UP			43

//ACCELEROMETER
#define PIN_ADXL_X				A12
#define PIN_ADXL_Y				A13
#define PIN_ADXL_Z				A14


//NRF24L01+
#define PIN_NRF_CE				7  //Com treuil
#define PIN_NRF_CSN				8
#define PIN_NRF_IRQ				18

//SECU
#define	PIN_SECU_FEEDBACK		A0

//ROBOT BUTTONS
#define PIN_BTN_NC				28
#define PIN_BTN_UP				26
#define PIN_BTN_DOWN			24
#define PIN_BTN_LED_RED			27
#define PIN_BTN_LED_GREEN		25
#define PIN_BTN_LED_BLUE		23


//VALVES
#define PIN_EPI					48
#define PIN_EPO					46
#define PIN_EVA					44
#define PIN_PS 					42
#define PIN_RRO					40
#define PIN_AMO					36 //NC2 normal AMO=34
#define PIN_BMO					32
#define PIN_CMO					30

//tableau de commande des EV : doit partir de 0, incrémental sans interruption
#define EPI						0
#define EPO						1
#define EVA						2
#define PS 						3
#define RRO						4
#define AMO						5
#define BMO						6
#define CMO						7
#define NB_EV					CMO+1

#define T_NAME_EV				"EPI","EPO","EVA","PS","RRO","AMO","BMO","CMO"
#define T_EV 					PIN_EPI,PIN_EPO,PIN_EVA,PIN_PS,PIN_RRO,PIN_AMO,PIN_BMO,PIN_CMO

//factorisation des verins
#define T_WET					AMO,BMO,CMO
#define JACK_A					0
#define JACK_B					1
#define JACK_C					2
#define JACK_D					3
#define JACK_ALL				-23
#define JACK_MSG_PILE			20

//ADRESSES CARTES VERINS
#define I2C_ADRR_ROBOT			0x8
#define I2C_ADRR_JACK_A			(I2C_ADRR_ROBOT+1)
#define I2C_ADRR_JACK_B			(I2C_ADRR_ROBOT+2)
#define I2C_ADRR_JACK_C			(I2C_ADRR_ROBOT+3)
#define I2C_ADRR_JACK_D			(I2C_ADRR_ROBOT+4)

#define DO_OPEN					HIGH	
#define DO_CLOSE				LOW	
#define DO_START				DO_OPEN	
#define DO_STOP					DO_CLOSE	
#define DO_LED_OFF	     		LOW  //LED switch OFF
#define DO_LED_ON              	HIGH //LED switch ON

//ETATS
#define INITIALIAZING			0
#define ROBOT_LINKED			1	
#define ROBOT_FREED				2	
#define MOVING_DOWN				4	
#define MOVING_UP				8	
#define CHANGING_WATER			16
#define FAILING					256	

#define JACK_FREED				0
#define JACK_LINKED				1
#define JACK_UNDETERMINED		-1
#define ALL_JACK_LINKED			7 //1+2+4
#define TWO_JACK_LINKED			3 //1+2+4
#define ALL_JACK_FREED			0

#define FORCE_LINK				1
#define FORCE_FREE				2

//TEMPOS
#define PURGE_DURATION			2000	//ms
#define LONG_PRESS_DURATION		3000	//ms
#define WATCHDOG_PULSE			250		//ms
#define SPRAY_PERIOD			10 		//ms
#define US_SAMPLE_DELAY		 	5 		//ms
#define JACK_WET_DURATION		2000	//ms
#define ROLL_WET_DURATION		1000	//ms
#define FORWARD_DELAY			100		//ms
#define INIT_WET_ROLL_PWM		32		//max 255

//BUZZER
#define PIN_BUZZER				12
#define BUZZ_ALERT_DURATION 	4000	//ms
#define BUZZ_INIT_DURATION 		500	//ms


//BUZZER SOUND FREQUENCY
#define NO_BUZZ_FREQ			0
#define BUZZ_INIT_FREQ			2000
#define BUZZ_END_FREQ			3000
#define BUZZ_ERROR_FREQ			100
#define BUZZ_TRAVERSE_FREQ		4000

//tableau des frequences buzzer : doit partir de 0, incrémental sans interruption
#define NO_BUZZ					0
#define BUZZ_INIT				1
#define BUZZ_END				2
#define BUZZ_ERROR				3
#define BUZZ_TRAVERSE 			4
#define T_BUZZ_FREQUENCY 		NO_BUZZ_FREQ,BUZZ_INIT_FREQ,BUZZ_END_FREQ,BUZZ_ERROR_FREQ,BUZZ_TRAVERSE_FREQ


//RC
#define PIN_IRQ_RC_IN         	3

//RC COMMAND
//#define RCCONTROL
#define multiplier 				(F_CPU/8000000)
#define RC_ANA_STEP				2

//RC HANNELS
#define RC_CHANNEL_HOOK			1
#define RC_CHANNEL_DIR			2
#define RC_CHANNEL_HOIST_SPEED	3
#define RC_CHANNEL_BRUSH_SPEED	4
#define RC_CHANNEL_CROSS		5
#define RC_CHANNEL_CLEAN		6

//RC COMMANDS
#define CK_STOP			        0
#define CK_DOWNWARD	            1
#define CK_UPWARD          		2
#define CK_CLEAN          		2
#define CK_FREE		          	1
#define CK_HOOK		          	2
#define CK_CROSS		        2
#define CK_NO_COMMAND	        0
#define CK_INIT_ORDER	        -1

//SEUILS
#define US_TOP_TRIG			 	330
#define US_DOWN_TRIG		 	300
#define US_TOP_DOWN_TRIG_RANGE	10

#define US_TOP 					0
#define US_DOWN					1
#define T_US_TOP				PIN_US_TOP,PIN_US_TEACH_TOP,US_TOP_TRIG
#define T_US_DOWN				PIN_US_DOWN,PIN_US_TEACH_DOWN,US_DOWN_TRIG
#define N_MOYENNE_GLISSANTE 	10

//INDEX JACK
#define INDEX_UP_JACK_A			1000
#define INDEX_UP_JACK_B			2000
#define INDEX_UP_JACK_C			3000
#define INDEX_UP_JACK_D			4000
#define INDEX_DOWN_JACK_A		1500
#define INDEX_DOWN_JACK_B		2500
#define INDEX_DOWN_JACK_C		3500
#define INDEX_DOWN_JACK_D		4500
//#define T_POS_INDEX				INDEX_UP_JACK_A,INDEX_UP_JACK_B,INDEX_UP_JACK_C,INDEX_DOWN_JACK_A,INDEX_DOWN_JACK_B,INDEX_DOWN_JACK_C
#define T_POS_INDEX				INDEX_UP_JACK_A,INDEX_UP_JACK_B,INDEX_UP_JACK_C,INDEX_UP_JACK_C,INDEX_DOWN_JACK_A,INDEX_DOWN_JACK_B,INDEX_DOWN_JACK_C,INDEX_DOWN_JACK_D

//DIALOG
#define DIALOG_JACK				0
#define DIALOG_HOIST			1
#define DIALOG_RC				2

#define T_RC_DIR				CDE_STOP_BRAKE,CDE_MOVE_DOWN,CDE_MOVE_UP
#define T_RC_HOOK               NO_CDE,CDE_ERASE_ERROR,CDE_DEEP_CATCH

