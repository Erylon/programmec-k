#define VERSION "TREUIL BASF 0.1 Dev"



//Constantes
#define MAX_LOAD				900.		//valeur de la charge max /!\ valeur
#define MIN_LOAD				300.		//valeur de la charge min 
#define MAX_LOAD_FREE_MOVE		200. //150.			//valeur de la charge max sur treuil sans robot /!\ valeur
#define	D_POULIE				90 			// En mm
#define D_CABLE					3			// En mm
#define NB_PAS  				360			//Pour le codeur
#define TIMEOUT					10000		//temps entre deux tours de roues codeuses dépassé donc arret
#define WATCHDOG_PULSE			1000			//ms
#define RESOL_Z					200			//affiche le Z tous les RESOL_Z points

#define MODE_INIT				0
#define MODE_NOLOAD				1
#define MODE_MANUAL				2
#define MODE_CLEANING			3
#define MODE_CLEANING_SLEEP		4


#define BUZZ_ALERT_DURATION 	4000		//ms
#define BUZZ_INIT_DURATION 		500			//ms

//VITESSE
#define NB_POINT_VITESSE		500
#define FORCE_DISPLAY			true

//BUZZER SOUND FREQUENCY
#define NO_BUZZ_FREQ			0
#define BUZZ_INIT_FREQ			2000 //2000
#define BUZZ_END_FREQ			3000
#define BUZZ_ERROR_FREQ			1000
#define BUZZ_UNDERLOAD_FREQ		1000
#define BUZZ_OVERLOAD_FREQ		700

//tableau des frequences buzzer : doit partir de 0, incrémental sans interruption
#define NO_BUZZ					0
#define BUZZ_INIT				1
#define BUZZ_END				2
#define BUZZ_ERROR				3
#define BUZZ_UNDERLOAD 			4
#define BUZZ_OVERLOAD			5

//Adresse des pins 
#define PIN_VOK     			2     // Broche 2
#define PIN_HSER_BTN			8     // Broche 8
#define PIN_ZSH     			22    // Broche 22
#define PIN_INDEX   			23    // Broche 23
#define PIN_WAL     			24    // Broche 24
#define PIN_VOIE_A  			25    // Broche 25
#define PIN_HSRST   			26    // Broche 26
#define PIN_VOIE_B  			27    // Broche 27
#define PIN_HSMU_BTN			30    // Broche 30
#define PIN_HSMD_BTN			32    // Broche 32
#define PIN_HSS_BTN 			34    // Broche 34
#define PIN_HSCL_BTN			36    // Broche 36
#define PIN_HSMA_BTN			38    // Broche 38
#define PIN_HSHB_BTN			40    // Broche 40
#define PIN_HSNL_BTN			44    // Broche 44
#define PIN_HSHA_BTN			46    // Broche 46

// Configuration des broches de sortie
#define PIN_HSHB_LED			3     // Broche 3
#define PIN_HSHA_LED			4     // Broche 4
#define PIN_HSNL_LED			5     // Broche 5
#define PIN_HSMA_LED			6     // Broche 6
#define PIN_HSMU_LED			7     // Broche 7
#define PIN_BUZZER  			12    // Broche 12
#define PIN_HSVC    			28    // Broche 28
#define PIN_ZIH     			29    // Broche 29
#define PIN_WIH     			31    // Broche 31
#define PIN_WIL     			33    // Broche 33
#define PIN_NIR     			35    // Broche 35
#define PIN_SCL     			37    // Broche 37
#define PIN_SCH     			39    // Broche 39
#define PIN_ZCDN    			41    // Broche 41
#define PIN_HSMD_LED			42    // Broche 42
#define PIN_ZCUP    			43    // Broche 43
#define PIN_LED     			45    // Broche 45
#define PIN_HSS_LED 			48    // Broche 48
#define PIN_HSCL_LED			50    // Broche 50
#define PIN_HSER_LED			52    // Broche 52

#define TREUIL_ENABLE			53    // Broche 53
#define TREUIL_DI    			16    // Broche 16
#define TREUIL_RO    			17    // Broche 17

#define PIN_RS485_DE 		2
#define PIN_RS485_RE 		3




/*
#define PIN_LOVERLOAD			39
#define PIN_LUNDERLOAD			41
#define PIN_LHAUT				43		
#define PIN_FDC_HAUT			29		
#define PIN_LUP					47	
#define PIN_LDOWN				45	
#define PIN_CW					37	
#define PIN_CCW					35	
#define PIN_BTN_DESCENTE		25
#define PIN_BTN_MONTEE			23	
#define PIN_BUZZER				9
#define PIN_VAR_VIT				6
#ifdef 	VIRTUAL_HOIST
	#define PIN_MODE_ROBOT		3
#else
	#define PIN_MODE_ROBOT		27	
#endif
#define PIN_WATCHDOG			5			
#define PIN_VOIE_A				2 			//cable vert
#define PIN_VOIE_B 				4 			//cable gris
#define PIN_IDX 				3			//cable bleu	
#define PIN_DATA_LOAD			20			//I²C
#define	PIN_CLOCK_LOAD			21			//I²C
#define PIN_POWERED				A0

#define PIN_IRQ_RC_IN			19

#define BTN_MOVE_DOWN			0
#define BTN_MOVE_UP				1
#define BTN_MANUAL_MODE			2
#define NB_BTN					3
#define T_BTN					PIN_BTN_DESCENTE,PIN_BTN_MONTEE,PIN_MODE_ROBOT
#define NB_ANTIBOUNCE			6
//*/

//Jauge
#define SCALE_MULTIPLIER 		17391.//4560.//2280.//57740.	//Facteur de conversion de la Jauge
#define SCALE_TARE 				0. //52
#define SCALE_A 				0.0002377//7.521E-5//6.025E-5
#define SCALE_B					-79.337//-74.611

/*
output		20mv/V
gain		128	
adc range	1024	
excitation	5V
cell range	45,4kg
Vcc 		5V
		
scale	57740,9691629956	

scale = (output * gain * adc_range * excitation)/(cell_range * vcc)
where:
output = 	the output rating of the load cell (mV/V). Should be in volts, if your final result looks wrong see if it is off by some factor of 10 to see if you forgot to account for the unit changes.
gain = 		the gain setting of the amplifier
adc_range = the range of values that the adc has (precision). For this 24 bit adc this is (2^24)-1= 16777215
excitation =the input voltage for the load cell. For Sparkfun's breakout board this is equal to vcc and the two values cancel out.
cell_range =the max rating for the load cell in whichever units you are using. I was using a load cell rated for 50KG, so to get an output in kilograms I would use the value 50 here.
vcc = 		the system voltage for the amplifier


//*/

//Mode de control
#define USER_CONTROL			0
#define ROBOT_CONTROL			1

//Ordre
#define STOP_BRAKE				0			//"STOP" deja pris		
#define MOVE_UP					1			
#define MOVE_DOWN				2			
#define SECU					3			
#define ON 						HIGH
#define OFF 					LOW

//Etat
#define NO_ALARM				0
#define	ALARM_OVERLOAD			1
#define	ALARM_UNDERLOAD			2
#define ALARM_FDC_HAUT			3
#define INIT_STATE				4

#define CCW 					PIN_CCW	
#define	CW 						PIN_CW

#define TRAV_SET				0
#define TRAV_BAS				1
#define TRAV_HAUT				2
#define NOT_SET					0
#define TRAV_SET_BAS			1
#define TRAV_SET_HAUT			2
#define TRAV_SET_FULL			3
#define MAX_TRAVERSE			25 //taille du tableau
#define MIN_TRAVERSE_THICKNESS 	5

//MEGEI
#define T_INIT_TRAVERSE         {TRAV_SET_FULL,1811,1883},\
								{TRAV_SET_FULL,3591,3788},\
								{TRAV_SET_FULL,5800,5934},\
								{TRAV_SET_FULL,7970,8148},\
								{TRAV_SET_FULL,9849,9920},\
								{TRAV_SET_FULL,11628,11826},\
								{TRAV_SET_FULL,13838,13971},\
								{TRAV_SET_FULL,16008,16186},\
								{TRAV_SET_FULL,17886,17958},\
								{TRAV_SET_FULL,19666,19864},\
								{TRAV_SET_FULL,21875,22009},\
								{TRAV_SET_FULL,24045,24223},\
								{TRAV_SET_FULL,25924,25996},\
								{TRAV_SET_FULL,27703,27901},\
								{TRAV_SET_FULL,29913,30046},\
								{TRAV_SET_FULL,32083,32261},\
								{TRAV_SET_FULL,33961,34033},\
								{TRAV_SET_FULL,35741,35939},\
								{TRAV_SET_FULL,37951,38084},\
								{TRAV_SET_FULL,40121,40299},\
								{TRAV_SET_FULL,41999,42071}//,{NOT_SET,,} //tableau des traverses mémorisées
#define T_INIT_TRAVERSE_NB		18

//*/

/*DUHEM
#define T_INIT_TRAVERSE         {TRAV_SET_FULL,(long)(-500/MM_PAR_POINT),(long)(80/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(1580/MM_PAR_POINT),(long)(1720/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(3220/MM_PAR_POINT),(long)(3360/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(4860/MM_PAR_POINT),(long)(5000/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(6500/MM_PAR_POINT),(long)(6640/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(8140/MM_PAR_POINT),(long)(8280/MM_PAR_POINT)},\
								{TRAV_SET_FULL,(long)(-116/MM_PAR_POINT),(long)(-116/MM_PAR_POINT)},\
								{TRAV_SET_FULL,41999,42071}//,{NOT_SET,,} //tableau des traverses mémorisées
#define T_INIT_TRAVERSE_NB		6
//*/




//CONVERSION
#define MM_PAR_POINT			0.40460 //0.3930332 //((D_POULIE+D_CABLE)*3.141592653589794/2*NB_PAS)
#define TEMPS_CYCLE_SIMU        250

#define REPERE					162
//prendre la valeur en mm et diviser par MM_par_point
#define SUCK_A_INDEX			(long)(((REPERE)+315+315)/MM_PAR_POINT)//361//position en point du bas de A par rapport a l'US bas//98 POUR VENTOUSE Ø75MM
#define SUCK_B_INDEX 			(long)(((REPERE)+315)/MM_PAR_POINT)//1512//
#define SUCK_C_INDEX			(long)((REPERE)/MM_PAR_POINT)//2105//
#define SUCK_D_INDEX			(long)((REPERE)/MM_PAR_POINT)//2105//
#define SUCK_E_INDEX			(long)((REPERE)/MM_PAR_POINT)//2105//
//#define SUCK_D_INDEX			100000000
#define SUCK_SIZE				(long)(110/MM_PAR_POINT)//Grosse:110 Petite:75
#define TRAV_MARGIN 			5 //	en point      5=2mm
#define TRAV_MARGIN_FREE		150 //	en point  	100=40mm
#define TRAV_MARGIN_CATCH		10 // 	en point 	 10=4mm

//SEQUENCE
#define SEQUENCE_START			1
#define SEQUENCE_STOP			0


#define LINKED					1
#define FREED					0	

#define T_DIR					"STOP","Move UP","Move DOWN","MOVE ERROR"
#define T_CONTROL				"MODE MANUEL","MODE ROBOT"
#define T_TRAV					"NOT SET","BAS","HAUT"
#define T_ALARM_PIN				NO_ALARM,PIN_LOVERLOAD,PIN_LUNDERLOAD,PIN_LHAUT
#define T_BUZZ_FREQUENCY 		NO_BUZZ_FREQ,BUZZ_INIT_FREQ,BUZZ_END_FREQ,BUZZ_ERROR_FREQ,BUZZ_UNDERLOAD_FREQ,BUZZ_OVERLOAD_FREQ


/*#define MAX_SPEED  				255
#define DUTY_STEP 				10
#define LOAD_ALARM_TIMER 		300			//ms
#define	FREQ_COMM				500
#define	SPEED					40			//mm/s
#define	SPEED_THRESHOLD			10	
#define	FIN_BAT					8000		// En mm Fin du batiment (limite la descente)
//*/