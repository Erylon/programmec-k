#ifndef HoistBoard_h
#define HoistBoard_h

#include <arduino.h>
//#include <Timer.h>
#include <math.h>
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteTreuil\HoistBoardPinList.h" 
//#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRF24\ProtocoleRF24.cpp"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleSerial\ProtocolSerial.cpp"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleVerinRobot.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleTreuilRobot.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ErrorList.h" 
#include <SPI.h> 					// gestion du bus SPI
#include <HX711.h>					//Jauge
#include <arduino.h>
#include <TimerOne.h> 


//RC
void		 			readPPM();							//calcul les temps
int 					PPMConvertAnalog(unsigned int value);
int 					PPMConvert3StateSwitch(unsigned int value, int retLOW,int retMID,int retHIGH);
int 					PPMConvert2StateSwitch(unsigned int value, int retLOW,int retHIGH);
volatile int          	RC_HoistSpeed;
volatile int          	RC_BrushSpeed;
volatile int            RC_OrderDir;
volatile int            RC_OrderCatch;
volatile int            RC_OrderClean;
volatile int            RC_OrderCross;
volatile int          	RC_OldHoistSpeed		=0;
volatile int          	RC_OldBrushSpeed		=0;
volatile int            RC_OldOrderDir			=CK_INIT_ORDER;
volatile int            RC_OldOrderCatch			=CK_INIT_ORDER;
volatile int            RC_OldOrderClean		=CK_INIT_ORDER;
volatile int            RC_OldOrderCross		=CK_INIT_ORDER;
volatile unsigned int 	maxAnaPPM				=1612;
volatile unsigned int 	minAnaPPM				=583;


volatile unsigned int   PPMvalue;
volatile unsigned long 	PPMcounter;
volatile int 			RC_channel;

unsigned long logN=0;

const int  				A_BUZZ_FREQ[]			={T_BUZZ_FREQUENCY};	
const int 				A_ALARM_PIN[]			={T_ALARM_PIN};
const char* 			A_DIR_NAME[]			={T_DIR};
const char* 			A_TRAV_NAME[]			={T_TRAV};
const char				A_RC_DIR[] 				={T_RC_DIR};
const char				A_RC_CATCH[]			={T_RC_CATCH};
const char*				A_MSG_REPORT[]			={T_MSG_REPORT};

void 					codeurGetPos();
void 					codeurGetRpm();
	
//nRF24L01+
int 					msgRole 				=MASTER_ROLE;
const int 				A_MSG_ORDER_DELAY[]		={T_MSG_ORDER_DELAY};
const char*				A_MSG_ROLE[] 			={T_MSG_ROLE};
const char*				A_MSG_ALERT[]			={T_MSG_ALERT};


const char*				A_MODE[]				={T_CONTROL};
const int 				A_PIN_BTN[]				={T_BTN};
const long 				A_INIT_TRAVERSE[][3]	={T_INIT_TRAVERSE};

#define MAX_BUF         12      // What is the longest message Arduino can store?
char buffer[MAX_BUF];     		// where we store the message until we get a '\n'
int sofar;                		// how much is in the buffer
int cnt;
int errorCode;
unsigned long			curTime;

long getInt();


class Hoist {
public:

						Hoist();		
	void				loop();
	void				move(int _dir);						//declenche le mouvement dans le sens demandé
	void				reportAlarm(int _alarmCode, bool _isAlarm, int duration=BUZZ_ALERT_DURATION);
	void				setTargetSpeed(int _speed);
	void 				displayHoistMsg();
	void 				displayHelp();
	void 				restart();
	void 				doSequence(int _do=0);
	void 				displayTraverse();
	void 				addTraverse(long _pos);
	void				checkGauge();
	void				checkFdc();
	void 				printIndex();
	void 				goAbsoluteMM(int _targetMM);
	void				displaySpeed(bool _forceDisplay=false);

//	void				getSpeed();

	const char* 		version;
	int 				curSpeed; 							//coefficient pour faire varier la vitesse [0-255]->[0-100%]
	int 				targetSpeed; 						//coefficient pour faire varier la vitesse [0-255]->[0-100%]
	bool 				isFdCHaut;
	long 				curPosIndex;						//position du robot en points +1000
	long 				startSpeedIndex;
	//long				prevPosIndex;						//position précédente
//	bool 				newHoistMsg;
	long 				curLoad;
	int					errorCode;
	int 				controlMode;
	int 				moveDir;
	int 				targetDir;
	long 				targetGo;
	bool 				isGoMode;
	//bool				btnMonteeState;
	//bool				btnDescenteState;
	int 				nbTour;								// nombre de tour réalisé par le codeur 
//	int 				countUnvalidnRFMsg;
	HX711* 				scale;
	int 				alarmState;
	int 				oldAlarmState;
	bool				isUnloadMove;
	bool 				prevUnloadMove;
	bool				doDisplayLoad;
	bool				doDisplaySpeed;
	bool				isFirstTimeFdcAlert;
	bool				isTempOverload;
	bool				isTempUnderload;
	unsigned long       loadAlarmStartTime;

	bool				oldCW;

	long 				maxLoad;
	long 				minLoad;
	long 				maxLoadFreeMove;
//	unsigned long		buzzStartTime;						//used to autostop buzz after a delay



private : 
	void				watchDog();
	void 				readButtons(); 						//traite l'antibounce
	void 				resetTraverse();
	void 				buzz(int buzzCode=0, int duration=BUZZ_ALERT_DURATION);
	bool 				isBtnPressed[NB_BTN];
    byte 				nbTimeBtnPressed[NB_BTN];
    bool 				passedBtnState[NB_BTN];
    bool				isRestart;
    //bool				isRadioOn;

	char 				c;									//character read from aerial input RC
	bool				watchDogStatus;
	unsigned long		prevTime,prevRF24MsgTime,startMoveTime,cycleTime,startSpeedTime;
	unsigned long 		startSimuMicroS;
	#ifdef MESURE_GAUGE_CYCLE
		unsigned long 	gaugeTime,cumulatedGaugeTime;
	#endif
	int 				gaugeCycle;
	unsigned long		startBuzz,buzzDuration;
	bool				isBuzz;

	bool				isSimu;
	bool				doSendStopMsg;
	bool				isFailurePosition;
	bool				isMoving;
	bool				isPowered;
	bool				isStartAutocatch;
	bool 				prevIsPowered;
	long 				suckTopPos[3];
	int 				simuSpeed;						//dm/min  ex : 130 = 13m/min
	long 				suckBottomPos[3];
	byte 				suckState[3];
	int 				Patm[3];
	long 				traverse[MAX_TRAVERSE][3];   //i,[indexTop/indexDown/set]
	long 				curTraverse;
	long 				topMargin,botMargin;

	int                 PPM_BrushSpeed;                 // Brush speed    calculated from the corresponding PPM pulse time
	int                 PPM_HoistSpeed;                 // Brush speed    calculated from the corresponding PPM pulse time
	byte                PPM_Dir;                    	// Clean-Kong Dir calculated form the corresponding PPM pulse time
    byte                PPM_Catch;                  		// Catch/free      calculated form the corresponding PPM pulse time
    byte                PPM_Clean;                 		// Spray On/Off   calculated form the corresponding PPM pulse time
    byte                PPM_Cross;                 		// Spray On/Off   calculated form the corresponding PPM pulse time
	int                 PPM_OldBrushSpeed;              // Brush speed    calculated from the corresponding PPM pulse time
	int                 PPM_OldHoistSpeed;              // Brush speed    calculated from the corresponding PPM pulse time
	byte                PPM_OldDir;                    	// Clean-Kong Dir calculated form the corresponding PPM pulse time
    byte                PPM_OldCatch;                   	// Catch/free      calculated form the corresponding PPM pulse time
    byte                PPM_OldClean;                  	// Spray On/Off   calculated form the corresponding PPM pulse time
    byte                PPM_OldCross;                  	// Spray On/Off   calculated form the corresponding PPM pulse time
    //*/
	bool				RC_On;
};


#endif