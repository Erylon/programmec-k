#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteTreuil\HoistBoard.h"


extern Hoist *oHoist;

long getInt(){
	Serial.println("Please set value");
	while (Serial.available()==0) {}
	return Serial.parseInt();
}

void codeurGetPos(){
	int a,b;
	a = digitalRead(PIN_VOIE_A);
	b = digitalRead(PIN_VOIE_B);
	//oHoist->prevPosIndex=oHoist->curPosIndex;
	if (a==b){
		oHoist->curPosIndex--;
	}
	else if(a!=b)   {
		oHoist->curPosIndex++;
    }
    if((oHoist->curPosIndex%RESOL_Z)==0){//Affiche la position tous les 50 points
    	Serial.print("Index=");
    	Serial.print(oHoist->curPosIndex);	
    	Serial.print("\t");Serial.print(MM_PAR_POINT*oHoist->curPosIndex);Serial.println("mm");
    }
    oHoist->displaySpeed();
    //Serial.print("new Pos=");
    //Serial.print(oHoist->curPosIndex);
    /*
    if(((oHoist->moveDir==MOVE_UP)||(oHoist->moveDir==MOVE_DOWN))&&(oHoist->prevPos==oHoist->curPos)){
        oHoist->errorCode=ERR_NO_ENCODEUR;        //on déclare erreur si le treuil monte ou descend et que le codeur ne change pas d'état
    }
    if((oHoist->moveDir==STOP)&&(oHoist->prevPos!=oHoist->curPos)){
        oHoist->errorCode=ERR_NO_STOP;            //on déclare erreur si le treuil est en stop et que le codeur change d'etat
    }
    //*/
}

Hoist::Hoist() {//Constructeur
	version=VERSION;
	Serial.println(version);

//	pinMode(PIN_CW, OUTPUT);
//    pinMode(PIN_CCW, OUTPUT);

    pinMode(PIN_VAR_VIT,  OUTPUT); 
	pinMode(PIN_LOVERLOAD, OUTPUT);
	pinMode(PIN_LUNDERLOAD, OUTPUT);
	pinMode(PIN_LHAUT, OUTPUT);
	pinMode(PIN_LDOWN, OUTPUT);
	pinMode(PIN_LUP, OUTPUT);
	pinMode(PIN_BUZZER,  OUTPUT);
	pinMode(PIN_WATCHDOG, OUTPUT);
   	pinMode(PIN_BTN_DESCENTE, INPUT);
    pinMode(PIN_BTN_MONTEE, INPUT);
	pinMode(PIN_FDC_HAUT, INPUT);
	pinMode(PIN_MODE_ROBOT, INPUT);
	pinMode(PIN_VOIE_A,INPUT);
	pinMode(PIN_VOIE_B, INPUT); 			
 	pinMode(PIN_IDX, INPUT);
 	pinMode(PIN_POWERED,INPUT);
	pinMode(PIN_IRQ_RC_IN, INPUT);     								//set the pin to input
	digitalWrite(PIN_IRQ_RC_IN, HIGH); 								//use the internal pullup resistor
	//attachInterrupt(digitalPinToInterrupt(PIN_IRQ_RC_IN), readPPM, CHANGE); 			// attach a PinChange Interrupt to our pin on the rising edge
	digitalWrite(PIN_BUZZER,OFF);
	digitalWrite(PIN_LUNDERLOAD, OFF);
	digitalWrite(PIN_LOVERLOAD, OFF);
	digitalWrite(PIN_LDOWN, OFF);
	digitalWrite(PIN_LUP, OFF);

	initProtocol();
	Serial.print("If help not displayed below : Gauge not connected, or VIRTUAL_GAUGE not set");

	#ifndef VIRTUAL_GAUGE
		scale=new HX711(PIN_DATA_LOAD, PIN_CLOCK_LOAD);
		delay(200); //otherwise bootpb
		scale->set_scale(SCALE_MULTIPLIER); // permet de convertir les données en donées lisibles 
		scale->set_gain(32);
	  	//scale->tare();
	#endif
	
	curPosIndex=0;
	attachInterrupt(digitalPinToInterrupt(PIN_VOIE_A), codeurGetPos, CHANGE);  // detection des fronts descendant
	//attachInterrupt(digitalPinToInterrupt(PIN_IDX),    codeurGetRpm, CHANGE);
	//Serial.println("la");

	watchDogStatus=true;
	digitalWrite(PIN_WATCHDOG,watchDogStatus);

	suckBottomPos[0]=SUCK_A_INDEX-TRAV_MARGIN;
	suckBottomPos[1]=SUCK_B_INDEX-TRAV_MARGIN;
	suckBottomPos[2]=SUCK_C_INDEX-TRAV_MARGIN;

	restart();
}

void Hoist::restart(){
	simuSpeed=130;
	isRestart=true;
	isFirstTimeFdcAlert=false;
	isFailurePosition=false;
	isStartAutocatch=false;
	doDisplaySpeed=false;
	gaugeCycle=0;
	startSpeedTime=-40000000;
	#ifdef MESURE_GAUGE_CYCLE
		cumulatedGaugeTime=0;
	#endif
	isFdCHaut=false;
	isUnloadMove=false;
	alarmState=NO_ALARM;
	oldAlarmState=NO_ALARM;
	prevIsPowered=false;
	doDisplayLoad=false;
	maxLoad=MAX_LOAD;
	minLoad=MIN_LOAD;
	isBuzz=false;
	isGoMode=false;
	isTempUnderload=false;
	isTempOverload=false;
	isOtherReady=false;
	isSimu=false;
	maxLoadFreeMove=MAX_LOAD_FREE_MOVE;
	displayHelp();
	initProtocol();

	//init button read with antibounce
	for(int i=0;i<NB_BTN;i++){
		nbTimeBtnPressed[i]=0;
		isBtnPressed[i]=!digitalRead(A_PIN_BTN[i]);
		passedBtnState[i]=isBtnPressed[i];
	}
	for(int i=0;i<NB_ANTIBOUNCE;i++){
		readButtons();
	}

	for(int i=0;i<3;i++){
		suckTopPos[i]=suckBottomPos[i]+SUCK_SIZE+2*TRAV_MARGIN;
		suckState[i]=FREED;
		Serial.print("suckBottomPos[");Serial.print(i);Serial.print("]=");Serial.print(suckBottomPos[i]);
		Serial.print("suckTopPos[");Serial.print(i);Serial.print("]=");Serial.println(suckTopPos[i]);
	}

	targetDir= SECU;
	moveDir = SECU;
	//move(STOP_BRAKE);

	
	curPosIndex=0;
	//prevPosIndex=0;
	nbTour=0;
	targetSpeed=0;
	setTargetSpeed(MAX_SPEED);
	controlMode=digitalRead(PIN_MODE_ROBOT);
	
	//traverse
	resetTraverse();

	readPPM();
	RC_OldOrderDir=RC_OrderDir;
	RC_OldOrderCatch=RC_OrderCatch;
	RC_OldOrderClean=RC_OrderClean;
	RC_OldOrderCross=RC_OrderCross;
	///////
	RC_OldHoistSpeed=RC_HoistSpeed;
	RC_OldBrushSpeed=RC_BrushSpeed;

	//bip init
	buzz(BUZZ_INIT,BUZZ_INIT_DURATION);
	delay(BUZZ_INIT_DURATION);

  	Serial.println(A_MODE[controlMode]);
  	Serial.println("Init done. Waiting for orders...");

	//time
	prevTime=millis();
	prevRF24MsgTime=curTime;
	cycleTime=prevTime;
}


void Hoist::displaySpeed(bool _forceDisplay){
    if((doDisplaySpeed && abs(curPosIndex-startSpeedIndex)>NB_POINT_VITESSE)||_forceDisplay){
    	Serial.print("Vitesse(m/min)=");Serial.println((float)(MM_PAR_POINT*abs(curPosIndex-startSpeedIndex)/((curTime-startSpeedTime)/60.)));
    	//Serial.print("distance(mm)=");Serial.println((long)(MM_PAR_POINT*(curPosIndex-startSpeedIndex)));
    	//Serial.print("temps(s)=");Serial.println((long)(curTime-startSpeedTime)/1000);
    	
    	startSpeedIndex=curPosIndex;
    	startSpeedTime=curTime;
    }//*/
}

void Hoist::resetTraverse(){
	for(int i=0;i<MAX_TRAVERSE;i++){
		traverse[i][TRAV_SET]=NOT_SET;
	}
	curTraverse=0;
	for(int i=0;i<T_INIT_TRAVERSE_NB;i++){
		traverse[i][TRAV_SET]=A_INIT_TRAVERSE[i][TRAV_SET];
		traverse[i][TRAV_SET_BAS]=A_INIT_TRAVERSE[i][TRAV_SET_BAS];
		traverse[i][TRAV_SET_HAUT]=A_INIT_TRAVERSE[i][TRAV_SET_HAUT];
	}
}

void Hoist::buzz(int buzzCode, int duration){
	if(buzzCode==NO_BUZZ){
		noTone(PIN_BUZZER);
		isBuzz=false;
		//Serial.println("no Buzz");
	}
	else {
		isBuzz=true;
		startBuzz=curTime;
		buzzDuration=duration;
		tone(PIN_BUZZER,A_BUZZ_FREQ[buzzCode]);
		//Serial.println("Buzz");
	}
} 

void Hoist::displayHelp(){
	Serial.println();				// Start writing from a new clean line
	Serial.print("Hoist\t");Serial.println(version);
	Serial.println("HOIST Commands:");
	Serial.println("--------------------------------------------");
	Serial.println("CDE_MOVE_UP\t\tU");
	Serial.println("CDE_CATCH_JACK\t\tC");
	Serial.println("CDE_MOVE_DOWN\t\tD");
	Serial.println("CDE_FREE_JACK\t\tF");
	Serial.println("CDE_MISTING\t\tG (mistinG) [0,1]");
	Serial.println("CDE_INVERSE_VALVE\tI param (ex I20->EVA=0) :EPI=0,EPO=1,EVA=2,PS=3,RRO=4,AMO=5,BMO=6,CMO=7");
	Serial.println("CDE_THIS_INFO\t\tH (tHis)");
	Serial.println("CDE_US_BOTTOM\t\tJ report US");
	Serial.println("CDE_START_REC_US\tK start recording US");
	Serial.println("CDE_STOP_REC_US\t\tk stop recording US");
	Serial.println("CDE_DISPLAY_GAUGE\tL");
	Serial.println("CDE_MICROFIBRE\t\tMoteur [0,1]"); 
	Serial.println("CDE_GET_POS_INDEX\tN (iNdex)");
	Serial.println("CDE_DISPLAY_PILE\tP");
	Serial.println("CDE_DISPLAY_SPEED\tQ (Quick)");
	Serial.println("CDE_RACCROCHE\t\tR");
	Serial.println("CDE_STOP_BRAKE\t\tS + MAJ Patm Ventouses");
	Serial.println("CDE_DECLENCHEMENT\tT (Trig)");
	Serial.println("CDE_WET_JACK\t\tW:on, w:off, A:JACK, B:ROLL. ex WB:start wet roll");
	Serial.println("CDE_ERASE_ERROR\t\tX");
	Serial.println("CDE_READY\t\tY (readY)");
	Serial.println("CDE_RAZ\t\t\tZ [''=hoist,R=Robot,J=robot and jacks");
	Serial.println("CDE_INC_SPEED\t\t+");
	Serial.println("CDE_DEC_SPEED\t\t-");
	Serial.println("CDE_DISPLAY_TRAVERSE\t=");
	Serial.println("CDE_MODE_A_VIDE\t\t0");
	Serial.println("CDE_INVALID\t\t![G,R]");
	Serial.println("CDE_PING\t\t|");
	Serial.println("CDE_REPORT_JACK\t\t@");
	Serial.println("CDE_SIMU\t\t& demarre une simu depuis l'offset[O] vers la target[G,R] a vitesse=[V]");
	Serial.println("CDE_ERASE_PILE\t\t/");
	Serial.println("CDE_SET_PARAM\t\t* exemple : *_200 : seuil haut US=200");
	Serial.println("\t\t\tCDE_US_SEUIL_HAUT\t^[max 1024]");
	Serial.println("\t\t\tCDE_US_SEUIL_BAS\t_ [min 0]");
	Serial.println("\t\t\tCDE_GO_ABSOLUTE\t\tG [mm]");
	Serial.println("\t\t\tCDE_GO_RELATIVE\t\tR [mm]");
	Serial.println("\t\t\tPARAM_JAUGE_SEUIL_HAUT\tL");
	Serial.println("\t\t\tPARAM_JAUGE_SEUIL_BAS\tl");
	Serial.println("\t\t\tPARAM_JAUGE_MULTIPLIER\tM [min 0]");
	Serial.println("\t\t\tCDE_SET_OFFSET\t\tO");
	Serial.println("\t\t\tPARAM_SIMU_SPEED\tV");
	Serial.println("\t\t\tCDE_SET_PARAM_PULSE\t% [0 255]");
	Serial.println("\t\t\tCDE_SET_PARAM_FILE\t$ US file Id");
	Serial.println("--------------------------------------------");
	Serial.println("Usage : Command");
	Serial.println("?\t\t\tDisplay this help");
}


void Hoist::reportAlarm(int _alarmCode, bool _isAlarm, int _duration){
	digitalWrite(A_ALARM_PIN[_alarmCode], _isAlarm);
	int thisAlarm=_isAlarm?1:NO_ALARM;
	alarmState|=thisAlarm<<(_alarmCode-1);
}






void Hoist::readButtons(){
	int thisBtnPressed;
	for (int i=0;i<NB_BTN;i++){
		thisBtnPressed=!digitalRead(A_PIN_BTN[i]); //cablé en pull up
		if(thisBtnPressed==passedBtnState[i]){
			nbTimeBtnPressed[i]++;			
		}
		else {
			nbTimeBtnPressed[i]=0;
		}
		if (nbTimeBtnPressed[i]>NB_ANTIBOUNCE){
			isBtnPressed[i]=passedBtnState[i];
		}
		passedBtnState[i]=thisBtnPressed;
	}

}

void Hoist::watchDog(){
	curTime=millis();	
	if(isBuzz && (curTime-startBuzz)>buzzDuration){
		buzz(NO_BUZZ);
	}
	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_WATCHDOG, watchDogStatus);
		prevTime=curTime;
	}
	#ifdef MESURE_TEMPS_CYLE
		if(++logN%100==0){
			Serial.print("100 temps cycle (ms) =");Serial.println(curTime-cycleTime);
			cycleTime=curTime;
		}
	#endif
}

void Hoist::move(int _dir){
	bool cw =OFF;
	bool ccw=OFF;
	if (isGoMode && _dir==MOVE_UP && curPosIndex>targetGo)	{
		_dir=STOP_BRAKE;
		Serial.println("Objectif Up atteint");
		isGoMode=false;
	}
	if (isGoMode && _dir==MOVE_DOWN && curPosIndex<targetGo)	{
		_dir=STOP_BRAKE;
		Serial.println("Objectif Down atteint");
		isGoMode=false;
	}


	switch (_dir){
		case MOVE_UP:
			if((!(alarmState&ALARM_OVERLOAD)) && !isFdCHaut){//||(curLoad<maxLoadFreeMove && isUnloadMove)
				cw=ON;
			}
			else {
				targetDir=STOP_BRAKE;
				isGoMode=false;
			}

			break;
		case MOVE_DOWN:
			if(!(alarmState&ALARM_UNDERLOAD)){//||isUnloadMove
				ccw=ON;
			}
			else {
				targetDir=STOP_BRAKE;
				isGoMode=false;
			}
			break;
		case STOP_BRAKE:
			targetDir=STOP_BRAKE;
			isGoMode=false;
			break;
		default:;
	}
	if(!prevIsPowered || (alarmState&ALARM_OVERLOAD)==ALARM_OVERLOAD){
		ccw=OFF;
		cw=OFF;
		if(oldCW!=cw){
			Serial.print("POWER PB:");Serial.print(isTempOverload);
		}

	}

	if(isFdCHaut && targetDir==MOVE_UP && !isFirstTimeFdcAlert){
		Serial.print("Fdc Haut => Impossible d'aller plus haut ! ");
		Serial.println(A_DIR_NAME[cw+2*ccw]);
		isFirstTimeFdcAlert=true;
	}
	
	oldCW=cw;

	digitalWrite(PIN_CW,cw);//UP
	digitalWrite(PIN_LUP, cw);
	
	analogWrite(PIN_VAR_VIT,0);// //5V:Speed=0 ; 0V:maxSpeed

	digitalWrite(PIN_CCW,ccw);
	digitalWrite(PIN_LDOWN, ccw);

	#ifdef	LOG
	if(targetDir!=moveDir){
		if(controlMode==USER_CONTROL){
			Serial.print(logN++);
			Serial.print(" MANUEL : ");
		}
		Serial.print(A_DIR_NAME[cw+2*ccw]);printIndex();
	}
	#endif

	moveDir=targetDir;
	startMoveTime=curTime;
}

void Hoist::setTargetSpeed(int _speed){
	if (targetSpeed!= _speed){
		if (targetSpeed< _speed+SPEED_THRESHOLD){				//On ne va pas assez vite
    		targetSpeed=min(_speed,MAX_SPEED);		//on augmente la vitesse
    		Serial.print("new target Speed+ :"); Serial.println(targetSpeed);
		}
		else if (targetSpeed> _speed -SPEED_THRESHOLD){		//on va trop vite
    		targetSpeed=max(_speed,0);				//on diminue la vitesse
			Serial.print("new target Speed- :"); Serial.println(targetSpeed);
		}
		curSpeed=targetSpeed;
	}
}


void Hoist::checkGauge(){
	#ifndef VIRTUAL_GAUGE
		long tempRead;
		long tempMaxOverLoad=isUnloadMove?maxLoadFreeMove:maxLoad;
		//long tempMinUnderLoad=minLoad;
		if(scale->is_ready()){
			#ifdef MESURE_GAUGE_CYCLE
				cumulatedGaugeTime+=curTime-gaugeTime;
				gaugeCycle++;
				if(gaugeCycle>=100){
					gaugeCycle=0;
					Serial.print("Temps de cycle Jauge=");Serial.println((float)(cumulatedGaugeTime/100.));
					cumulatedGaugeTime=0;
					gaugeTime=curTime;
				}
			#endif
			scale->power_up();
			//curLoad=scale->get_units(10)-SCALE_TARE; //read();
			tempRead=scale->read();//read_average(3);
			curLoad=SCALE_A*tempRead+SCALE_B;//read_average(3);

			if(doDisplayLoad){
				Serial.print(logN++);
				Serial.print(" / charge :");
				Serial.print(curLoad);
				Serial.print(" N \t");
				Serial.print(tempRead);
				Serial.print("pt");
				Serial.println();
			}

			if(curLoad>tempMaxOverLoad){
				if(!isTempOverload){
					loadAlarmStartTime=curTime;
					isTempOverload=true;
					
				}
				else if((curTime-loadAlarmStartTime)>LOAD_ALARM_TIMER){
					reportAlarm(ALARM_OVERLOAD ,curLoad>tempMaxOverLoad);	
					//Serial.println("ici");
					//targetDir=STOP_BRAKE;
				}
			}
			else {
				/*Serial.println("fin");
				//*/
				isTempOverload=false;	
				reportAlarm(ALARM_OVERLOAD ,false);	
				
			}

			if(curLoad<minLoad && !isUnloadMove && curLoad>=0){//on ignore les valeurs négatives
				if(!isTempUnderload){
					loadAlarmStartTime=curTime;
					isTempUnderload=true;
				}
				else if((curTime-loadAlarmStartTime)>LOAD_ALARM_TIMER){
					reportAlarm(ALARM_UNDERLOAD,curLoad<minLoad);
				}
			}
			else {
				isTempUnderload=false;	
				reportAlarm(ALARM_UNDERLOAD ,false);	
			}

				
		}
		else {
			alarmState=oldAlarmState;
		}
	#endif	
}

void Hoist::checkFdc(){
	isFdCHaut=!digitalRead(PIN_FDC_HAUT);
	reportAlarm(ALARM_FDC_HAUT ,isFdCHaut);
}


//*/
void Hoist::displayTraverse(){
	for(int i=0;i<MAX_TRAVERSE;i++){
		if(i==curTraverse){
			Serial.print(">");	
		}
		else {
			Serial.print(" ");
		}
		Serial.print(i);
		Serial.print(":\t");
		switch (traverse[i][TRAV_SET]){
			case TRAV_SET_FULL:
				Serial.print(traverse[i][TRAV_BAS]);
				Serial.print(" -> ");
				Serial.println(traverse[i][TRAV_HAUT]);
				break;
			case TRAV_SET_BAS:
				Serial.print(traverse[i][TRAV_BAS]);
				Serial.println(" -> ?");
				break;
			case TRAV_SET_HAUT:
				Serial.print("? -> ");
				Serial.println(traverse[i][TRAV_HAUT]);
				break;
			case NOT_SET:
				Serial.println(A_TRAV_NAME[traverse[i][TRAV_SET]]);
				break;
			default:
				Serial.print("Erreur traverse:");
				Serial.println(traverse[i][TRAV_SET]);
		}
	}
}

void Hoist::addTraverse(long _pos){
 	if((traverse[curTraverse][TRAV_SET]!=_pos) && !((traverse[curTraverse][TRAV_SET]== NOT_SET) && (_pos==TRAV_BAS))){
		// Si l'epaisseur d'une traverse est trop petite : ignorer
		if(_pos==TRAV_BAS && traverse[curTraverse][TRAV_BAS]-traverse[curTraverse][TRAV_HAUT]<MIN_TRAVERSE_THICKNESS){
			traverse[curTraverse][TRAV_SET]== NOT_SET;
			Serial.println("Traverse ignoree car trop fine");
		}
		else {
			traverse[curTraverse][_pos]=curPosIndex;
			traverse[curTraverse][TRAV_SET]=traverse[curTraverse][TRAV_SET] | _pos;
			if(traverse[curTraverse][TRAV_SET]==TRAV_SET_FULL){
				curTraverse=(curTraverse+1)%MAX_TRAVERSE;
				if(curTraverse==0){
					Serial.println("Nb max de traverses atteintes : on ecrase les traverses passées");
				}
			}
			Serial.print("Traverse ajoutee :");
			Serial.print(curPosIndex);
			Serial.print(" ");
			Serial.println(A_TRAV_NAME[_pos]);
		}
	}
	else {
		Serial.println("Erreur : traverse deja detectee => ignore");
		Serial.print("_pos=");Serial.println(_pos);
		errorCode=ERR_ALREADY_DETECTED_TRAVERSES;
	}
}
void Hoist::printIndex(){
	Serial.print("Index=");
	Serial.print(curPosIndex);	
	Serial.print("\t");Serial.print(MM_PAR_POINT*curPosIndex);Serial.println("mm");
}

void Hoist::doSequence(int _do){
	addRadioMsg(CDE_MISTING,_do,NO_JACK);
	addRadioMsg(CDE_ASPI,_do,NO_JACK);
	addRadioMsg(CDE_MICROFIBRE,_do,NO_JACK);

}

void Hoist::goAbsoluteMM(int _targetMM){
	isGoMode=true;
	targetGo=(long)(_targetMM/MM_PAR_POINT);
	if(curPosIndex>targetGo){
		if(isFdCHaut){
			Serial.println("FDC Haut => mouvement impossible");	
			targetDir=STOP_BRAKE;
		} else {
			targetDir=MOVE_DOWN;	
			Serial.print("Moving Down to ");Serial.print(targetGo);Serial.print("\t");Serial.print(_targetMM);Serial.println("mm");
		}
	} else {
		targetDir=MOVE_UP;		
		Serial.println("Moving Up to ");Serial.print(targetGo);Serial.print("\t");Serial.print(_targetMM);Serial.println("mm");	
	}
}

void Hoist::loop(){ 
	int inputValue=0;
	errorCode=ERR_NO_ERROR;

	//Check for alarm
	alarmState=NO_ALARM;
	checkGauge();
	checkFdc();
	readButtons();

	isPowered=digitalRead(PIN_POWERED);
	if(!isPowered){
		if(prevIsPowered || isRestart){						//Affiche 1er fois uniquement
			Serial.println("PERTE ALIM !");
		}
		prevIsPowered=false;
	}



	if(alarmState!=oldAlarmState && alarmState!=NO_ALARM){	//buzz mais qu'une seule fois
		int freq=BUZZ_ERROR;
		if((alarmState&ALARM_OVERLOAD)==ALARM_OVERLOAD){
			Serial.print("OVERLOAD ! : ");Serial.println(curLoad);
			freq=BUZZ_OVERLOAD;
		} 
		if((alarmState&ALARM_UNDERLOAD)==ALARM_UNDERLOAD){ //Attention alarmState en bit marche par ce que ALARM_UNDERLOAD=2^1=2
			Serial.print("UNDERLOAD ! : ");Serial.println(curLoad);
		}
		if(isFdCHaut){
			Serial.println("FDC HAUT !");
		}
		#ifndef NO_SOUND
			buzz(BUZZ_ERROR);//,_duration);				
		#endif
	} 
	else if(alarmState==NO_ALARM) {
		if(oldAlarmState) {
			Serial.println("Fin Alarm !");
			#ifndef NO_SOUND
				buzz(NO_BUZZ);
			#endif
		}
	}

	unsigned long curMicroS=micros();
	if(isSimu && (curMicroS-startSimuMicroS)>(long)((MM_PAR_POINT*600000./simuSpeed)-TEMPS_CYCLE_SIMU)){ //1600->13m/min
		startSimuMicroS=curMicroS;
		curPosIndex--;
		displaySpeed();
	}

	oldAlarmState=alarmState;
	//getSpeed();
	if( Serial.available() ) {  	// if something is available
	    c = Serial.read();   		// get it
	    Serial.print(c);          	// optional: repeat back what I got for debugging
	    if(sofar < MAX_BUF) buffer[sofar++]=c;
	     
	    if(c=='\n') {             	// if we got a return character (\n) the message is done.
	        buffer[sofar]=0;        // strings must end with a \0.
	        sofar=0;
	        int tempSuckIndex;
	        switch(buffer[0]){
				case CDE_SET_PARAM: //PARAMETRES
					if(buffer[1]==0){
						Serial.println("wrong input. Usage : *[_;^;L;l;M;$] then integer");
					}
					else {
			        	inputValue=getInt();
						if(Serial.available()){ Serial.read();}
						switch(buffer[1]){
							case CDE_SET_US_TOP:
								addRadioMsg(CDE_SET_US_TOP,(long)inputValue);
								break;
							case CDE_SET_US_LOW:
								addRadioMsg(CDE_SET_US_LOW,(long)inputValue);
								break;
							case CDE_GO_ABSOLUTE:
								Serial.print("Go:");Serial.println(inputValue);
								goAbsoluteMM(inputValue);
								break;
							case CDE_GO_RELATIVE:
								Serial.print("Go:");Serial.println((((float)inputValue/MM_PAR_POINT)+curPosIndex)*MM_PAR_POINT);
								goAbsoluteMM((int)(((float)inputValue/MM_PAR_POINT)+curPosIndex)*MM_PAR_POINT);
								break;
							case PARAM_JAUGE_SEUIL_HAUT:
								Serial.print("Nouvelle valeur seuil haut jauge :");
								Serial.println(inputValue);
								maxLoad=inputValue;
								break;
							case PARAM_JAUGE_SEUIL_BAS:
								Serial.print("Nouvelle valeur seuil bas jauge :");
								Serial.println(inputValue);
								minLoad=inputValue;
								break;
							case PARAM_JAUGE_MULTIPLIER:
								Serial.print("Nouvelle valeur multiplier :");
								Serial.println(inputValue);
								scale->set_scale(inputValue); // permet de convertir les données en donées lisibles 
								break;
							case CDE_SET_PARAM_PULSE:
								Serial.print("new Pulse :");Serial.println(inputValue);
								addRadioMsg(CDE_SET_PARAM_PULSE,(long)inputValue);
								break;
							case CDE_SET_OFFSET:
								curPosIndex=(long)((float)inputValue/MM_PAR_POINT);	
								printIndex();
								resetTraverse();
								break;
							case PARAM_SIMU_SPEED:
								Serial.print("Nouvelle valeur vitesse (dm/min) :");
								Serial.println(inputValue);
								simuSpeed=inputValue;
								break;
							case CDE_SET_PARAM_FILE:
								Serial.print("new file ID :");Serial.println(inputValue);
								addRadioMsg(CDE_SET_PARAM_FILE,(long)inputValue);
								break;
						}
					}
					break;
	        	case CDE_SIMU:
	        		isSimu=!isSimu;
	        		Serial.print("Simu=");Serial.println(isSimu);
	        		if(isSimu){
		        		isGoMode=true;
		        		startSimuMicroS=curMicroS;
		        		doDisplaySpeed=true;
	        		}
	        	case CDE_INVALID:
		        	if(buffer[1]==0){
							Serial.println("wrong input. Usage : *[G;R]");
					}
					else {
						if(Serial.available()){ Serial.read();}
							switch(buffer[1]){
							case CDE_GO_RELATIVE:
							case CDE_GO_ABSOLUTE:
								isGoMode=false;
								Serial.println("isGoMode=false");
								break;
							}
					}
	        		break;
	        	case '?':
	        		displayHelp();
	        		break;
/*
	        	case CDE_TOGGLE_RADIO:
	        		isRadioOn=!isRadioOn;
	        		Serial.print("isRadioOn=");Serial.println(isRadioOn);
	        		break;
//*/				
	        	case CDE_RAZ:
					switch(buffer[1]){
						case PARAM_RAZ_ROBOT_ALL:
							Serial.println("RAZ jacks");
							addRadioMsg(CDE_RAZ,NO_VAL,buffer[1]);
							//isRAZasked=true;
							break;
						case PARAM_RAZ_ROBOT_ONLY:
							Serial.println("RAZ robot");
							addRadioMsg(CDE_RAZ,NO_VAL,buffer[1]);
							//isRAZasked=true;
							break;
						default:	
							restart();
						}			
					break;
	        	case CDE_REPORT_JACK:
					addRadioMsg(CDE_REPORT_JACK);
	        		break;
				case CDE_INVERSE_VALVE:
					addRadioMsg(CDE_INVERSE_VALVE,buffer[2]-48,buffer[1]-48);
					break;
				case CDE_DISPLAY_LOAD:
					doDisplayLoad=!doDisplayLoad;
					break;
				case CDE_DISPLAY_SPEED:
					doDisplaySpeed=!doDisplaySpeed;
					Serial.print("display speed=");Serial.println(doDisplaySpeed);
					startSpeedIndex=curPosIndex;
					startSpeedTime=curTime;
					break;
				case CDE_MODE_A_VIDE:
					prevUnloadMove=isUnloadMove;
					isUnloadMove=!isUnloadMove;
					if(isUnloadMove){
						isTempUnderload=false;
						reportAlarm(ALARM_UNDERLOAD,false);
					}
					Serial.print("Mouvement a vide autorise :");
					Serial.println(isUnloadMove);
					break;
				case CDE_THIS_INFO:
					//waitingForAnswer=true;
					Serial.print("targetSpeed=\t\t");Serial.println(targetSpeed);
					Serial.print("curSpeed=\t\t");Serial.println(curSpeed);
					Serial.print("isUnloadMove=\t\t");Serial.println(isUnloadMove);
					Serial.print("alarmState=\t\t");Serial.println(alarmState);
					Serial.print("curLoad=\t\t");Serial.println(curLoad);
					Serial.print("maxLoad=\t\t");Serial.println(maxLoad);
					Serial.print("minLoad=\t\t");Serial.println(minLoad);
					Serial.print("maxLoadFreeMove=\t");Serial.println(maxLoadFreeMove);
					Serial.print("moveDir=\t\t");Serial.println(moveDir);
					Serial.print("targetDir=\t\t");Serial.println(targetDir);
					Serial.print("BTN_MOVE_DOWN=\t\t");Serial.println(isBtnPressed[BTN_MOVE_DOWN]);
					Serial.print("BTN_MOVE_UP=\t\t");Serial.println(isBtnPressed[BTN_MOVE_UP]);
					Serial.print("MODE_MANUEL=\t\t");Serial.println(isBtnPressed[BTN_MANUAL_MODE]);
					Serial.print("isPowered=\t\t");Serial.println(isPowered);
					Serial.print("prevIsPowered=\t\t");Serial.println(isPowered);
					Serial.print("isFdCHaut=\t\t");Serial.print(isFdCHaut);
						Serial.print(" ");Serial.println(!digitalRead(PIN_FDC_HAUT));
					Serial.print("isGoMode=\t\t");Serial.println(isGoMode);
					Serial.print("targetGo=\t\t");Serial.println(targetGo);
	        		Serial.print("controlMode [1:ROBOT, 0:USER] =");Serial.println(controlMode);
	        		Serial.print("simuSpeed=\t\t");Serial.println(simuSpeed);
	        		
	        		displayProtocolStatus();

					for(int i=0; i<3;i++){
						Serial.print("suckState[");Serial.print(i);Serial.print("]=\t\t");Serial.println(suckState[i]);
					}
					/*Serial.println("TRAVERSES---------------------");
					for(int trav=0;trav<T_INIT_TRAVERSE_NB ;trav++){//Scanner les traverses
						Serial.print(trav);Serial.print(":\t");Serial.print(traverse[trav][TRAV_BAS]);Serial.print("->");Serial.println(traverse[trav][TRAV_HAUT]);
					}
					//*/
					break;
				case CDE_INC_SPEED:
					//setTargetSpeed(targetSpeed+10);
					addRadioMsg(CDE_INC_SPEED);
					break;
				case CDE_DEC_SPEED:
					//setTargetSpeed(targetSpeed-10);
					addRadioMsg(CDE_DEC_SPEED);
					break;
				case CDE_MOVE_UP:
					targetDir=MOVE_UP;
					startSpeedTime=curTime;
					break;
				case CDE_MOVE_DOWN:
					targetDir=MOVE_DOWN;
					startSpeedTime=curTime;
					break;
				case CDE_STOP_BRAKE:
					targetDir=STOP_BRAKE;
					displaySpeed(FORCE_DISPLAY);
					addRadioMsg(CDE_STOP_BRAKE); //force la prise de mesure pAtm sur chaque verin
					break;
				case CDE_START_WET:
					Serial.print("start Wetting : ");Serial.println(buffer[1]);
					addRadioMsg(buffer[0],NO_VAL,buffer[1]);
					break;
				case CDE_STOP_WET:
					Serial.print("stop Wetting : ");Serial.println(buffer[1]);
					addRadioMsg(buffer[0],NO_VAL,buffer[1]);
					break;
				case CDE_ASPI:
		        	if(buffer[1]==0){
							Serial.println("wrong input. Usage : V[0;1]");
					}
					else {
						if(Serial.available()){ Serial.read();}
						Serial.print("Aspi=");Serial.println(buffer[1]);
						addRadioMsg(CDE_ASPI,buffer[1]-48,NO_JACK);
					}
					break;
	        	case CDE_MISTING:
		        	if(buffer[1]==0){
							Serial.println("wrong input. Usage : G[0;1]");
					}
					else {
						if(Serial.available()){ Serial.read();}
						Serial.print("Misting=");Serial.println(buffer[1]);
						addRadioMsg(buffer[0],buffer[1]-48,NO_JACK);
					}
					break;
				case CDE_MICROFIBRE:
		        	if(buffer[1]==0){
							Serial.println("wrong input. Usage : M[0;1]");
					}
					else {
						if(Serial.available()){ Serial.read();}
						Serial.print("Moteur Microfibre=");Serial.println(buffer[1]);
						addRadioMsg(CDE_MICROFIBRE,buffer[1]-48,NO_JACK);
					}
					break;

				case CDE_DECLENCHEMENT:
					addTraverse(TRAV_HAUT);
					break;
				case CDE_RACCROCHE:
					addTraverse(TRAV_BAS);
					break;
				case CDE_DISPLAY_TRAVERSE:
					displayTraverse();
					break;
				case CDE_ERROR_ALERT:
					break;
				case CDE_ERASE_ERROR:
					isTempOverload=false;
					isGoMode=false;
					isOtherReady=true;
					for(int i=0;i<3;i++){
						suckState[i]=FREED;
					}
					break;
				case CDE_CATCH_JACK:
					//Serial.print(buffer[1]-65);
					addRadioMsg(CDE_CATCH_JACK,0,buffer[1]-65);
					break;
				case CDE_FREE_JACK:
					addRadioMsg(CDE_FREE_JACK,0,buffer[1]-65);
					break;
				case CDE_READY:
					addRadioMsg(CDE_READY,NO_VAL,NO_JACK);
					break;
				case CDE_US_BOTTOM:
					//waitingForAnswer=true;
					addRadioMsg(CDE_US_BOTTOM,NO_VAL,NO_JACK);
					break;
				case CDE_START_REC_US:
					Serial.println("Start US record");
					addRadioMsg(CDE_START_REC_US,NO_VAL,NO_JACK);
					break;
				case CDE_STOP_REC_US:
					//waitingForAnswer=true;
					Serial.println("Stop US record");
					addRadioMsg(CDE_STOP_REC_US,NO_VAL,NO_JACK);
					break;
				case CDE_GET_POS_INDEX:
					Serial.print("Get Pos :");
					printIndex();
					break;
				case CDE_DISPLAY_PILE:
					//displayRadioMsg(ALL_MSG);	
					break;
				default:
					Serial.print("Commande inconnue !(");
					Serial.print(buffer[0]);
					Serial.println(")");
	        }// end switch buffer[0]

		}// end c='\n'
  	}//end serial available
//*/


/////////////////

	errorCode=ERR_NO_ERROR;


	
	//User control
	if(isBtnPressed[BTN_MANUAL_MODE]){ //if user mode selected (0=manuel)
		if(controlMode!=USER_CONTROL){//was not in user mode before
	
			Serial.println("MODE MANUEL");
			setTargetSpeed(MAX_SPEED);
			targetDir=SECU;
			moveDir=SECU;
			
		}
		controlMode=USER_CONTROL;
	}
	else {
		if(controlMode!=ROBOT_CONTROL){//First time
			Serial.println("MODE ROBOT");//AutoCatch
			//isStartAutocatch=true;

			targetDir=SECU;
			moveDir=SECU;
		}
		controlMode=ROBOT_CONTROL;
	}



//*/
	//On vient de tourner le bouton vers up
	if(isBtnPressed[BTN_MOVE_UP] && !isBtnPressed[BTN_MOVE_DOWN] && (moveDir==STOP_BRAKE) && prevIsPowered && !isFdCHaut && !isGoMode){//Securité : il faut passer par STOP avant MOVE_UP ou DOWN
		targetDir=MOVE_UP;
		startSpeedTime=curTime;
		if (controlMode==ROBOT_CONTROL){
			addRadioMsg(CDE_INVERSE_VALVE,4,1); //I41 pulvé rouleau
			addRadioMsg(CDE_ASPI,1,NO_JACK);
			addRadioMsg(CDE_MICROFIBRE,1,NO_JACK);
		}
	}


	//On vient de tourner le bouton vers down
	if(isBtnPressed[BTN_MOVE_DOWN] && !isBtnPressed[BTN_MOVE_UP] && (moveDir==STOP_BRAKE) && prevIsPowered && !isGoMode){
		targetDir=MOVE_DOWN;
		startSpeedTime=curTime;
		if (controlMode==ROBOT_CONTROL){
			addRadioMsg(CDE_ASPI,1,NO_JACK);
			addRadioMsg(CDE_MICROFIBRE,1,NO_JACK);
		}

	}

	if(isBtnPressed[BTN_MOVE_DOWN]==isBtnPressed[BTN_MOVE_UP] && !isGoMode){// && (moveDir!=STOP_BRAKE)){
		if(targetDir!=STOP_BRAKE){
			addRadioMsg(CDE_INVERSE_VALVE,1,0); //I10 Arret pulvé haut
			addRadioMsg(CDE_INVERSE_VALVE,2,0); //I20 Arret pulvé milieu
			addRadioMsg(CDE_INVERSE_VALVE,3,0); //I30 Arret pulvé bas
			addRadioMsg(CDE_INVERSE_VALVE,4,0); //I40 Arret pulvé rouleau
			addRadioMsg(CDE_ASPI,0,NO_JACK);
			addRadioMsg(CDE_MICROFIBRE,0,NO_JACK);

			displaySpeed(FORCE_DISPLAY);	
		}

		targetDir=STOP_BRAKE;
		isGoMode=false;
		if(isPowered){
			prevIsPowered=true;
		}
	}

	if(isBtnPressed[BTN_MOVE_UP] && !isBtnPressed[BTN_MOVE_DOWN] && isGoMode){
		Serial.println("Go not possible : button not in stop position");
		targetDir=STOP_BRAKE;
		isGoMode=false;
	}

/*	if(isBtnPressed[BTN_MOVE_UP] && !isBtnPressed[BTN_MOVE_DOWN] && isUnloadMove!=prevUnloadMove){
		Serial.println("Warning : button not in stop position");
		targetDir=STOP_BRAKE;
	}
//*/
	if((curPosIndex<targetGo) && isSimu){
		Serial.println("Position reached");
		buzz(BUZZ_END,1000);
		isGoMode=false;
		isSimu=false;
		doSequence(SEQUENCE_STOP);

	}

	if(isGoMode && ((curPosIndex<targetGo && moveDir==MOVE_DOWN)||(curPosIndex>targetGo && moveDir==MOVE_UP))){
		targetDir=STOP_BRAKE;
		displaySpeed(FORCE_DISPLAY);
		Serial.println("Position reached");
		isGoMode=false;
		isSimu=false;
		doSequence(SEQUENCE_STOP);
	}

	if(controlMode==ROBOT_CONTROL){
		switch (targetDir){
			case MOVE_UP:
				botMargin=TRAV_MARGIN_FREE;
				topMargin=TRAV_MARGIN_CATCH;
				break;
			case MOVE_DOWN:
				botMargin=TRAV_MARGIN_CATCH;
				topMargin=TRAV_MARGIN_FREE;
				break;
			default:
				botMargin=TRAV_MARGIN_CATCH;
				topMargin=TRAV_MARGIN_CATCH;

		}
		int	tempSuckState;
		for(int i=2;i>-1;i--){//scanner les verins
			tempSuckState=LINKED;
			for(int trav=0;trav<MAX_TRAVERSE ;trav++){//Scanner les traverses
/*
				if (    traverse[trav][TRAV_SET]==TRAV_SET_FULL
					&& (traverse[trav][TRAV_HAUT]-suckBottomPos[i])>curPosIndex //l'index est dans la plage de la traverse
					&& (traverse[trav][TRAV_BAS]-suckTopPos[i])<curPosIndex	    
					//&& suckState[i]==LINKED
					) { 									//pour ne lancer la commande qu'une fois
//*/
				if (   traverse[trav][TRAV_SET]==TRAV_SET_FULL
					&& traverse[trav][TRAV_HAUT]>(curPosIndex+suckBottomPos[i]-topMargin) //l'index est dans la plage de la traverse
					&& traverse[trav][TRAV_BAS]<(curPosIndex+suckTopPos[i]+botMargin)	    
					//&& suckState[i]==LINKED
					) { 									//pour ne lancer la commande qu'une fois
					tempSuckState=FREED;
				}
			}
			if(tempSuckState==FREED && suckState[i]==LINKED){
				#ifndef LEARNING_FACADE
				if(controlMode==ROBOT_CONTROL){
					addRadioMsg(CDE_FREE_JACK,0,i);
				}
				#endif
				//Serial.print("\tsuckState[=");Serial.print(i);Serial.print("]=");Serial.print(suckState[i]);
				suckState[i]=FREED;				
				//Serial.print("\tnew suckState=");Serial.println(suckState[i]);
			}
			else if(tempSuckState==LINKED && suckState[i]==FREED){
				#ifndef LEARNING_FACADE
				if(controlMode==ROBOT_CONTROL){
					Serial.print("Autocatch ");Serial.print((char)(i+65));Serial.print(" a ");printIndex();
					addRadioMsg(CDE_CATCH_JACK,0,i);
				}
				#endif
				suckState[i]=LINKED;				
			}
		}
	//		isStartAutocatch=false;

	}


/*
	}
/*	
	else {
		if(controlMode!=ROBOT_CONTROL){//First time
			Serial.println("MODE ROBOT");
		}
		controlMode=ROBOT_CONTROL;
		//INPUTS
	//*/



		// TO : to many ACK retryes : le renvoie de la commande indique qu'on a pas recu le ACK
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////
	checkIncomingMsg();
	if(isNewRcvMsg){
			switch(curRcvMsg.cde){
				case CDE_READY:
				case ACKNOWLEDGE:
				case ACKNOWLEDGE_RDY:
					processProtocolReading();
					break;
				case CDE_GET_POS_INDEX:
					addRadioMsg(REP_GET_POS_INDEX,curPosIndex);					//MOVE_UP jack when curPosIndex+A_POS_INDEX[i] detected bu hoist
					//Serial.print("curPos=");
					//Serial.println(curPosIndex);
					break;
				case REP_US_BOTTOM:
					Serial.print("US Bottom=");
					Serial.println(curRcvMsg.val);
					break;
				case REP_ETAT_VANNES:
					Serial.print("Vanne [AJI,AJO,AVI,AVO,API](");
					Serial.print((char)(curRcvMsg.jack+65));
					Serial.print(")=");
					Serial.println(curRcvMsg.val,BIN);
					Serial.println();
					break;
				case REP_ETAT_FDC_BAS:
				case REP_ETAT_FDC_MID:
				case REP_ETAT_FDC_HAUT:
				case REP_DEMANDE_RANK:
				case REP_MESURE_PRESSION:
				case REP_MESURE_P_ATM:
					int nMsg;
					switch(curRcvMsg.cde){
					case REP_ETAT_FDC_BAS:
						nMsg=0;
						break;
					case REP_ETAT_FDC_MID:
						nMsg=1;
						break;
					case REP_ETAT_FDC_HAUT:
						nMsg=2;
						break;
					case REP_DEMANDE_RANK:
						nMsg=3;
						break;
					case REP_MESURE_P_ATM:
						nMsg=4;
						Patm[curRcvMsg.jack]=curRcvMsg.val;
						break;
					case REP_MESURE_PRESSION:
						nMsg=5;
						break;
					}
					Serial.print(A_MSG_REPORT[nMsg]);
					Serial.print((char)(curRcvMsg.jack+65));
					Serial.print(")=\t");
					Serial.print(curRcvMsg.val);
					if(curRcvMsg.cde>=REP_MESURE_PRESSION){
						Serial.print(" / ");Serial.print((curRcvMsg.val-Patm[curRcvMsg.jack])/120.);Serial.print(" bar");
					}
					Serial.println();
					break;
				case REP_FREE_JACK:
					//Serial.print("->Free jack[");Serial.print((char)(curRcvMsg.jack+65));Serial.print("]=");Serial.println(curRcvMsg.val);
					Serial.println("Success");
					suckState[curRcvMsg.jack]=!curRcvMsg.val;
					break;
				case REP_CATCH_JACK:
					Serial.println("Success");
					//Serial.print("->Catch jack[");Serial.print((char)(curRcvMsg.jack+65));Serial.print("]=");Serial.println(curRcvMsg.val);
					suckState[curRcvMsg.jack]=curRcvMsg.val;
					break;
				case CDE_INFO_ALERT:
					Serial.print("ALERT : ");
					Serial.println(A_MSG_ALERT[curRcvMsg.val]);
					break;
				case CDE_MOVE_UP:
					targetDir=MOVE_UP;
					startSpeedTime=curTime;
					break;
				case CDE_MOVE_DOWN:
					targetDir=MOVE_DOWN;
					startSpeedTime=curTime;
					break;
				case CDE_STOP_BRAKE:
					targetDir=STOP_BRAKE;
					displaySpeed(FORCE_DISPLAY);
					break;
				case CDE_DECLENCHEMENT:
					addTraverse(TRAV_HAUT);
					break;
				case CDE_RACCROCHE:
					addTraverse(TRAV_BAS);
					break;
				case CDE_RAZ:
					restart();
					break;
				default:
					Serial.print("Invalid msg received :");
					Serial.println(curRcvMsg.cde);
//				}
		}
	}//endif if newMsgReceived
	isNewRcvMsg=false;
	processProtocolMatching();
/*
	if(errorCode){
		Serial.print(errorMsg[errorCode]);
	}
//*/

	#ifdef RCCONTROL
		if(isOtherReady){
			// RC
		  	if(RC_HoistSpeed!=RC_OldHoistSpeed){
		  		Serial.print("New Hoist Speed :");
		  		Serial.println(RC_HoistSpeed);
		  	    RC_OldHoistSpeed=RC_HoistSpeed;
		  	}
		  	if(RC_BrushSpeed!=RC_OldBrushSpeed){
		  		Serial.print("New Brush Speed :");
		  		Serial.println(RC_BrushSpeed);
			  	RC_OldBrushSpeed=RC_BrushSpeed;
			  	//setBrushSpeed(map(RC_BrushSpeed,0 ,100 , MIN_PWM, MAX_PWM));

		  	}

		  	if(RC_OrderDir!=RC_OldOrderDir){
		        RC_OldOrderDir=RC_OrderDir;
				Serial.print("RC Dir:");
				Serial.println(RC_OrderDir);
				/*if(isReadyToOperate){
					targetDir=A_RC_DIR[RC_OrderDir];
				}
				//*/
				
		  	}

		  	if(RC_OrderCatch!=RC_OldOrderCatch){
		        RC_OldOrderCatch=RC_OrderCatch;
				Serial.print("RC Catch:");
				Serial.println(RC_OrderCatch);
				for(int i=0;i<3;i++){
					addRadioMsg(A_RC_CATCH[RC_OrderCatch],0,i);
				}
		  	}

		  	if(RC_OrderClean!=RC_OldOrderClean){
		        RC_OldOrderClean=RC_OrderClean;
				Serial.print("RC Clean:");
				Serial.println(RC_OrderClean);
				//regenerate(RC_OrderClean==CK_CLEAN);
		  	}

		  	if(RC_OrderCross!=RC_OldOrderCross){
				RC_OldOrderCross=RC_OrderCross;
				Serial.print("RC Cross:");
				Serial.println(RC_OrderCross);
				//addRadioMsg(CDE_GET_POS_INDEX,NO_VAL,NO_JACK);
		  	}
		}
	#endif

	move(targetDir);
	watchDog();
	isRestart=false;
}

int PPMConvertAnalog(unsigned int value){
	maxAnaPPM=value>maxAnaPPM?value:maxAnaPPM;
	minAnaPPM=value<minAnaPPM?value:minAnaPPM;
	return map(value, minAnaPPM, maxAnaPPM, 0, 100);
}

int PPMConvert3StateSwitch(unsigned int value, int retLOW,int retMID,int retHIGH){
	if (value<700) { //589
		return retLOW;
	}
	else if (value>1500)  { //1100
		return retHIGH;
	}
	return retMID;
}

int PPMConvert2StateSwitch(unsigned int value, int retLOW,int retHIGH){
	if (value<700) {
		return retLOW;
	}
	return retHIGH;
}

void readPPM(){ 
  PPMcounter = TCNT1;
  TCNT1 = 0;      //reset counter

  if(PPMcounter > 2100*multiplier){  		//sync pulses over 2100us
    RC_channel = 0;
  }
  else if (digitalRead(PIN_IRQ_RC_IN)==LOW){		//end of signal
  	PPMcounter/=multiplier;
    RC_channel++;
  	switch(RC_channel){
      case RC_CHANNEL_HOIST_SPEED:		
      	PPMvalue=PPMConvertAnalog(PPMcounter);
      	if (PPMvalue>RC_OldHoistSpeed+RC_ANA_STEP || PPMvalue<RC_OldHoistSpeed-RC_ANA_STEP){ // step by step filter
      		RC_HoistSpeed=PPMvalue;
      	}
        break;
      case RC_CHANNEL_BRUSH_SPEED:		
      	PPMvalue=PPMConvertAnalog(PPMcounter);
      	if (PPMvalue>RC_OldBrushSpeed+RC_ANA_STEP || PPMvalue<RC_OldBrushSpeed-RC_ANA_STEP){ // step by step filter
      		RC_BrushSpeed=PPMvalue;
      	}
        break;
      case RC_CHANNEL_DIR:
        RC_OrderDir=PPMConvert3StateSwitch(PPMcounter,CK_DOWNWARD,CK_STOP,CK_UPWARD);
        break;
      case RC_CHANNEL_CATCH:
        RC_OrderCatch=PPMConvert2StateSwitch(PPMcounter,CK_FREE,CK_CATCH);
        break;
      case RC_CHANNEL_CLEAN:			
        RC_OrderClean=PPMConvert2StateSwitch(PPMcounter,CK_NO_COMMAND,CK_CLEAN);
        break;
      case RC_CHANNEL_CROSS:
      	RC_OrderCross=PPMConvert2StateSwitch(PPMcounter,CK_NO_COMMAND,CK_CROSS);
        break;
      default:										//Autre voies
        break;
  	}
  }
}

