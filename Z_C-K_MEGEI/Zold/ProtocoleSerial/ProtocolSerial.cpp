#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleSerial\ProtocolSerial.h"
long u=0;
void addRadioMsg(char _cde, long _val, char _jack){
	if(isOtherReady || _cde==CDE_READY){ // pour eviter que Serial3 reboucle sur lui mm
		Serial3.print(_cde);
		Serial3.print(_jack);
		//Serial.print(++u);
		//Serial.print("sending:");Serial.println(_cde);

		for(int i=0;i<4;i++){
			Serial3.write(_val>>(8*i));
		}
		prevSendMsgTime=curTime;
	}
}


void checkIncomingMsg(){
	curRcvMsg.cde=NO_CDE;
	curRcvMsg.jack=NO_JACK;
	curRcvMsg.val=NO_VAL;
	isNewRcvMsg=false;
	int b=Serial3.available();
	if(b<1){
		prevRcvMsgTime=curTime;
	}
	else if(curTime-prevRcvMsgTime>30){
		while(Serial3.read()){} //purger
	}
	else if(b>=sizeof(curRcvMsg)){
		curRcvMsg.cde=Serial3.read();
		//Serial.print(">");Serial.print(curRcvMsg.cde);
		curRcvMsg.jack=Serial3.read();
		//Serial.print(curRcvMsg.jack);

		byte buffer[4];
		for(int i=0;i<4;i++){
			buffer[i]=Serial3.read();
			curRcvMsg.val+=((long)(buffer[i]))<<(8*i);
		}
		//Serial.print("~");
		//Serial.print(curRcvMsg.val);

		isNewRcvMsg=true;
	}
	else {

	}

}

void processProtocolReading(bool _isFromIHM){
	switch(curRcvMsg.cde){
	case CDE_READY:									
		if(isOtherReady){							//on a recu le READY de l'autre reboot from other
			Serial.println("Other has reboot & Communication established");
		}
		else {
			Serial.println("Communication established from other");
		}
		addRadioMsg(ACKNOWLEDGE_RDY);
		isOtherReady=true;
		break;
	case ACKNOWLEDGE_RDY:
		if(!isOtherReady){ 
			Serial.println("Communication established from me");
		}
		else {
			Serial.println("received an unexpected rdy_ack -> ignored");
		}
		isOtherReady=true;
		break;
	case ACKNOWLEDGE:
		Serial.println("received an unexpected ack -> ignored");
		break;
	}
}

void displayProtocolStatus(){
	Serial.println("PROTOCOL--------------------------------------");
	Serial.print("isNewRcvMsg=\t\t");Serial.println(isNewRcvMsg);
/*	Serial.print("pileOverFlow=\t\t");Serial.println(pileOverFlow);
	Serial.print("curPileNb=\t\t");Serial.println(curPileNb);
	Serial.print("lastPileNb=\t\t");Serial.println(lastPileNb);
//*/
	Serial.print("isOtherReady=\t\t");Serial.println(isOtherReady);
}

void processProtocolMatching(){
	if (!isOtherReady && ((curTime-prevSendMsgTime)>1000)){//try to resend Y until it gets connected
		Serial.println("Try Connecting ->");
		
		addRadioMsg(CDE_READY);
	}
}

void initProtocol(){
	isOtherReady=false;
	isNewRcvMsg=false;
	prevSendMsgTime=millis();
/*	lastPileNb=curPileNb=0;
	pileOverFlow=false;
//*/
}

