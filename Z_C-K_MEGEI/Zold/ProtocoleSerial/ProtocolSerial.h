#ifndef ProtocoleSerial_h
#define ProtocoleSerial_h

#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleTreuilRobot.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ErrorList.h"

/*extern int 				msgRole;
extern int 				errorCode;
extern const char*		A_MSG_ALERT[];
//*/
type_msgHoist  			curRcvMsg;
type_msgHoist			pileMsg[PILE_SIZE];

bool					isOtherReady;
bool					isNewRcvMsg;						// flag for new radio message received to be computed
/*bool					pileOverFlow;
int 					curPileNb;
int 					lastPileNb;
//*/
unsigned long      	 	prevSendMsgTime;
unsigned long      	 	prevRcvMsgTime;
extern	unsigned long	curTime;


void 					addRadioMsg(char _cde, long _val=NO_VAL, char _jack=NO_JACK);//, char _expected=ACKNOWLEDGE);
void 					processProtocolReading(bool _isFromIHM=false);
void 					processProtocolMatching();
void 					displayProtocolStatus();
void 					checkIncomingMsg();
void 					initProtocol();

#endif