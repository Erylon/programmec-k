#define PIN_XVS          6
#define PIN_XVP         7
#define PIN_XVV         8
#define PIN_XVJ         9


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_XVS, OUTPUT);
  pinMode(PIN_XVP, OUTPUT);
  pinMode(PIN_XVV, OUTPUT);
  pinMode(PIN_XVJ, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)

  for (int i=0;i<4;i++){
    digitalWrite(((i)%4)+6,HIGH);
    digitalWrite(((i+1)%4)+6,LOW);
    digitalWrite(((i+2)%4)+6,LOW);
    digitalWrite(((i+3)%4)+6,LOW);
    delay(1000);
  }
 
}
