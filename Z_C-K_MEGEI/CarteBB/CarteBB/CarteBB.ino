#define PIN_API        5
#define PIN_AVO        6
#define PIN_AVI        7
#define PIN_AJO        8
#define PIN_AJI        9
#define PIN_POTAR      A4

int pwmWet=32;
unsigned long curTime, startRollWetTime;
#define ROLL_WET_DURATION     2000  //ms
#define DO_OPEN               HIGH  
#define DO_CLOSE              LOW 


void setup() {
  pinMode(PIN_API,  OUTPUT);
  pinMode(PIN_AVO,  OUTPUT);
  pinMode(PIN_AVI,  OUTPUT);
  pinMode(PIN_AJO,  OUTPUT);
  pinMode(PIN_AJI,  OUTPUT);
  pinMode(PIN_POTAR,INPUT);
  digitalWrite(PIN_API,DO_OPEN);
  digitalWrite(PIN_AVO,DO_OPEN);
  digitalWrite(PIN_AVI,DO_OPEN);
  digitalWrite(PIN_AJO,DO_OPEN);
  digitalWrite(PIN_AJI,DO_OPEN);

  startRollWetTime=millis();
}

void loop() {
  curTime=millis();
  pwmWet=analogRead(PIN_POTAR);
  if ((curTime-startRollWetTime)>ROLL_WET_DURATION){
      startRollWetTime=curTime;
      digitalWrite(PIN_API,DO_OPEN);
    }
    else if(curTime-startRollWetTime>((unsigned long)(pwmWet)*ROLL_WET_DURATION/1024)){
      digitalWrite(PIN_API,DO_CLOSE);
    }
  delay(5);
}
