#define VERSION "VERIN 1.7 BASF"

/*
CONSIGNES : 
Pression Verrin 			2.7bar affiché soit 2.2Bar
Pression Venturi 			1 bar affiché soit 1.5bar
Depression 					-0.2bar




//*/

//tempos
#define TIMEOUT_JACK_ZSH		600		//ms pour free 1500
#define MOVING_UP_DURATION		1500	//ms
#define MOVE_DOWN_SOFT_TIME 	1000	//ms pour un soft catch
#define MOVE_DOWN_DEEP_TIME		1000	//ms pour un deep catch
#define VACCUM_DURATION_A		400		//D75mm=>400 D110=>700		//ms 
#define VACCUM_DURATION_B		400		//ms 
#define VACCUM_DURATION_C		400		//ms 
#define VACCUM_DURATION_D		400		//ms 
#define VACCUM_DURATION_E		400		//ms 
#define EV_CLOSE_TIME			100		//ms
#define FILL_UP_TIME			600 	//300//Temps pendant lequel on remplit d’air la ventouse pour la décoller lors d'un Free
#define WATCHDOG_PULSE			1000	//ms
#define PATM_WAITING_TIME		2000    //ms
#define AUTOCATCH_DELAY 		400 	//ms
//Seuils
#define SEUIL_PRESS_HAUT		4	    //Seuil au dessus de PRESS_BAS a partir duquel on decide qu'on a perdu le catch
#define SEUIL_PRESS_ATMO		-3		//Seuil a partir duquel on declare qu'on est plus sous vide et donc le verin suffit pour decoller la ventouse

#define INIT_PRESSION_ATMO_A  	305
#define INIT_PRESSION_ATMO_B  	305
#define INIT_PRESSION_ATMO_C  	305
#define INIT_PRESSION_ATMO_D  	305
#define INIT_PRESSION_ATMO_E  	930
#define N_MOYENNE_GLISSANTE 	10
#define SEUIL_PRESS_BAS_A		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_B		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_C		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_D		-7		//Seuil a partir duquel on arrete le pompage
#define SEUIL_PRESS_BAS_E		-7		//Seuil a partir duquel on arrete le pompage

#define T_INIT_PRESS_ATMO		INIT_PRESSION_ATMO_A,INIT_PRESSION_ATMO_B,INIT_PRESSION_ATMO_C,INIT_PRESSION_ATMO_D,INIT_PRESSION_ATMO_E,
#define T_SEUIL_PRESS_BAS		SEUIL_PRESS_BAS_A,SEUIL_PRESS_BAS_B,SEUIL_PRESS_BAS_C,SEUIL_PRESS_BAS_D,SEUIL_PRESS_BAS_E
#define T_VACCUM_DURATION		VACCUM_DURATION_A,VACCUM_DURATION_B,VACCUM_DURATION_C,VACCUM_DURATION_D,VACCUM_DURATION_E

//Ordre
#define NO_GRAFCET				0
#define DO_CATCH 				1	
#define DO_DEEP_CATCH 			2
#define DO_FREE					3	

#define T_NAME_GRAFCET 			"Pas de grafcet","Catch","Deep Catch","Free"

#define DO_OPEN					HIGH	
#define DO_CLOSE				LOW	
//Etat
//#define PLEASE_WAIT				0
#define INITIALIAZING			0
#define MOVING_DOWN				1	
#define MOVING_UP				2	
#define PUMPING					3	
#define MONITORING				4 //monitoring vaccum
#define FAILING					5	
#define FILLING_UP				6
#define FREED					7
#define WAITING_TO_CATCH 		8

#define T_NAME_ETAPE 			"Init.","Moving Down","Moving Up","Pumping","Monitoring","Failing","Filling Up","Freed","WaitingToCatch"


//Adresse des pin
//fdc:
#define PIN_ZSH					12 //ZSH = FDC_H
#define PIN_ZSL					11 //ZSL FDC_B
//ev:
//other
#define PIN_PT					A7	//Analog
//adress
#define PIN_DIP0				2	//Analog
#define PIN_DIP1				3	//Analog
#define PIN_DIP2				4	//Analog
#define PIN_LED					A2	//Analog //WATCHDOG
#define PIN_RS485_DE 			10
#define PIN_RS485_RE 			5

#define PIN_XVS          		6
#define PIN_XVP        			7
#define PIN_XVV         		8
#define PIN_XVJ         		9
//tableau de commande des EV : doit partir de 0, incrémental sans interruption
#define XVS 					0
#define XVP 					1
#define XVV 					2
#define XVJ 					3

#define T_NAME_EV				"XVS","XVP","XVV","XVJ"

#define NB_EV					4//XVS+1
#define T_EV 					PIN_XVS, PIN_XVP, PIN_XVV, PIN_XVJ

#define PASS					false //ne force pas la commande des EV

