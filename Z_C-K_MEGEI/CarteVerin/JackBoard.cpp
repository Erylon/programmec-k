#include "D:\Owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteVerin\JackBoard.h"
//#include <Wire.h> //for I2C purposes
/*color
\033[3nm
n:
0 noir
1 rouge
2 vert
3 jaune
4 bleu
5 violet
6 cyan
7 blanc
//*/

extern Jack *oJack;

Jack::Jack(HardwareSerial *refSerial) {
	version=VERSION;
    versionGCode=VERSION_GCODE;

	pinMode(PIN_DIP0, INPUT);
	pinMode(PIN_DIP1, INPUT);
	pinMode(PIN_DIP2, INPUT);

	//Determination de l'adresse de la carte en fonction du DIP
	curAdress=digitalRead(PIN_DIP0)+2*digitalRead(PIN_DIP1)+4*digitalRead(PIN_DIP2);
	curAdress+=RS485_ADRR_START;

	IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
	IFSOFT_SERIAL(mySerial.print("Jack \033[36m"));
	IFSOFT_SERIAL(mySerial.print(curAdress));
	IFSOFT_SERIAL(mySerial.print("\033[37m version "));
	IFSOFT_SERIAL(mySerial.print(version));
	IFSOFT_SERIAL(mySerial.print(" GCode:V"));
  	IFSOFT_SERIAL(mySerial.println(versionGCode));
	IFSOFT_SERIAL(mySerial.println(" Initializing..."));

	pinMode(PIN_ZSH,	INPUT);
	pinMode(PIN_ZSL,	INPUT);
	pinMode(PIN_PT,		INPUT);
	pinMode(PIN_XVJ,	OUTPUT);
	pinMode(PIN_XVV,	OUTPUT);
	pinMode(PIN_XVP,	OUTPUT);
	pinMode(PIN_XVS,	OUTPUT);
	pinMode(PIN_LED,	OUTPUT);
	watchDogStatus=true; 
	//digitalWrite(PIN_LED,watchDogStatus);
	
	oRS485=  new RS485(curAdress, HOIST, PIN_RS485_DE, PIN_RS485_RE , refSerial);
	
	newOrder=false;
	isErrorSent=false;
	isSoftCatch=false;
	isResuckUpMode=false;
	isAutoRecatchMode=false;
	isFrameDetected=false;
	vaccumDuration=A_VACCUM_DURATION[curAdress-RS485_ADRR_START];
	timeOutJackZSH=TIMEOUT_JACK_ZSH;
	movingUpDuration=MOVING_UP_DURATION;
	autocatchDelay=AUTOCATCH_DELAY;
	fillUpTime=FILL_UP_TIME;
	prevTime=millis();
	delay(2);
	startTime=millis();
	mesureTime=startTime;
	moveDownDeepTime=MOVE_DOWN_DEEP_TIME;
	moveDownSoftTime=MOVE_DOWN_SOFT_TIME;

	curGrafcet=DO_FREE;
	curState=	INITIALIAZING;
	curPress=	analogRead(PIN_PT);
	for(int i=0;i<N_MOYENNE_GLISSANTE;i++){
		pilePress[i]=curPress;
	}	
	curPilePress=1;

	PSH=		SEUIL_PRESS_HAUT; 			  					//seuil pression haut
	PSL=		A_SEUIL_PRESS_BAS[curAdress-RS485_ADRR_START];	//seuil pression bas

	pAtm=		A_INIT_PRESS_ATMO[curAdress-RS485_ADRR_START];


	int tEV[NB_EV]={T_EV};				//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_EV;i++){
		EV_Pin[i]=tEV[i];
		//EV_State[i]=LOW;
	}

	cdeEV(XVV,DO_CLOSE);
	cdeEV(XVP,DO_CLOSE);
	cdeEV(XVS,DO_CLOSE);
	cdeEV(XVJ,DO_OPEN);

	cnt=0;			

	errorCode=ERR_NO_ERROR;
	delay(500);
	delayMicroseconds((curAdress-RS485_ADRR_START)*100); //1ms/message
	oRS485->addMsg(HOIST,'G',CDE_PING,VERSION_GCODE);
	//Wire.onRequest(requestEvent);


}


void Jack::cdeEV(int _EV, int _cde, bool _force){
	if(EV_State[_EV]!=_cde || _force){
		digitalWrite(EV_Pin[_EV],_cde);
		//IFSOFT_SERIAL(mySerial.print(EV_State[_EV]));
		//IFSOFT_SERIAL(mySerial.print(_cde));
		if(_cde){
			IFSOFT_SERIAL(mySerial.print("\033[31m"));
		}
		else {
			IFSOFT_SERIAL(mySerial.print("\033[32m"));

		}		
		IFSOFT_SERIAL(mySerial.print(A_EV_NAME[_EV]));
		IFSOFT_SERIAL(mySerial.println("\033[37m"));
	}
	EV_State[_EV]=_cde;
}



void Jack::watchDog(){
	curTime=millis();
	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		prevTime=curTime;
#ifndef MEGA_BOARD
#endif
	}
	#ifdef MESURE_TEMPS_CYLE
		if(++logN%100==0){
			IFSOFT_SERIAL(mySerial.print("100 temps cycle (ms) ="));IFSOFT_SERIAL(mySerial.println(curTime-cycleTime));
			cycleTime=curTime;
		}
	#endif

}


void Jack::loop(){
	//temps de cycle 130µs
	//Serial.println(analogRead(PIN_PT));
	/*
	//*/
/*
	bool canRead=false;
	IFTRACE_SERIAL(canRead=true);

	if(canRead){
		if(Serial.available()){
			while(Serial.available()){
				Serial.read();
			}
			IFTRACE_SERIAL(Serial.println("Sending PING to Hoist"));
			oRS485->addMsg(HOIST,'G',CDE_PING,107);
		}
	}
//*/
	//LIRE LA PRESSION
	curTime=millis();
	if(curTime>mesureTime+2){ //prise de mesure pas plus rapide que 3ms
		mesureTime=curTime;
		//curPress=analogRead(PIN_PT);
		pilePress[curPilePress]=analogRead(PIN_PT);
		curPress=0;
		for (int i=0;i<N_MOYENNE_GLISSANTE;i++){
			curPress+=pilePress[i];
		}
		curPress=(int)(curPress/N_MOYENNE_GLISSANTE);
		curPilePress=(++curPilePress)%N_MOYENNE_GLISSANTE;
		//if(oRS485->isNoMsgOut() && zz>0){
		if(zz>0){
			IFSOFT_SERIAL(mySerial.println(curPress));
			//oRS485->addMsg(HOIST,'V',GET_PRESSURE,(long)curPress);		
			zz--;
		}
		if(curPress<835 && curGrafcet==DO_CATCH && curState==MONITORING && !isFrameDetected){
			IFSOFT_SERIAL(mySerial.println("Trav start"));
			oRS485->addMsg(HOIST,'G',CDE_FRAME_DETECT,(long)1);
			isFrameDetected=true;
		}
		if(curPress>840 && curGrafcet==DO_CATCH && curState==MONITORING && isFrameDetected){
			IFSOFT_SERIAL(mySerial.println("Trav end"));
			oRS485->addMsg(HOIST,'G',CDE_FRAME_DETECT,(long)0);
			isFrameDetected=false;
		}//*/
	}
	oRS485->processMsg();



	//ECOUTE COMMANDES RS485
	if (oRS485->isNewRcvMsg()){
		newOrder=false;
		int curEVState=0;
		switch (oRS485->curRcvMsg->cdeC){
			case '$':
				oRS485->moveNextRcvMsg();
				oRS485->displayPile();
				break;
			case '!':
				displayParams();
				break;
			case 'V':
				switch (oRS485->curRcvMsg->cdeN){
//GET
					case GET_GCODE_RELEASE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,versionGCode);
						break;
					case GET_PRESSURE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)curPress);
						zz=50;
						break;
					case GET_RAW_PRESSURE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,analogRead(PIN_PT));
						break;
					case GET_ATM_PRESSURE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)pAtm);
						break;
					case GET_FILL_UP_TIME:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)fillUpTime);
						break;
					case GET_PSL:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,PSL);
						break;
					case GET_PSH:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,PSH);
						break;
					case GET_ZSH:
					case GET_ZSL:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,digitalRead(oRS485->curRcvMsg->cdeN-GET_ZSL+PIN_ZSL)); //GET_ZSL=120 PIN_ZSL=29 H=+1
						break;
					case GET_XVS:
					case GET_XVP:
					case GET_XVV:
					case GET_XVJ:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,EV_State[oRS485->curRcvMsg->cdeN-GET_XVS]);
						break;
					case GET_MOVE_DOWN_DEEP_TIME:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)moveDownDeepTime);
						break;
					case GET_MOVE_DOWN_SOFT_TIME:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)moveDownSoftTime);
						break;
					case GET_RESUCK_UP_MODE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)isResuckUpMode);
						break;
					case GET_AUTO_RECATCH_MODE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)isAutoRecatchMode);
						break;
					case GET_VACCUM_DURATION:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)vaccumDuration);
						break;
					case GET_TIMEOUT_JACK_ZSH:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)timeOutJackZSH);
						break;
					case GET_MOVING_UP_DURATION:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)movingUpDuration);
						break;
					case GET_AUTOCATCH_DELAY:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)autocatchDelay);
						break;
					default :
						oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);
						break;
				}
				break;
//SET
			case 'S':
				switch (oRS485->curRcvMsg->cdeN){
					case SET_RESUCK_UP_MODE:
						isResuckUpMode=(oRS485->curRcvMsg->param>0);
						isAutoRecatchMode=false;
						break;
					case SET_AUTO_RECATCH_MODE:
						isAutoRecatchMode=(oRS485->curRcvMsg->param>0);
						isResuckUpMode=false;
						break;
					case SET_ATM_PRESSURE:
						if(digitalRead(PIN_ZSH)){
							pAtm=curPress;
							oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,GET_PRESSURE,(long)pAtm);
						}
						else {
							oRS485->addMsg(HOIST,'E',ERR_CANT_READ_PRESS_ATM);
						}
						break;
					case SET_MOVE_DOWN_DEEP_TIME:
						moveDownDeepTime=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("moveDownDeepTime="));
						IFSOFT_SERIAL(mySerial.println(moveDownDeepTime));
						break;
					case SET_MOVE_DOWN_SOFT_TIME:
						moveDownSoftTime=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("moveDownSoftTime="));
						IFSOFT_SERIAL(mySerial.println(moveDownSoftTime));
						break;
					case SET_FILL_UP_TIME:
						fillUpTime=oRS485->curRcvMsg->param;
						break;
					case SET_VACCUM_DURATION:
						vaccumDuration=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("vaccumDuration="));
						IFSOFT_SERIAL(mySerial.println(vaccumDuration));
						break;
					case SET_PSL:
						PSL=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("PSL="));
						IFSOFT_SERIAL(mySerial.println(PSL));
						break;
					case SET_PSH:
						PSH=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("PSH="));
						IFSOFT_SERIAL(mySerial.println(PSH));
						break;
					case SET_TIMEOUT_JACK_ZSH:
						timeOutJackZSH=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("timeOutJackZSH="));
						IFSOFT_SERIAL(mySerial.println(timeOutJackZSH));
						break;
					case SET_MOVING_UP_DURATION:
						movingUpDuration=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("movingUpDuration="));
						IFSOFT_SERIAL(mySerial.println(movingUpDuration));
						break;
					case SET_AUTOCATCH_DELAY:
						autocatchDelay=oRS485->curRcvMsg->param;
						IFSOFT_SERIAL(mySerial.print("autocatchDelay="));
						IFSOFT_SERIAL(mySerial.println(autocatchDelay));
						break;
					default :
						oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);
						break;
				}
				break;
			case 'G' :
				switch (oRS485->curRcvMsg->cdeN){
				case CDE_DISPLAY_PARAMS:
					displayParams();
					break;
				case CDE_CATCH:
					curGrafcet=DO_CATCH;
					isSoftCatch=true;
					errorCode=ERR_NO_ERROR;
					curState=INITIALIAZING;
					break;
				case CDE_DEEP_CATCH:
					curGrafcet=DO_CATCH;
					curGrafcet=DO_DEEP_CATCH;
					isSoftCatch=false;
					errorCode=ERR_NO_ERROR;
					curState=INITIALIAZING;
					break;
				case CDE_FREE:
					curGrafcet=DO_FREE;
					errorCode=ERR_NO_ERROR;
					curState=INITIALIAZING;
					break;
				case CDE_XVS_OPEN:
				case CDE_XVP_OPEN:
				case CDE_XVV_OPEN:
				case CDE_XVJ_OPEN:
					cdeEV(oRS485->curRcvMsg->cdeN-CDE_XVS_OPEN,oRS485->curRcvMsg->param>0);
					if(EV_State[oRS485->curRcvMsg->cdeN-CDE_XVS_OPEN]){
						IFSOFT_SERIAL(mySerial.print("\033[31m"));
					}
					else {
						IFSOFT_SERIAL(mySerial.print("\033[32m"));
					}
					IFSOFT_SERIAL(mySerial.print(A_EV_NAME[oRS485->curRcvMsg->cdeN-CDE_XVS_OPEN]));
					IFSOFT_SERIAL(mySerial.println("\033[37m"));
					break;
				case CDE_PING:
					oRS485->addMsg(HOIST,'G',CDE_PONG,VERSION_GCODE);
					break;
				case CDE_PONG:
					if(oRS485->curRcvMsg->fromAdr==HOIST){
						isMasterReady=true;
					}
					break;
				case CDE_ACK_ERROR:
					errorCode=ERR_NO_ERROR;
					curGrafcet=NO_GRAFCET;
					break;
				default :
					oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);//oRS485->curRcvMsg->msgId);
					break;
				}
				break;
			default ://It's not G V S GCode : 
				oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);
				break;		
		}
		oRS485->moveNextRcvMsg();
	}



/******************************************************************************************************* */
/**                                                      GRAFETS                                         */
/******************************************************************************************************* */

	//SUIVI DES ACTION
	if (!errorCode){
		switch(curGrafcet){
			case DO_FREE:
				switch(curState){
					case MOVING_UP:
						if(digitalRead(PIN_ZSH)){
							#ifdef MESURE_TEMPS
								oRS485->addMsg(HOIST,'G',CDE_INFO,curTime-startChrono);
							#endif
							//cdeEV(XVJ,DO_CLOSE);
							cdeEV(XVP,DO_CLOSE);	//Au cas fdc atteint avant fin libération du vide
							IFSOFT_SERIAL(mySerial.println("FREED"));
							oRS485->addMsg(HOIST,'G',CDE_FREE,SUCCESS);
							startTime=curTime;
							if(isAutoRecatchMode){
								curState=WAITING_TO_CATCH;
							}
							else {
								curState=MONITORING;
							}
						}
						else if(curTime-startTime>timeOutJackZSH){//Time out etant plus long que FILL_UP_TIME on le verifie ne premier
							IFSOFT_SERIAL(mySerial.println("TIMEOUT MOVING UP"));
							//errorCode=ERR_TIMEOUT_JACK_ZSH;
							//oRS485->addMsg(HOIST,'E',ERR_TIMEOUT_JACK_ZSH);	
							oRS485->addMsg(HOIST,'G',CDE_FREE,SUCCESS);
							//isErrorSent=true;
							cdeEV(XVP,DO_CLOSE);
							//cdeEV(XVJ,DO_CLOSE);
							startTime=curTime;
							if(isAutoRecatchMode){
								curState=WAITING_TO_CATCH;
							}
							else {
								curGrafcet=NO_GRAFCET;
							}

						} 
						else if (curTime-startTime>fillUpTime){
							cdeEV(XVP,DO_CLOSE);
						}
//*/
						break;
					case WAITING_TO_CATCH:
						if(curTime-startTime>autocatchDelay){
								curGrafcet=DO_CATCH;
								isSoftCatch=true;
								errorCode=ERR_NO_ERROR;
								curState=INITIALIAZING;
						}
						break;
					case MONITORING:
						if(curTime-startTime>PATM_WAITING_TIME){
							curGrafcet=NO_GRAFCET;
							pAtm=curPress;
							//oRS485->addMsg(HOIST,'G',GET_ATM_PRESSURE,pAtm);
						}
						break;
					default:
						//IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
						//IFSOFT_SERIAL(mySerial.println("\033[35m____\033[37m"));
						#ifdef MESURE_TEMPS
							startChrono=curTime;
						#endif
						IFSOFT_SERIAL(mySerial.println("START MOVING UP"));
						cdeEV(XVJ,DO_OPEN);//cdeEV(XVJ,DO_CLOSE);
						cdeEV(XVP,DO_OPEN);
						cdeEV(XVV,DO_CLOSE);
						cdeEV(XVS,DO_CLOSE);
						isFrameDetected=false;
						curState=MOVING_UP;
						startTime=curTime;
						break;
				}
				break;
			//END DO_FREE
			case DO_DEEP_CATCH:
			case DO_CATCH:
				switch(curState){
					case MOVING_DOWN:
						#ifdef MESURE_TEMPS
						if(digitalRead(PIN_ZSL)){
							oRS485->addMsg(HOIST,'G',CDE_INFO,curTime-startChrono);
						}
						#endif

						//isEndOfCatch : arret sur fdc dans le cas d'une soft catch
						if((!isSoftCatch &&  (curTime-startTime>moveDownDeepTime))
						 ||( isSoftCatch && ((curTime-startTime>moveDownSoftTime) || digitalRead(PIN_ZSL)))){
							if(!isSoftCatch){
								IFSOFT_SERIAL(mySerial.println("deep catch"));
							}
							cdeEV(XVV,DO_OPEN); 				// ouverture EV pompage
							startTime=curTime;
							curState=PUMPING;
							IFSOFT_SERIAL(mySerial.println("Pumping"));
						}
						break;
					case PUMPING:
						if((curTime-startTime>vaccumDuration) ){//}  ||( (curPress<(PSH+pAtm))) ){
							IFSOFT_SERIAL(mySerial.println("MOVING UP"));
							cdeEV(XVV,DO_CLOSE);				// On ferme le pompage
							cdeEV(XVJ,DO_OPEN);					// On envoi de l'air dans le verin pour le faire monter
							startTime=curTime;
							curState=MOVING_UP;
						}
						break;
					case MOVING_UP:
						if((curTime-startTime>movingUpDuration) || digitalRead(PIN_ZSH)) {
							curState=FAILING;	
							if(curPress<(PSH+pAtm)){
								curState=MONITORING;
								IFSOFT_SERIAL(mySerial.print("Linked P="));
								IFSOFT_SERIAL(mySerial.print(curPress));
								IFSOFT_SERIAL(mySerial.print("<Patm="));
								IFSOFT_SERIAL(mySerial.println(pAtm));
								oRS485->addMsg(HOIST,'G',CDE_CATCH,SUCCESS);
							}
							//else if(digitalRead(PIN_ZSH)){
							//	IFSOFT_SERIAL(mySerial.print("ZSH !"));
							//curState=FAILING;
							//}
						}
						break;
					case MONITORING:
						if(curPress>pAtm+PSH+PSL){
							IFSOFT_SERIAL(mySerial.print("P="));
							IFSOFT_SERIAL(mySerial.print(curPress));
							IFSOFT_SERIAL(mySerial.print(">Patm="));
							IFSOFT_SERIAL(mySerial.print(pAtm));
							IFSOFT_SERIAL(mySerial.print("+PSL="));
							IFSOFT_SERIAL(mySerial.print(PSL));
							IFSOFT_SERIAL(mySerial.print("+PSH="));
							IFSOFT_SERIAL(mySerial.println(PSH));
							curState=FAILING;
						}
						break;
					case FAILING:
						cdeEV(XVV,DO_CLOSE);
						IFSOFT_SERIAL(mySerial.println("Failure"));
						oRS485->addMsg(HOIST,'G',CDE_CATCH,CATCH_FAILURE);
						curGrafcet=DO_FREE;
						curState=INITIALIAZING;
						/*
						if(isAutoRecatchMode){
							curGrafcet=DO_CATCH;
							isSoftCatch=true;
							errorCode=ERR_NO_ERROR;
							curState=INITIALIAZING;
						}
						//*/
						break;
					default:
						//IFSOFT_SERIAL(mySerial.print("\033[2J"));							// Start writing from a new clean line
						//IFSOFT_SERIAL(mySerial.println("\033[35m____\033[37m"));
						#ifdef MESURE_TEMPS
							startChrono=curTime;
						#endif
						IFSOFT_SERIAL(mySerial.println("START MOVING DOWN"));
						cdeEV(XVS,DO_CLOSE);
						cdeEV(XVP,DO_CLOSE);
						cdeEV(XVV,DO_CLOSE);
						cdeEV(XVJ,DO_CLOSE);
						curState=MOVING_DOWN;
						startTime=curTime;
						isFrameDetected=false;
						break;
				}
				break;
			case NO_GRAFCET:
				break;
			//END DO_CATCH
			default:
				errorCode=ERR_TARGET_UNDEFINED;
				IFSOFT_SERIAL(mySerial.print("Undefined order="));
				IFSOFT_SERIAL(mySerial.println(curGrafcet));
			break;
		}

	}

	if (errorCode && !isErrorSent){
        IFSOFT_SERIAL(mySerial.print("Error : "));
        IFSOFT_SERIAL(mySerial.print(errorCode));
        //IFTRACE_SERIAL(Serial.println(errorMsg[errorCode]));
        oRS485->addMsg(HOIST,'E',errorCode);
        isErrorSent=true;			//empeche le spamm
        //errorCode=false;
    }
    watchDog();
}

void Jack::displayParams(){
	#ifdef SOFT_SERIAL
		mySerial.print("\n--------------------------------------------\r\nETAT DU VERIN \033[36m");
		mySerial.println(curAdress);
		mySerial.print("\033[37mversion=\t\t");mySerial.println(version);
		mySerial.print("version GCode=\t\t");mySerial.println(versionGCode);
		mySerial.print("ZSL:\t\t\t");mySerial.println(digitalRead(PIN_ZSL));
		mySerial.print("PSL:\t\t\t");mySerial.print(PSL);mySerial.print("\t bar:");mySerial.println(PSL/120.);
		mySerial.print("PSH:\t\t\t");mySerial.print(PSH);mySerial.print("\t bar:");mySerial.println(PSH/120.);
		mySerial.print("pAtm=\t\t\t");mySerial.print(pAtm);mySerial.print("\t bar:");mySerial.println(pAtm/120.);
		mySerial.print("curPress=\t\t");mySerial.print(curPress);mySerial.print("\t bar:");mySerial.println(curPress/120.);
		mySerial.print("curGrafcet=\t\t");mySerial.print(A_NAME_GRAFCET[curGrafcet]);mySerial.print("\t Etape ");mySerial.println(A_NAME_ETAPE[curState]);
		mySerial.print("vaccumDuration(ms)=\t");mySerial.println(vaccumDuration);
		mySerial.print("timeOutJackZSH(ms)=\t");mySerial.println(timeOutJackZSH);
		mySerial.print("movingUpDuration(ms)=\t");mySerial.println(movingUpDuration);
		mySerial.print("isSoftCatch=\t\t");mySerial.println(isSoftCatch);
		mySerial.print("isResuckUpMode=\t\t");mySerial.println(isResuckUpMode);
		mySerial.print("isAutoRecatchMod=\t");mySerial.println(isAutoRecatchMode);
		mySerial.print("curTime=\t\t");mySerial.println(curTime);
		mySerial.print("prevTime=\t\t");mySerial.println(prevTime);
		mySerial.print("mesureTime=\t\t");mySerial.println(mesureTime);
		mySerial.print("startTime=\t\t");mySerial.println(startTime);
		mySerial.print("moveDownSoftTim=\t");mySerial.println(moveDownSoftTime);
		mySerial.print("moveDownDeepTim=\t");mySerial.println(moveDownDeepTime);
		mySerial.print("cycleTime=\t\t");mySerial.println(cycleTime);
		for(int i=0;i<NB_EV;i++){
			mySerial.print(A_EV_NAME[i]);mySerial.print("=\t\t\t");mySerial.println(EV_State[i]);
		}
	#endif
}
