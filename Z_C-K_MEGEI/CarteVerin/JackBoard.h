#ifndef JackBoard_h
#define JackBoard_h

#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CodeRelease.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorList.h"
#include "D:\Owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteVerin\JackBoardPinList.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\GCodeC-K.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRS485\ProtocoleRS485.cpp"

#define RS485_ADRR_START	65
//void	requestEvent();

unsigned long		curTime,startChrono;

unsigned long 		zz=0;

unsigned long 		logN=0;

const char* 		A_EV_NAME[]=		{T_NAME_EV};
const char* 		A_VALVE_STATUS[]=	{"CLOSED","OPENED"};
int 				A_SEUIL_PRESS_BAS[]={T_SEUIL_PRESS_BAS};
int 				A_VACCUM_DURATION[]={T_VACCUM_DURATION};
int 				A_INIT_PRESS_ATMO[]={T_INIT_PRESS_ATMO};
const char*			A_NAME_ETAPE[]={T_NAME_ETAPE};
const char*			A_NAME_GRAFCET[]={T_NAME_GRAFCET};

class Jack {
public:
					Jack(HardwareSerial *_refSerial);
	void			loop();
	void			cdeEV(int _pinEV, int _cde, bool _force=false);
	void 			displayParams();

	RS485 			*oRS485;

	int				curGrafcet;
	int				curState;
	int				curPress;
	char 			curAdress;
	int 			pilePress[N_MOYENNE_GLISSANTE];
	int 			curPilePress;
	int				PSH; 			//seuil pression haut
	int				PSL;			//seuil pression bas
	int				pAtm;			//pression Atmospherique

	int				EV_Pin[NB_EV];
	int				EV_State[NB_EV];
	int				errorCode;

	const char* 	version;
	unsigned int 	versionGCode;

	bool			newOrder;


private:
	void			watchDog();
	bool			watchDogStatus;
	unsigned long	curTime,prevTime,startTime,cycleTime,mesureTime;
	unsigned long   moveDownDeepTime, moveDownSoftTime, vaccumDuration,fillUpTime,
					timeOutJackZSH,movingUpDuration,autocatchDelay;

	bool			isErrorSent;
	bool 			isMasterReady;
	bool 			isFrameDetected;
	
	int 			cnt;
	bool 			isSoftCatch;
	bool 			isResuckUpMode;
	bool 			isAutoRecatchMode;
};



#endif
