#ifndef C_KErrorList_h
#define C_KErrorList_h

//Etat d'erreur
#define ERR_NO_ERROR					0 //Obligatoirement 0
#define ERR_TARGET_UNDEFINED    		1
//#define ERR_COM 						2

/*#define ERR_ADR_CONFLICT				3
//#define ERR_TIMEOUT_VACCUM				4
#define ERR_TIMEOUT_FDC					5
#define ERR_COM_INCOMING_ADR			6
*/
#define ERR_PTP_SL_ERROR				7
#define ERR_PTV_SH_ERROR				8
#define ERR_DURATION_OUT_OF_RANGE  		9
#define ERR_GCODE_NOT_SUPPORTED    		10

#define	ERR_ANSW_ROBOT					11 
#define ERR_NO_MOVE_UP					12
#define ERR_NO_MOVE_DOWN				13
#define	ERR_NO_STOP						14
#define ERR_NO_ENCODEUR					15
#define ERR_NRF_NOT_HERE				16
#define ERR_COM_PROTOCOL				17
#define ERR_CORRUPTED_PAYLOAD_FLUSHED	18
#define ERR_PILE_IN_OVERFLOW    		19
#define ERR_PILE_OUT_OVERFLOW    		20
#define ERR_TOO_MANY_RETRY				21

#define ERR_TOO_MANY_TRAVERSES			22
#define ERR_ALREADY_DETECTED_TRAVERSES	23
//#define ERR_OVERFLOW_JACK_PILE			24
#define ERR_CANT_READ_PRESS_ATM			25
#define ERR_SD_CARD						26
#define ERR_CATCH_FAILURE	    		27

#define ERR_TOO_LONG_PACKET				28
#define ERR_WRONG_CRC					29
#define ERR_MALFORMED_PACKED			30
#define ERR_HEADLESS_PACKET				31
#define ERR_TIMEOUT_JACK_ZSH 			32
#define ERR_TIMEOUT_FREE				33
#define ERR_GRAFCET_TIMEOUT 			34


#define LAST_ERROR_CODE					34


#endif