#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRS485\ProtocoleRS485.h"


uint16_t recurseCRC16(unsigned char *CRC16_Data_Array, uint8_t arraySize, uint16_t crc=0xFFFF, uint8_t curByte=0) { //array size max 255
  int i;
  crc ^= CRC16_Data_Array[curByte];
  for (i = 0; i < 8; ++i) {
    if (crc & 1)
      crc = (crc >> 1) ^ 0xA001;
    else
      crc = (crc >> 1);
  }
  curByte++;
  if(curByte==arraySize){
    return crc;
  }
  else {
    return recurseCRC16(CRC16_Data_Array,arraySize,crc,curByte);
  }

}

RS485::RS485(char _myAdress, char _reportAdress, int _pinRS485_DE, int _pinRS485_RE, HardwareSerial *_refSerial){
	changeAdress(_myAdress);
	reportAdress=_reportAdress;
	refSerial=_refSerial;
	isHeadLost=isBadCRCMsgRcv=false;
	nbMsgError=0;
	curErrorIdx=0;
	curRcvMsg=NULL;
	pinRS485_DE=_pinRS485_DE;
	pinRS485_RE=_pinRS485_RE;
	errorMsg.fromAdr=myAdress;
	errorMsg.toAdr=reportAdress;
	errorMsg.cdeC='A';
	errorMsg.cdeN=ALERT;
	errorMsg.param=NO_PARAM;
	errorMsg.crc=0;
	
	noMsg.fromAdr=myAdress;
	noMsg.toAdr=reportAdress;
	noMsg.cdeC='G';
	noMsg.cdeN=CDE_REPING;//CDE_QUERY_MSG;
	noMsg.param=0;
	noMsg.crc=0;
	noMsg.crc=recurseCRC16((uint8_t*)&noMsg,MSG_SIZE);

	queryMsg.fromAdr=myAdress;
	queryMsg.cdeC='G';
	queryMsg.cdeN=CDE_QUERY_MSG;
	queryMsg.param=0;
	queryMsg.crc=0;

	queryIdx=NB_SLAVE-1;
	lastAnswerFrom=RS485_ADRESS[queryIdx];
	msgByteIdx=0;
	nLog=0;
	delay(5);
	prevPingTime=prevMsgTime=millis();
	pinMode(pinRS485_DE, OUTPUT);
	digitalWrite(pinRS485_DE, LOW);
	pinMode(pinRS485_RE, OUTPUT);
	digitalWrite(pinRS485_RE, LOW);

}

void RS485::changeAdress(char newAdress){						//Used for debug purposes
	myAdress=newAdress;
	curMsgId=0;
	errorMsg.fromAdr=myAdress;
	isMaster=(myAdress==reportAdress)?true:false;
	for(int i=0;i<2;i++){
		pileOverFlow[i]=false;
		isPileLocked[i]=false;
		curPileIdx[i]=0;
		lastPileIdx[i]=curPileIdx[i];
	}
	for(int d=0; d<2 ; d++){
		for(int i=0;i<PILE_SIZE;i++){
			pileMsg[i][d].fromAdr=	'@';
			pileMsg[i][d].toAdr=	'~';
			pileMsg[i][d].cdeC=		'-';
			pileMsg[i][d].cdeN=		d;
			pileMsg[i][d].param=	99999;
			pileMsg[i][d].msgId=	0;
			pileMsg[i][d].crc=		0;
		}
	}
	IFTRACE_SERIAL(Serial.print("I am "));IFTRACE_SERIAL(Serial.println(myAdress));
	IFSOFT_SERIAL(mySerial.print("I am "));IFSOFT_SERIAL(mySerial.println(myAdress));
}


bool RS485::peekMsg(char _fromAdr,char _cdeC, int _cdeN){
	bool result=false;

	if(isNewRcvMsg()){//There is a message
		int nextPileIdx,tempPileIdx=(curPileIdx[MSG_IN]+1)%PILE_SIZE;
		while(tempPileIdx!=lastPileIdx[MSG_IN] || result){
			if(    pileMsg[tempPileIdx][MSG_IN].fromAdr==	_fromAdr 
				&& pileMsg[tempPileIdx][MSG_IN].cdeC==		_cdeC
				&& pileMsg[tempPileIdx][MSG_IN].cdeN==		_cdeN){
				result=true;
				peekedParam=pileMsg[tempPileIdx][MSG_IN].param;

				//enlever le message de la pile
				while(tempPileIdx!=lastPileIdx[MSG_IN]){
					nextPileIdx=(curPileIdx[MSG_IN]+1)%PILE_SIZE;
					pileMsg[tempPileIdx][MSG_IN].fromAdr=pileMsg[nextPileIdx][MSG_IN].fromAdr;
					pileMsg[tempPileIdx][MSG_IN].toAdr 	=pileMsg[nextPileIdx][MSG_IN].toAdr;
					pileMsg[tempPileIdx][MSG_IN].cdeC 	=pileMsg[nextPileIdx][MSG_IN].cdeC;
					pileMsg[tempPileIdx][MSG_IN].cdeN 	=pileMsg[nextPileIdx][MSG_IN].cdeN;
					pileMsg[tempPileIdx][MSG_IN].param 	=pileMsg[nextPileIdx][MSG_IN].param;
					pileMsg[tempPileIdx][MSG_IN].msgId 	=pileMsg[nextPileIdx][MSG_IN].msgId;
					pileMsg[tempPileIdx][MSG_IN].msgId 	=pileMsg[nextPileIdx][MSG_IN].msgId;
					pileMsg[tempPileIdx][MSG_IN].crc 	=pileMsg[nextPileIdx][MSG_IN].crc;
					tempPileIdx=nextPileIdx;
					lastPileIdx[MSG_IN]=(lastPileIdx[MSG_IN]-1)%PILE_SIZE;
				}
				moveNextRcvMsg(); //Only if result
			}
			tempPileIdx=(curPileIdx[MSG_IN]+1)%PILE_SIZE;
		}
	}
	return result;
}

bool RS485::isNoMsgOut(){
	return lastPileIdx[MSG_OUT]==curPileIdx[MSG_OUT];
}

void RS485::addMsg(char _toFromAdrOut_toAdrIn, char _cdeC, int _cdeN, long _param, byte _curMsgId, byte _dir){	 // ajoute un message à la liste des messages
	#ifndef NO_RS485
		if(!pileOverFlow[_dir]){
			lastPileIdx[_dir]=(lastPileIdx[_dir]+1) %PILE_SIZE;		//move next
			if(curPileIdx[_dir]==lastPileIdx[_dir]){
				if(!isMaster){
					lastPileIdx[_dir]=(lastPileIdx[_dir]-1)%PILE_SIZE;	//get back to previous Idx
				}
				pileOverFlow[_dir]=true;							//message not added. Priority : overflow report
				errorMsg.param=ERR_PILE_OUT_OVERFLOW;
				errorMsg.crc=0;
				errorMsg.crc=recurseCRC16((uint8_t*)&errorMsg,MSG_SIZE); // compute CRC16
			}
			if (!pileOverFlow[_dir] || isMaster) {
				pileMsg[lastPileIdx[_dir]][_dir].cdeC 	=_cdeC;
				pileMsg[lastPileIdx[_dir]][_dir].cdeN 	=_cdeN;
				pileMsg[lastPileIdx[_dir]][_dir].param 	=_param;
				pileMsg[lastPileIdx[_dir]][_dir].msgId  =_curMsgId;
				if(_dir == MSG_OUT){
					pileMsg[lastPileIdx[_dir]][_dir].fromAdr=myAdress;
					pileMsg[lastPileIdx[_dir]][_dir].toAdr	=_toFromAdrOut_toAdrIn;
					pileMsg[lastPileIdx[_dir]][_dir].msgId=++curMsgId;
					pileMsg[lastPileIdx[_dir]][_dir].crc=0;			//need to be done because crc might not be =0 to that index the last time it cycled the pile
					pileMsg[lastPileIdx[_dir]][_dir].crc=recurseCRC16((uint8_t*)&pileMsg[lastPileIdx[_dir]][_dir],MSG_SIZE); // compute CRC16
				}
				else{
					pileMsg[lastPileIdx[_dir]][_dir].fromAdr=_toFromAdrOut_toAdrIn;
					pileMsg[lastPileIdx[_dir]][_dir].toAdr	=myAdress;
					if(lastPileIdx[MSG_IN]==(curPileIdx[MSG_IN]+1)%PILE_SIZE){//Pile previously empty : set curRcvMsg
						curRcvMsg=&pileMsg[lastPileIdx[MSG_IN]][MSG_IN];
					}

				}
			}
		}
		//ignore if overflow
	#endif
}

bool RS485::isNewRcvMsg(){
	#ifdef NO_RS485
		return false;
	#else
		return (curPileIdx[MSG_IN]!=lastPileIdx[MSG_IN]);
	#endif
}

void RS485::moveNextRcvMsg(){
	#ifndef NO_RS485
		if(curPileIdx[MSG_IN]!=lastPileIdx[MSG_IN]){
			curPileIdx[MSG_IN]=(curPileIdx[MSG_IN]+1)%PILE_SIZE;
			//IFTRACE_SERIAL(Serial.print("Next msg from Id :"));IFTRACE_SERIAL(Serial.println(pileMsg[curPileIdx[MSG_IN]][MSG_IN].msgId));
			//IFSOFT_SERIAL(mySerial.print("Next msg from Id :"));IFSOFT_SERIAL(mySerial.println(pileMsg[curPileIdx[MSG_IN]][MSG_IN].msgId));
			curRcvMsg=&pileMsg[curPileIdx[MSG_IN]][MSG_IN];
		}
	#endif
}


void RS485::sendMsg(type_msgRS485 *_msg){
	#ifndef NO_RS485
		#ifdef VIRTUAL_BOARD
			IFTRACE_SERIAL(Serial.print("sending Virtual"));displayMsg(_msg);IFTRACE_SERIAL(Serial.println());
		#else
			if(!(_msg->cdeN==CDE_QUERY_MSG && _msg->cdeC=='G') && !(_msg->cdeN==CDE_REPING && _msg->cdeC=='G')){
				#ifdef VERBOSE_RS485
				IFTRACE_SERIAL(Serial.print(nLog++));IFTRACE_SERIAL(displayMsg(_msg));IFTRACE_SERIAL(Serial.println());
				IFSOFT_SERIAL(displayMsg(_msg));IFSOFT_SERIAL(mySerial.println());
				#endif
			}	
			else {
				//IFTRACE_SERIAL(Serial.print(_msg->toAdr));
				//IFTRACE_SERIAL(Serial.print(_msg->cdeN));
			}
			//*/
			if(pileOverFlow[MSG_IN] || pileOverFlow[MSG_OUT]){
				_msg=&errorMsg;	
			}
	
	#ifdef MESURE_TEMPS_CYLE
		if(++nLog%1000==0){
			Serial.print("1000 temps cycle (ms) =");Serial.println(curTime-cycleTime);
			cycleTime=curTime;
		}
	#endif
			#ifndef NO_SEND //Prend <1.7ms si PIN_DELAY=100 et DELAY_TO_SEND=40
				delayMicroseconds(DELAY_TO_SEND);
				digitalWrite(pinRS485_DE, HIGH);
				digitalWrite(pinRS485_RE, HIGH);
				delayMicroseconds (PIN_DELAY);								//gives time before sending
				refSerial->write(HEAD);
				refSerial->write(_msg->fromAdr);
				refSerial->write(_msg->toAdr);
				refSerial->write(_msg->cdeC);
				refSerial->write((byte)(_msg->cdeN>>8));
				refSerial->write((byte)(_msg->cdeN));
				refSerial->write((byte)(_msg->param>>24));
				refSerial->write((byte)(_msg->param>>16));
				refSerial->write((byte)(_msg->param>>8));
				refSerial->write((byte)(_msg->param));
				refSerial->write(_msg->msgId);
				refSerial->write((byte)(_msg->crc>>8));
				refSerial->write((byte)(_msg->crc));
				refSerial->write(TAIL);
				refSerial->println();
				refSerial->flush();
				delayMicroseconds (PIN_DELAY);								//to be sure to have sent everything. TODO : test removal of this command
				digitalWrite(pinRS485_DE, LOW);
				digitalWrite(pinRS485_RE, LOW);
				delayMicroseconds(DELAY_TO_SEND);
				//delay(2);//ER107

			#endif
		#endif
		prevMsgTime=curTime; //ER107
	#endif
}

void RS485::processMsg(){										//General idea : Priority is to listen. We can write if there is nothing more to listen
	curTime=millis();
	//if(curTime-prevMsgTime>1){									//but it's not yet ready to be read

#ifndef NO_RS485
	/*if(pileOverFlow[MSG_IN] || pileOverFlow[MSG_OUT]){			//we cannot add anymore messages we are on overflow
		if (!refSerial->available()){							//rien dans le buffer : on peut ecrire
			for(int d=0;d<2;d++){
				if(pileOverFlow[d] && !isPileLocked[d]) { 
					if (isMaster){								//noone to report to
						IFTRACE_SERIAL(Serial.print(nLog++));
						IFTRACE_SERIAL(Serial.print(":overflow [in 0 /out 1] :"));IFTRACE_SERIAL(Serial.println(d));
						IFTRACE_SERIAL(Serial.println(" -> overwriting"));
						pileOverFlow[d]=false;						
					}
					else {
						isPileLocked[d]=true;				//to send it only once
						//sendMsg(&errorMsg);
						nbMsgError++;
						memError[curErrorIdx]=ERR_PILE_OUT_OVERFLOW;
						curErrorIdx=min(curErrorIdx++,ERROR_ARRAY_SIZE-1);

					}
				}//else : do nothing to avoid spamming
			}
			
    	}//else : we don't take into account what's coming because we are on overflow. TODO : Except RAZ ?
    	
	}
	
	else {//*/  	
		if(refSerial->available()){											//there is something to be read
			//Serial.print(msgByteIdx);
			rawMsgRcv[msgByteIdx]=refSerial->read(); 
			//Serial.print((char)rawMsgRcv[msgByteIdx]);
			/*IFSOFT_SERIAL(mySerial.print(rawMsgRcv[msgByteIdx]));
			IFSOFT_SERIAL(mySerial.print("/"));
			IFSOFT_SERIAL(mySerial.println((char)rawMsgRcv[msgByteIdx]));
			*/
/*
			IFDEBUG(Serial.print(nLog++));
			IFDEBUG(Serial.print(":["));
			IFDEBUG(Serial.print(msgByteIdx));
			IFDEBUG(Serial.print("]="));
			IFDEBUG(Serial.print(rawMsgRcv[msgByteIdx]));
			IFDEBUG(Serial.print("/"));
			IFDEBUG(Serial.println((char)rawMsgRcv[msgByteIdx]));
/*
			IFSOFT_SERIAL(mySerial.print(nLog++));
			IFSOFT_SE                                                     RIAL(mySerial.print(":["));
			IFSOFT_SERIAL(mySerial.print(msgByteIdx));
			IFSOFT_SERIAL(mySerial.print("]="));
			IFSOFT_SERIAL(mySerial.print(rawMsgRcv[msgByteIdx]));
			IFSOFT_SERIAL(mySerial.print("/"));
			IFSOFT_SERIAL(mySerial.println((char)rawMsgRcv[msgByteIdx]));
//*/
			if(isHeadLost){
				IFTRACE_SERIAL(Serial.print(rawMsgRcv[msgByteIdx]));
			}
			if(rawMsgRcv[msgByteIdx]==HEAD && isHeadLost){
				isHeadLost=false;
				msgByteIdx=0;
				rawMsgRcv[msgByteIdx]=HEAD;
				#ifdef COLOR
					IFTRACE_SERIAL(Serial.print("\033[31m"));
				#endif
				IFTRACE_SERIAL(Serial.println("HEAD Refound"));
				#ifdef COLOR
					IFTRACE_SERIAL(Serial.print("\033[37m"));
				#endif
			}
			msgByteIdx++;
			if (rawMsgRcv[0]==HEAD){
				if(msgByteIdx>MSG_SIZE+1){//HEAD+MSG+TAIL
					prevMsgTime=curTime;
					if(rawMsgRcv[msgByteIdx-1]==TAIL){								//new packet received
						type_msgRS485  		tempRcvMsg;
						tempRcvMsg.fromAdr 	=rawMsgRcv[1];							//0 is HEAD, so we start from 1
						tempRcvMsg.toAdr 	=rawMsgRcv[2];
						tempRcvMsg.cdeC 	=rawMsgRcv[3];
						tempRcvMsg.cdeN 	=((int)rawMsgRcv[4]<<8)+rawMsgRcv[5];
						tempRcvMsg.param 	=((long)rawMsgRcv[6]<<24)+((long)rawMsgRcv[7]<<16)+((long)rawMsgRcv[8]<<8)+rawMsgRcv[9];
						tempRcvMsg.msgId 	=rawMsgRcv[10];
						tempRcvMsg.crc 		=0;
						if(tempRcvMsg.toAdr==myAdress){								//this message is for me
							uint16_t tempCRC=recurseCRC16((uint8_t*)&tempRcvMsg,MSG_SIZE);
							if(tempCRC==(uint16_t)((uint16_t)(rawMsgRcv[11]<<8)+rawMsgRcv[12])){//this message as a valid CRC
								isBadCRCMsgRcv=false;
								prevPingTime=curTime; //Any a priori valid received message is like a ping so restarts PingTime
								if((tempRcvMsg.cdeC==queryMsg.cdeC) && (tempRcvMsg.cdeN==queryMsg.cdeN)){//This is a query message
									lastAnswerFrom=tempRcvMsg.fromAdr;				//for master to check from where he gets a reply
									if(!isMaster) {									//ignore if master : can only be a no_msg
										if(pileOverFlow[MSG_IN]){					//we cannot add anymore messages we are on overflow
										}
								    	//else : we don't take into account what's coming because we are on overflow. TODO : Except RAZ ?
										else if(lastPileIdx[MSG_OUT]!=curPileIdx[MSG_OUT]){	//is there something to be written ?
											isPileLocked[MSG_OUT]=false;			  	//free the pileOut overflow in case
											pileOverFlow[MSG_OUT]=false;
											curPileIdx[MSG_OUT]=(curPileIdx[MSG_OUT]+1)%PILE_SIZE;	//select the index of the msg to be sent 
											sendMsg(&pileMsg[curPileIdx[MSG_OUT]][MSG_OUT]);
										}
										else {
											//ER 107
											sendMsg(&noMsg);							//There is nothing to send : tell it
										}
									}
								}
								else { 												//this is not a query msg : it is an order msg
									//if(tempRcvMsg.cdeC!='G' || tempRcvMsg.cdeN!=CDE_REPING){//When it's a reping dont put it in the pile
										addMsg(tempRcvMsg.fromAdr, tempRcvMsg.cdeC, tempRcvMsg.cdeN, tempRcvMsg.param, tempRcvMsg.msgId, MSG_IN );
									//}
									/*if(!isMaster) {									
										sendMsg(&noMsg); //send a reply
									}*/
									#ifdef VERBOSE_RS485
									if(isMaster && !(tempRcvMsg.cdeC=='G' && (tempRcvMsg.cdeN==CDE_CATCH || tempRcvMsg.cdeN==CDE_REPING))){
										IFTRACE_SERIAL(Serial.print(nLog++));
										IFTRACE_SERIAL(Serial.print(":"));
										IFTRACE_SERIAL(displayMsg(&tempRcvMsg));
										IFTRACE_SERIAL(Serial.println());
									}
									#endif
								} 
							}
							else {												//Wrong CRC
								nbMsgError++;
								isBadCRCMsgRcv=true;
								IFTRACE_SERIAL(Serial.print(nLog++));
								IFTRACE_SERIAL(Serial.println(":Bad CRC"));
								IFSOFT_SERIAL(mySerial.print(nLog++));
								IFSOFT_SERIAL(mySerial.println(":Bad CRC"));
								memError[curErrorIdx]=ERR_WRONG_CRC;
								curErrorIdx=min(curErrorIdx++,ERROR_ARRAY_SIZE-1);
								//ask for resend ?
							}
						}
					}
					else {//Ignore : not well formed packet
						nbMsgError++;
						IFTRACE_SERIAL(Serial.print(nLog++));
						IFTRACE_SERIAL(Serial.print(": malformed "));
						IFTRACE_SERIAL(Serial.print(msgByteIdx));
						type_msgRS485  		tempRcvMsg;
						tempRcvMsg.fromAdr 	=rawMsgRcv[1];							//0 is HEAD, so we start from 1
						tempRcvMsg.toAdr 	=rawMsgRcv[2];
						tempRcvMsg.cdeC 	=rawMsgRcv[3];
						tempRcvMsg.cdeN 	=((int)rawMsgRcv[4]<<8)+rawMsgRcv[5];
						tempRcvMsg.param 	=((long)rawMsgRcv[6]<<24)+((long)rawMsgRcv[7]<<16)+((long)rawMsgRcv[8]<<8)+rawMsgRcv[9];
						tempRcvMsg.msgId 	=rawMsgRcv[10];
						IFTRACE_SERIAL(displayMsg(&tempRcvMsg));
						/*
						IFSOFT_SERIAL(mySerial.print(nLog++));
						IFSOFT_SERIAL(mySerial.print(":Not well formed packet."));
						IFSOFT_SERIAL(mySerial.print(MSG_SIZE+2));
						IFSOFT_SERIAL(mySerial.print(". Received "));
						IFSOFT_SERIAL(mySerial.println(msgByteIdx));
						//*/
						memError[curErrorIdx]=ERR_MALFORMED_PACKED;
						curErrorIdx=min(curErrorIdx++,ERROR_ARRAY_SIZE-1);
					}
					msgByteIdx=0;

				} 
			}
			else {													//Message not starting with Head or NL or CR		
				if (rawMsgRcv[0]!=13 && rawMsgRcv[0]!=10 && rawMsgRcv[0]!=0){
					if(!isHeadLost){
						isHeadLost=true;
						nbMsgError++;
						headLostTime=curTime;
						#ifdef COLOR
							IFTRACE_SERIAL(Serial.print("\033[31m"));
						#endif
						IFTRACE_SERIAL(Serial.print(nLog++));
						IFTRACE_SERIAL(Serial.print(":No HEAD"));
						type_msgRS485  		tempRcvMsg;
						tempRcvMsg.fromAdr 	=rawMsgRcv[1];							//0 is HEAD, so we start from 1
						tempRcvMsg.toAdr 	=rawMsgRcv[2];
						tempRcvMsg.cdeC 	=rawMsgRcv[3];
						tempRcvMsg.cdeN 	=((int)rawMsgRcv[4]<<8)+rawMsgRcv[5];
						tempRcvMsg.param 	=((long)rawMsgRcv[6]<<24)+((long)rawMsgRcv[7]<<16)+((long)rawMsgRcv[8]<<8)+rawMsgRcv[9];
						tempRcvMsg.msgId 	=rawMsgRcv[10];
						
						#ifdef COLOR
							IFTRACE_SERIAL(Serial.print("\033[37m"));
						#endif
						memError[curErrorIdx]=ERR_HEADLESS_PACKET;
						curErrorIdx=min(curErrorIdx++,ERROR_ARRAY_SIZE-1);
					}
					IFTRACE_SERIAL(Serial.print((char)rawMsgRcv[0]));
				}
				msgByteIdx=0;
			}
		} 
		else { //else du Serial.available
			//Serial.print(curTime-prevMsgTime);
			if(isMaster && (curTime-prevMsgTime)>=ANSWER_DELAY){
				msgByteIdx=0;
				if(isHeadLost && (curTime-headLostTime >=DELAY_FORGET_LOST)){
					isHeadLost=false;
					msgByteIdx=0;
					#ifdef COLOR
						IFTRACE_SERIAL(Serial.print("\033[31m"));
					#endif
					IFTRACE_SERIAL(Serial.println("no HEAD Cleared"));
					#ifdef COLOR
						IFTRACE_SERIAL(Serial.print("\033[37m"));
					#endif
				}
				if (msgByteIdx==0 && !isHeadLost){			//there is nothing to be read and we are master
															//first we send messages that we have to send
					if(lastPileIdx[MSG_OUT]!=curPileIdx[MSG_OUT] ){		//is there something to be written
						//if((curTime-prevMsgTime>QUERY_DELAY)){
							isPileLocked[MSG_OUT]=false;					//free the pileOut overflow in case
							pileOverFlow[MSG_OUT]=false;
							curPileIdx[MSG_OUT]=(curPileIdx[MSG_OUT]+1)%PILE_SIZE;	//select the index of the msg to be sent 
							sendMsg(&pileMsg[curPileIdx[MSG_OUT]][MSG_OUT]);
							//IFDEBUG(displayPile());
						/*}
						else {
							//Serial.print("M");Serial.print(curTime);Serial.print(" ");Serial.print(prevMsgTime);Serial.print(" ");Serial.println(curTime-prevMsgTime);
						}
						*/
					}
					else { //pile is free : we can ask for messages to customers if previous customer as answer for not a too long time
					//Serial.print("x");

						if(/*RS485_ADRESS[queryIdx]==RS485_ADRESS[lastAnswerFrom] ||*/ curTime-prevMsgTime>QUERY_DELAY){//move next
							queryIdx=(queryIdx+1)% NB_SLAVE;
							queryMsg.toAdr=RS485_ADRESS[queryIdx];
							queryMsg.crc=0;
							queryMsg.crc=recurseCRC16((uint8_t*)&queryMsg,MSG_SIZE);
							//IFTRACE_SERIAL(Serial.print("Q"));
							//IFTRACE_SERIAL(Serial.println(queryMsg.toAdr));
							sendMsg(&queryMsg);
						}
					}
				}

			}
			//Serial.println("z");
		}
		//otherwise we wait for being asked to send a message
/*
		if((curTime-prevPingTime>DELAY_TO_REPING) && !isMaster){
			addMsg(reportAdress, 'G', CDE_REPING,VERSION_GCODE);
			IFTRACE_SERIAL(Serial.println("ici L468Proto"));
			prevPingTime=curTime;
		}
//*/

	if (nLog>900){ //Max 3 nLog digits to display
		nLog=0;
	}

#endif
}

void RS485::displayPile(){
#ifdef DISPLAY_PILE
	Serial.println("\nIN\t\t\t\t\tOUT");
	for (int i=0;i<PILE_SIZE;i++){
		for(int d=0; d<2 ; d++){
			if (curPileIdx[d]==i){
				Serial.print("C");
			}
			else {
				Serial.print(" ");
			}
			if (lastPileIdx[d]==i){
				Serial.print("L (Id");
			}
			else {
				Serial.print("  (Id");
			}
			Serial.print(pileMsg[i][d].msgId);
			//Serial.print("/C");Serial.print(pileMsg[i][d].crc);
			Serial.print(") \t");Serial.print((char)pileMsg[i][d].fromAdr);
			Serial.print("->");Serial.print((char)pileMsg[i][d].toAdr);
			Serial.print(" ");Serial.print((char)pileMsg[i][d].cdeC);Serial.print(pileMsg[i][d].cdeN);
			Serial.print(" P");Serial.print((long)pileMsg[i][d].param);Serial.print("\t");
		} 
		Serial.println();
	}
	if(isNewRcvMsg()){
		Serial.print("Msg to be processed Id=");
		Serial.println(curRcvMsg->msgId);
	}		
#endif
#ifdef SOFT_SERIAL
	mySerial.println("\nIN\t\t\t\t\tOUT");
	for (int i=0;i<PILE_SIZE;i++){
		for(int d=0; d<2 ; d++){
			if (curPileIdx[d]==i){
				mySerial.print("C");
			}
			else {
				mySerial.print(" ");
			}
			if (lastPileIdx[d]==i){
				mySerial.print("L (Id");
			}
			else {
				mySerial.print("  (Id");
			}
			mySerial.print(pileMsg[i][d].msgId);
			//Serial.print("/C");Serial.print(pileMsg[i][d].crc);
			mySerial.print(") \t");mySerial.print((char)pileMsg[i][d].fromAdr);
			mySerial.print("->");mySerial.print((char)pileMsg[i][d].toAdr);
			mySerial.print(" ");mySerial.print((char)pileMsg[i][d].cdeC);mySerial.print(pileMsg[i][d].cdeN);
			mySerial.print(" P");mySerial.print((long)pileMsg[i][d].param);mySerial.print("\t");
		} 
		mySerial.println();
	}
	if(isNewRcvMsg()){
		mySerial.print("Msg to be processed Id=");
		mySerial.println(curRcvMsg->msgId);
	}		
#endif
}

void RS485::displayMsg(type_msgRS485* _msg, bool isNL){
const char* NL=isNL?"\r\n":"\t";
#ifdef TRACE_SERIAL 
	Serial.print("(");Serial.print(_msg->msgId);
	//Serial.print("/C");Serial.print(_msg->crc);
	Serial.print(")");Serial.print((char)_msg->fromAdr);
	Serial.print("->");Serial.print((char)_msg->toAdr);
	Serial.print(" ");Serial.print((char)_msg->cdeC);Serial.print(_msg->cdeN);
	Serial.print(" P");Serial.print((long)_msg->param);Serial.print(NL);
#else
#ifdef SOFT_SERIAL
	mySerial.print("(");mySerial.print(_msg->msgId);
	//mySerial.print("/C");mySerial.print(_msg->crc);
	mySerial.print(")");mySerial.print((char)_msg->fromAdr);
	mySerial.print("->");mySerial.print((char)_msg->toAdr);
	mySerial.print(" ");mySerial.print((char)_msg->cdeC);mySerial.print(_msg->cdeN);
	mySerial.print(" P");mySerial.print((long)_msg->param);mySerial.print(NL);
#endif
#endif
}


//*/



