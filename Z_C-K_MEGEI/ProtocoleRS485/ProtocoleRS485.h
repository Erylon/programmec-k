#ifndef ProtocoleRS485_h
#define ProtocoleRS485_h

#include "arduino.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\GCodeC-K.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorList.h"
#include <SoftwareSerial.h>

#define BAUD_RATE	115200

#ifdef DEBUG
	#define IFDEBUG(X)	(X)
#else
	#define IFDEBUG(X)
#endif

#ifdef SOFT_SERIAL
	#define IFSOFT_SERIAL(X)	(X)
#else
	#define IFSOFT_SERIAL(X)
#endif

#ifdef MEGA_BOARD
  #define IFMEGA_BOARD(X) (X)
  char c;
  bool isNewSpy=true;
#else
  #define IFMEGA_BOARD(X)
#endif

#ifdef TRACE_SERIAL
	#define IFTRACE_SERIAL(X) (X)
#else
	#define IFTRACE_SERIAL(X)
#endif

#define VERSION_RS485			0.11

#ifdef SOFT_SERIAL
	extern SoftwareSerial mySerial;
#endif
//*/

const char* 	RS485_ADRESS=		"ABCDER";
#define 		NB_SLAVE			6

struct	type_msgRS485{
	char 		fromAdr;	//1 byte
	char 		toAdr;
	char		cdeC;
	int			cdeN;		//2 byte
	long		param;		//4 byte
	byte 		msgId;
	uint16_t	crc;		//2 byte
};

#define MSG_SIZE			sizeof(type_msgRS485) //12

#define HEAD 				'['
#define TAIL 				']'
#define PIN_DELAY			100 //micros before or after switching pin to use serial. 
								//should be 2 bytes delay to insure no pb : 140µs at 115200bauds
								//formerly 20
#define MSG_IN 				0
#define MSG_OUT				1
#define ERROR_ARRAY_SIZE	20
//#define NO_ANSWER_DELAY		2	//ms    3
#define QUERY_DELAY			5	//ms 	5
#define ANSWER_DELAY 		3   //ms delai mini d'attente pour que le maitre envoit un message
#define DELAY_TO_SEND 		40 //µs 20
#define DELAY_TO_REPING 	1000
#define DELAY_FORGET_LOST 	4//3 //ms 10 >ANSWER_DELAY

#ifndef PILE_SIZE
	#define PILE_SIZE			10
#endif
//
extern	unsigned long		curTime;
type_msgRS485  				errorMsg,noMsg,queryMsg;


class RS485 {
public:
							//RS485();
							RS485(char _myAdress, char _reportAdress, int _pinRS485_DE, int _pinRS485_RE, HardwareSerial *_refSerial);
	void					processMsg();
	void 					addMsg(char _toFromAdrOut_toAdrIn, char _cdeC, int _cdeN, long _param=NO_PARAM, byte _curMsgId=0, byte _dir=MSG_OUT); // add a msg to pile.
	void					displayPile();
	void					displayMsg(type_msgRS485* _msg,bool isNL=false);
	void 					changeAdress(char newAdress);
	bool					isNewRcvMsg();
	bool					isNoMsgOut();
	void					moveNextRcvMsg();
	bool 					peekMsg(char _fromAdr,char _cdeC, int _cdeN); //Enleve le message de la pile s'il le trouve. mets param dans peekedParam
	
	int 					memError[ERROR_ARRAY_SIZE];
	byte 					curErrorIdx;
type_msgRS485  				*curRcvMsg;

public:
	unsigned long			nbMsgError;					
	long 					peekedParam;
	bool 					isMaster;
	byte 					msgByteIdx;							//Read Index

private:
	void 					sendMsg(type_msgRS485 *_msg);
	type_msgRS485			pileMsg[PILE_SIZE][2];
	bool					pileOverFlow[2];
	byte 					curPileIdx[2];							//  
	byte 					lastPileIdx[2];						// prochain enregistrement
	bool					isPileLocked[2];
	unsigned long      	 	prevPingTime;
	unsigned long 			prevMsgTime;
	unsigned long			headLostTime;
	HardwareSerial 			*refSerial;
	char					myAdress;
	char					reportAdress;
	char 					lastAnswerFrom;
	int						pinRS485_DE;
	int						pinRS485_RE;
	int 					queryIdx;
	bool					isBadCRCMsgRcv;
	bool 					isHeadLost;
	byte 					curMsgId;
	byte 					rawMsgRcv[sizeof(type_msgRS485)+2]; //HEAD +MSG_RS485+ TAIL
	unsigned int 			nLog;								//log number	

};


/*
	void 					processProtocolReading(bool _isFromIHM=false);
	void 					processProtocolMatching();
	void 					displayProtocolStatus();
	void 					checkIncomingMsg();
//*/

/**************************************************************************************************
               FONCTIONNEMENT DU PROTOCOLE
/**************************************************************************************************
Le maitre et les clients stockent dans une pile les données à envoyer et dans une autre pile les données à recevoir
Le maitre a seul le pouvoir d'interroger les esclaves. Les escalves peuvent seulement répondre au maitre 

Le maitre cherche d'abord à lire  les commandes qu'il recoit des esclaves et les places dans la pile.
Si le maitre a des messages a envoyer il les envoie dans l'ordre de la pile a leur destinataire
Lorsque le maitre n'a plus de message à envoyer, il interroge les clients un par un au bout de QUERY_DELAY avec une commande CDE_QUERY_MSG
Le maitre ne peut pas passer au message suivant s'il n'a pas recu le REPING ou la reponse à sa question

L'esclave attend de recevoir une requete
Si c'est une commande il l'ajoute à la pile des commandes à traiter
Si c'est un CDE_QUERY_MSG s'il a une commande à envoyer, il l'envoie sinon, cad s'il n'a rien a dire, il envoit un REPING




*/



#endif