#ifndef ARDUINO_AVR_MEGA2560
  #error This file is for Mega only
#endif
//*/
//#define LOG
//#define VIRTUAL_ZSH //BTN erreur => ZSH
//#define VIRTUAL_IHM_BUTTON

//#define TEST_BUT_CANT_LOAD_ON_HOOK
#define TRACE_SERIAL
//#define VIRTUAL_GAUGE
#define DISPLAY_PILE
//#define DISPLAY_SPEED
//#define MESURE_TEMPS //Catch 2.5s free 0.8s

//#define SIMU_OVERLOAD
//#define SIMU_UNDERLOAD
#define NB_VENTOUSES 4

#define Z_INIT_PT ((long)(0/MM_PAR_POINT)) //mm->pt 
#define MODULO_CODEUR 3  //3 !equivalent à >20cm/s = 12m/min
#define INDEX_IN_MM
//#define DEBUG
//#define VIRTUAL_HOIST
//#define TRACE_GAUGE
//#define AUTO_CROSS

//#define NO_SOUND
//#define MESURE_TEMPS_CYLE // temps de cycle max = 0.35ms soit 0.07mm à 12m/min 1.15ms/mot dont 0.05 de delay entre mots
//#define MESURE_GAUGE_CYCLE
//  #define TRACE_CODEUR

//#define BUREAU
//#define SIMU_CODEUR
//#define VIRTUAL_GAUGE
//#define DISPLAY_GAUGE_READ
//#define WATER_ON_DESCENT
  
/****************  MACRO DEFINE  ******************************/
#ifdef BUREAU
  #define NO_LIMIT_SWITCH
  //#define SIMU_CODEUR
//  #define TRACE_CODEUR
  //#define VIRTUAL_GAUGE
  //#undef INDEX_IN_MM
  //#undef AUTO_CROSS
#endif

#ifdef SIMU_OVERLOAD
  #define BUMP_UP_INDEX
  #undef VIRTUAL_ZSH
#endif
#ifdef SIMU_UNDERLOAD
  #define VIRTUAL_GAUGE
  #undef VIRTUAL_ZSH
#endif

#define PILE_SIZE      20
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteTreuil\HoistBoard.cpp"

Hoist *oHoist;

void setup() {
  Serial.begin(BAUD_RATE);//115200);
  Serial2.begin(BAUD_RATE); //RS485

  oHoist =  new Hoist();
  Serial.println("initialization done.");
  
}
void loop() {
  oHoist->loop();
}
