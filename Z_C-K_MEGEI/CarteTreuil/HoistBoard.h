#ifndef HoistBoard_h
#define HoistBoard_h

#include <arduino.h>
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CodeRelease.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorList.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorTextList.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteTreuil\HoistBoardPinList.h" 
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\GCodeC-K.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\Interpreter\Interpreter.cpp"

#include <HX711.h>					//Jauge
//#include <TimerOne.h> 

const int  				A_BUZZ_FREQ[]=		{T_BUZZ_FREQUENCY};	
const char* 			A_NAME_IHM[]=		{T_IHM_NAME};
const char*				A_NAME_MODE[]=		{T_MODE_NAME};
const char*				A_NAME_MOVE[]=		{T_MOVE_NAME};
const char* 			A_MSG_ERROR[]=		{MSG_ERROR};
const char* 			A_MSG_JACK_V[]=		{MSG_GCODE_JACK_V};
//const char* 			RS485_ADRESS=		"ABCDER";
const char* 			A_GRAFCET_NAME[]=	{T_GRAFCET_NAME};
const char* 			A_STATE_NAME[]=		{T_STATE_NAME};
const long				A_SUCK_BOTTOM_POS[]={T_SUCK_BOTTOM_POS}; //tableau des positions des ventouses

#define RS485_ADRR_START	65


//const char* 			A_DIR_NAME[]=		{T_DIR};
//const int 			A_ALARM_PIN[]		={T_ALARM_PIN};
const char* 			A_TRAV_NAME[]=		{T_TRAV};
#ifdef AUTO_CROSS
long 					A_INIT_TRAVERSE[T_TRAVERSE_MAX+2][4]=	{T_INIT_TRAVERSE}; //tableau des traverses memorisées
#endif
const int 				A_BUMP_DOWN_TRAV[]=	{T_BUMP_DOWN_TRAV};
const int 				A_BUMP_UP_TRAV[]=	{T_BUMP_UP_TRAV};
const int 				A_INC_TRAV[]=		{T_INC_TRAV};
//const char*			A_MSG_REPORT[]		={T_MSG_REPORT};		//Liste texte des messages en fonction du code erreur



void 					codeurGetPos();
void 					codeurGetRpm();
	

//void	requestEvent();

unsigned long		curTime,mesureStartTime;

unsigned long 		zz=0;

unsigned long 		nLog=0;

void(* resetFunc) (void) = 0; //declare reset function @ address 0

class Hoist {
public:
					Hoist();
	void			loop();
	void 			displayParams();
	void 			printIndex(bool _force=false,int _a=2, int _b=2);
	long 			curPosIndex;



private:
	void			watchDog();
	void			buzz(int buzzCode=0, int duration=BUZZ_ERROR_DURATION);
	void 			checkGauge();
	void 			clean(bool doStartCleaning);
	void			updateMode(byte _mode);
	void 			move(byte _dir, bool _isFast=true);
	void 			freeAll();
	void 			catchAll();
	void 			pingAll();
	void			displayTraverses(bool _forceInit=false);
	void			displayUndetectedTraverse(int _index);
	void			addTraverse(int _pos, long _posIndex, int _incTrav);//Trav bas/haut , Z , up/down
	void 			resetTraverses();
	void 			initTraverse();
	bool 			isKnownTraverse(int _pos, long _posIndex);

	Interpreter 	*oInterpreter;
	char 			curAdress;
	
	HX711* 			gauge;
	long 			curLoad;
	long 			prevSpeedIndex;
	long 			minLoad,maxLoad;
	float 			curSpeed;

	const char* 	version;
	unsigned int 	versionGCode;
	int 			nbInitTraverse;
	
	byte 			mode;
	bool			isHold;
	bool			isSleeping;
	int 			dir;
	bool 			isMoveBlocked;		//le mouvement n'est pas possible tant que la commande précédente n'est pas termiéne

	bool			newOrder;
	bool			watchDogStatus;
	unsigned long	curTime,prevTime,startTime,cycleTime,holdStartTime,newModeStartTime,pingStartTime,loadStartTime, speedStartTime;
	unsigned long   sleepTime;
	bool			isErrorSent;
	bool 			isOverLoad,isUnderLoad;
	bool			isCatched[5],isAtmPressureSet[5],isFree[5];
	unsigned long	isPongTime[6];
	bool			isRollDown;
	bool 			isSWP;
	bool			isGenerating;
	bool 			isComOK;
	int 			pongId;				//pour la boucle pong du cleaning start
	bool			wasBtnPressed[NB_BTN];
	int 			Btn_Pin[NB_BTN];
	int 			Btn_State[NB_BTN];
	bool 			isRobotBtnPressed[NB_ROBOT_BTN];
	bool 			isRobotAskedStop;
	int 			Old_Btn_State[NB_BTN];
	int 			Led_Pin[NB_LED];
	bool 			Led_State[NB_LED];
	int				errorCode;
	unsigned long	startBuzz,buzzDuration;
	unsigned 		US_DownAlertTime,US_UpAlertTime;
	bool			US_Alert_Down,US_Alert_Up;
	bool			isBuzz;
	bool			doDisplayLoad;
	byte 			curGrafcet;
	byte			curState;
	long 			curTraverse;		//index des traverses détectées en tableau dynamique
	long 			curDetecTraverse;	//index des traverses détectées
	long 			curKnownTraverseTop;  //index borne haute des traverses memorisées qui bornent le robot
	long 			curKnownTraverseDown; //index borne basse des traverses memorisées qui bornent le robot
	long 			curLastTraverse;   //derniere traverse du tableau traverse: suivante a etre effacée
	long 			traverse[NB_MEM_TRAVERSE][3]; //X,Y,SET ? tableau des traverses detectees dont tableau dynamique
	bool 			isBumpDown,isBumpUp,oldIsBumpDown,oldIsBumpUp;
	long 			suckTopPos[5];
	long 			topMargin,botMargin;
	bool			forceIsZSH;
	int 			tempDir;
	int 			tempMode;

#ifdef SIMU_CODEUR
	byte 			tempoCodeur;
#endif
	int 			cnt;
};

#endif