#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteTreuil\HoistBoard.h"

/*TODO
si pas ZSH et cleaning down alors la buse du haut marche 

-------------------------

ZSH et cleaning Up : reste sur up mais permet de faire des regenerations

--------------------------
ZSH + sleep PB : 
down : ne redamarre pas.
si je perds le ZSH il faut faire stop puis down pour que ca redemarre
si je perds le ZSH down ne redemarre pas
--------------------------
Aucun mode : j'appuie sur UP ca met en cleanning

--------------------------
Quand je monte et que overload ca fait over et underload
--------------------------
 Qd je descend en cleaning et underload ca s'arret mais pas la buse du bas
--------------------------
Apres le catch suppirmer l'aspertion des ventouses. La remettre en marche au mouvement

--------------------------

Cleanning se met en mode sleep alors qu'il monte
perte de donnée : verifier que l'on a recu ce qui a ete envoyé
Qd en manual le verin ne remonte pas et que je passe en cleaning il ne veut pas descendre

//*/



extern Hoist *oHoist; //necessaire pour codeurGetPos

long getInt(){
	Serial.println("Please set value");
	while (Serial.available()==0) {}
	return Serial.parseInt();
}



void codeurGetPos(){
	int a =digitalRead(PIN_VOIE_A);
	int b =digitalRead(PIN_VOIE_B);
	
	if(a==b){
		oHoist->curPosIndex++;
	}
	else {
		oHoist->curPosIndex--;
	}
	//oHoist->printIndex(FORCE_DISPLAY);
    //oHoist->curPosIndex+=digitalRead(PIN_VOIE_A)*digitalRead(PIN_VOIE_B);
    
    //Serial.print("new Pos=");
    //Serial.print(oHoist->curPosIndex);
    /*
    if(((oHoist->moveDir==MOVE_UP)||(oHoist->moveDir==MOVE_DOWN))&&(oHoist->prevPos==oHoist->curPosIndex)){
        oHoist->errorCode=ERR_NO_ENCODEUR;        //on déclare erreur si le treuil monte ou descend et que le codeur ne change pas d'état
    }
    if((oHoist->moveDir==STOP)&&(oHoist->prevPos!=oHoist->curPosIndex)){
        oHoist->errorCode=ERR_NO_STOP;            //on déclare erreur si le treuil est en stop et que le codeur change d'etat
    }
    //*/
}

Hoist::Hoist() {//Constructeur
	version=VERSION;
    versionGCode=VERSION_GCODE;
	curAdress=HOIST;
	curTime=millis();
	curSpeed=0;
	curGrafcet=NO_GRAFCET;
	Serial.println();							// Start writing from a new clean line
	Serial.print("Hoist ");
	Serial.print(curAdress);
	Serial.print(" version ");
	Serial.print(version);
	Serial.print(" GCode:V");
  	Serial.print(versionGCode);
	Serial.print(" Code Release:");
  	Serial.print(CODE_RELEASE);
	Serial.print(" Pile size :");
	Serial.println(PILE_SIZE);
	Serial.println(" Initializing...");
	resetTraverses();
 	initTraverse();


	/// Configuration des broches en entrée
	pinMode(PIN_VOK,INPUT);				//2
	pinMode(PIN_HSER_BTN,INPUT);		//8
	pinMode(PIN_ZSH,INPUT);				//22
	pinMode(PIN_INDEX,INPUT);			//23
	pinMode(PIN_WAL,INPUT);				//24
	pinMode(PIN_VOIE_A,INPUT);			//25
	pinMode(PIN_VOIE_AA,INPUT);			//25
	pinMode(PIN_VOIE_B,INPUT);			//27
	pinMode(PIN_HSRST,INPUT);			//26

	int tBtn[NB_BTN]={T_BTN};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_BTN;i++){
		Btn_Pin[i]=tBtn[i];
		pinMode(Btn_Pin[i],	INPUT);
		wasBtnPressed[i]=false;
	}

	for (int i=0;i<NB_ROBOT_BTN;i++){
		isRobotBtnPressed[i]=false;
	}
	isRobotAskedStop=false;

	int tLed[NB_LED]={T_LED};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_LED;i++){
		Led_Pin[i]=tLed[i];
		pinMode(Led_Pin[i],	OUTPUT);	
		digitalWrite(Led_Pin[i],LOW);
		Led_State[i]=LOW;
	}

	for(int i=0; i<5;i++){
		isCatched[i]=false;
		isFree[i]=false;
		isPongTime[i]=0;
		isAtmPressureSet[i]=false;
		#ifdef BUREAU
		//	isCatched[i]=true;
		#endif
	}
	isPongTime[ROBOT_ID]=0;
	isRollDown=false;
	isSWP=false;
	isGenerating=false;
	isUnderLoad=false;
	isOverLoad=false;
	isBumpDown=false;
	isBumpUp=false;
	isComOK=false;
	forceIsZSH=false;
	tempMode=MODE_MANUAL;
	tempDir=MOVE_STOP;

//Configurationdesbrochesdesortie
	pinMode(PIN_BUZZER,OUTPUT);			//
	pinMode(PIN_BUZZER2,OUTPUT);			//
	pinMode(PIN_LED,OUTPUT);			//
	pinMode(PIN_HSVC,OUTPUT);			//
	pinMode(PIN_SCL,OUTPUT);			//LowSpeed
	pinMode(PIN_SCH,OUTPUT);			//MoveAck
	pinMode(PIN_ZCDN,OUTPUT);			//Counterclockwise
	pinMode(PIN_ZCUP,OUTPUT);			//ClockWise
	
	//US alert
	US_DownAlertTime=US_UpAlertTime=0;
	US_Alert_Down=US_Alert_Up=false;


  	// Forcage au démarage
  	digitalWrite(PIN_LED, LOW);            // Extinction de la LED
  	digitalWrite(PIN_BUZZER, LOW);          // Extinction du BUZZER
  	digitalWrite(PIN_BUZZER2, LOW);          // Extinction du BUZZER

	Serial.println("If help not displayed below : Gauge not connected, or VIRTUAL_GAUGE not set");
	Serial.flush();

	#ifdef VIRTUAL_GAUGE
		delay(200);
		Serial.println("Virtual Gauge set");
	#else
		gauge=new HX711(PIN_DATA_GAUGE, PIN_CLOCK_GAUGE);
		delay(200); //otherwise bootpb
		gauge->set_scale(SCALE_MULTIPLIER); // permet de convertir les données en donées lisibles 
		gauge->set_gain(128);//32 //voie A
	  	//scale->tare();
	#endif

	oInterpreter=new  Interpreter(curAdress, PIN_RS485_DE, PIN_RS485_RE, &Serial2);
    oInterpreter->setup();

	digitalWrite(PIN_SCH,HIGH);	//ON=!STOP
	digitalWrite(PIN_ZCUP,HIGH);//UP
	digitalWrite(PIN_ZCDN,HIGH);//DOWN
	
	prevSpeedIndex=curPosIndex=Z_INIT_PT;
	#ifndef NO_CODEUR
	attachInterrupt(digitalPinToInterrupt(PIN_VOIE_A), codeurGetPos, CHANGE);  // detection des fronts descendant
	//attachInterrupt(digitalPinToInterrupt(PIN_INDEX),  codeurGetRpm, CHANGE);
	#endif
	watchDogStatus=true;

	newOrder=false;
	isErrorSent=false;
	doDisplayLoad=false;
	isSleeping=false;
	sleepTime=SLEEP_TIME;
	isMoveBlocked=false;
	isOverLoad=isUnderLoad=false;

	dir=MOVE_STOP;
	mode=MODE_INIT;//MODE_BEFORE_INIT; //Mettre n'importe quoi pour different de mode_init pour qu'il n'y ait pas de mode à l'init
	//mode=MODE_NO_LOAD;
		//updateMode(MODE_INIT);

	prevTime=millis();
	startBuzz=speedStartTime=loadStartTime=newModeStartTime=curTime;//newDirStartTime
	errorCode=ERR_NO_ERROR;
	
	//coordonnées des pattes
	Serial.print("BUMP_UP_INDEX=");Serial.print(BUMP_UP_INDEX);Serial.print("pt=");
	Serial.print(BUMP_UP_INDEX*MM_PAR_POINT);Serial.println("mm");
	for(int i=4;i>4-NB_VENTOUSES;i--){
		suckTopPos[i]=A_SUCK_BOTTOM_POS[i]+SUCK_SIZE+2*TRAV_MARGIN;
		Serial.print("suck ");Serial.print((char)(65+i));
		Serial.print("[");Serial.print(A_SUCK_BOTTOM_POS[i]);
		Serial.print(",");Serial.print(suckTopPos[i]);
		Serial.print("]=[");
		Serial.print(A_SUCK_BOTTOM_POS[i]*MM_PAR_POINT);
		Serial.print(",");Serial.print(suckTopPos[i]*MM_PAR_POINT);
		Serial.println("]mm");
	}

	Serial.print("BUMP_DOWN_INDEX=");Serial.println(BUMP_DOWN_INDEX);
	
	isBuzz=false;
	buzz(BUZZ_INIT, BUZZ_INIT_DURATION);
	displayTraverses();
}

void Hoist::move(byte _dir, bool _isFast){
	bool isNotZSH=digitalRead(PIN_ZSH);
	#ifdef NO_LIMIT_SWITCH
		isNotZSH=!forceIsZSH;
	#endif
	byte nbPong=0;
	byte nbCatched=0;
	for(int i=0;i<5;i++){
		nbPong+=(curTime-isPongTime[i]>PONG_WAITING_TIME)?0:1;//false:true;
		nbCatched+=isCatched[i];
	}
	isRobotAskedStop=false;
	if(_dir!=dir){			//Le changement de dir arrête les fonctions en cours
		Serial.println("New Dir");
		isHold=false;
	/*			IFTRACE_SERIAL(Serial.print("NoHoldIci D="));
				IFTRACE_SERIAL(Serial.print(dir));
				IFTRACE_SERIAL(Serial.print(" _D= "));
				IFTRACE_SERIAL(Serial.print(_dir));
//*/
		//newDirStartTime=curTime;
		isSleeping=false;
		holdStartTime=curTime;
		
		if(mode==MODE_CLEANING /*&& nbPong==5 /*&& nbCatched>1 && !isMoveBlocked*/){
			//digitalWrite(PIN_NIR_LED,HIGH);
			curGrafcet=(_dir==MOVE_DOWN)?CLEANING_DOWN:CLEANING_STOP;
			curGrafcet=(_dir==MOVE_UP)?CLEANING_UP:curGrafcet;
			Serial.print("new cleaning grafcet ");Serial.println(A_GRAFCET_NAME[curGrafcet]);
			/**********************************************/
			curGrafcet=(isUnderLoad || isOverLoad || !isNotZSH )?NO_GRAFCET:curGrafcet;
			curState=(curGrafcet!=NO_GRAFCET)?INITIALIAZING:curState;
			startTime=curTime; //Il n'y a pas tjs de curState et donc d'init du temps dans les grafcets
		}
		else {
			isMoveBlocked=false;
			//digitalWrite(PIN_NIR_LED,LOW);
		}
			/**********************************************/

		dir=(_dir==MOVE_UP) && !isOverLoad && isNotZSH ? MOVE_UP:MOVE_STOP;
		dir=(_dir==MOVE_DOWN) && !isUnderLoad ? MOVE_DOWN:dir;
//				IFTRACE_SERIAL(Serial.print(" newD= "));
//				IFTRACE_SERIAL(Serial.println(dir));
		//dir=(mode==MODE_CLEANING && (nbPong<5 /*|| nbCatched<2*/)) ? MOVE_STOP:dir;	//No move while cleaning if not 5 pong or not at least 2 catched
		#ifndef VIRTUAL_IHM_BUTTON
		if(mode==MODE_CLEANING && (nbPong<NB_VENTOUSES)){//} || nbCatched<2)){
			Serial.println("COM->should STOP");
		}
		#endif
		/*
		if(mode!=MODE_INIT && !isOverLoad && !isUnderLoad && isNotZSH && !isMoveBlocked){
		}
		*/
	}

 int uu=dir;
	//Arret en cas de problème
			/**********************************************/
	dir=(   mode==MODE_INIT 
		 || (isOverLoad && dir ==MOVE_UP)
		 || (isUnderLoad && dir ==MOVE_DOWN)
		 || (!isNotZSH && dir == MOVE_UP)
		 || isMoveBlocked 
//		 || (mode==MODE_CLEANING && (nbPong<5 || nbCatched<2))
		)?MOVE_STOP:dir;

/*	if(uu!=dir){
		IFTRACE_SERIAL(Serial.print(" new'D= "));
		IFTRACE_SERIAL(Serial.println(dir));
	}		
*/
	if(dir==MOVE_STOP || (US_Alert_Down && dir==MOVE_UP)) {//Supprime alert US si stop ou mvt inverse
		US_Alert_Down=false;
	}
	if(dir==MOVE_STOP || (US_Alert_Up && dir==MOVE_DOWN)) {//Supprime alert US si stop ou mvt inverse
		US_Alert_Up=false;
	}

	//Led d'erreurs
			/**********************************************/

	digitalWrite(PIN_WIH_LED,isOverLoad);
	digitalWrite(PIN_WIL_LED,isUnderLoad);
	digitalWrite(PIN_ZIH_LED,!isNotZSH);

#ifdef SIMU_CODEUR
	if(dir!=MOVE_STOP){
		tempoCodeur++;
		if(tempoCodeur>MODULO_CODEUR){
			tempoCodeur=0;
			if(dir==MOVE_UP){
				curPosIndex++;
			}
			else if (dir==MOVE_DOWN){
				curPosIndex--;
			}
			#ifdef TRACE_CODEUR
				printIndex();
			#endif
		}
	}
#endif

	//led boutons
	digitalWrite(PIN_HSMU_LED,dir == MOVE_UP); 
	digitalWrite(PIN_HSMD_LED,dir == MOVE_DOWN);
	digitalWrite(PIN_HSS_LED,dir == MOVE_STOP);

	//commande treuil
	//Par sécurité on remet les conditions :
	digitalWrite(PIN_SCH,dir==MOVE_STOP);		//ON=!STOP
	#ifdef NO_LIMIT_SWITCH
		digitalWrite(PIN_ZCUP,dir != MOVE_UP || isOverLoad); //UP cablé en pull up
	#else
		digitalWrite(PIN_ZCUP,dir != MOVE_UP || !digitalRead(PIN_ZSH) || isOverLoad); //UP cablé en pull up
	#endif
	digitalWrite(PIN_ZCDN,dir != MOVE_DOWN || isUnderLoad);	//DOWN cablé en pull up
	digitalWrite(PIN_SCL,_isFast);				//Fast

	//Fonction HOLD
	if(curTime-holdStartTime>HOLD_DURATION && dir!=MOVE_STOP && isHold==false && mode!=MODE_MANUAL){
		isHold=true;
		buzz(BUZZ_HOLD,BUZZ_ALERT_HOLD);
		IFTRACE_SERIAL(Serial.println("Hold"));
	}
}

void Hoist::freeAll(){
	IFTRACE_SERIAL(Serial.println("\033[41mFree All\033[40m"));
	for(int i=0;i<5;i++){
		//if(isCatched[i]){
		oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,0);
		oInterpreter->oRS485->addMsg(65+i,'G',CDE_FREE);
		//}
	}
}

void Hoist::catchAll(){
	IFTRACE_SERIAL(Serial.println("\033[41mCatch All\033[40m"));
	bool isAllFreed=false;
	for(int i=0;i<5;i++){
		isAllFreed=isAllFreed||!isCatched[i];
	}
	int CDE_NB=isAllFreed?CDE_DEEP_CATCH:CDE_CATCH;
	for(int i=5-NB_VENTOUSES;i<5;i++){
		if(!isCatched[i]){
			oInterpreter->oRS485->addMsg((char)(65+i),'G',CDE_NB);
		}
	}
}

void Hoist::pingAll(){
	for(int i=0;i<5;i++){
			oInterpreter->oRS485->addMsg(65+i,'G',CDE_PING,VERSION_GCODE);
	}
	oInterpreter->oRS485->addMsg('R','G',CDE_PING,VERSION_GCODE);
}

void Hoist::updateMode(byte _mode){
	if(mode!=_mode){		//Le changement de mode arrête les fonctions ne cours
		isGenerating=isHold=false;
		IFTRACE_SERIAL(Serial.println("NoHoldLa"));
		newModeStartTime=curTime;
		move(MOVE_STOP);
		for(int i=0;i<6;i++){//Eteindre toutes les leds de mode et de deplacement
			digitalWrite(Led_Pin[i],LOW);
		}
		//digitalWrite(PIN_NIR_LED,LOW);
		IFTRACE_SERIAL(Serial.print("New mode : "));
		IFTRACE_SERIAL(Serial.print("\033[32m"));
		IFTRACE_SERIAL(Serial.print(A_NAME_MODE[_mode]));
		IFTRACE_SERIAL(Serial.println("\033[37m"));
		if(_mode!=MODE_CLEANING && _mode !=MODE_CLEANING_SLEEP){
			freeAll();
			curGrafcet=CLEANING_HALT;//Start le grafcet
			curState=INITIALIAZING;
		}
		else if(_mode==MODE_CLEANING){
			#ifndef VIRTUAL_IHM_BUTTON
			if((curTime-isPongTime[ROBOT_ID])>PONG_WAITING_TIME){
				resetFunc();
			}
			#endif
			curGrafcet=CLEANING_START;//Start le grafcet
			pongId=0;
			if(isSleeping){			  //On sort du sleeping
				curState=ROLL_DOWN;
			}
			else {
				curState=INITIALIAZING;
				//isMoveBlocked=true;
			}
			//digitalWrite(PIN_NIR_LED,HIGH);
		}
		isSleeping=false;
	}
	isRobotBtnPressed[HSMA]=false;
	isRobotBtnPressed[HSCL]=false;
	

	//Reallumer les leds du mode
	switch (_mode){
		case MODE_NO_LOAD:
			digitalWrite(PIN_HSNL_LED,HIGH);
			maxLoad=MAX_LOAD_FREE_MOVE;
			minLoad=NO_MIN_LOAD;
			if(isSWP) {
				isSWP=false;
				oInterpreter->oRS485->addMsg('R','G',CDE_SWP,0);
			}
			break;
		case MODE_MANUAL:
			digitalWrite(PIN_HSMA_LED,HIGH);
			maxLoad=MAX_LOAD;
			minLoad=NO_MIN_LOAD;
			break;
		case MODE_CLEANING:
			maxLoad=MAX_LOAD;
			minLoad=MIN_LOAD;
			if((curTime-newModeStartTime>sleepTime) && (dir==MOVE_STOP)){
				if(!isSleeping) {
					IFTRACE_SERIAL(Serial.println(" \033[33mSleep\033[37m"));
					curGrafcet=CLEANING_SLEEP;
					_mode=MODE_CLEANING_SLEEP;
				}
				isSleeping=true;
			}
			else {
				digitalWrite(PIN_HSCL_LED,HIGH);	
			}
			break;
		case MODE_CLEANING_SLEEP:
			break;
		default :
			_mode=MODE_INIT;
			maxLoad=MAX_LOAD_FREE_MOVE;
			minLoad=NO_MIN_LOAD;

			break;
	}

	mode=_mode;
}

void Hoist::buzz(int buzzCode, int duration){
	if(buzzCode==NO_BUZZ){
		noTone(PIN_BUZZER);
		digitalWrite(PIN_BUZZER2,LOW);
		isBuzz=false;
		startBuzz=curTime;
		//Serial.println("no Buzz");
	}
	else {
		isBuzz=true;
		startBuzz=curTime;
		buzzDuration=duration;
		#ifdef NO_SOUND
			Serial.println("Buzz");
		#else
			tone(PIN_BUZZER,A_BUZZ_FREQ[buzzCode]);
		digitalWrite(PIN_BUZZER2,HIGH);
		#endif
	}
} 


void Hoist::watchDog(){
	if(isBuzz && (curTime-startBuzz)>buzzDuration){
		buzz(NO_BUZZ);
	}

	if((US_Alert_Down || US_Alert_Up) && !isBuzz){
		if(dir==MOVE_DOWN && ((curTime-startBuzz)>(US_DownAlertTime/7))){
			buzz(BUZZ_US_ALERT,BUZZ_US_ALERT_DURATION);
		} //	unsigned 		US_DownAlertTime,US_UpAlertTime;

	}


	if((curTime-speedStartTime)>SPEED_PERIOD){
		speedStartTime=curTime;
		curSpeed=(float)((prevSpeedIndex-curPosIndex))/SPEED_PERIOD;
		prevSpeedIndex=curPosIndex;

		#ifdef DISPLAY_SPEED
			Serial.print(curSpeed);Serial.println("pt/ms");
		#endif

	}
	
	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		if(isSleeping){
			digitalWrite(PIN_HSCL_LED,watchDogStatus);
		}
		isComOK=true;
		for(int i=0;i<6;i++){
			isComOK=isComOK && (curTime-isPongTime[i])<PONG_WAITING_TIME;
			 /*if((curTime-isPongTime[i])>PONG_WAITING_TIME){
			 	Serial.print("Com:");Serial.print((char)(i+65));Serial.println(curTime-isPongTime[i]);
			 }
			 */
		}
		if(isComOK){
			digitalWrite(PIN_NIR_LED,HIGH);
		}
		else {
			digitalWrite(PIN_NIR_LED,watchDogStatus);
		}

		prevTime=curTime;
	}
	#ifdef MESURE_TEMPS_CYLE
		if(++nLog%1000==0){
			Serial.print("1000 temps cycle (ms) =");Serial.println(curTime-cycleTime);
			cycleTime=curTime;
		}
	#endif
}

void Hoist::displayParams(){
	Serial.print("\n-----------------------------------------------\nETAT DU TREUIL ");
	Serial.println(curAdress);
	Serial.print("version=\t\t");Serial.println(version);
	Serial.print("version GCode=\t\t");Serial.println(versionGCode);
	Serial.print("curLoad=\t\t");Serial.println(curLoad);
	Serial.print("maxLoad=\t\t");Serial.println(maxLoad);
	Serial.print("minLoad=\t\t");Serial.println(minLoad);
	Serial.print("isUnderLoad=\t\t");Serial.println(isUnderLoad);
	Serial.print("isOverLoad=\t\t");Serial.println(isOverLoad);
	Serial.print("isSWP=\t\t\t");Serial.println(isSWP);
	Serial.print("isHold=\t\t\t");Serial.println(isHold);
	Serial.print("isSleeping=\t\t");Serial.println(isSleeping);
	Serial.print("isRollDown=\t\t");Serial.println(isRollDown);
	Serial.print("isGenerating=\t\t");Serial.println(isGenerating);
	Serial.print("isMaster=\t\t");Serial.println(oInterpreter->oRS485->isMaster);
	Serial.print("msgByteIdx=\t\t");Serial.println(oInterpreter->oRS485->msgByteIdx);
	Serial.print("curKnownTraverseDown=\t");Serial.println(curKnownTraverseDown);
	Serial.print("curKnownTraverseTop=\t");Serial.println(curKnownTraverseTop);
	Serial.print("curPosIndex=\t\t");printIndex(FORCE_DISPLAY);
	#ifdef VIRTUAL_ZSH
		Serial.print("vZSH=\t\t\t");Serial.println(forceIsZSH);
	 #else
		Serial.print("ZSH=\t\t\t");Serial.println(!digitalRead(PIN_ZSH));
	#endif		
	Serial.print("curTime=\t\t");Serial.println(curTime);
	Serial.print("prevTime=\t\t");Serial.println(prevTime);
	Serial.print("startTime=\t\t");Serial.println(startTime);
	Serial.print("cycleTime=\t\t");Serial.println(cycleTime);
	Serial.print("mode=\t\t\t");Serial.println(A_NAME_MODE[mode]);
	Serial.print("dir=\t\t\t");Serial.println(A_NAME_MOVE[dir]);
	Serial.print("curGrafcet=\t\t");Serial.println(A_GRAFCET_NAME[curGrafcet]);
	Serial.print("curState=\t\t");Serial.println(curState);

	Serial.println("Name\tBtn\tLed\tRobot btn");
	for(int i=0;i<NB_BTN;i++){
		Serial.print(A_NAME_IHM[i]);Serial.print("\t");Serial.print(digitalRead(Btn_Pin[i]));Serial.print("\t");Serial.print(Led_State[i]);
		if(i<NB_ROBOT_BTN){
			Serial.print("\t");Serial.println(isRobotBtnPressed[i]);
		}
		else {
			Serial.println();
		}
	}
	
	Serial.println("Jack\tisPongTime\tCatched\tFreed\tisPAtm");
	for(int i=0;i<5;i++){
		Serial.print(char(65+i));Serial.print("\t");Serial.print(isPongTime[i]);
		Serial.print("\t");Serial.print(isCatched[i]);Serial.print("\t");Serial.print(isFree[i]);
		Serial.print("\t");Serial.println(isAtmPressureSet[i]);
	}
	Serial.print("isRobotPongTime=\t");Serial.println(isPongTime[ROBOT_ID]);

	Serial.print("Error Led \t");Serial.println(Led_State[NB_LED-1]);
}

void Hoist::checkGauge(){
		long tempRead;
	#ifndef VIRTUAL_GAUGE
		if(gauge->is_ready()){
	#endif

			#ifdef MESURE_GAUGE_CYCLE
				cumulatedGaugeTime+=curTime-gaugeTime;
				gaugeCycle++;
				if(gaugeCycle>=100){
					gaugeCycle=0;
					Serial.print("Temps de cycle Jauge=");Serial.println((float)(cumulatedGaugeTime/100.));
					cumulatedGaugeTime=0;
					gaugeTime=curTime;
				}
			#endif
			#ifdef VIRTUAL_GAUGE
				curLoad=(mode==MODE_NO_LOAD)?0:VIRTUAL_LOAD;
			#else
				gauge->power_up();
				tempRead=gauge->read();//read_average(3);
				curLoad=SCALE_A*tempRead+SCALE_B;//read_average(3);
			#endif			
			#ifdef SIMU_OVERLOAD
				tempRead=digitalRead(PIN_HSER_BTN)?MAX_LOAD+1:VIRTUAL_LOAD;
				if(tempRead!=curLoad){
					Serial.print("New Load=");Serial.println(tempRead);	
				} 
				curLoad=tempRead;


			#endif
			#ifdef SIMU_UNDERLOAD
				tempRead=digitalRead(PIN_HSER_BTN)?10:VIRTUAL_LOAD;
				if(tempRead!=curLoad){
					Serial.print("New Load=");Serial.println(tempRead);	
				} 
				curLoad=tempRead;
			#endif

			//filtrage temporel
			if(curLoad>minLoad && curLoad<maxLoad){
				loadStartTime=curTime;
				isOverLoad=isUnderLoad=false;
			} else if(curTime-loadStartTime>LOAD_FILTER_TIME){
				isOverLoad=curLoad>maxLoad?true:false;
				isUnderLoad=curLoad<minLoad?true:false;
			}

			#ifdef DISPLAY_GAUGE_READ
			//doDisplayLoad=true;
			//if(doDisplayLoad){
				Serial.print(nLog++);
				Serial.print(" / charge :");
				Serial.print(curLoad);
				Serial.print(" N \t");
				Serial.print(tempRead);
				Serial.print("pt");
				Serial.println();
			//}
			#endif
	#ifndef VIRTUAL_GAUGE
		}
	#endif	
	
}


void Hoist::loop(){
	curTime=millis();
/******************************************************************************************************* */
/**                                                TRAITEMENT DES INPUTS                                 */
/******************************************************************************************************* */

	
	//Gauge	
	checkGauge();
	//Button	
	for(int i=0;i<NB_BTN;i++){
		Old_Btn_State[i]=Btn_State[i];
		Btn_State[i]=digitalRead(Btn_Pin[i]);
	}

	if (oInterpreter->doDisplayTraverse) {
		displayTraverses(true);
		oInterpreter->doDisplayTraverse=false;
	}
	if (oInterpreter->doDisplayParams) {
		displayParams();
		oInterpreter->doDisplayParams=false;
	}
	if(oInterpreter->doResetIndex) {
		curPosIndex=0;
		printIndex(FORCE_DISPLAY);
		oInterpreter->doResetIndex=false;
	}

/******************************************************************************************************* */
/**                                                TRAITEMENT DES BOUTONS                                */
/******************************************************************************************************* */
#ifndef VIRTUAL_IHM_BUTTON
	if(!Old_Btn_State[HSRST] && Btn_State[HSRST]){ //Reset Altitude
		Serial.println("Pos 0 Set by Hoist");
		curPosIndex=Z_INIT_PT;
		resetTraverses();
	}

	//MOVING
	tempDir=isHold?dir:MOVE_STOP;
	/*if(!isComOK && (mode==MODE_CLEANING_SLEEP || mode==MODE_CLEANING)) {
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;
	}//*/
	if(!Btn_State[HSMU] && !Btn_State[HSMD] && Btn_State[HSS]){ //Stop
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;
	}
	if((Btn_State[HSMU]^isRobotBtnPressed[HSMU]) && !Btn_State[HSMD] && !Btn_State[HSS] && !isRobotBtnPressed[HSMD] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMU])){ //Up
		tempDir=MOVE_UP;
		newModeStartTime=curTime;
	}
	if(!Btn_State[HSMU] && (Btn_State[HSMD]^isRobotBtnPressed[HSMD]) && !Btn_State[HSS] && !isRobotBtnPressed[HSMU] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMD])){ //Down
		tempDir=MOVE_DOWN;
		newModeStartTime=curTime;
	}
	if(isRobotAskedStop){
		Serial.println("\033[31mSTOP ASKED\033[37m");
		tempDir=MOVE_STOP;
		newModeStartTime=curTime;	
	}
	
	//mode defini par les boutons
	////update but doesn't change mode on invalid conf :
	tempMode=mode;
	if(!Btn_State[HSCL] && !Btn_State[HSMA] && Btn_State[HSNL]){ //No Load
		tempMode=MODE_NO_LOAD;
	}
	if((Btn_State[HSCL]^isRobotBtnPressed[HSCL]) && !Btn_State[HSMA] && !Btn_State[HSNL] && !isRobotBtnPressed[HSMA]  && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSCL])){ //Cleaning
		tempMode=MODE_CLEANING;
	}
	if(!Btn_State[HSCL] && (Btn_State[HSMA] ^ isRobotBtnPressed[HSMA] ) && !Btn_State[HSNL] && !isRobotBtnPressed[HSCL] && (mode!=MODE_NO_LOAD || !isRobotBtnPressed[HSMA])){ //Manual
		tempMode=MODE_MANUAL;
	}
	if(Btn_State[HSRST] && Btn_State[HSNL] && tempMode==MODE_NO_LOAD && !isGenerating){//regeneration
		oInterpreter->oRS485->addMsg('R','G',CDE_VM,1);
		oInterpreter->oRS485->addMsg('R','G',CDE_RM,1);
		isGenerating=true;
	}
#endif
	
	if(tempDir!=MOVE_STOP && mode==MODE_CLEANING_SLEEP){
		//Serial.print(A_NAME_MOVE[tempDir]);
		Serial.println("\033[31mRestart Cleaning\033[37m");
		tempMode=MODE_CLEANING;
		oInterpreter->oRS485->addMsg('R','G',CDE_VM,1);
		oInterpreter->oRS485->addMsg('R','G',CDE_RM,1);
		if(tempDir==MOVE_UP){
			Serial.println("XVRO On");
			oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,1);	
		}
		
	}

	if(Btn_State[HSER] ){//reset error
		digitalWrite(PIN_HSER_LED,LOW);
		digitalWrite(PIN_ZIH_LED,LOW);
	}

#ifndef VIRTUAL_IHM_BUTTON
	if(Btn_State[HSER] && Btn_State[HSRST]){//reset Arduino
		resetFunc();
	}
#endif
	updateMode(tempMode);	
	move(tempDir);	

	oInterpreter->loop();



/******************************************************************************************************* */
/**                                                      GRAFETS                                         */
/******************************************************************************************************* */

	//SUIVI DES ACTION
	if (!errorCode){
		bool curPong;
		byte nbPong;
		byte nbCatched;
		int i;
		char RS485_ID;
		switch(curGrafcet){
			case CLEANING_HALT: //Sortie du mode cleaning
				switch(curState){
					case FREE_NEXT:
						if(curTime-startTime>1000){
							oInterpreter->oRS485->addMsg('B','G',CDE_FREE);				
							oInterpreter->oRS485->addMsg('D','G',CDE_FREE);				
							oInterpreter->oRS485->addMsg('R','G',CDE_XVRJ_OPEN,1);
							oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,0);
							oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,0);
							oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,0);
							oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0);
							oInterpreter->oRS485->addMsg('R','G',CDE_VM,0);
							oInterpreter->oRS485->addMsg('R','G',CDE_RM,0);
							//oInterpreter->oRS485->addMsg('R','G',CDE_CLEAN,0);
							Serial.println("\033[31mCLEANING STOP\033[37m");
							curGrafcet=NO_GRAFCET;	
						}
						break;					
					default :
						oInterpreter->oRS485->addMsg('A','G',CDE_FREE);				
						oInterpreter->oRS485->addMsg('C','G',CDE_FREE);				
						oInterpreter->oRS485->addMsg('E','G',CDE_FREE);
						Serial.println("\033[31mCLEANING HALT\033[37m");
						curState=FREE_NEXT;
						startTime=curTime;		
						break;
				}
				break;
			case CLEANING_STOP:
				oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0); // derite pour eviter perte message
				if(curTime-startTime>REDUCE_WATER_DELAY){
					IFTRACE_SERIAL(Serial.println("\033[31mSTOP & REDUCE WATER\033[37m"));
					oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,0);
					oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,0);
					oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,0);
					curGrafcet=NO_GRAFCET;
				}
				break;
			case CLEANING_UP:
				switch(curState){
					case REDUCE_WATER:
						if(curTime-startTime>REDUCE_WATER_DELAY){
							Serial.println("\033[31mUP & REDUCE WATER\033[37m");
							//oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,0);
							curGrafcet=NO_GRAFCET;
						}
						break;
					default:
						oInterpreter->oRS485->addMsg('R','S',SET_XVRO_DURATION,4000);
						oInterpreter->oRS485->addMsg('R','S',SET_XVRO_PERIOD,4000);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,1);//1
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,0);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,1);//1
						oInterpreter->oRS485->addMsg('R','G',CDE_VM,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_RM,1);
						Serial.println("CLEANING UP XVRO on");
						curState=REDUCE_WATER;
						startTime=curTime;
						break;
				}
				break;
			case CLEANING_DOWN:
				switch(curState){
					case REDUCE_WATER:
						#ifndef WATER_ON_DESCENT
/*
							if(curTime-startTime>REDUCE_WATER_DELAY){
								//IFTRACE_SERIAL(Serial.println("DOWN & REDUCE WATER"));
								//oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,0);
								oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0); // derite pour eviter perte message
							}
//*/
						#endif

						curGrafcet=NO_GRAFCET;
						break;
					default:
						#ifdef WATER_ON_DESCENT
							oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,1);
						#else
							oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0);
						#endif
						//oInterpreter->oRS485->addMsg('R','S',SET_XVRO_DURATION,2000);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,0);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,1);//1
						oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0);
						oInterpreter->oRS485->addMsg('R','G',CDE_VM,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_RM,1);
						Serial.println("XVRO Off");
						//oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0); // derite pour eviter perte message
						Serial.println("\033[31mCLEANING DOWN\033[37m");
						curState=REDUCE_WATER;
						startTime=curTime;
						break;
				}
				break;
			case CLEANING_SLEEP:
				oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,0);
				oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,0);
				oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,0);
				oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,0);
				oInterpreter->oRS485->addMsg('R','G',CDE_VM,0);
				oInterpreter->oRS485->addMsg('R','G',CDE_RM,0);
				//oInterpreter->oRS485->addMsg('R','G',CDE_CLEAN,CLEANING_SLEEP);
				isSleeping=true;
				Serial.println("\033[32mCLEANING SLEEP\033[37m");
				curGrafcet=NO_GRAFCET;
				break;
			case CLEANING_START:
				switch(curState){
					case PING_ALL:
						isPongTime[0]=curTime;	
						#ifdef VIRTUAL_IHM_BUTTON
							for(int zz=0;zz<5;zz++){isPongTime[zz]=curTime;
							}
						#endif

						if(curTime-isPongTime[pongId]<PONG_WAITING_TIME){
							//Serial.print("PongId=");Serial.println(pongId);
							pongId++;
							if(pongId>=NB_VENTOUSES){
								isSWP=true;
								oInterpreter->oRS485->addMsg('R','G',CDE_SWP,1);//N'indique pas si fdc atteint
								oInterpreter->oRS485->addMsg('R','G',CDE_XVSH_OPEN,1);
								oInterpreter->oRS485->addMsg('R','G',CDE_XVSM_OPEN,1);
								oInterpreter->oRS485->addMsg('R','G',CDE_XVSL_OPEN,1);
								for(i=5;i>-1;i--){
									oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,1);//BASF
								}

								startTime=curTime;
								curState=WETTING;
								Serial.println("\033[31mnew STEP : WETTING\033[37m");
							}
						}
						else if(curTime-isPongTime[pongId]>PONG_WAITING_TIME && (curTime-pingStartTime)>PONG_WAITING_TIME){
							RS485_ID=RS485_ADRESS[pongId];
							Serial.print("Re Sending Ping to :");Serial.print(pongId);Serial.println(RS485_ID);
							oInterpreter->oRS485->addMsg(RS485_ID,'G',CDE_PING,VERSION_GCODE);
							pingStartTime=curTime;
						}

						else if(curTime-startTime>GRAFCET_TIMEOUT){ 
							Serial.println("Timeout Grafcet ping all");
							errorCode=ERR_GRAFCET_TIMEOUT;
							updateMode(MODE_MANUAL);
						}
						break;
					case WETTING:
						if(curTime-startTime>WETTING_TIME){
							Serial.println("\033[31mnew STEP : CATCH ALL\033[37m");
							catchAll();
							startTime=curTime;
							curState=CATCH_ALL;
						}
						break;
					case CATCH_ALL:
						#ifndef VIRTUAL_IHM_BUTTON
							nbCatched=0;
							for(int i=4;i>-1;i--){
								if(oInterpreter->oRS485->peekMsg((char)RS485_ADRR_START+i,'G',CDE_CATCH)){
									if(oInterpreter->oRS485->peekedParam==SUCCESS){ 
										isCatched[i]=true;
									}
									isFree[i]=false;
								}
								if(isCatched[i]){
									nbCatched++;
								}
							}
						#else
							startTime=curTime+3000;
							nbCatched=4;
						#endif
						if(nbCatched>=1 && curTime-startTime>2000){ //Au moins 2 catch en moins de 2s
							Serial.println("\033[31mNew step : ROLL DOWN\033[37m");
							curState=ROLL_DOWN;
							isRollDown=false;
							startTime=curTime;
						}
						if(curTime-startTime>GRAFCET_TIMEOUT){
							Serial.println("***gracet timout L972 Hoist : le catch all n'a pas marché dans les temps"); 
							errorCode=ERR_GRAFCET_TIMEOUT;
							updateMode(MODE_MANUAL);
						}
						break;
					case AUTOCATCH_ALL:
						break;
					case ROLL_DOWN:
						oInterpreter->oRS485->addMsg('R','G',CDE_XVRJ_OPEN,0);//N'indique pas si fdc atteint
						oInterpreter->oRS485->addMsg('R','G',CDE_XVRO_OPEN,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_VM,1);
						oInterpreter->oRS485->addMsg('R','G',CDE_RM,1);

						isSleeping=false;
						isMoveBlocked=false;
						Serial.println("\033[31mCLEANING READY\033[37m");
						//digitalWrite(PIN_NIR_LED,HIGH);
						//oInterpreter->oRS485->addMsg('R','G',CDE_CLEAN,1);
						curGrafcet=NO_GRAFCET;

						break;
					default:
						IFTRACE_SERIAL(Serial.println("\033[31mSTART CATCH ALL\033[37m"));
						curState=PING_ALL;
						pingAll();
						startTime=curTime;
						pingStartTime=curTime;
						pongId=0;
						break;
				}
				break;
			default:
				curGrafcet=NO_GRAFCET;
				break;
		}

	}
	if (errorCode) {
        Serial.print("Grafcet Error : ");
        Serial.print(A_MSG_ERROR[errorCode]);
       	errorCode=ERR_NO_ERROR;
       	curGrafcet=NO_GRAFCET;
    }


/******************************************************************************************************* */
/**                                                TRAITEMENT DE LA PILE                                 */
/******************************************************************************************************* */
	if (oInterpreter->oRS485->isNewRcvMsg()){
		newOrder=false;
		bool isMsgComputed=true;
		isPongTime[(int)(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?
									ROBOT_ID:
									oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START)]=curTime;
		switch (oInterpreter->oRS485->curRcvMsg->cdeC){
			case 'G':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case CDE_REPING:
/*						isPongTime[(int)(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?
									ROBOT_ID:
									oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START)]=curTime;
*/
						//Serial.print(nLog++);Serial.print("rcv Reping from ");Serial.println(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?ROBOT_ID:oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START);
						break;					
					case CDE_PING:
						Serial.print("Received a PING from ");
						Serial.println(oInterpreter->oRS485->curRcvMsg->fromAdr);
						oInterpreter->oRS485->addMsg(oInterpreter->oRS485->curRcvMsg->fromAdr,'G',CDE_PONG,VERSION_GCODE);
						pingStartTime=curTime;
					case CDE_PONG:
						if(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'){
//							isPongTime[ROBOT_ID]=curTime;
							isSWP=true;
							oInterpreter->oRS485->addMsg('R','G',CDE_SWP,1);
						}
						else {
//							isPongTime[oInterpreter->oRS485->curRcvMsg->fromAdr-65]=curTime;
							//Serial.print("Pong[");Serial.print((int)(oInterpreter->oRS485->curRcvMsg->fromAdr-65));
							//Serial.print("]=");Serial.println(isPongTime[oInterpreter->oRS485->curRcvMsg->fromAdr-65]);
						}
						isPongTime[(int)(oInterpreter->oRS485->curRcvMsg->fromAdr=='R'?
									ROBOT_ID:
									oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START)]=curTime;

						if(oInterpreter->oRS485->curRcvMsg->param!=VERSION_GCODE){
							Serial.print("BAD GCODE RELEASE :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);
							Serial.print(" from \033[31m");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);
							Serial.print("\033[37m expected :");Serial.println(VERSION_GCODE);
						}
						break;
					case CDE_FREE:
						//Serial.print("FREE (");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						if(oInterpreter->oRS485->curRcvMsg->param==1){
							//Serial.println(") SUCCESS !");
							isFree[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START ]=true;
						}
						else {
							//Serial.println(") FAILURE !");	
						}
						#ifdef MESURE_TEMPS
							Serial.print("Temps Free ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.print(curTime-mesureStartTime);Serial.println("ms");
						#endif
						isCatched[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=false;
						break;
					case CDE_CATCH:
						//Serial.print("CATCH (");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						if(oInterpreter->oRS485->curRcvMsg->param==1){
							//Serial.println(") SUCCESS !");	
							isCatched[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=true;
						}
						else {
							//Serial.println(") FAILURE !");	
						}
						isFree[oInterpreter->oRS485->curRcvMsg->fromAdr-RS485_ADRR_START]=false;
						#ifdef MESURE_TEMPS
							Serial.print("Temps Catch ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.print(curTime-mesureStartTime);Serial.println("ms");
						#endif
						break;
					case CDE_INFO:
						#ifdef MESURE_TEMPS
							Serial.print("Info ");Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(":");Serial.println(oInterpreter->oRS485->curRcvMsg->param);
						#endif
						break;
					case CDE_HSRC:
					case CDE_HSRM:
					case CDE_HSRU:
					case CDE_HSRD:
						isRobotBtnPressed[oInterpreter->oRS485->curRcvMsg->cdeN-CDE_HSRC]=(bool)oInterpreter->oRS485->curRcvMsg->param;
						//Si j'ai recu arret HSRU ou arret HSRD => Stop from Robot (il n'y a pas de hold sur le robot)
						isRobotAskedStop=   ((oInterpreter->oRS485->curRcvMsg->cdeN==CDE_HSRU) && !isRobotBtnPressed[HSMU])
										 || ((oInterpreter->oRS485->curRcvMsg->cdeN==CDE_HSRD) && !isRobotBtnPressed[HSMD]);
						break;
					case CDE_HSRP:
						if(!oInterpreter->oRS485->curRcvMsg->param){
							Serial.println("Pos 0 Set by Robot");
							curPosIndex=Z_INIT_PT;
							resetTraverses();
						}
						break;
					case CDE_FRAME_DETECT:
						Serial.print("Frame(");
						Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);
						Serial.print(")=");
						Serial.println(oInterpreter->oRS485->curRcvMsg->param);
						break;
			        case CDE_FORCE_ZSH_STATE:
						forceIsZSH=oInterpreter->oRS485->curRcvMsg->param;
			            Serial.print("forceIsZSH=");Serial.println(forceIsZSH);
			        	break;
			        case CDE_FORCE_MOVE_UP:
			        case CDE_FORCE_MOVE_DOWN:
			        case CDE_FORCE_MOVE_STOP:
			        	tempDir=oInterpreter->oRS485->curRcvMsg->cdeN-40;
			        	Serial.print("force Dir=");Serial.println(A_NAME_MOVE[tempDir]);
			        	break;
			        case CDE_FORCE_CLEANING:
			        case CDE_FORCE_MANUAL:
			        	tempMode=oInterpreter->oRS485->curRcvMsg->cdeN-50;
			        	Serial.print("force Mode=");Serial.println(A_NAME_MODE[tempMode]);
			            break;

					default:
						IFTRACE_SERIAL(Serial.println("Received G Msg not computed."));
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					break;
				}
				break;
			case 'V':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case GET_PTP:
						Serial.print("PTP=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_PTV:
						Serial.print("PTV=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_ZSLR:
						isRollDown=(bool)oInterpreter->oRS485->curRcvMsg->param;
						break;
					case GET_ZSL:
						Serial.print("ZSL(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_ZSH:
						Serial.print("ZSH(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_FILL_UP_TIME:
						Serial.print("FillUpTime(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_MOVE_DOWN_DEEP_TIME:
						Serial.print("Move Down Deep Time(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_MOVE_DOWN_SOFT_TIME:
						Serial.print("Move Soft Deep Time(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_VACCUM_DURATION:
						Serial.print("Vaccum duration(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_TIMEOUT_JACK_ZSH:
						Serial.print("Timeout Jack ZSH(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					case GET_PRESSURE:
						Serial.print("P(");Serial.print((char)oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(")=");Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						break;
					default:
						Serial.print("Received Msg not computed : V");
						Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
						Serial.print(" P");
						Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					break;
				}
				break;
			case 'A':
				switch (oInterpreter->oRS485->curRcvMsg->cdeN){
					case ALERT_BUMP_DOWN:
						Serial.print("Bump Down :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);Serial.print("\t");
						printIndex(FORCE_DISPLAY);
						isBumpDown=oInterpreter->oRS485->curRcvMsg->param;
						break;
					case ALERT_BUMP_UP:
						Serial.print("Bump Up :");Serial.print(oInterpreter->oRS485->curRcvMsg->param);Serial.print("\t");
						printIndex(FORCE_DISPLAY);
						isBumpUp=oInterpreter->oRS485->curRcvMsg->param;
						break;
					case ALERT_US_DOWN:
						US_DownAlertTime=oInterpreter->oRS485->curRcvMsg->param;
						US_Alert_Down=true;
						break;
					case ALERT_US_UP:
						US_UpAlertTime=oInterpreter->oRS485->curRcvMsg->param;
						US_Alert_Up=true;
						break;
					default:
						Serial.print("Received Msg not computed : A");
						Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
						Serial.print(" P");
						Serial.println((long)oInterpreter->oRS485->curRcvMsg->param);
						oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
						isMsgComputed=false;
					}
				break;
			case 'E':
				//if(oInterpreter->oRS485->curRcvMsg->cdeN!=ERR_PTV_SH_ERROR){
					Serial.print(oInterpreter->oRS485->curRcvMsg->fromAdr);Serial.print(" : Error ");Serial.print(oInterpreter->oRS485->curRcvMsg->cdeN);
					Serial.print(" ");
					Serial.print(A_MSG_ERROR[oInterpreter->oRS485->curRcvMsg->cdeN]);
					Serial.println(oInterpreter->oRS485->curRcvMsg->param);
				//}
				if (/*oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTP_SL_ERROR || */
					oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTV_SH_ERROR){
					digitalWrite(PIN_HSER_LED,HIGH);	
				}
				
				if (oInterpreter->oRS485->curRcvMsg->cdeN==ERR_PTP_SL_ERROR){
					digitalWrite(PIN_ZIH_LED,HIGH);	
				}

				break;
			default:
				IFTRACE_SERIAL(Serial.println("Received Msg not computed."));
				oInterpreter->oRS485->displayMsg(oInterpreter->oRS485->curRcvMsg,true);
				isMsgComputed=false;
				break;
		}
		oInterpreter->oRS485->moveNextRcvMsg();	
	}

#ifdef AUTO_CROSS
	//Si debut ou fin détection traverse, l'ajouter
	if(dir==MOVE_DOWN && isBumpDown!=oldIsBumpDown){
		addTraverse(A_BUMP_DOWN_TRAV[isBumpDown],curPosIndex+BUMP_DOWN_INDEX,A_INC_TRAV[dir]);
		oldIsBumpDown=isBumpDown;
		displayTraverses();
	}
	if(dir==MOVE_UP && isBumpUp!=oldIsBumpUp){
		addTraverse(A_BUMP_UP_TRAV[isBumpUp],curPosIndex+BUMP_UP_INDEX,A_INC_TRAV[dir]);
		oldIsBumpUp=isBumpUp;
		displayTraverses();
	}
	

//TODO supprimer NOT_SET du code ?
	//dans le tableau dynamique, supprimer les traverses passées  a la descente
	if(	A_INC_TRAV[dir]==-1 && (curPosIndex+BUMP_UP_INDEX)<traverse[curLastTraverse][TRAV_BAS] 
		&& traverse[curLastTraverse][TRAV_SET]==TRAV_SET_FULL){
		Serial.print("Traverse [");
		Serial.print(traverse[curLastTraverse][TRAV_BAS]);
		Serial.print(",");
		Serial.print(traverse[curLastTraverse][TRAV_HAUT]);
		Serial.print("]");
		//Effacer la derniere traverse en memoire
		traverse[curLastTraverse][TRAV_SET]=NOT_SET;
		//celle d'avant devient la derniere traverse a effacer
		curLastTraverse=(curLastTraverse-1+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE;
		Serial.println(" supprimée");
	}

	//dans le tableau dynamique, supprimer les traverses passées a la montée
	if(	A_INC_TRAV[dir]==+1 && (curPosIndex+BUMP_DOWN_INDEX)>traverse[curLastTraverse][TRAV_HAUT] 
		&& (curPosIndex+BUMP_DOWN_INDEX)>traverse[curLastTraverse][TRAV_BAS]
		&& traverse[curLastTraverse][TRAV_SET]==TRAV_SET_FULL){
		Serial.print("Traverse [");
		Serial.print(traverse[curLastTraverse][TRAV_BAS]);
		Serial.print(",");
		Serial.print(traverse[curLastTraverse][TRAV_HAUT]);
		Serial.print("]");
		//Effacer la derniere traverse en memoire
		traverse[curLastTraverse][TRAV_SET]=NOT_SET;
		//celle d'avant devient la derniere traverse a effacer
		curLastTraverse=(curLastTraverse+A_INC_TRAV[dir])%NB_MEM_TRAVERSE;
		Serial.println(" supprimée");
	}
//END TODO

	//Si en montant je suis plus haut que la traverse haute memorisée en cours : prendre la traverse mémorisée suivante
	if(	A_INC_TRAV[dir]==1 && (curPosIndex+BUMP_UP_INDEX>A_INIT_TRAVERSE[curKnownTraverseTop][TRAV_HAUT]) &&
		A_INIT_TRAVERSE[curKnownTraverseTop][TRAV_SET]==TRAV_SET_FULL){ //pour traiter le cas de la fin de tableau
		displayUndetectedTraverse(curKnownTraverseTop);
		curKnownTraverseTop=min(curKnownTraverseTop+1,nbInitTraverse-1);
	}

	//Si en montant je suis plus haut que la traverse basse memorisée suivante : prendre la traverse mémorisée suivante
	if(	A_INC_TRAV[dir]==1 && (curPosIndex+BUMP_DOWN_INDEX>A_INIT_TRAVERSE[curKnownTraverseDown+1][TRAV_HAUT]) &&
		A_INIT_TRAVERSE[curKnownTraverseDown+1][TRAV_SET]==TRAV_SET_FULL){ //pour traiter le cas de la fin de tableau
		//displayUndetectedTraverse(curKnownTraverseDown);
		curKnownTraverseDown=min(curKnownTraverseDown+1,nbInitTraverse-1);
		Serial.print("new t.dyn down :");
		Serial.println(curKnownTraverseDown);
	}

	//Si en descendant je suis plus bas que la traverse haute memorisée precedente : prendre la traverse mémorisée précédente
	if(	A_INC_TRAV[dir]==-1 && (curPosIndex+BUMP_UP_INDEX<A_INIT_TRAVERSE[curKnownTraverseTop-1][TRAV_BAS]) &&
		A_INIT_TRAVERSE[curKnownTraverseTop-1][TRAV_SET]==TRAV_SET_FULL){ //pour traiter le cas de la fin de tableau
		//displayUndetectedTraverse(curKnownTraverseTop);
		curKnownTraverseTop=max(curKnownTraverseTop-1,1);
		Serial.print("new t.dyn top :");
		Serial.println(curKnownTraverseTop);
	}

	//Si en descendant je suis plus bas que la traverse basse memorisée en cours: prendre la traverse mémorisée précédente
	if(	A_INC_TRAV[dir]==-1 && (curPosIndex+BUMP_DOWN_INDEX<A_INIT_TRAVERSE[curKnownTraverseDown][TRAV_BAS]) &&
		curKnownTraverseDown>1 &&
		A_INIT_TRAVERSE[curKnownTraverseDown][TRAV_SET]==TRAV_SET_FULL){ //pour traiter le cas de la fin de tableau
		displayUndetectedTraverse(curKnownTraverseDown);
		curKnownTraverseDown=max(curKnownTraverseDown-1,1);
	}


	switch (dir){
		case MOVE_UP:
			botMargin=TRAV_MARGIN_FREE_UP; 
			topMargin=TRAV_MARGIN_CATCH;
			break;
		case MOVE_DOWN:
			botMargin=TRAV_MARGIN_CATCH;
			topMargin=TRAV_MARGIN_FREE_DOWN;
			break;
		default:
			botMargin=TRAV_MARGIN_STOP;
			topMargin=TRAV_MARGIN_STOP;

	}

/*

	bool doCross; 									//pour ne lancer la commande qu'une fois
	for(int i=4;i>4-NB_VENTOUSES;i--){							//scanner les verins
		doCross=false;	
		for(int trav=0;trav<NB_MEM_TRAVERSE ;trav++){//Scanner les traverses
			if (   traverse[trav][TRAV_SET]==TRAV_SET_FULL
				&& traverse[trav][TRAV_HAUT]>(curPosIndex+A_SUCK_BOTTOM_POS[i]-topMargin) //l'index est dans la plage de la traverse
				&& traverse[trav][TRAV_BAS]<(curPosIndex+suckTopPos[i]+botMargin)	    
				) { 									
				doCross=true;						//pour ne lancer la commande qu'une fois
			}
		}
		if(doCross && !isFree[i]){
				oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,0);
				oInterpreter->oRS485->addMsg(65+i,'G',CDE_FREE);
				isFree[i]=true; //pour ne lancer la commande qu'une fois				
		}
		else if(!doCross && isFree[i]){
				oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,1);
				oInterpreter->oRS485->addMsg(65+i,'G',CDE_CATCH);
		}
	}
//*/


/***************************************************************************************
/***************************************************************************************

                                          DO CROSS

/***************************************************************************************
/***************************************************************************************/

	bool doCross; 	
	int lastTrav;	
#ifdef BUREAU
	if(mode==MODE_CLEANING || mode==MODE_MANUAL){
#else
	if(mode==MODE_CLEANING){
#endif

		for(int i=4;i>4-NB_VENTOUSES;i--)							//scanner les verins
			{
			doCross=false;
			for(int trav=curKnownTraverseDown;trav<=curKnownTraverseTop ;trav++){//Scanner les traverses
				if (   A_INIT_TRAVERSE[trav][TRAV_SET]==TRAV_SET_FULL
					&& A_INIT_TRAVERSE[trav][TRAV_HAUT]+topMargin>(curPosIndex+A_SUCK_BOTTOM_POS[i]) //l'index est dans la plage de la traverse
					&& A_INIT_TRAVERSE[trav][TRAV_BAS]<(curPosIndex+suckTopPos[i]+botMargin)	    
					) { 									
					doCross=true;						//pour ne lancer la commande qu'une fois
					lastTrav=trav;
					/*
					Serial.print("curPosIndex= ");
					Serial.print(curPosIndex);
					Serial.print("botMargin= ");
					Serial.print(botMargin);
					Serial.print("topMargin= ");
					Serial.print(topMargin);
					Serial.print(" Cross ");
					Serial.print((char)(65+i));
					Serial.print(" [");
					Serial.print(A_SUCK_BOTTOM_POS[i]);
					Serial.print(",");
					Serial.print(suckTopPos[i]);
					Serial.print("] vs T[");
					Serial.print(A_INIT_TRAVERSE[lastTrav][TRAV_BAS]);
					Serial.print("<");
					Serial.print(curPosIndex+suckTopPos[i]);
					Serial.print(",");
					Serial.print(A_INIT_TRAVERSE[lastTrav][TRAV_HAUT]);
					Serial.print(">");
					Serial.print(curPosIndex+A_SUCK_BOTTOM_POS[i]);
					Serial.println("]");
//*/
				}
			}
			


			if(doCross && !isFree[i]){
					oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,0);
					oInterpreter->oRS485->addMsg(65+i,'G',CDE_FREE);
					/*Serial.print("dir=");
					Serial.print(dir);
					Serial.print(" Cross ");
					Serial.print((char)(65+i));
					Serial.print("@");
					printIndex(FORCE_DISPLAY);
//*/
					isFree[i]=true; //pour ne lancer la commande qu'une fois				
			}
			else if(!doCross && isFree[i]){
					oInterpreter->oRS485->addMsg(65+i,'S',SET_AUTO_RECATCH_MODE,1);
					oInterpreter->oRS485->addMsg(65+i,'G',CDE_CATCH);
					/*Serial.print("topMargin=");
					Serial.print(topMargin);
					Serial.print(" botMargin=");
					Serial.print(botMargin);
					Serial.print(" dir=");
					Serial.print(dir);
					Serial.print(" Recatch ");
					Serial.print((char)(65+i));
					Serial.print("@");
					printIndex(FORCE_DISPLAY);
//*/
					isFree[i]=false; //pour ne lancer la commande qu'une fois				
			}
		}
	}
//*/
#endif


	watchDog();

}

void Hoist::printIndex(bool _force, int _a, int _b){
    if((oHoist->curPosIndex%RESOL_Z)==0 || _force){//Affiche la position tous les RESOL_Z points
	   	/*if(_a<2 && _b<2){
		   	Serial.print(_a);Serial.print(_b);
	   	}*/
		Serial.print(curPosIndex);	
#ifdef INDEX_IN_MM
		Serial.print("\t");Serial.print(MM_PAR_POINT*curPosIndex);Serial.print("mm");
#endif
		Serial.println();
    }
}

void Hoist::displayUndetectedTraverse(int _index){
#ifdef AUTO_CROSS
	if(A_INIT_TRAVERSE[_index][TRAV_KNOWN]!=1){
			Serial.print("Traverse ");
			Serial.print(_index);
			Serial.print(" non détectée : [");
			Serial.print(A_INIT_TRAVERSE[_index][TRAV_BAS]);
			Serial.print(",");
			Serial.print(A_INIT_TRAVERSE[_index][TRAV_HAUT]);
			Serial.println("]");
			printIndex(FORCE_DISPLAY);
	}
	else {
		A_INIT_TRAVERSE[_index][TRAV_KNOWN]=2;
	}
#endif
}


void Hoist::displayTraverses(bool _forceInit){
#ifdef AUTO_CROSS
	int iTrav;
	if(_forceInit){
		Serial.println("\nTraverses mémorisées :\n______________________");
		for(int i=0;i<NB_TRAV_RAW;i++){
			for(int col=0;col<NB_TRAV_COL;col++){
				iTrav=i+col*NB_TRAV_RAW;
				if(iTrav>=nbInitTraverse){
					col=NB_TRAV_COL;
					i=NB_TRAV_RAW;
				}
				else {
					Serial.print(iTrav);
					Serial.print(":");
					if((iTrav<=curKnownTraverseTop) && (iTrav>=curKnownTraverseDown)){
						Serial.print("*");
					}
					switch (A_INIT_TRAVERSE[iTrav][TRAV_SET]){
						case TRAV_SET_FULL:
							Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_BAS]);
							Serial.print("pt=");
							Serial.print((long)(A_INIT_TRAVERSE[iTrav][TRAV_BAS]*MM_PAR_POINT));
							Serial.print("mm -> ");
							Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_HAUT]);
							Serial.print("pt=");
							Serial.print((long)(A_INIT_TRAVERSE[iTrav][TRAV_HAUT]*MM_PAR_POINT));
							Serial.print("mm");
							break;
						case TRAV_SET_BAS:
							Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_BAS]);
							Serial.print(" -> ?");
							break;
						case TRAV_SET_HAUT:
							Serial.print("? -> ");
							Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_HAUT]);
							break;
						case NOT_SET:
							Serial.print("Not Set");
							break;
						default:
							Serial.print("Erreur traverse:");
							Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_SET]);
					}
					Serial.print(" ");
					Serial.print(A_INIT_TRAVERSE[iTrav][TRAV_KNOWN]);
					Serial.print("\t\t");
				}	
			}
			Serial.println();	
		}
		Serial.print("\nTraverses détectées (");
		Serial.print(curDetecTraverse);
		Serial.println("):\n______________________");
		for(int iTrav=0;iTrav<curDetecTraverse;iTrav++){
			Serial.print(iTrav);
			Serial.print(":");
			Serial.print(traverse[iTrav][TRAV_BAS]);
			Serial.print(" pt=");
			Serial.print((long)(traverse[iTrav][TRAV_BAS]*MM_PAR_POINT));
			Serial.print(" mm ->\t");
			Serial.print(traverse[iTrav][TRAV_HAUT]);
			Serial.print(" pt=");
			Serial.print((long)(traverse[iTrav][TRAV_HAUT]*MM_PAR_POINT));
			Serial.print(" mm");
			Serial.println();

		}




	}
	//*/
	Serial.println("\nTableau dynamique des traverses :\n_________________________________");	
	for(int iTrav=NB_MEM_TRAVERSE-1;iTrav>-1;iTrav--){
		if(iTrav==curTraverse){
			Serial.print(">");	
		}
		else {
			Serial.print(" ");
		}
		Serial.print(iTrav);
		Serial.print(":\t");
		switch (traverse[iTrav][TRAV_SET]){
			case TRAV_SET_FULL:
				Serial.print(traverse[iTrav][TRAV_BAS]);
				Serial.print(" -> ");
				Serial.print(traverse[iTrav][TRAV_HAUT]);
				break;
			case TRAV_SET_BAS:
				Serial.print(traverse[iTrav][TRAV_BAS]);
				Serial.print(" -> ?");
				break;
			case TRAV_SET_HAUT:
				Serial.print("? -> ");
				Serial.print(traverse[iTrav][TRAV_HAUT]);
				break;
			case NOT_SET:
				Serial.print(A_TRAV_NAME[traverse[iTrav][TRAV_SET]]);
				break;
			default:
				Serial.print("Erreur traverse:");
				Serial.print(traverse[iTrav][TRAV_SET]);
		}
		Serial.println();
	}
	Serial.println();
#else
	Serial.println("Mode AUTO_CROSS : pas de traverse");
#endif
}

bool Hoist::isKnownTraverse(int _pos, long _posIndex){
#ifdef AUTO_CROSS
	for(int i=0;i<nbInitTraverse;i++){
		if(A_INIT_TRAVERSE[i][_pos]<_posIndex+MAX_ERROR_POS_TRAVERSE && A_INIT_TRAVERSE[i][_pos]>_posIndex-MAX_ERROR_POS_TRAVERSE){
			A_INIT_TRAVERSE[i][TRAV_KNOWN]=1; //La traverse est détectée : l'indiquer dans le tableau
			Serial.print("Traverse détectée conforme !");		
			return true;
		}
	}
	Serial.print("Traverse ");
	Serial.print(A_TRAV_NAME[_pos]);
	Serial.print(" inconnue detectee ! (");
	Serial.print(_posIndex);
	Serial.println(")");
		return false;
#endif
}

void Hoist::addTraverse(int _pos, long _posIndex, int _incTrav){//_pos = Haut || Bas  _incTrav = 1||-1
 	if(traverse[curTraverse][TRAV_SET]==NOT_SET){
 		traverse[curTraverse][TRAV_SET]=_pos;
 		traverse[curTraverse][_pos]=_posIndex;
		//si la traverse suivante ou précedente suivant la direction, pour peu qu'elle soit definie, 
		//est trop proche : le signaler mais elle est qd mm prise en compte

		if( (_pos==TRAV_BAS && _incTrav==1   && 
				traverse[(curTraverse-1+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE][TRAV_SET]!=NOT_SET &&
				_posIndex-traverse[(curTraverse-1+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE][TRAV_HAUT]<MIN_TRAVERSE_SPACE) ||
			(_pos==TRAV_HAUT && _incTrav==-1 && 
				traverse[(curTraverse+1)%NB_MEM_TRAVERSE][TRAV_SET]!=NOT_SET &&
				traverse[(curTraverse+1)%NB_MEM_TRAVERSE][TRAV_BAS]-_posIndex<MIN_TRAVERSE_SPACE)) {
/*
			Serial.print("traverse[(curTraverse-1+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE][TRAV_HAUT]=");
			Serial.print(traverse[(curTraverse-1+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE][TRAV_HAUT]);
			Serial.print(" traverse[(curTraverse+1)%NB_MEM_TRAVERSE][TRAV_HAUT]=");
			Serial.println(traverse[(curTraverse+1)%NB_MEM_TRAVERSE][TRAV_HAUT]);
			Serial.print(" _incTrav=");
			Serial.print(_incTrav);
*/
			Serial.print(" Traverse trop proche de la précédente détectée : ");	
			traverse[curTraverse][TRAV_SET]=NOT_SET;
			curTraverse=(curTraverse-_incTrav+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE;
			traverse[curTraverse][TRAV_SET]=_pos; //3-_pos;//inversion TRAV_HAUT/TRAV_BAS par rapport à _pos
			Serial.print(" curTraverse =");
			Serial.print(curTraverse);
			Serial.println();
			displayTraverses();
		}

		Serial.print("Declaration Traverse :");
		Serial.print(_posIndex);
		Serial.print(" _pos=");
		Serial.print(_pos);
		Serial.print("=");
		Serial.println(A_TRAV_NAME[_pos]);

		//Trace if traverse is known
		isKnownTraverse(_pos, _posIndex);
 	}
	else if(traverse[curTraverse][TRAV_SET]!=_pos){
		traverse[curTraverse][_pos]=_posIndex;
		traverse[curTraverse][TRAV_SET]=traverse[curTraverse][TRAV_SET] | (long)_pos;

		// Si l'epaisseur d'une traverse est trop petite : ignorer
		if(traverse[curTraverse][TRAV_HAUT]-traverse[curTraverse][TRAV_BAS]<MIN_TRAVERSE_THICKNESS){
			traverse[curTraverse][TRAV_SET]== NOT_SET;
			Serial.println("Traverse ignoree car trop fine");
		}
		else {
			Serial.print("Traverse ajoutee : index ");
			Serial.print(_posIndex);
			Serial.print(" /_pos=");
			Serial.print(_pos);
			Serial.print("=");
			Serial.println(A_TRAV_NAME[_pos]);
			Serial.print(" curTrav=");
			Serial.println(curTraverse);
			curTraverse=(curTraverse+_incTrav+NB_MEM_TRAVERSE)%NB_MEM_TRAVERSE; 
			//on cycle sur (4) traverses dynamiques : si la prochaine est occupée c'est qu'il y a débordement
			if(traverse[curTraverse][TRAV_SET]!=NOT_SET){
				Serial.println("Erreur : débordement du tableau des traverses");
				traverse[curTraverse][TRAV_SET]=NOT_SET;
			}
			if((curDetecTraverse+1)>=T_TRAVERSE_MAX){
				Serial.println("Erreur : débordement du tableau des detections");
			}
			curDetecTraverse=(curDetecTraverse+1)%T_TRAVERSE_MAX;

		}
		//Trace if traverse is known
		isKnownTraverse(_pos, _posIndex);
	}
	else {
		Serial.println("Erreur : traverse deja detectee => ignore");
		//errorCode=ERR_ALREADY_DETECTED_TRAVERSES;
	}
}

void Hoist::initTraverse(){
#ifdef AUTO_CROSS
	nbInitTraverse=T_INIT_TRAVERSE_NB;
	///conversion de coordonnées et passage en point
	for(int trav=0;trav<T_INIT_TRAVERSE_NB;trav++){
		A_INIT_TRAVERSE[1+trav][1]=(A_INIT_TRAVERSE[1+trav][1]+OFFSET_TRAVERSE)/MM_PAR_POINT;          					//H1 bas
		A_INIT_TRAVERSE[1+trav][2]=A_INIT_TRAVERSE[1+trav][1]+A_INIT_TRAVERSE[1+trav][2]/MM_PAR_POINT; 	//H2 haut
	}


	//repetition si present
	#ifdef NB_MOTIF
		long int moduleZ=A_INIT_TRAVERSE[T_INIT_TRAVERSE_NB][2];//la hauteur de la derniere traverse est la hauteur du module
		if(T_INIT_TRAVERSE_NB*NB_MOTIF<T_TRAVERSE_MAX){
			for(int nbMotif=1;nbMotif<NB_MOTIF;nbMotif++){
				for (int nbTravInMotif=0;nbTravInMotif<T_INIT_TRAVERSE_NB;nbTravInMotif++){
					A_INIT_TRAVERSE[1+nbTravInMotif+T_INIT_TRAVERSE_NB*nbMotif][0]=A_INIT_TRAVERSE[1+nbTravInMotif][0];
					A_INIT_TRAVERSE[1+nbTravInMotif+T_INIT_TRAVERSE_NB*nbMotif][1]=A_INIT_TRAVERSE[1+nbTravInMotif][1]+moduleZ*nbMotif;
					A_INIT_TRAVERSE[1+nbTravInMotif+T_INIT_TRAVERSE_NB*nbMotif][2]=A_INIT_TRAVERSE[1+nbTravInMotif][2]+moduleZ*nbMotif;
					A_INIT_TRAVERSE[1+nbTravInMotif+T_INIT_TRAVERSE_NB*nbMotif][3]=A_INIT_TRAVERSE[1+nbTravInMotif][3];
				}
			}
			nbInitTraverse=T_INIT_TRAVERSE_NB*NB_MOTIF+2; //+2 : les extremités NOT_SET

		}
		else {
			Serial.print("\033[31mMozaique traverses ignorée : il y a plus de ");
			Serial.print(T_TRAVERSE_MAX);
			Serial.println(" traverses in finé\033[37m");
		}
	#else
		nbInitTraverse+=2; //+2 : les extremités NOT_SET
	#endif
	

	//Traverse NOT_SET finale (coordonnée opposées dont les 0)
	for(int i=0;i<4;i++){
		A_INIT_TRAVERSE[nbInitTraverse-1][i]=-A_INIT_TRAVERSE[0][i]; //- pour passer les Z en positif sans changer le 0 du not_set 
	}
	A_INIT_TRAVERSE[nbInitTraverse-1][0]=TRAV_SET_FULL; //- pour passer les Z en positif sans changer le 0 du not_set 
//*/
#endif				
}

void Hoist::resetTraverses(){
	for(int i=0;i<NB_MEM_TRAVERSE;i++){
		traverse[i][TRAV_SET]=NOT_SET;
	}
	curTraverse=0;
	curDetecTraverse=0;
	curKnownTraverseTop=1;
	curKnownTraverseDown=1;
	curLastTraverse=0;
}

/*

void Hoist::goAbsoluteMM(int _targetMM){
	isGoMode=true;
	targetGo=(long)(_targetMM/MM_PAR_POINT);
	if(curPosIndex>targetGo){
		if(isFdCHaut){
			Serial.println("FDC Haut => mouvement impossible");	
			targetDir=STOP_BRAKE;
		} else {
			targetDir=MOVE_DOWN;	
			Serial.print("Moving Down to ");Serial.print(targetGo);Serial.print("\t");Serial.print(_targetMM);Serial.println("mm");
		}
	} else {
		targetDir=MOVE_UP;		
		Serial.println("Moving Up to ");Serial.print(targetGo);Serial.print("\t");Serial.print(_targetMM);Serial.println("mm");	
	}
}


//*/