#ifndef C_KErrorTextList_h
#define C_KErrorTextList_h
/*color
\033[3nm
n:
0 noir
1 rouge
2 vert
3 jaune
4 bleu
5 violet
6 cyan
7 blanc
//*/
#define MSG_ERROR \
	"Pas d'erreur",									\
	"Objectif non valable",				/*ERR_TARGET_UNDEFINED*/ \
	"",				/*ERR_COM*/ 					\
	"", 				/*ERR_ADR_CONFLICT*/ 		\
	"Timeout Vide",						/*ERR_TIMEOUT_VACCUM*/ \
	"Timeout fdc",						/*ERR_TIMEOUT_FDC*/ \
	"pb message in",					/*ERR_COM_INCOMING_ADR*/ \
	"Low P",										\
	"Low V",										\
	"Duration depasse Period. New Period =",		\
	"GCode non supporte",				/*ERR_GCODE_NOT_SUPPORTED*/\
	"Erreur réponse robot", /*pas de retrour après envoie de donées. */\
	"Montée impossible",							\
	"Descente impossible",							\
	"Arret impossible",								\
	"Codeur absent",								\
	"Pas de changement d'état du codeur",			\
	"Emetteur nRF absent ou en Panne",				\
	"Conflit RX TX",								\
	"Corrupt Payload has been flushed",				\
	"Pile in overflow : too many messages do read",	\
	"Pile out overflow : too many messages do send",\
	"Echec transmission : trop d'essais infructeux",\
	"Trop de traverses : pas assez de mémoire",		\
	"Traverse deja détecté",						\
	"Lecture Impossible de la pression atmosphérique : le verin est peut etre catché ?",\
	"Erreur lecture carte SD",						\
	"Catch Failure",								\
	"Protocole : Packet trop long",					\
	"Protocole : Erreur CRC",						\
	"Protocole : Packet mal formé",					\
	"Protocole : Packet sans entete",				\
	"Timeout : Jack ZSH",							\
	"Timeout : Free",								\
	"Timeout : Grafcet"								

#endif