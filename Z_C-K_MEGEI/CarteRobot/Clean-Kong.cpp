#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteRobot\Clean-Kong.h"

extern CleanKong *oCleanKong;

void mesureUS_Start(){
  if(measuringUS){
      echoStart=micros();
  }
}
void mesureUS_End(){
  if(measuringUS){
      echoEnd=micros();
      US_Done=true;
  }
}


CleanKong::CleanKong() {
    versionGCode=VERSION_GCODE;

	curTime=millis();
	Serial.println();							// Start writing from a new clean line
	Serial.print("Clean-Kong ");
	Serial.print(curAdress);
	Serial.print(" version ");
	Serial.print(CODE_RELEASE);
	Serial.print(" Protocole RS485:V");
	Serial.print(VERSION_RS485);
	Serial.print(" GCode:V");
  	Serial.print(versionGCode);
	Serial.print(" Pile size :");
	Serial.println(PILE_SIZE);
	Serial.println(" Initializing...");

	pinMode(PIN_ZSHSW,	INPUT);		//Niveau Haut eau sale
	//pinMode(PIN_ZSLCW,	INPUT);		//Niveau Bas eau propre   Ignoré
	pinMode(PIN_PTP,	INPUT);		//Pression d'air comprimé
	pinMode(PIN_PTV,	INPUT);		//Pression vide
	pinMode(PIN_ZSLR,	INPUT);		//Fdc Bas Verin Rouleau
	pinMode(PIN_ZSHR,	INPUT);		//Fdc Haut Verin Rouleau
	pinMode(PIN_BUMP1, 	INPUT);
	pinMode(PIN_BUMP2, 	INPUT);

	/*pinMode(PIN_HSRP,	INPUT);		//Bouton inutilise
	pinMode(PIN_HSRF,	INPUT);		//Bouton inutilise
	//*/
	pinMode(PIN_SWP,	OUTPUT);	//Pompe vide 
	isSWP=false;
	digitalWrite(PIN_SWP,isSWP);

	isBumpDown=!digitalRead(PIN_BUMP1);
	isBumpUp=!digitalRead(PIN_BUMP2);
	isHold=false;
	
	//Mesure US
	pinMode(PIN_US_ECHO,INPUT);
	pinMode(PIN_US_ECHOO,INPUT); //cf header
	pinMode(PIN_US_TRIG,OUTPUT);
	#ifndef NO_US
	 	attachInterrupt(digitalPinToInterrupt(PIN_US_ECHO), mesureUS_Start, RISING);  
  		attachInterrupt(digitalPinToInterrupt(PIN_US_ECHO), mesureUS_End,   FALLING);   
  	#endif
  	measuringUS=false;
  	US_Done=false;
  	echoStart=micros();
	
	pinMode(PIN_VM,		OUTPUT);	//Moteur Aspirateur
	pinMode(PIN_RM,		OUTPUT);	//Moteur Rouleau
	//pinMode(PIN_HVSW,	OUTPUT);	//Commande
	//pinMode(PIN_HVCW,	OUTPUT);	//Commande
	
	pinMode(PIN_BUZZER,	OUTPUT);
	pinMode(PIN_LED,	OUTPUT);

	digitalWrite(PIN_LED,LOW);
	digitalWrite(PIN_BUZZER,LOW);
	digitalWrite(PIN_US_TRIG,LOW);
	isVM=isRM=false;
	digitalWrite(PIN_VM,isVM);
	digitalWrite(PIN_RM,isRM);


	watchDogStatus=true; 
	//digitalWrite(PIN_LED,watchDogStatus);
	
	//Determination de l'adresse de la carte en fonction du DIP
	curAdress='R';
	oRS485=  new RS485(curAdress, HOIST, PIN_RS485_DE, PIN_RS485_RE ,  &Serial1);
	
	newOrder=false;
	isErrorSent=false;

	prevTime=millis();
	delay(2);
	holdManualStartTime=lastPTSampleTime=xvroStartTime=xvsxStartTime=curTime=lastUSSampleTime=startTime=millis();

	isBuzz=false;
	

	xvroPeriod=XVRO_PERIOD;
	xvsxPeriod=XVSX_PERIOD;
	xvroDuration=XVRO_DURATION;
	xvsxDuration=XVSX_DURATION;

	int tEV[NB_EV]={T_EV};				//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_EV;i++){
		EV_Pin[i]=tEV[i];
		pinMode(EV_Pin[i],	OUTPUT);
		EV_State[i]=LOW;
	}
	EV_State[XVRJ]=HIGH;
	/*cdeEV(XVRJ,DO_OPEN,true);
	cdeEV(XVRO,DO_CLOSE,true);
	cdeEV(XVSH,DO_CLOSE,true);
	cdeEV(XVSM,DO_CLOSE,true);
	cdeEV(XVSL,DO_CLOSE,true);
	//*/

	rollSpeed=MAX_ROLL_SPEED;
	cnt=0;			

	int tBtn[NB_BTN]={T_BTN};			//Init simplifé à partir du pinlinst.h
	for (int i=0;i<NB_BTN;i++){
		Btn_Pin[i]=tBtn[i];
		pinMode(Btn_Pin[i],	INPUT);	
		isBtnSent[i]=false;
	}

	errorCode=ERR_NO_ERROR;

	oRS485->addMsg(HOIST,'G',CDE_PING,VERSION_GCODE);
	//Wire.onRequest(requestEvent);


}

void CleanKong::checkUS(){
	if((curTime-lastUSSampleTime)>US_SAMPLE_PERIOD){ //prise de mesure pas plus rapide que 3ms
		lastUSSampleTime=curTime;
		echoStart=micros();
		digitalWrite(PIN_US_TRIG,HIGH);
		delayMicroseconds(10);
		digitalWrite(PIN_US_TRIG,LOW);
		US_Done=false;
		measuringUS=true;
	}

	//If Timeout ignore
    if(((micros()-echoStart)>MEASURE_TIMEOUT) && measuringUS){
		measuringUS=false;
		US_Done=false;
    }

    if(US_Done){ //prepare for next sample and eventually send alert
		measuringUS=false;
		US_Done=false;
		if((echoEnd-echoStart)<US_LIMIT){
			IFTRACE_SERIAL(Serial.print("US alert"));
			IFTRACE_SERIAL(Serial.println(echoEnd-echoStart));
			oRS485->addMsg(HOIST,'A',ALERT_US_DOWN,echoEnd-echoStart);
		}
    }

}

/*void CleanKong::cdeEV(int _EV, int _cde, bool _force){
	//digitalWrite(EV_Pin[_EV],_cde);
	EV_State[_EV]=_cde;
	#ifdef TRACE_SERIAL
		if(EV_State[_EV]!=_cde || _force){
			if(_cde==DO_OPEN){
				Serial.print(" Opening ");
			}
			else  {
				Serial.print(" Closing ");	
			}
			Serial.println(A_EV_NAME[_EV]);
		}
	#endif
}
//*/

void CleanKong::buzz(int buzzCode, int duration){
	if(buzzCode==NO_BUZZ){
		noTone(PIN_BUZZER);
		isBuzz=false;
		//Serial.println("no Buzz");
	}
	else {
		isBuzz=true;
		startBuzz=curTime;
		buzzDuration=duration;
		#ifdef NO_SOUND
			Serial.println("Buzz");
		#else
			tone(PIN_BUZZER,A_BUZZ_FREQ[buzzCode]);
		#endif
	}
}

void CleanKong::watchDog(){
	//curTime=millis();
	if(isBuzz && (curTime-startBuzz)>buzzDuration){
		buzz(NO_BUZZ);
	}

	if((curTime-prevTime)>WATCHDOG_PULSE){
		watchDogStatus=!watchDogStatus;
		digitalWrite(PIN_LED, watchDogStatus);
		prevTime=curTime;
	}

	if((curTime-xvroStartTime)>xvroDuration && EV_State[XVRO] || !EV_State[XVRO]){
		digitalWrite(EV_Pin[XVRO],LOW);
	}
	if(((curTime-xvroStartTime)>xvroPeriod && EV_State[XVRO]) ){
		xvroStartTime=curTime;
		digitalWrite(EV_Pin[XVRO],HIGH);	
	}

	if((curTime-xvsxStartTime)>xvsxDuration){
		for(int i=XVSH; i<XVRJ; i++){
			digitalWrite(EV_Pin[i],LOW);
		}
	} 
	
	if((curTime-xvsxStartTime)>xvsxPeriod) {
		for(int i=XVSH; i<XVRJ; i++){
			digitalWrite(EV_Pin[i],EV_State[i]);	
		}
		xvsxStartTime=curTime;
	}

	digitalWrite(EV_Pin[XVRJ],EV_State[XVRJ]);

//	digitalWrite(PIN_RM,isRM);

	if(isRM){
		if((curTime-rollSpeedStartTime)>rollSpeed){
			digitalWrite(PIN_RM,LOW);
		}

		if((curTime-rollSpeedStartTime)>ROLL_SPEED_PERIOD){
			digitalWrite(PIN_RM,HIGH);
			rollSpeedStartTime=curTime;
		}

	}
//*/
	#ifdef MESURE_TEMPS_CYLE
		if(++logN%100==0){
			IFTRACE_SERIAL(Serial.print("100 temps cycle (ms) =");Serial.println(curTime-cycleTime);)
			cycleTime=curTime;
		}
	#endif

}


void CleanKong::loop(){
	//temps de cycle 130µs

	curTime=millis();
	bool btnState;
	#ifndef DEACTIVATE_ROBOT_BUTTONS
		for(int i=0;i<NB_BTN;i++){
			btnState=digitalRead(Btn_Pin[i]);
			if( btnState){
				if(!isBtnSent[i]){
					isBtnSent[i]=true;
					IFTRACE_SERIAL(Serial.print("Send ON :"));
					IFTRACE_SERIAL(Serial.println(A_BTN_NAME[i]));
					oRS485->addMsg(HOIST,'G',CDE_HSRC+i,(long)true);
					if(i==1){							//Manual Btn
						holdManualStartTime=curTime; 	//start holdManualStartTime of Manual mode (for regeneration) only once : when btn pressed
					}
					if(isHold){
						IFTRACE_SERIAL(Serial.println("Stop Regen"));
						isHold=isVM=isRM=false;
						digitalWrite(PIN_VM,isVM);
						digitalWrite(PIN_RM,isRM);
					}
				}
			}	
			else {
				if(isBtnSent[i]){
					IFTRACE_SERIAL(Serial.print("Send OFF :"));
					IFTRACE_SERIAL(Serial.println(A_BTN_NAME[i]));
					oRS485->addMsg(HOIST,'G',CDE_HSRC+i,(long)false);
				}
				isBtnSent[i]=false;
				if(i==1){							//Manual Btn
					holdManualStartTime=curTime; 	
				}
			}
		}

		if(curTime-holdManualStartTime>HOLD_DURATION && !isHold){
			isVM=isRM=isHold=true;
			IFTRACE_SERIAL(Serial.println("Regen"));
			isHold=isVM=isRM=true;
			digitalWrite(PIN_VM,isVM);
			digitalWrite(PIN_RM,isRM);
		}
	#endif	

	if((isBumpDown && digitalRead(PIN_BUMP1))||(!isBumpDown && !digitalRead(PIN_BUMP1))){
		isBumpDown=!digitalRead(PIN_BUMP1);
		oRS485->addMsg(HOIST,'A',ALERT_BUMP_DOWN,(long)isBumpDown);
	}
	if((isBumpUp && digitalRead(PIN_BUMP2))||(!isBumpUp && !digitalRead(PIN_BUMP2))){
		isBumpUp=!digitalRead(PIN_BUMP2);
		oRS485->addMsg(HOIST,'A',ALERT_BUMP_UP,(long)isBumpUp);
	}
	

	if(curTime-lastPTSampleTime>PT_SAMPLE_DELAY){
		lastPTSampleTime=curTime;
		#ifndef NO_PRESSURE_FEEDBACK
			int tempPT=analogRead(PIN_PTV);
			if(tempPT>PTVSH){
				oRS485->addMsg(HOIST,'E',ERR_PTV_SH_ERROR,(long)tempPT);
			}
			tempPT=analogRead(PIN_PTP);
			if(tempPT<PTPSL){
				oRS485->addMsg(HOIST,'E',ERR_PTP_SL_ERROR,(long)tempPT);	
			}
		#endif		
	}
	//*/

	//User input
	#ifndef NO_US	
		checkUS();
	#endif

	//read or write RS485 msg
	oRS485->processMsg();


	//ECOUTE COMMANDES RS485
	if (oRS485->isNewRcvMsg()){
		newOrder=false;

		if(oRS485->curRcvMsg->fromAdr==HOIST){
			isMasterReady=true;
		}
		int curEVState=0;
		if(oRS485->curRcvMsg->cdeC=='$'){
			Serial.println("*************************");
		}



		switch (oRS485->curRcvMsg->cdeC){
			case '$':
				oRS485->moveNextRcvMsg();
				oRS485->displayPile();
				break;
			case 'V':
				switch (oRS485->curRcvMsg->cdeN){
//GET
					case GET_GCODE_RELEASE:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,versionGCode);
						break;
					case GET_HSRC:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_HSRC));
						break;
					case GET_HSRM:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_HSRM));
						break;
					case GET_HSRU:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_HSRU));
						break;
					case GET_HSRD:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_HSRD));
						break;
					case GET_HSRP:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_HSRP));
						break;
					case GET_ZSHSW:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_ZSHSW));
						break;
					case GET_ZSLR:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_ZSLR));
						break;
					case GET_ZSHR:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)digitalRead(PIN_ZSHR));
						break;
					case GET_VM:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)isVM);
						break;
					case GET_ROLL_SPEED:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)rollSpeed);
						break;
					case GET_RM:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)isRM);
						break;
					case GET_CWP:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)isSWP);
						break;
					case GET_PTP:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)analogRead(PIN_PTP));
						break;
					case GET_PTV:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)analogRead(PIN_PTV));
						break;
					case GET_XVRO:
					case GET_XVSH:
					case GET_XVSM:
					case GET_XVSL:
					case GET_XVRJ:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,(long)EV_State[oRS485->curRcvMsg->cdeN-GET_XVRO]);
						break;
					case GET_XVRO_DURATION:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,xvroDuration);
						break;
					case GET_XVRO_PERIOD:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,xvroPeriod);
						break;
					case GET_XVSX_DURATION:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,xvsxDuration);
						break;
					case GET_XVSX_PERIOD:
						oRS485->addMsg(HOIST,oRS485->curRcvMsg->cdeC,oRS485->curRcvMsg->cdeN,xvsxPeriod);
						break;
					default :
						oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);
						break;
				}
				break;
//SET
			case 'S':
				switch (oRS485->curRcvMsg->cdeN){
					case SET_ROBOT_BUZZER:
						buzz(oRS485->curRcvMsg->param);
						break;
					case SET_XVRO_DURATION:
						xvroDuration=max(0,oRS485->curRcvMsg->param);
						if(xvroDuration>xvroPeriod){
							xvroPeriod=xvroDuration;
							oRS485->addMsg(HOIST,'E',ERR_DURATION_OUT_OF_RANGE,xvroDuration);
						}
						break;
					case SET_XVRO_PERIOD:
						xvroPeriod=max(0,oRS485->curRcvMsg->param);
						if(xvroDuration>xvroPeriod){
							xvroPeriod=xvroDuration;
							oRS485->addMsg(HOIST,'E',ERR_DURATION_OUT_OF_RANGE,xvroPeriod);
						}
						break;
					case SET_XVSX_DURATION:
						xvsxDuration=max(0,oRS485->curRcvMsg->param);
						if(xvsxDuration>xvsxPeriod){
							xvsxPeriod=xvsxDuration;
							oRS485->addMsg(HOIST,'E',ERR_DURATION_OUT_OF_RANGE,xvsxDuration);
						}
						break;
					case SET_XVSX_PERIOD:
						xvsxPeriod=max(0,oRS485->curRcvMsg->param);
						if(xvsxDuration>xvsxPeriod){
							xvsxPeriod=xvsxDuration;
							oRS485->addMsg(HOIST,'E',ERR_DURATION_OUT_OF_RANGE,xvsxPeriod);
						}
						break;
					case SET_ROLL_SPEED:
						rollSpeed=max(0,oRS485->curRcvMsg->param);
						rollSpeed=min(rollSpeed,MAX_ROLL_SPEED); //rollSpeed in [0;MAX] range
						IFTRACE_SERIAL(Serial.print("New Roll Speed "));IFTRACE_SERIAL(Serial.println(rollSpeed));
						break;
					default :
						oRS485->addMsg(HOIST,'E',ERR_DURATION_OUT_OF_RANGE,(long)oRS485->curRcvMsg->msgId);
						break;
				}
				break;

//CDE
			case 'G' :
				switch (oRS485->curRcvMsg->cdeN){
				case CDE_CLEAN:
					switch(oRS485->curRcvMsg->param){
						case 1:
							break;
						case CLEANING_SLEEP:
							break;
						default:
							break;
					}
					IFTRACE_SERIAL(Serial.print("CLEANING : "));
					IFTRACE_SERIAL(Serial.println(oRS485->curRcvMsg->param));
					//allumer la led
					break;
				case CDE_DISPLAY_PARAMS:
					displayParams();
					break;
				case CDE_VM:
					isVM=oRS485->curRcvMsg->param>0;
					digitalWrite(PIN_VM,isVM);
					break;
				case CDE_RM:
					isRM=oRS485->curRcvMsg->param>0;
					rollSpeedStartTime=curTime;
					IFTRACE_SERIAL(Serial.print("RM"));IFTRACE_SERIAL(Serial.println(isRM));
					digitalWrite(PIN_RM,isRM);
					break;
				case CDE_SWP:
					isSWP=oRS485->curRcvMsg->param>0;
					#ifndef NO_PUMP_NO_NOISE
						digitalWrite(PIN_SWP,isSWP);
					#endif
					break;
				case CDE_XVRO_OPEN:
				case CDE_XVSH_OPEN:
				case CDE_XVSM_OPEN:
				case CDE_XVSL_OPEN:
				case CDE_XVRJ_OPEN:
					EV_State[oRS485->curRcvMsg->cdeN-CDE_XVRO_OPEN]=oRS485->curRcvMsg->param>0;
					//cdeEV(oRS485->curRcvMsg->cdeN-CDE_XVRO_OPEN,oRS485->curRcvMsg->param>0);
					IFTRACE_SERIAL(Serial.print(A_EV_NAME[oRS485->curRcvMsg->cdeN-CDE_XVRO_OPEN]));
					if(EV_State[oRS485->curRcvMsg->cdeN-CDE_XVRO_OPEN]){
						IFTRACE_SERIAL(Serial.println(" open"));
					}
					else {
						IFTRACE_SERIAL(Serial.println(" closed"));
					}


					break;

				case CDE_PING:
					oRS485->addMsg(HOIST,'G',CDE_PONG,versionGCode);
					break;
				case CDE_PONG:
/*					if(oRS485->curRcvMsg->fromAdr==HOIST){
						isMasterReady=true;
					}//*/
					break;
				case CDE_ACK_ERROR:
					errorCode=ERR_NO_ERROR;
					break;
				default :
					oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->cdeN);//oRS485->curRcvMsg->msgId);
					break;
				}
				break;
			default ://It's not G V S GCode : 
				oRS485->addMsg(HOIST,'E',ERR_GCODE_NOT_SUPPORTED,(long)oRS485->curRcvMsg->msgId);
				break;		
		}
		oRS485->moveNextRcvMsg();
	}

	if (errorCode && !isErrorSent){
        IFTRACE_SERIAL(Serial.print("Error : "));
        IFTRACE_SERIAL(Serial.print(errorCode));
        //IFTRACE_SERIAL(Serial.println(errorMsg[errorCode]));
        oRS485->addMsg(HOIST,'E',errorCode);
        isErrorSent=true;			//empeche le spamm
        //errorCode=false;
    }
    watchDog();

}

void CleanKong::displayParams(){
	Serial.print("\n-----------------------------------------------\nETAT DU ROBOT ");
	Serial.println(curAdress);
	Serial.print("version=\t");Serial.println(CODE_RELEASE);
	Serial.print("version GCode=\t");Serial.println(versionGCode);
	Serial.print("curTime=\t");Serial.println(curTime);
	Serial.print("prevTime=\t");Serial.println(prevTime);
	Serial.print("startTime=\t");Serial.println(startTime);
	Serial.print("cycleTime=\t");Serial.println(cycleTime);
	Serial.print("rollSpeedStartT.=\t");Serial.println(rollSpeedStartTime);
	Serial.print("rollSpeed=\t");Serial.println(rollSpeed);
	Serial.print("isMasterReady=\t");Serial.println(isMasterReady);
	Serial.print("isMasterReady=\t");Serial.println(isMasterReady);
	for(int i=0;i<NB_BTN;i++){
		Serial.print(A_BTN_NAME[i]);
		Serial.print("=\t");
		Serial.println(isBtnSent[i]);
	}
	Serial.print("isVM=\t\t");Serial.println(isVM);
	Serial.print("isRM=\t\t");Serial.println(isRM);
	Serial.print("isSWP=\t\t");Serial.println(isSWP);
	Serial.print("PTP=\t\t");Serial.println(analogRead(PIN_PTP));
	Serial.print("PTV=\t\t");Serial.println(analogRead(PIN_PTV));
	for(int i=0;i<NB_EV;i++){
		Serial.print(A_EV_NAME[i]);Serial.print("=\t\t");Serial.println(EV_State[i]);
	}
}




