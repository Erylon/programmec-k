#ifndef CleanKongMegei_h
#define CleanKongMegei_h

#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CodeRelease.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\C-KErrorList.h"
#include "D:\Owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\CarteRobot\RobotBoardPinList.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\GCodeC-K.h"
#include "D:\owncloud\Projet\Clean-Kong\InfoC-K\C-K_MEGEI\ProtocoleRS485\ProtocoleRS485.cpp"

//void	requestEvent();

unsigned long		curTime;
volatile long 		echoStart,echoEnd; 	//mesure du temps US en µs
bool    			US_Done;			//mesure US terminée
bool    			measuringUS;		//mesure US en cours


unsigned long 		logN=0;

const char* 		A_EV_NAME[]=		{T_NAME_EV};
const char* 		A_BTN_NAME[]=		{T_NAME_BTN};
const char* 		A_VALVE_STATUS[]=	{"CLOSED","OPENED"};
const int  			A_BUZZ_FREQ[]=		{T_BUZZ_FREQUENCY};	

void mesureUS_Start();
void mesureUS_End();

class CleanKong {
public:
					CleanKong();
	void			loop();
//	void			cdeEV(int _pinEV, int _cde, bool _force=true);
	void 			displayParams();



private:
	void			watchDog();
	void			buzz(int buzzCode=0, int duration=BUZZ_ALERT_DURATION);
	void 			checkUS();							//declenche si traverse detectee
	bool			watchDogStatus;
	unsigned long	curTime,prevTime,holdManualStartTime,startTime,cycleTime,lastUSSampleTime,lastPTSampleTime;
	unsigned long 	xvroPeriod,xvsxPeriod,xvroDuration,xvsxDuration,xvroStartTime,xvsxStartTime,rollSpeed,rollSpeedStartTime;
	bool			isErrorSent;
	bool 			isMasterReady;
	bool 			isBumpDown;
	bool 			isBumpUp;
	bool			isHold;
	unsigned int 	versionGCode;
	char 			curAdress;
	long 			distUS;
	int				EV_Pin[NB_EV];
	int				EV_State[NB_EV];
	int 			Btn_Pin[NB_BTN];
	bool 			isBtnSent[NB_BTN];
	int				errorCode;
	RS485 			*oRS485;
	unsigned long	startBuzz,buzzDuration;
	bool			isBuzz;

	bool			newOrder;
	bool 			isVM, isRM, isSWP;

	
	int 			cnt;
};



#endif
